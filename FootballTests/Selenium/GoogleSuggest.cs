﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;

namespace FootballTests.Selenium
{
    // https://gist.github.com/deanhume/2488641
    //  http://stackoverflow.com/questions/4822568/how-do-i-test-multiple-browsers-with-selenium-and-a-single-nunit-suite-and-keep
    
    [TestFixture(typeof(FirefoxDriver))]
    [TestFixture(typeof(InternetExplorerDriver))]
    public class GoogleSuggest<TWebDriver> where TWebDriver : IWebDriver, new()
    {
        
        private IWebDriver driver;

        [SetUp]
        public void CreateDriver()
        {
            this.driver = new TWebDriver();
        }

        [Test]
        public void testGoogle()
        {            
            //IWebDriver driver = new RemoteWebDriver(new Uri("http://127.0.0.1:4444/wd/hub"),DesiredCapabilities.HtmlUnit());
            driver.Navigate().GoToUrl("http://www.google.com");

            IWebElement query = driver.FindElement(By.Name("q"));            
            query.SendKeys("Cheese");
            query.Submit();

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until((d) => { return d.Title.ToLower().StartsWith("cheese"); });

            Assert.AreEqual("cheese - Google Search", driver.Title);
                       
            //(new Actions(driver)).DragAndDrop

            driver.Quit();
        }
    }
}

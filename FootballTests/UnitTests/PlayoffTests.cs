﻿using System.Collections.Generic;
using Football.League.Conferences;
using Football.League.Playoffs;
using Football.League.Schedules;
using Football.League.Standings;
using Football.League.Teams.TeamRecord;
using FootballTests.TestData;
using NUnit.Framework;

//  http://www.manas.com.ar/spalladino/2009/11/13/parameterized-tests/
namespace FootballTests.UnitTests
{
    [TestFixture]
    public class PlayoffTests
    {
        private Conference conference;
        private Structure leagueStructure;
        private PlayoffTestData testData;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            this.testData = new PlayoffTestData();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            this.testData = null;
        }

        [SetUp]
        public void SetupTest()
        {
            conference = testData.getConference();
            leagueStructure = new Structure();
            leagueStructure.addConference(conference);
        }

        [TearDown]
        public void TearDownTest()
        {
            conference = null;
            leagueStructure = null;
        }
        
        [Test]        
        public void testBestRecordReturnsCorrectTeams()
        {
            TeamRecordList teamRecordList = testData.getBestRecordTeamList(conference);         
            Playoff playoff = new Playoff( new FootballStandings(teamRecordList), new Schedule(), teamRecordList, leagueStructure );

            List<PlayoffTeam> expectedTeams = testData.getBestRecordExpectedTeams(conference);
            List<PlayoffTeam> actualTeams = playoff.getPlayoffTeams(conference);

            PlayoffHandlerReturnsCorrectResults( expectedTeams, actualTeams );            
        }        
        
        [Test]        
        public void testHeadToHeadReturnsCorrectTeams()
        {
            TeamRecordList teamRecordList = testData.getHeadToHeadRecordTeamList(conference);
            Playoff playoff = new Playoff(new FootballStandings(teamRecordList), testData.getHeadToHeadSchedule(conference), teamRecordList, leagueStructure);

            List<PlayoffTeam> expectedTeams = testData.getHeadToHeadExpectedTeams(conference);
            List<PlayoffTeam> actualTeams = playoff.getPlayoffTeams(conference);

            PlayoffHandlerReturnsCorrectResults(expectedTeams, actualTeams);  
        }          
        
        [Test]               
        public void testDivisionBestRecordReturnsCorrectTeams()
        {
            TeamRecordList teamRecordList = testData.getDivisionRecordTeamList(conference);
            Playoff playoff = new Playoff(new FootballStandings(teamRecordList), testData.getDivisionRecordSchedule(conference), teamRecordList, leagueStructure);

            List<PlayoffTeam> expectedTeams = testData.getDivisionRecordExpectedTeams(conference);
            List<PlayoffTeam> actualTeams = playoff.getPlayoffTeams(conference);

            PlayoffHandlerReturnsCorrectResults(expectedTeams, actualTeams);  
        }

        [Test]
        public void testConferenceBestRecordReturnsCorrectTeams()
        {
            TeamRecordList teamRecordList = testData.getConferenceRecordTeamList(conference);
            Playoff playoff = new Playoff(new FootballStandings(teamRecordList), new Schedule(), teamRecordList, leagueStructure);

            List<PlayoffTeam> expectedTeams = testData.getConferenceRecordExpectedTeams(conference);
            List<PlayoffTeam> actualTeams = playoff.getPlayoffTeams(conference);

            PlayoffHandlerReturnsCorrectResults(expectedTeams, actualTeams);  
        }

        [Test]
        public void testCommonGamesReturnsCorrectTeams()
        {
            TeamRecordList teamRecordList = testData.getCommonTeamRecordList(conference);
            Playoff playoff = new Playoff(new FootballStandings(teamRecordList), testData.getCommonTeamSchedule(conference), teamRecordList, leagueStructure);

            List<PlayoffTeam> expectedTeams = testData.getCommonTeamExpectedTeams(conference);
            List<PlayoffTeam> actualTeams = playoff.getPlayoffTeams(conference);

            PlayoffHandlerReturnsCorrectResults(expectedTeams, actualTeams);  
        }

        private void PlayoffHandlerReturnsCorrectResults(List<PlayoffTeam> expectedTeams, List<PlayoffTeam> actualTeams)
        {
            Assert.AreEqual(expectedTeams[0].Team.Abbr, actualTeams[0].Team.Abbr);
            Assert.AreEqual(expectedTeams[1].Team.Abbr, actualTeams[1].Team.Abbr);
            Assert.AreEqual(expectedTeams[2].Team.Abbr, actualTeams[2].Team.Abbr);
            Assert.AreEqual(expectedTeams[3].Team.Abbr, actualTeams[3].Team.Abbr);
            Assert.AreEqual(expectedTeams[4].Team.Abbr, actualTeams[4].Team.Abbr);
            Assert.AreEqual(expectedTeams[5].Team.Abbr, actualTeams[5].Team.Abbr);
        }
    }
}

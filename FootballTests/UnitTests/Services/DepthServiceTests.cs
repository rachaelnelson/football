﻿using System.Collections.Generic;
using Football.League.Players;
using Football.League.Teams;
using Football.Services;
using Football.Services.Depth;
using FootballTests.TestData;
using Moq;
using NUnit.Framework;

namespace FootballTests.UnitTests.Services
{
    [TestFixture]
    public class DepthServiceTests
    {
        private DepthServiceTestData testData;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            testData = new DepthServiceTestData();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            testData = null;
        }

        [Test]        
        [Ignore]
        public void FillDepth_AddTwoQBPlayers_PlayersAddedToRoster()
        {
            // Setup teams, rosters, and free agent list
            // Setup a position below the required limit

            FootballSettingsService settingsService = testData.GetSettingsServiceTwoPlayersAddedToRoster();
            DepthService depthService = testData.GetDepthService(settingsService);
            
            // Call autofill
            depthService.FillRoster();

            // Verify player was added to team and number of players == limit
            List<Player> playersList = settingsService.getTeamPlayersByPosition("ARI", PlayerType.QB);

            // QB list should have two players in list.
            Assert.That(playersList.Count == 2);
        }

        [Test]
        [Ignore]
        public void FillDepth_AddTwoQBEachToTwoTeams_PlayersAddedToRoster()
        {
            // Setup teams, rosters, and free agent list
            // Setup a position below the required limit

            FootballSettingsService settingsService = testData.GetSettingsServiceOnePlayersAddedToTwoRosters();
            DepthService depthService = testData.GetDepthService(settingsService);

            // Call autofill
            depthService.FillRoster();

            // Verify player was added to team and number of players == limit
            List<Player> playersList = settingsService.getTeamPlayersByPosition("ARI",PlayerType.QB);

            // QB list should have two players in list.
            Assert.That(playersList.Count == 2);

            // Verify player was added to team and number of players == limit
            List<Player> playersList2 = settingsService.getTeamPlayersByPosition("BAL", PlayerType.QB);

            // QB list should have two players in list.
            Assert.That(playersList2.Count == 2);
        }

        [Test]
        [Ignore]
        public void FillDepth_MyTeam_PlayersNotAddedToRoster()
        {
            // Setup teams, rosters, and free agent list
            // Setup a position below the required limit

            FootballSettingsService settingsService = testData.GetSettingsServiceOnePlayersAddedToMyRoster();
            DepthService depthService = testData.GetDepthService(settingsService);   

            // Call autofill
            depthService.FillRoster();

            // Verify player was added to team and number of players == limit
            List<Player> playersList = settingsService.getTeamPlayersByPosition("ARI", PlayerType.QB);

            // QB list should have two players in list.
            Assert.That(playersList.Count == 0);            
        }

        [Test]
        [Ignore]
        public void FillDepth_OnePlayersNeeded_CutLowPlayer()
        {
            FootballSettingsService settingsService = testData.GetSettingsServiceCutOnePlayer();
            DepthService depthService = testData.GetDepthService(settingsService);

            // Call autofill
            depthService.FillRoster();

            // Verify player was added to team and number of players == limit
            List<Player> playersListRB = settingsService.getTeamPlayersByPosition("ARI", PlayerType.RB);                     
            List<Player> playersListDB = settingsService.getTeamPlayersByPosition("ARI", PlayerType.DB);

            // RB list should have two players in list.
            Assert.That(playersListRB.Count == 4);            
            Assert.That(playersListDB.Count == 9); 
        }

        [Test]
        public void FillDepth_AddStarter_PlayerAddedToTeam()
        {
            FootballSettingsService settingsService = testData.GetSettingsServiceAddOneStarter();
            DepthService depthService = testData.GetDepthService(settingsService);

            // Call autofill
            depthService.FillRoster();

            // Verify player was added to team and number of players == limit
            List<Player> playersListQB = settingsService.getTeamPlayersByPosition("ARI", PlayerType.QB);            

            // RB list should have two players in list.
            Assert.That(playersListQB.Count == 1);            
        }
    }
}

﻿using Football.Services;
using Football.League.Players;
using FootballTests.TestData;
using NUnit.Framework;

namespace FootballTests.UnitTests.Services
{
    [TestFixture]
    public class CapManagementServiceTests
    {
        private CapManagementTestData testData;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            testData = new CapManagementTestData();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            testData = null;
        }

        [Test]
        public void testGetCurrentCapYearReturnsCorrectYear()
        {            
            FootballSettingsService settingsService = testData.GetSettingsServiceWithCurrentYear();
            RosterService rosterService = new RosterService(settingsService);
            CapManagementService capService = new CapManagementService(settingsService,rosterService);

            int expectedYear = 2005;
            int year = capService.getCurrentCapYear();
            Assert.AreEqual(expectedYear,year);
        }

        [Test]
        public void testGetCurrentCapSpaceReturnsValidAmount()
        {
            FootballSettingsService settingsService = testData.GetSettingsServiceWithCapSpace();
            RosterService rosterService = new RosterService(settingsService);
            CapManagementService capService = new CapManagementService(settingsService, rosterService);

            int expectedCap = 90000000;
            int actualCap = capService.getCurrentCapSpace();
            Assert.AreEqual(expectedCap,actualCap);
        }
    }
}

﻿using System.Collections.Generic;
using Football.League.Injuries;
using Football.League.Players;
using Football.Services;
using FootballTests.TestData;
using NUnit.Framework;

namespace FootballTests.UnitTests.Services
{
    [TestFixture]
    public class InjuryServiceTests
    {
        private InjuryServiceTestData testData;
                
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            testData = new InjuryServiceTestData();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            testData = null;
        }

        [Test]
        public void UpdateInjuries_InjuriesDisabled_NoInjuryUpdate()
        {           
            string myTeam = "ARI";
            
            InjuryTestView testView = new InjuryTestView { EnableInjuries=false, PlayerTeam=myTeam,InjuryDuration=2, InjuryType=Injury.Ankle, MyTeam=myTeam };
            InjuryService injuryService = testData.GetInjuryService(testView);

            injuryService.UpdateInjuries();
            
            List<Player> injuredPlayers = injuryService.GetInjuredPlayers(myTeam);
           
            Assert.AreEqual(testView.InjuryDuration, injuredPlayers[0].injury.duration);
            Assert.AreEqual(testView.InjuryType, injuredPlayers[0].injury.injury);
        }

        [Test]
        public void UpdateInjuries_InjuriesEnabled_InjuryUpdate()
        {            
            string myTeam = "ARI";

            InjuryTestView testView = new InjuryTestView { EnableInjuries = true, PlayerTeam = myTeam, InjuryDuration = 2, InjuryType = Injury.Ankle, MyTeam = myTeam };
            InjuryService injuryService = testData.GetInjuryService(testView);

            injuryService.UpdateInjuries();

            List<Player> injuredPlayers = injuryService.GetInjuredPlayers(myTeam);            

            int expectedInjuryDuration = testView.InjuryDuration - 1;

            Assert.AreEqual(expectedInjuryDuration, injuredPlayers[0].injury.duration);
            Assert.AreEqual(testView.InjuryType, injuredPlayers[0].injury.injury);
        }

        [Test]
        public void UpdateInjuries_InjuriesEnabled_InjuryUpdateToNotInjured()
        {            
            string myTeam = "ARI";

            InjuryTestView testView = new InjuryTestView { EnableInjuries = true, PlayerTeam = myTeam, InjuryDuration = 1, InjuryType = Injury.Ankle, MyTeam = myTeam };
            InjuryService injuryService = testData.GetInjuryService(testView);

            injuryService.UpdateInjuries();

            List<Player> injuredPlayers = injuryService.GetInjuredPlayers(myTeam);
            Assert.AreEqual(0, injuredPlayers.Count);
        }

        [Test]
        public void AddPlayersToIR_InjuriesDisabled_NoPlayersAddedToIR()
        {
            string myTeam = "BAL";
            string playerTeam = "ARI";

            InjuryTestView testView = new InjuryTestView { EnableInjuries = false, PlayerTeam = playerTeam, InjuryDuration = 1, InjuryType = Injury.Ankle, MyTeam = myTeam, AlreadyOnIR=false };
            InjuryService injuryService = testData.GetInjuryService(testView);

            injuryService.AddPlayersToIR();

            List<Player> IRPlayers = injuryService.GetIRPlayers(playerTeam);
            Assert.AreEqual(0, IRPlayers.Count);
        }

        [Test]
        public void AddPlayersToIR_MyPlayersInjured_PlayerNotAddedToIR()
        {
            string team = "ARI";

            InjuryTestView testView = new InjuryTestView { EnableInjuries = true, PlayerTeam = team, InjuryDuration = 1, InjuryType = Injury.Ankle, MyTeam = team, AlreadyOnIR = false };
            InjuryService injuryService = testData.GetInjuryService(testView);

            injuryService.AddPlayersToIR();

            List<Player> IRPlayers = injuryService.GetIRPlayers(team);
            Assert.AreEqual(0, IRPlayers.Count);
        }

        [Test]
        public void AddPlayersToIR_InjuriesEnabled_PlayersAddedToIR()
        {
            string myTeam = "BAL";
            string playerTeam = "ARI";

            InjuryTestView testView = new InjuryTestView { EnableInjuries = true, PlayerTeam = playerTeam, InjuryDuration = 25, InjuryType = Injury.Ankle, MyTeam = myTeam, AlreadyOnIR = false };
            InjuryService injuryService = testData.GetInjuryService(testView);

            injuryService.AddPlayersToIR();

            List<Player> IRPlayers = injuryService.GetIRPlayers(playerTeam);

            Assert.AreEqual(1, IRPlayers.Count);
        }

        [Test]
        public void AddPlayersToIR_PlayerAlreadyOnIR_PlayerNotAddedToIR()
        {           
            string myTeam = "BAL";
            string playerTeam = "ARI";

            InjuryTestView testView = new InjuryTestView { EnableInjuries = true, PlayerTeam = playerTeam, InjuryDuration = 25, InjuryType = Injury.Ankle, MyTeam = myTeam, AlreadyOnIR = true };
            InjuryService injuryService = testData.GetInjuryService(testView);

            injuryService.AddPlayersToIR();

            List<Player> IRPlayers = injuryService.GetIRPlayers(playerTeam);

            Assert.AreEqual(1, IRPlayers.Count);
        }

        [Test]
        public void AddPlayersToIR_PlayerInjuryShort_PlayerNotAddedToIR()
        {
            string myTeam = "BAL";
            string playerTeam = "ARI";

            InjuryTestView testView = new InjuryTestView { EnableInjuries = true, PlayerTeam = playerTeam, InjuryDuration = 1, InjuryType = Injury.Ankle, MyTeam = myTeam, AlreadyOnIR = false };
            InjuryService injuryService = testData.GetInjuryService(testView);

            injuryService.AddPlayersToIR();

            List<Player> IRPlayers = injuryService.GetIRPlayers(playerTeam);

            Assert.AreEqual(0, IRPlayers.Count);
        }
    }
}

﻿using FootballTests.TestData;
using NUnit.Framework;
using Football.Services.Starter;
using Football.UiClasses.ViewModel;
using Football.Services;
using Football.Settings;
using Football.League.Players;

namespace FootballTests.UnitTests.Services
{
    [TestFixture]
    public class ReleasePlayerServiceTest
    {
        private ReleasePlayerServiceTestData testData = new ReleasePlayerServiceTestData();

        [Test]
        public void ReleasePlayer_ReleasePlayerView_Released()
        {
            FootballSettings setting = FootballSettings.setSettings();
            FootballSettingsService settingsService = new FootballSettingsService(setting);
            RosterService rosterService = new RosterService(settingsService);

            ReleasePlayerService service = testData.TestReleasePlayerView( setting, settingsService, rosterService );
            ReleasePlayerView playerView = testData.TestRelease_GetReleasePlayerView();

            Assert.IsTrue( service.ReleasePlayer(playerView) );
            Assert.AreEqual( 0, rosterService.GetAvailablePlayerCountByPosition("ARI", PlayerType.QB) ); // verify count
        }

        [Test]
        public void ReleasePlayer_Player_Released()
        {
            FootballSettings setting = FootballSettings.setSettings();
            FootballSettingsService settingsService = new FootballSettingsService(setting);
            RosterService rosterService = new RosterService(settingsService);

            ReleasePlayerService service = testData.TestReleasePlayerView(setting, settingsService, rosterService);
            Player player = testData.TestRelease_GetReleasePlayer();

            Assert.IsTrue(service.ReleasePlayer(player));
            Assert.AreEqual(0, rosterService.GetAvailablePlayerCountByPosition("ARI", PlayerType.QB)); // verify count
        }

        /*
        [Test]
        public void CutPlayers_AvailableRoom_NoPlayersReleased()
        {
            FootballSettings setting = FootballSettings.setSettings();
            FootballSettingsService settingsService = new FootballSettingsService(setting);
            RosterService rosterService = new RosterService(settingsService);

            ReleasePlayerService service = testData.TestCutPlayers_HasRoom(setting, settingsService, rosterService);            

            service.CutPlayers("ARI");

            Assert.AreEqual(0, rosterService.GetAvailablePlayerCountByPosition("ARI", PlayerType.QB)); // verify count   
        }
         */ 

        [Test]
        public void CutPlayers_NoAvailableRoom_PlayersReleased()
        {

        }
    }
}

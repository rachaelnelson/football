﻿using Football.Services;
using FootballTests.TestData;
using NUnit.Framework;
using Football.League.Players;
using System.Collections.Generic;
using Football.Settings;

namespace FootballTests.UnitTests.Services
{
    [TestFixture]
    public class StarterServiceTests
    {
        private StarterServiceTestData testData;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            testData = new StarterServiceTestData();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            testData = null;
        }

        //**** REQUIRED STARTERS ***/
        [Test]
        public void GetRequiredStarterCountByPosition_FourThree()
        {
            StarterService service = testData.GetStarterService(BaseAll.TwoRB, BaseAll.FourThree);
            
            Dictionary<PlayerType, int> expectedFourThree = new Dictionary<PlayerType, int>()
            {
                  { PlayerType.DL, 4 }
                , { PlayerType.LB, 3 }
                , { PlayerType.DB, 4 }
            };

            Assert.AreEqual( expectedFourThree[PlayerType.DL], service.GetRequiredStarterCountByPosition("ARI", PlayerType.DL) );           
            Assert.AreEqual( expectedFourThree[PlayerType.LB], service.GetRequiredStarterCountByPosition("ARI", PlayerType.LB) );
            Assert.AreEqual( expectedFourThree[PlayerType.DB], service.GetRequiredStarterCountByPosition("ARI", PlayerType.DB) );
        }

        [Test]
        public void GetRequiredStarterCountByPosition_ThreeFour()
        {
            StarterService service = testData.GetStarterService(BaseAll.TwoRB, BaseAll.ThreeFour);
            
            Dictionary<PlayerType, int> expectedThreeFour = new Dictionary<PlayerType, int>()
            {
                  { PlayerType.DL, 3 }
                , { PlayerType.LB, 4 }
                , { PlayerType.DB, 4 }
            };

            Assert.AreEqual( expectedThreeFour[PlayerType.DL], service.GetRequiredStarterCountByPosition("ARI", PlayerType.DL) );           
            Assert.AreEqual( expectedThreeFour[PlayerType.LB], service.GetRequiredStarterCountByPosition("ARI", PlayerType.LB) );
            Assert.AreEqual( expectedThreeFour[PlayerType.DB], service.GetRequiredStarterCountByPosition("ARI", PlayerType.DB) );
        }

        [Test]
        public void GetRequiredStarterCountByPosition_ThreeWR()
        {
            StarterService service = testData.GetStarterService(BaseAll.ThreeWR, BaseAll.FourThree);

            Dictionary<PlayerType, int> expectedThreeWR = new Dictionary<PlayerType, int>()
            {
                  { PlayerType.QB, 1 }
                , { PlayerType.RB, 1 }
                , { PlayerType.WR, 3 }
                , { PlayerType.TE, 1 }
                , { PlayerType.OL, 5 }
                , { PlayerType.K, 1 }
                , { PlayerType.P, 1 }
            };

            Assert.AreEqual(expectedThreeWR[PlayerType.QB], service.GetRequiredStarterCountByPosition("ARI", PlayerType.QB));
            Assert.AreEqual(expectedThreeWR[PlayerType.RB], service.GetRequiredStarterCountByPosition("ARI", PlayerType.RB));
            Assert.AreEqual(expectedThreeWR[PlayerType.WR], service.GetRequiredStarterCountByPosition("ARI", PlayerType.WR));
            Assert.AreEqual(expectedThreeWR[PlayerType.TE], service.GetRequiredStarterCountByPosition("ARI", PlayerType.TE));
            Assert.AreEqual(expectedThreeWR[PlayerType.OL], service.GetRequiredStarterCountByPosition("ARI", PlayerType.OL));
            Assert.AreEqual(expectedThreeWR[PlayerType.K], service.GetRequiredStarterCountByPosition("ARI", PlayerType.P));
        }

        [Test]
        public void GetRequiredStarterCountByPosition_TwoRB()
        {
            StarterService service = testData.GetStarterService(BaseAll.TwoRB, BaseAll.FourThree);

            Dictionary<PlayerType, int> expectedTwoRB = new Dictionary<PlayerType, int>()
            {
                  { PlayerType.QB, 1 }
                , { PlayerType.RB, 2 }
                , { PlayerType.WR, 2 }
                , { PlayerType.TE, 1 }
                , { PlayerType.OL, 5 }
                , { PlayerType.K, 1 }
                , { PlayerType.P, 1 }
            };

            Assert.AreEqual(expectedTwoRB[PlayerType.QB], service.GetRequiredStarterCountByPosition("ARI", PlayerType.QB));
            Assert.AreEqual(expectedTwoRB[PlayerType.RB], service.GetRequiredStarterCountByPosition("ARI", PlayerType.RB));
            Assert.AreEqual(expectedTwoRB[PlayerType.WR], service.GetRequiredStarterCountByPosition("ARI", PlayerType.WR));
            Assert.AreEqual(expectedTwoRB[PlayerType.TE], service.GetRequiredStarterCountByPosition("ARI", PlayerType.TE));
            Assert.AreEqual(expectedTwoRB[PlayerType.OL], service.GetRequiredStarterCountByPosition("ARI", PlayerType.OL));
            Assert.AreEqual(expectedTwoRB[PlayerType.K], service.GetRequiredStarterCountByPosition("ARI", PlayerType.P));
        }

        [Test]
        public void GetRequiredStarterCountByPosition_TwoTE()
        {
            StarterService service = testData.GetStarterService(BaseAll.TwoTE, BaseAll.FourThree);

            Dictionary<PlayerType, int> expectedTwoTE = new Dictionary<PlayerType, int>()
            {
                  { PlayerType.QB, 1 }
                , { PlayerType.RB, 1 }
                , { PlayerType.WR, 2 }
                , { PlayerType.TE, 2 }
                , { PlayerType.OL, 5 }
                , { PlayerType.K, 1 }
                , { PlayerType.P, 1 }
            };

            Assert.AreEqual(expectedTwoTE[PlayerType.QB], service.GetRequiredStarterCountByPosition("ARI", PlayerType.QB));
            Assert.AreEqual(expectedTwoTE[PlayerType.RB], service.GetRequiredStarterCountByPosition("ARI", PlayerType.RB));
            Assert.AreEqual(expectedTwoTE[PlayerType.WR], service.GetRequiredStarterCountByPosition("ARI", PlayerType.WR));
            Assert.AreEqual(expectedTwoTE[PlayerType.TE], service.GetRequiredStarterCountByPosition("ARI", PlayerType.TE));
            Assert.AreEqual(expectedTwoTE[PlayerType.OL], service.GetRequiredStarterCountByPosition("ARI", PlayerType.OL));
            Assert.AreEqual(expectedTwoTE[PlayerType.K], service.GetRequiredStarterCountByPosition("ARI", PlayerType.P));
        }

        //**** REQUIRED MINIMUMS ***/

        [Test]
        public void GetRequiredMinimumCountByPosition_FourThree()
        {
            StarterService service = testData.GetStarterService(BaseAll.TwoRB, BaseAll.FourThree);

            Dictionary<PlayerType, int> expectedFourThree = new Dictionary<PlayerType, int>()
            {
                  { PlayerType.DL, 7 }
                , { PlayerType.LB, 6 }
                , { PlayerType.DB, 8 }
            };

            Assert.AreEqual(expectedFourThree[PlayerType.DL], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.DL));
            Assert.AreEqual(expectedFourThree[PlayerType.LB], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.LB));
            Assert.AreEqual(expectedFourThree[PlayerType.DB], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.DB));
        }

        [Test]
        public void GetRequiredMinimumCountByPosition_ThreeFour()
        {
            StarterService service = testData.GetStarterService(BaseAll.TwoRB, BaseAll.ThreeFour);

            Dictionary<PlayerType, int> expectedThreeFour = new Dictionary<PlayerType, int>()
            {
                  { PlayerType.DL, 6 }
                , { PlayerType.LB, 7 }
                , { PlayerType.DB, 8 }
            };

            Assert.AreEqual(expectedThreeFour[PlayerType.DL], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.DL));
            Assert.AreEqual(expectedThreeFour[PlayerType.LB], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.LB));
            Assert.AreEqual(expectedThreeFour[PlayerType.DB], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.DB));
        }

        [Test]
        public void GetRequiredMinimumCountByPosition_ThreeWR()
        {
            StarterService service = testData.GetStarterService(BaseAll.ThreeWR, BaseAll.FourThree);

            Dictionary<PlayerType, int> expectedThreeWR = new Dictionary<PlayerType, int>()
            {
                  { PlayerType.QB, 2 }
                , { PlayerType.RB, 3 }
                , { PlayerType.WR, 5 }
                , { PlayerType.TE, 2 }
                , { PlayerType.OL, 8 }
                , { PlayerType.K, 1 }
                , { PlayerType.P, 1 }
            };

            Assert.AreEqual(expectedThreeWR[PlayerType.QB], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.QB));
            Assert.AreEqual(expectedThreeWR[PlayerType.RB], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.RB));
            Assert.AreEqual(expectedThreeWR[PlayerType.WR], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.WR));
            Assert.AreEqual(expectedThreeWR[PlayerType.TE], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.TE));
            Assert.AreEqual(expectedThreeWR[PlayerType.OL], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.OL));
            Assert.AreEqual(expectedThreeWR[PlayerType.K], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.P));
        }

        [Test]
        public void GetRequiredMinimumCountByPosition_TwoRB()
        {
            StarterService service = testData.GetStarterService(BaseAll.TwoRB, BaseAll.FourThree);

            Dictionary<PlayerType, int> expectedTwoRB = new Dictionary<PlayerType, int>()
            {
                  { PlayerType.QB, 2 }
                , { PlayerType.RB, 4 }
                , { PlayerType.WR, 4 }
                , { PlayerType.TE, 2 }
                , { PlayerType.OL, 8 }
                , { PlayerType.K, 1 }
                , { PlayerType.P, 1 }
            };

            Assert.AreEqual(expectedTwoRB[PlayerType.QB], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.QB));
            Assert.AreEqual(expectedTwoRB[PlayerType.RB], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.RB));
            Assert.AreEqual(expectedTwoRB[PlayerType.WR], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.WR));
            Assert.AreEqual(expectedTwoRB[PlayerType.TE], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.TE));
            Assert.AreEqual(expectedTwoRB[PlayerType.OL], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.OL));
            Assert.AreEqual(expectedTwoRB[PlayerType.K], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.P));
        }

        [Test]
        public void GetRequiredMinimumCountByPosition_TwoTE()
        {
            StarterService service = testData.GetStarterService(BaseAll.TwoTE, BaseAll.FourThree);

            Dictionary<PlayerType, int> expectedTwoTE = new Dictionary<PlayerType, int>()
            {
                  { PlayerType.QB, 2 }
                , { PlayerType.RB, 3 }
                , { PlayerType.WR, 4 }
                , { PlayerType.TE, 3 }
                , { PlayerType.OL, 8 }
                , { PlayerType.K, 1 }
                , { PlayerType.P, 1 }
            };

            Assert.AreEqual(expectedTwoTE[PlayerType.QB], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.QB));
            Assert.AreEqual(expectedTwoTE[PlayerType.RB], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.RB));
            Assert.AreEqual(expectedTwoTE[PlayerType.WR], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.WR));
            Assert.AreEqual(expectedTwoTE[PlayerType.TE], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.TE));
            Assert.AreEqual(expectedTwoTE[PlayerType.OL], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.OL));
            Assert.AreEqual(expectedTwoTE[PlayerType.K], service.GetRequiredMinimumCountByPosition("ARI", PlayerType.P));
        }

        [Test]
        public void GetBottomStarterByPosition()
        {
        }

    }
}

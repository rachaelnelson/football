﻿using Football.League.Players;
using Football.League.Teams;
using Football.Services;
using FootballTests.TestData;
using NUnit.Framework;

namespace FootballTests.UnitTests.Services
{
    [TestFixture]
    public class InactiveServiceTests
    {
        private InactiveServiceTestData testData;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            testData = new InactiveServiceTestData();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            testData = null;
        }

        [Test]
        public void SelectInactives_InactivatePlayers_PlayerInactivated()
        {
            FootballSettingsService settingsService = testData.GetSettingsServiceInactivatePlayers();
            InactiveService inactiveService = new InactiveService(settingsService, new PlayerTypeValues());
            RosterService rosterService = new RosterService(settingsService);

            inactiveService.SelectInactives();

            RosterCountView rosterCounts = rosterService.GetRosterCounts("ARI");
            Assert.AreEqual(rosterCounts.ActiveCount, rosterCounts.ActiveLimit);
        }
    }
}

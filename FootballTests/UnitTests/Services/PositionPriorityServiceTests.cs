﻿
using Football.Services;
using FootballTests.TestData;
using NUnit.Framework;
using Football.League.Players;

namespace FootballTests.UnitTests.Services
{                 
    [TestFixture]
    public class PositionPriorityServiceTests
    {
        private PositionPriorityServiceTestData testData;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            testData = new PositionPriorityServiceTestData();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            testData = null;
        }

        [Test]
        public void PPS_QueueCountEmpty_IsEmpty()
        {
            PositionPriorityService pps = testData.TestIsEmptyEmpty();

            int expected = 0;
            Assert.AreEqual( expected, pps.Count("ARI") );
        }

        [Test]
        public void PPS_QueueCountEmpty_IsNotEmpty()
        {
            PositionPriorityService pps = testData.TestIsNotEmpty();

            int expected = 1;
            Assert.AreEqual(expected, pps.Count("ARI"));
        }

        [Test]
        public void PPS_GetNextInQueue_IsNotEmpty()
        {
            PositionPriorityService pps = testData.TestNextInListIsNotEmpty();

            PlayerType expected = PlayerType.RB;
            Assert.AreEqual(expected, pps.GetNext("ARI").Position);
        }

        [Test]
        public void PPS_GetNextInQueue_IsEmpty()
        {
            PositionPriorityService pps = testData.TestNextInListIsEmpty();

            PlayerType expected = null;
            Assert.AreEqual(expected, pps.GetNext("ARI"));
        }

        [Test]
        public void PPS_HasOpenSpots_True()
        {
            PositionPriorityService pps = testData.TestHasOpenSpotsTrue();

            bool expected = true;
            Assert.AreEqual(expected, pps.hasOpenSpots());
        }

        [Test]
        public void PPS_HasOpenSpots_False()
        {
            PositionPriorityService pps = testData.TestHasOpenSpotsFalse();

            bool expected = false;
            Assert.AreEqual(expected, pps.hasOpenSpots());
        }

        [Test]
        public void PPS_GetTeamsWithQueues_NotEmpty()
        {
            PositionPriorityService pps = testData.TestGetTeamsWithListNotEmpty();

            int expected = 2;
            Assert.AreEqual(expected, pps.GetTeamsWithLists().Count);
        }

        [Test]
        public void PPS_GetTeamsWithQueues_Empty()
        {
            PositionPriorityService pps = testData.TestGetTeamsWithListEmpty();

            int expected = 0;
            Assert.AreEqual(expected, pps.GetTeamsWithLists().Count);
        }

        [Test]
        public void PPS_PositionMultipleStarterOpenings_AvgRatingUpdated()
        {
            PositionPriorityService pps = testData.TestPositionMultipleStarterOpenings();

            PlayerType expected1 = PlayerType.WR;
            PlayerType expected2 = PlayerType.RB;
            PlayerType expected3 = PlayerType.WR;

            Assert.AreEqual(expected1, pps.GetNext("ARI").Position);
            Assert.AreEqual(expected2, pps.GetNext("ARI").Position);
            Assert.AreEqual(expected3, pps.GetNext("ARI").Position);
        }

        [Test]
        public void PPS_PositionMultipleMinimumOpenings_AvgRatingUpdated()
        {
            PositionPriorityService pps = testData.TestPositionMultipleMinimumOpenings();

            PlayerType expected1 = PlayerType.WR;
            PlayerType expected2 = PlayerType.RB;
            PlayerType expected3 = PlayerType.WR;

            Assert.AreEqual(expected1, pps.GetNext("ARI").Position);
            Assert.AreEqual(expected2, pps.GetNext("ARI").Position);
            Assert.AreEqual(expected3, pps.GetNext("ARI").Position);
        }

        [Test]
        public void PPS_PositionMultipleStarterAndMinimumOpenings_AvgRatingUpdated()
        {
            PositionPriorityService pps = testData.TestPositionStarterAndMinimumOpenings();

            PlayerType expected1 = PlayerType.WR;
            PlayerType expected2 = PlayerType.RB;
            PlayerType expected3 = PlayerType.TE;
            PlayerType expected4 = PlayerType.WR;

            Assert.AreEqual(expected1, pps.GetNext("ARI").Position);
            Assert.AreEqual(expected2, pps.GetNext("ARI").Position);
            Assert.AreEqual(expected3, pps.GetNext("ARI").Position);
            Assert.AreEqual(expected4, pps.GetNext("ARI").Position);
        }
    }
}

﻿using Football.League.Players;
using Football.Services;
using FootballTests.TestData;

using NUnit.Framework;

// TODO:  Need to verify actual player being signed
namespace FootballTests.UnitTests.Services
{
    [TestFixture]
    public class SignPlayerServiceTests
    {
        private SignPlayerServiceTestData testData;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            testData = new SignPlayerServiceTestData();
        }

        [TestFixtureTearDown]
        public void FixtureTearDown()
        {
            testData = null;
        }

        [Test]
        public void SignFreeAgentByPosition_SignTopPlayer_PlayerAddedToRoster()
        {
            SignPlayerService faService = testData.GetSignTopPlayer();     
            string teamAbbr = "ARI";

            bool expectedResult = true; 
            bool actualResult = faService.SignFreeAgentByPosition( teamAbbr, PlayerType.QB, 1000000 );

            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void SignFreeAgentByPosition_SignTopPlayerToReplaceStarter_PlayerAddedToRoster()
        {
            SignPlayerService faService = testData.GetSignTopPlayerToReplaceStarter();
            string teamAbbr = "ARI";

            bool expectedResult = true;
            bool actualResult = faService.SignFreeAgentByPosition(teamAbbr, PlayerType.QB, 1000000);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void SignFreeAgentByPosition_SignTopPlayerWithinBudget_PlayerSigned()
        {
            SignPlayerService faService = testData.GetSignTopPlayerWithinBudget();
            string teamAbbr = "ARI";

            bool expectedResult = true;
            bool actualResult = faService.SignFreeAgentByPosition(teamAbbr, PlayerType.QB, 1000000);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void SignFreeAgentByPosition_SignBestCheapestPlayer_PlayerSigned()
        {
            SignPlayerService faService = testData.GetSignBestCheapestPlayer();
            string teamAbbr = "ARI";

            bool expectedResult = true;
            bool actualResult = faService.SignFreeAgentByPosition(teamAbbr, PlayerType.QB, 1000000);

            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}

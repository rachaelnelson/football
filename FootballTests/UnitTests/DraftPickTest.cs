﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.FSharp.Core;
using Microsoft.FSharp.Collections;

using NUnit.Framework;


namespace FootballTests.UnitTests
{
    [TestFixture]
    public class DraftPickTest
    {
        [Test]
        public void testDraftPick()
        {
            IEnumerable<DraftPicks.DraftPick> picks = DraftPicks.NewDraftYear(2006, new List<string>() { "ARI", "BAL", "DAL" });
            foreach (var item in picks)
                System.Console.WriteLine(item);
        }

        public void testUpdateDraftPick()
        {
            IEnumerable<DraftPicks.DraftPick> picks = DraftPicks.NewDraftYear(2006, new List<string>() { "ARI", "BAL", "DAL" });
            //DraftPicks.UpdateHoldingTeam("MIN", new List<string>(){}, picks)


            foreach (var item in picks)
                System.Console.WriteLine(item.round);
        }
    }
}

﻿using Football.League.Players;
using Football.League.Salary;
using FootballTests.TestData;
using NUnit.Framework;


namespace FootballTests.UnitTests
{
    [TestFixture]
    public class SalaryCalculatorTest
    {
        [Test]
        public void testCalculateExpectedSalary_NonRandom_ReturnsCorrectSalary()
        {
            SalaryCalculatorTestData data = new SalaryCalculatorTestData();

            Player player = data.GetNonRandomSalaryPlayer();

            int expectedSalary = 1800000;
            int actualSalary = SalaryCalculator.CalculateExpectedSalary(player, 1M);

            Assert.AreEqual(expectedSalary, actualSalary);
        }

        [Test]
        public void testCalculateExpectedSalary_RandomIncrease_ReturnsCorrectSalary()
        {
            SalaryCalculatorTestData data = new SalaryCalculatorTestData();

            Player player = data.GetNonRandomSalaryPlayer();

            int expectedSalary = 2160000;
            int actualSalary = SalaryCalculator.CalculateExpectedSalary(player, 1.2M);

            Assert.AreEqual(expectedSalary, actualSalary);
        }
    }
}

﻿using Football.League.Coaches;
using Football.League.Injuries;
using Football.League.Players;
using Football.League.Teams;
using Football.Services;
using Football.Settings;

namespace FootballTests.TestData
{
    public class InactiveServiceTestData
    {
        public FootballSettingsService GetSettingsServiceInactivatePlayers()
        {
            FootballSettings setting = setting = FootballSettings.setSettings();
            setting.myTeam = "DAL";

            Team teamA = new Team("Arizona", "Cardinals", "ARI");
            teamA.Coach = new Coach();
            teamA.Coach.BaseOffense = BaseAll.TwoRB;
            teamA.Coach.BaseDefense = BaseAll.FourThree;

            Team teamC = new Team("Dallas", "Cowboys", "DAL");

            Team teamB = new Team("Free Agent", "", "FA");

            TeamList teamList = new TeamList();
            teamList.addTeam(teamA);
            teamList.addTeam(teamC);

            Player playerB = new Player();
            playerB.position = PlayerType.QB;
            playerB.injury = new PlayerInjury();

            setting.rosterList.Add("ARI", new Roster(teamA));
            setting.rosterList.Add("FA", new Roster(teamB));

            CommonTestData.GetTeamRoster(setting);

            setting.teams = teamList;
            setting.freeAgentsTeam = teamB;

            return new FootballSettingsService(setting);
        }
    }
}

﻿using System.Collections.Generic;
using Football.League.Players;
using Football.Services;
using Football.Services.Starter;
using Football.Settings;
using Moq;
using Football.League.Teams;
using Football.League.Coaches;

namespace FootballTests.TestData
{
    public class PositionPriorityServiceTestData
    {        
        public PositionPriorityService TestIsEmptyEmpty()
        {
            FootballSettings setting = FootballSettings.setSettings();
            FootballSettingsService settingsService = new FootballSettingsService(setting);
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());

            Dictionary<string, List<OpenStarter>> teamLists = new Dictionary<string, List<OpenStarter>>()
            {
                { "ARI", new List<OpenStarter>() }
            }; 

            PositionPriorityService pps = new PositionPriorityService(ptv, new StarterService(settingsService, ptv, new RosterService(settingsService)), teamLists);

            return pps;
        }

        public PositionPriorityService TestIsNotEmpty()
        {
            FootballSettings setting = FootballSettings.setSettings();
            FootballSettingsService settingsService = new FootballSettingsService(setting);
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());

            Dictionary<string, List<OpenStarter>> teamLists = new Dictionary<string, List<OpenStarter>>();
            teamLists.Add("ARI", new List<OpenStarter>());
            teamLists["ARI"].Add(new OpenStarter() { AvgStarterRating = 1.0M, Position = PlayerType.QB, IsStarterPosition = true });
            
            PositionPriorityService pps = new PositionPriorityService(ptv, new StarterService(settingsService, ptv, new RosterService(settingsService)), teamLists);

            return pps;
        }

        public PositionPriorityService TestNextInListIsNotEmpty()
        {
            FootballSettings setting = FootballSettings.setSettings();
            
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());

            Team teamA = new Team("Arizona", "Cardinals", "ARI");
            teamA.Coach = new Coach();
            teamA.Coach.BaseOffense = BaseAll.TwoRB;
            teamA.Coach.BaseDefense = BaseAll.FourThree;

            TeamList teamList = new TeamList();
            teamList.addTeam(teamA);
            setting.teams = teamList;
            setting.rosterList.Add("ARI", new Roster(teamA));

            FootballSettingsService settingsService = new FootballSettingsService(setting);

            Dictionary<string, List<OpenStarter>> teamLists = new Dictionary<string, List<OpenStarter>>();
            teamLists.Add("ARI", new List<OpenStarter>());
            teamLists["ARI"].Add(new OpenStarter() { AvgStarterRating = 1.0M, Position = PlayerType.RB, IsStarterPosition=true});

            PositionPriorityService pps = new PositionPriorityService(ptv, new StarterService(settingsService, ptv, new RosterService(settingsService)), teamLists);

            return pps;
        }

        public PositionPriorityService TestNextInListIsEmpty()
        {
            FootballSettings setting = FootballSettings.setSettings();
            FootballSettingsService settingsService = new FootballSettingsService(setting);
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());

            Dictionary<string, List<OpenStarter>> teamLists = new Dictionary<string, List<OpenStarter>>();
            teamLists.Add("ARI", new List<OpenStarter>());            

            PositionPriorityService pps = new PositionPriorityService(ptv, new StarterService(settingsService, ptv, new RosterService(settingsService)), teamLists);

            return pps;
        }

        public PositionPriorityService TestHasOpenSpotsTrue()
        {
            FootballSettings setting = FootballSettings.setSettings();
            FootballSettingsService settingsService = new FootballSettingsService(setting);
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());

            Dictionary<string, List<OpenStarter>> teamLists = new Dictionary<string, List<OpenStarter>>();
            teamLists.Add("ARI", new List<OpenStarter>());
            teamLists.Add("BAL", new List<OpenStarter>());
            teamLists.Add("DAL", new List<OpenStarter>());

            teamLists["ARI"].Add(new OpenStarter() { AvgStarterRating = 1.0M, Position = PlayerType.RB, IsStarterPosition = true });

            PositionPriorityService pps = new PositionPriorityService(ptv, new StarterService(settingsService, ptv, new RosterService(settingsService)), teamLists);

            return pps;
        }

        public PositionPriorityService TestHasOpenSpotsFalse()
        {
            FootballSettings setting = FootballSettings.setSettings();
            FootballSettingsService settingsService = new FootballSettingsService(setting);
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());

            Dictionary<string, List<OpenStarter>> teamLists = new Dictionary<string, List<OpenStarter>>();
            teamLists.Add("ARI", new List<OpenStarter>());
            teamLists.Add("BAL", new List<OpenStarter>());
            teamLists.Add("DAL", new List<OpenStarter>());

            PositionPriorityService pps = new PositionPriorityService(ptv, new StarterService(settingsService, ptv, new RosterService(settingsService)), teamLists);

            return pps;
        }

        public PositionPriorityService TestGetTeamsWithListNotEmpty()
        {
            FootballSettings setting = FootballSettings.setSettings();
            FootballSettingsService settingsService = new FootballSettingsService(setting);
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());

            Dictionary<string, List<OpenStarter>> teamLists = new Dictionary<string, List<OpenStarter>>();
            teamLists.Add("ARI", new List<OpenStarter>());
            teamLists.Add("BAL", new List<OpenStarter>());
            teamLists.Add("DAL", new List<OpenStarter>());

            teamLists["ARI"].Add(new OpenStarter() { AvgStarterRating = 1.0M, Position = PlayerType.WR, IsStarterPosition = true });
            teamLists["ARI"].Add(new OpenStarter() { AvgStarterRating = 1.0M, Position = PlayerType.DB, IsStarterPosition = true });
            teamLists["BAL"].Add(new OpenStarter() { AvgStarterRating = 1.0M, Position = PlayerType.RB, IsStarterPosition = true });

            PositionPriorityService pps = new PositionPriorityService(ptv, new StarterService(settingsService, ptv, new RosterService(settingsService)), teamLists);

            return pps;
        }

        public PositionPriorityService TestGetTeamsWithListEmpty()
        {
            FootballSettings setting = FootballSettings.setSettings();
            FootballSettingsService settingsService = new FootballSettingsService(setting);
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());

            Dictionary<string, List<OpenStarter>> teamLists = new Dictionary<string, List<OpenStarter>>();
            teamLists.Add("ARI", new List<OpenStarter>());
            teamLists.Add("BAL", new List<OpenStarter>());
            teamLists.Add("DAL", new List<OpenStarter>());
            
            PositionPriorityService pps = new PositionPriorityService(ptv, new StarterService(settingsService, ptv, new RosterService(settingsService)), teamLists);

            return pps;
        }

        public PositionPriorityService TestPositionMultipleStarterOpenings()
        {

            FootballSettings setting = FootballSettings.setSettings();
            FootballSettingsService settingsService = new FootballSettingsService(setting);
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());

            Dictionary<string, List<OpenStarter>> teamLists = new Dictionary<string, List<OpenStarter>>();
            teamLists.Add("ARI", new List<OpenStarter>());
            teamLists["ARI"].Add(new OpenStarter() { AvgStarterRating = 2.0M, Position = PlayerType.RB, IsStarterPosition = true });
            teamLists["ARI"].Add(new OpenStarter() { AvgStarterRating = 1.0M, Position = PlayerType.WR, IsStarterPosition = true });
            teamLists["ARI"].Add(new OpenStarter() { AvgStarterRating = 1.0M, Position = PlayerType.WR, IsStarterPosition = true });

            var callCount = 0;
            var mock = new Mock<StarterService>(settingsService, ptv, new RosterService(settingsService));
            mock.Setup(x => x.GetAverageStarterRating("ARI", It.IsAny<PlayerType>()))
                .Callback((string team, PlayerType position) => { callCount++; })
                .Returns((string team, PlayerType position) => { return FuncTestMultipleOpenings(team, position, callCount); });
                    
            
            PositionPriorityService pps = new PositionPriorityService(ptv, mock.Object, teamLists);

            return pps;
        }

        public PositionPriorityService TestPositionMultipleMinimumOpenings()
        {

            FootballSettings setting = FootballSettings.setSettings();
            FootballSettingsService settingsService = new FootballSettingsService(setting);
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());

            Dictionary<string, List<OpenStarter>> teamLists = new Dictionary<string, List<OpenStarter>>();
            teamLists.Add("ARI", new List<OpenStarter>());
            teamLists["ARI"].Add(new OpenStarter() { AvgStarterRating = 2.0M, Position = PlayerType.RB, IsStarterPosition = false });
            teamLists["ARI"].Add(new OpenStarter() { AvgStarterRating = 1.0M, Position = PlayerType.WR, IsStarterPosition = false });
            teamLists["ARI"].Add(new OpenStarter() { AvgStarterRating = 1.0M, Position = PlayerType.WR, IsStarterPosition = false });

            var callCount = 0;
            var mock = new Mock<StarterService>(settingsService, ptv, new RosterService(settingsService));
            mock.Setup(x => x.GetAverageStarterRating("ARI", It.IsAny<PlayerType>()))
                .Callback((string team, PlayerType position) => { callCount++; })
                .Returns((string team, PlayerType position) => { return FuncTestMultipleOpenings(team, position, callCount); });


            PositionPriorityService pps = new PositionPriorityService(ptv, mock.Object, teamLists);

            return pps;
        }

        public PositionPriorityService TestPositionStarterAndMinimumOpenings()
        {
            FootballSettings setting = FootballSettings.setSettings();
            FootballSettingsService settingsService = new FootballSettingsService(setting);
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());

            Dictionary<string, List<OpenStarter>> teamLists = new Dictionary<string, List<OpenStarter>>();
            teamLists.Add("ARI", new List<OpenStarter>());
            teamLists["ARI"].Add(new OpenStarter() { AvgStarterRating = 2.6M, Position = PlayerType.RB, IsStarterPosition = true });
            teamLists["ARI"].Add(new OpenStarter() { AvgStarterRating = 3.0M, Position = PlayerType.TE, IsStarterPosition = false });
            teamLists["ARI"].Add(new OpenStarter() { AvgStarterRating = 2.0M, Position = PlayerType.WR, IsStarterPosition = true });
            teamLists["ARI"].Add(new OpenStarter() { AvgStarterRating = 1.0M, Position = PlayerType.WR, IsStarterPosition = false });

            var callCount = 0;
            var mock = new Mock<StarterService>(settingsService, ptv, new RosterService(settingsService));
            mock.Setup(x => x.GetAverageStarterRating("ARI", It.IsAny<PlayerType>()))
                .Callback((string team, PlayerType position) => { callCount++; })
                .Returns((string team, PlayerType position) => { return FuncTestMultipleOpenings(team, position, callCount); });


            PositionPriorityService pps = new PositionPriorityService(ptv, mock.Object, teamLists);

            return pps;
        }


        // this is pretty gross
        private decimal FuncTestMultipleOpenings(string team, PlayerType position, int callCount)
        {
            decimal result = 2.0M;
           
            if (position == PlayerType.WR)
            {
                result = (callCount > 3)? 2.5M : 1.0M;         // super fucking hacky...       
            }

            return result;
        }
    }
}

﻿using Football.League.Coaches;
using Football.League.Injuries;
using Football.League.Players;
using Football.League.Salary;
using Football.League.Teams;
using Football.Services;
using Football.Settings;

using System.Collections.Generic;

namespace FootballTests.TestData
{
    public class SignPlayerServiceTestData
    {
        public SignPlayerService GetSignTopPlayer()
        {
            FootballSettings setting = FootballSettings.setSettings();
            setting.myTeam = "DAL";

            Team teamA = new Team("Arizona", "Cardinals", "ARI");
            teamA.Coach = new Coach();
            teamA.Coach.BaseOffense = BaseAll.TwoRB;
            teamA.Coach.BaseDefense = BaseAll.FourThree;

            Team teamB = new Team("Baltimore", "Ravens", "BAL");
            teamB.Coach = new Coach();
            teamB.Coach.BaseOffense = BaseAll.TwoRB;
            teamB.Coach.BaseDefense = BaseAll.FourThree;

            Team teamC = new Team("Free Agent", "", "FA");

            Team teamD = new Team("Dallas", "Cowboys", "DAL");

            TeamList teamList = new TeamList();
            teamList.addTeam(teamA);
            teamList.addTeam(teamB);
            teamList.addTeam(teamC);
            teamList.addTeam(teamD);

            Player playerA = new Player()
            {
                id = 1,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 5,
                accuracy = 4,
                distance = 4,
                firstName = "A",
                lastName = "A",
                expectedSalary = 500000,
                team = teamC
            };

            Player playerB = new Player()
            {
                id = 2,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 3,
                accuracy = 3,
                distance = 3,
                firstName = "B",
                lastName = "B",
                expectedSalary = 400000,
                team = teamC
            };

            Player playerC = new Player()
            {
                id = 3,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 2,
                accuracy = 2,
                distance = 2,
                firstName = "C",
                lastName = "C",
                expectedSalary = 300000,
                team = teamC
            };

            Player playerD = new Player()
            {
                id = 4,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 1,
                accuracy = 1,
                distance = 1,
                firstName = "D",
                lastName = "D",
                expectedSalary = 200000,
                team = teamC
            };       

            setting.rosterList.Add("ARI", new Roster(teamA));
            setting.rosterList.Add("BAL", new Roster(teamB));
            setting.rosterList.Add("FA", new Roster(teamC));
            setting.rosterList["FA"].addPlayer(playerA);
            setting.rosterList["FA"].addPlayer(playerB);
            setting.rosterList["FA"].addPlayer(playerC);
            setting.rosterList["FA"].addPlayer(playerD);

            setting.teams = teamList;
            setting.freeAgentsTeam = teamC;

            FootballSettingsService settingsService = new FootballSettingsService(setting);
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());
            RosterService rosterService = new RosterService(settingsService);
            CapManagementService capService = new CapManagementService(settingsService, rosterService);

            return new SignPlayerService(rosterService, new StarterService(settingsService, ptv, rosterService), new TransactionService(rosterService, capService, settingsService), settingsService);
        }

        public SignPlayerService GetSignTopPlayerWithinBudget()
        {
            FootballSettings setting = setting = FootballSettings.setSettings();
            setting.myTeam = "DAL";

            Team teamA = new Team("Arizona", "Cardinals", "ARI");
            teamA.Coach = new Coach();
            teamA.Coach.BaseOffense = BaseAll.TwoRB;
            teamA.Coach.BaseDefense = BaseAll.FourThree;

            Team teamB = new Team("Baltimore", "Ravens", "BAL");
            teamB.Coach = new Coach();
            teamB.Coach.BaseOffense = BaseAll.TwoRB;
            teamB.Coach.BaseDefense = BaseAll.FourThree;

            Team teamC = new Team("Free Agent", "FA", "FA");

            Team teamD = new Team("Dallas", "Cowboys", "DAL");

            TeamList teamList = new TeamList();
            teamList.addTeam(teamA);
            teamList.addTeam(teamB);
            teamList.addTeam(teamC);
            teamList.addTeam(teamD);

            Player playerA = new Player()
            {
                id = 1,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 5,
                accuracy = 4,
                distance = 4,
                firstName = "A",
                lastName = "A",
                expectedSalary = 5000000,
                team = teamC
            };

            Player playerB = new Player()
            {
                id = 2,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 3,
                accuracy = 3,
                distance = 3,
                firstName = "B",
                lastName = "B",
                expectedSalary = 4000000,
                team = teamC
            };

            Player playerC = new Player()
            {
                id = 3,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 2,
                accuracy = 2,
                distance = 2,
                firstName = "C",
                lastName = "C",
                expectedSalary = 3000000,
                team = teamC
            };

            Player playerD = new Player()
            {
                id = 4,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 1,
                accuracy = 1,
                distance = 1,
                firstName = "D",
                lastName = "D",
                expectedSalary = 1000000,
                team = teamC
            };

            setting.rosterList.Add("ARI", new Roster(teamA));
            setting.rosterList.Add("BAL", new Roster(teamB));
            setting.rosterList.Add("FA", new Roster(teamC));
            setting.rosterList["FA"].addPlayer(playerA);
            setting.rosterList["FA"].addPlayer(playerB);
            setting.rosterList["FA"].addPlayer(playerC);
            setting.rosterList["FA"].addPlayer(playerD);

            setting.teams = teamList;
            setting.freeAgentsTeam = teamC;

            FootballSettingsService settingsService = new FootballSettingsService(setting);
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());
            RosterService rosterService = new RosterService(settingsService);
            CapManagementService capService = new CapManagementService(settingsService, rosterService);

            return new SignPlayerService(rosterService, new StarterService(settingsService, ptv, rosterService), new TransactionService(rosterService, capService, settingsService), settingsService);
        }

        public SignPlayerService GetSignTopPlayerToReplaceStarter()
        {
            FootballSettings setting = setting = FootballSettings.setSettings();
            setting.myTeam = "DAL";

            Team teamA = new Team("Arizona", "Cardinals", "ARI");
            teamA.Coach = new Coach();
            teamA.Coach.BaseOffense = BaseAll.TwoRB;
            teamA.Coach.BaseDefense = BaseAll.FourThree;

            Team teamB = new Team("Baltimore", "Ravens", "BAL");
            teamB.Coach = new Coach();
            teamB.Coach.BaseOffense = BaseAll.TwoRB;
            teamB.Coach.BaseDefense = BaseAll.FourThree;

            Team teamC = new Team("Free Agent", "", "FA");

            Team teamD = new Team("Dallas", "Cowboys", "DAL");

            TeamList teamList = new TeamList();
            teamList.addTeam(teamA);
            teamList.addTeam(teamB);
            teamList.addTeam(teamC);
            teamList.addTeam(teamD);

            Player playerA = new Player()
            {
                id = 1,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 5,
                accuracy = 4,
                distance = 4,
                firstName = "A",
                lastName = "A",
                expectedSalary = 500000,
                team = teamC
            };

            Player playerB = new Player()
            {
                id = 2,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 3,
                accuracy = 3,
                distance = 3,
                firstName = "B",
                lastName = "B",
                expectedSalary = 400000,
                team = teamC
            };

            Player playerC = new Player()
            {
                id = 3,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 2,
                accuracy = 2,
                distance = 2,
                firstName = "C",
                lastName = "C",
                expectedSalary = 300000,
                team = teamC
            };

            Player playerD = new Player()
            {
                id = 4,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 1,
                accuracy = 1,
                distance = 1,
                firstName = "D",
                lastName = "D",
                expectedSalary = 200000,
                team = teamC
            };

            int[] playerEContract = { 200000 };

            Player playerE = new Player()
            {
                id = 5,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 1,
                accuracy = 1,
                distance = 1,
                firstName = "E",
                lastName = "E",
                expectedSalary = 0,       
                team = teamA
            };
            playerE.contract = new PlayerContract(teamA, playerE, playerEContract);
           
            setting.rosterList.Add("ARI", new Roster(teamA));
            setting.rosterList.Add("BAL", new Roster(teamB));
            setting.rosterList.Add("FA", new Roster(teamC));
            setting.rosterList["FA"].addPlayer(playerA);
            setting.rosterList["FA"].addPlayer(playerB);
            setting.rosterList["FA"].addPlayer(playerC);
            setting.rosterList["FA"].addPlayer(playerD);
            setting.rosterList["ARI"].addPlayer(playerE);

            setting.teams = teamList;
            setting.freeAgentsTeam = teamC;

            FootballSettingsService settingsService = new FootballSettingsService(setting);
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());
            RosterService rosterService = new RosterService(settingsService);
            CapManagementService capService = new CapManagementService(settingsService, rosterService);

            return new SignPlayerService(rosterService, new StarterService(settingsService, ptv, rosterService), new TransactionService(rosterService, capService, settingsService), settingsService);
        }

        public SignPlayerService GetSignBestCheapestPlayer()
        {
            FootballSettings setting = setting = FootballSettings.setSettings();
            setting.myTeam = "DAL";

            Team teamA = new Team("Arizona", "Cardinals", "ARI");
            teamA.Coach = new Coach();
            teamA.Coach.BaseOffense = BaseAll.TwoRB;
            teamA.Coach.BaseDefense = BaseAll.FourThree;

            Team teamB = new Team("Baltimore", "Ravens", "BAL");
            teamB.Coach = new Coach();
            teamB.Coach.BaseOffense = BaseAll.TwoRB;
            teamB.Coach.BaseDefense = BaseAll.FourThree;

            Team teamC = new Team("Free Agent", "", "FA");

            Team teamD = new Team("Dallas", "Cowboys", "DAL");

            TeamList teamList = new TeamList();
            teamList.addTeam(teamA);
            teamList.addTeam(teamB);
            teamList.addTeam(teamC);
            teamList.addTeam(teamD);

            Player playerA = new Player()
            {
                id = 1,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 5,
                accuracy = 4,
                distance = 4,
                firstName = "A",
                lastName = "A",
                expectedSalary = 500000,
                team = teamC
            };

            Player playerB = new Player()
            {
                id = 2,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 3,
                accuracy = 3,
                distance = 3,
                firstName = "B",
                lastName = "B",
                expectedSalary = 400000,
                team = teamC
            };

            Player playerC = new Player()
            {
                id = 3,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 2,
                accuracy = 2,
                distance = 2,
                firstName = "C",
                lastName = "C",
                expectedSalary = 100000,
                team = teamC
            };

            Player playerD = new Player()
            {
                id = 4,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 1,
                accuracy = 1,
                distance = 1,
                firstName = "D",
                lastName = "D",
                expectedSalary = 2000000,
                team = teamC
            };

            int[] playerEContract = { 200000 };

            Player playerE = new Player()
            {
                id = 5,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 5,
                accuracy = 5,
                distance = 5,
                firstName = "E",
                lastName = "E",
                expectedSalary = 0,
                team = teamA
            };
            playerE.contract = new PlayerContract(teamA, playerE, playerEContract);

            setting.rosterList.Add("ARI", new Roster(teamA));
            setting.rosterList.Add("BAL", new Roster(teamB));
            setting.rosterList.Add("FA", new Roster(teamC));
            setting.rosterList["FA"].addPlayer(playerA);
            setting.rosterList["FA"].addPlayer(playerB);
            setting.rosterList["FA"].addPlayer(playerC);
            setting.rosterList["FA"].addPlayer(playerD);
            setting.rosterList["ARI"].addPlayer(playerE);

            setting.teams = teamList;
            setting.freeAgentsTeam = teamC;

            FootballSettingsService settingsService = new FootballSettingsService(setting);
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());
            RosterService rosterService = new RosterService(settingsService);
            CapManagementService capService = new CapManagementService(settingsService, rosterService);

            return new SignPlayerService(rosterService, new StarterService(settingsService, ptv,rosterService), new TransactionService(rosterService, capService, settingsService), settingsService);
        }
    }
}

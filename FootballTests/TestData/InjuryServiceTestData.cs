﻿
using Football.League.Injuries;
using Football.League.Players;
using Football.League.Teams;
using Football.Services;
using Football.Settings;

namespace FootballTests.TestData
{
    public class InjuryServiceTestData
    {               
        public InjuryService GetInjuryService(InjuryTestView testView)
        {
            FootballSettings setting = FootballSettings.setSettings();

            Team ari = new Team("Arizona", "Cardinals", "ARI");
            Team bal = new Team("Baltimore", "Ravens", "BAL");          

            setting.teams = new TeamList();
            setting.teams.addTeam(ari);
            setting.teams.addTeam(bal);

            Player playerA = new Player { id = 1, team = ari, position = PlayerType.DB, injury = new PlayerInjury { duration = testView.InjuryDuration, injury = testView.InjuryType } };

            setting.rosterList.Add(ari.Abbr, new Roster(ari));
            setting.rosterList.Add(bal.Abbr, new Roster(bal));

            setting.rosterList[testView.PlayerTeam].addPlayer(playerA);

            setting.injuriesEnabled = testView.EnableInjuries;
            setting.myTeam = testView.MyTeam;

            if (testView.AlreadyOnIR)
            {
                setting.rosterList[testView.PlayerTeam].addPlayerToIR(playerA);
            }

            FootballSettingsService settingsService = new FootballSettingsService(setting);
            return new InjuryService(new FootballSettingsService(setting), new RosterService(settingsService));
        }
    }

    public class InjuryTestView
    {
        public bool AlreadyOnIR { get; set; }
        public bool EnableInjuries { get; set; }
        public int InjuryDuration { get; set; }
        public Injury InjuryType { get; set; }
        public string MyTeam { get; set; }
        public string PlayerTeam { get; set; }  
    }

}

﻿using Football.League.Players;
using Football.League.Salary;
using Football.League.Teams;
using Football.Services;
using Football.Settings;

namespace FootballTests.TestData
{
    public class CapManagementTestData
    {        
        public FootballSettingsService GetSettingsServiceWithCurrentYear()
        {
            FootballSettings setting = FootballSettings.setSettings();
            setting.year = 2005;            
            return new FootballSettingsService(setting);
        }
        
        public FootballSettingsService GetSettingsServiceWithCapSpace()
        {
            string myTeam = "ARI";

            Team teamA = new Team("Arizona","Cardinals","ARI");

            Player playerA = new Player { position = PlayerType.DB };
            playerA.contract = new PlayerContract(teamA,playerA,new int[]{ 7000000 }, 1 );

            Player playerB = new Player { position = PlayerType.DB };
            playerB.contract = new PlayerContract(teamA, playerB, new int[] { 3000000 }, 1 );

            FootballSettings setting = setting = FootballSettings.setSettings();            
            setting.myTeam = myTeam;
            setting.rosterList.Add("ARI", new Roster(teamA));
            setting.rosterList[myTeam].addPlayer(playerA);
            setting.rosterList[myTeam].addPlayer(playerB);

            return new FootballSettingsService(setting);
        }
    }
}

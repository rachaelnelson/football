﻿using Football.League.Players;

namespace FootballTests.TestData
{
    public class SalaryCalculatorTestData
    {
        public Player GetNonRandomSalaryPlayer()
        {
            return new Player { position=PlayerType.OL, passBlocking = 5, awareness=1, runBlocking=1, speed=1 };
        }
    }
}

﻿using Football.League.Injuries;
using Football.League.Players;
using Football.Settings;

namespace FootballTests.TestData
{
    public class CommonTestData
    {
        public static void GetTeamRoster(FootballSettings setting)
        {
            // QBS
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.QB, injury = new PlayerInjury(), accuracy = 5, awareness = 1, distance = 1, speed = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.QB, injury = new PlayerInjury(), accuracy = 4, awareness = 1, distance = 1, speed = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.QB, injury = new PlayerInjury(), accuracy = 3, awareness = 1, distance = 1, speed = 1 });

            // RBS
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.RB, injury = new PlayerInjury(), awareness = 5, speed = 1, receiving = 1, passBlocking = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.RB, injury = new PlayerInjury(), awareness = 4, speed = 1, receiving = 1, passBlocking = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.RB, injury = new PlayerInjury(), awareness = 3, speed = 1, receiving = 1, passBlocking = 1 });

            // WRS
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.WR, injury = new PlayerInjury(), awareness = 5, speed = 1, receiving = 1, runBlocking = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.WR, injury = new PlayerInjury(), awareness = 4, speed = 1, receiving = 1, runBlocking = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.WR, injury = new PlayerInjury(), awareness = 3, speed = 2, receiving = 1, runBlocking = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.WR, injury = new PlayerInjury(), awareness = 3, speed = 1, receiving = 1, runBlocking = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.WR, injury = new PlayerInjury(), awareness = 2, speed = 1, receiving = 1, runBlocking = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.WR, injury = new PlayerInjury(), awareness = 2, speed = 1, receiving = 1, runBlocking = 1 });

            // TE
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.TE, injury = new PlayerInjury(), awareness = 10, speed = 1, receiving = 1, runBlocking = 1, passBlocking = 1 });

            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.TE, injury = new PlayerInjury(), awareness = 5, speed = 1, receiving = 1, runBlocking = 1, passBlocking = 1 });

            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.TE, injury = new PlayerInjury(), awareness = 2, speed = 1, receiving = 1, runBlocking = 1, passBlocking = 1 });

            // OL
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.OL, injury = new PlayerInjury(), awareness = 10, speed = 1, runBlocking = 1, passBlocking = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.OL, injury = new PlayerInjury(), awareness = 9, speed = 1, runBlocking = 1, passBlocking = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.OL, injury = new PlayerInjury(), awareness = 8, speed = 1, runBlocking = 1, passBlocking = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.OL, injury = new PlayerInjury(), awareness = 7, speed = 1, runBlocking = 1, passBlocking = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.OL, injury = new PlayerInjury(), awareness = 6, speed = 1, runBlocking = 1, passBlocking = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.OL, injury = new PlayerInjury(), awareness = 5, speed = 1, runBlocking = 1, passBlocking = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.OL, injury = new PlayerInjury(), awareness = 4, speed = 1, runBlocking = 1, passBlocking = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.OL, injury = new PlayerInjury(), awareness = 3, speed = 1, runBlocking = 1, passBlocking = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.OL, injury = new PlayerInjury(), awareness = 2, speed = 1, runBlocking = 1, passBlocking = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.OL, injury = new PlayerInjury(), awareness = 2, speed = 1, runBlocking = 1, passBlocking = 1 });

            // DL
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DL, injury = new PlayerInjury(), awareness = 10, speed = 1, runDefense = 1, sack = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DL, injury = new PlayerInjury(), awareness = 9, speed = 1, runDefense = 1, sack = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DL, injury = new PlayerInjury(), awareness = 8, speed = 1, runDefense = 1, sack = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DL, injury = new PlayerInjury(), awareness = 7, speed = 1, runDefense = 1, sack = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DL, injury = new PlayerInjury(), awareness = 6, speed = 1, runDefense = 1, sack = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DL, injury = new PlayerInjury(), awareness = 5, speed = 1, runDefense = 1, sack = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DL, injury = new PlayerInjury(), awareness = 4, speed = 1, runDefense = 1, sack = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DL, injury = new PlayerInjury(), awareness = 3, speed = 1, runDefense = 1, sack = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DL, injury = new PlayerInjury(), awareness = 2, speed = 1, runDefense = 1, sack = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DL, injury = new PlayerInjury(), awareness = 2, speed = 1, runDefense = 1, sack = 1 });
            // LB
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.LB, injury = new PlayerInjury(), awareness = 10, speed = 1, runDefense = 1, passDefense = 1, sack = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.LB, injury = new PlayerInjury(), awareness = 9, speed = 1, runDefense = 1, passDefense = 1, sack = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.LB, injury = new PlayerInjury(), awareness = 8, speed = 1, runDefense = 1, passDefense = 1, sack = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.LB, injury = new PlayerInjury(), awareness = 7, speed = 1, runDefense = 1, passDefense = 1, sack = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.LB, injury = new PlayerInjury(), awareness = 6, speed = 1, runDefense = 1, passDefense = 1, sack = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.LB, injury = new PlayerInjury(), awareness = 5, speed = 1, runDefense = 1, passDefense = 1, sack = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.LB, injury = new PlayerInjury(), awareness = 4, speed = 1, runDefense = 1, passDefense = 1, sack = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.LB, injury = new PlayerInjury(), awareness = 3, speed = 1, runDefense = 1, passDefense = 1, sack = 1 });

            // DB 
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DB, injury = new PlayerInjury(), awareness = 10, speed = 1, runDefense = 1, passDefense = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DB, injury = new PlayerInjury(), awareness = 9, speed = 1, runDefense = 1, passDefense = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DB, injury = new PlayerInjury(), awareness = 8, speed = 1, runDefense = 1, passDefense = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DB, injury = new PlayerInjury(), awareness = 7, speed = 1, runDefense = 1, passDefense = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DB, injury = new PlayerInjury(), awareness = 6, speed = 1, runDefense = 1, passDefense = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DB, injury = new PlayerInjury(), awareness = 5, speed = 1, runDefense = 1, passDefense = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DB, injury = new PlayerInjury(), awareness = 4, speed = 1, runDefense = 1, passDefense = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DB, injury = new PlayerInjury(), awareness = 3, speed = 1, runDefense = 1, passDefense = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DB, injury = new PlayerInjury(), awareness = 2, speed = 1, runDefense = 1, passDefense = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.DB, injury = new PlayerInjury(), awareness = 1, speed = 1, runDefense = 1, passDefense = 1 });

            // K and P
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.K, injury = new PlayerInjury(), awareness = 10, distance = 1, accuracy = 1 });
            setting.rosterList["ARI"].addPlayer(new Player { position = PlayerType.P, injury = new PlayerInjury(), awareness = 10, distance = 1, accuracy = 1 });
        }
    }
}

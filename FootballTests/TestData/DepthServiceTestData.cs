﻿using Football.League.Coaches;
using Football.League.Injuries;
using Football.League.Players;
using Football.League.Teams;
using Football.Services;
using Football.Settings;
using Football.Services.Depth;
using Moq;
using System.Collections.Generic;

namespace FootballTests.TestData
{
    public class DepthServiceTestData
    {
        public DepthService GetDepthService(FootballSettingsService settingsService)
        {
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());
            PositionPriorityService pps = new PositionPriorityService(ptv, new StarterService(settingsService, ptv, new RosterService(settingsService)));            
            
            RosterService rosterService = new RosterService(settingsService);
            CapManagementService capService = new CapManagementService(settingsService, rosterService );
            DepthSign depthSign = new DepthSign( new SignPlayerService(rosterService, 
                                                                      new StarterService(settingsService, ptv, rosterService), 
                                                                      new TransactionService(rosterService,capService, settingsService), 
                                                                      settingsService),
                                                  capService);
            
            DepthService depthService = new DepthService(settingsService, pps, depthSign); 

            return depthService;
        }

        public FootballSettingsService GetSettingsServiceAddOneStarter()
        {
            FootballSettings setting = setting = FootballSettings.setSettings();
            setting.myTeam = "DAL";

            Team teamA = new Team("Arizona", "Cardinals", "ARI");
            teamA.Coach = new Coach();
            teamA.Coach.BaseOffense = BaseAll.TwoRB;
            teamA.Coach.BaseDefense = BaseAll.FourThree;

            Team teamC = new Team("Dallas", "Cowboys", "DAL");

            Team teamB = new Team("Free Agent", "", "FA");

            TeamList teamList = new TeamList();
            teamList.addTeam(teamA);
            teamList.addTeam(teamC);

            Player playerA = new Player();
            playerA.position = PlayerType.QB;
            playerA.injury = new PlayerInjury();

            Player playerB = new Player();
            playerB.position = PlayerType.QB;
            playerB.injury = new PlayerInjury();

            setting.rosterList.Add("ARI", new Roster(teamA));
            setting.rosterList.Add("FA", new Roster(teamB));
            setting.rosterList["FA"].addPlayer(playerA);
            setting.rosterList["FA"].addPlayer(playerB);

            setting.teams = teamList;
            setting.freeAgentsTeam = teamB;

            return new FootballSettingsService(setting);
        }

        public FootballSettingsService GetSettingsServiceTwoPlayersAddedToRoster()
        {
            FootballSettings setting = setting = FootballSettings.setSettings();
            setting.myTeam = "DAL";

            Team teamA = new Team("Arizona", "Cardinals", "ARI");
            teamA.Coach = new Coach();
            teamA.Coach.BaseOffense = BaseAll.TwoRB;
            teamA.Coach.BaseDefense = BaseAll.FourThree;

            Team teamC = new Team("Dallas", "Cowboys", "DAL");

            Team teamB = new Team("Free Agent", "", "FA");

            TeamList teamList = new TeamList();
            teamList.addTeam(teamA);
            teamList.addTeam(teamC);  

            Player playerA = new Player();
            playerA.position = PlayerType.QB;
            playerA.injury = new PlayerInjury();            

            Player playerB = new Player();
            playerB.position = PlayerType.QB;
            playerB.injury = new PlayerInjury();
           
            setting.rosterList.Add("ARI", new Roster(teamA));
            setting.rosterList.Add("FA", new Roster(teamB));
            setting.rosterList["FA"].addPlayer(playerA);
            setting.rosterList["FA"].addPlayer(playerB);

            setting.teams = teamList;
            setting.freeAgentsTeam = teamB;                                          
            
            return new FootballSettingsService(setting);
        }

        public FootballSettingsService GetSettingsServiceOnePlayersAddedToTwoRosters()
        {
            FootballSettings setting = setting = FootballSettings.setSettings();
            setting.myTeam = "DAL";

            Team teamA = new Team("Arizona", "Cardinals", "ARI");
            teamA.Coach = new Coach();
            teamA.Coach.BaseOffense = BaseAll.TwoRB;
            teamA.Coach.BaseDefense = BaseAll.FourThree;

            Team teamB = new Team("Baltimore", "Ravens", "BAL");
            teamB.Coach = new Coach();
            teamB.Coach.BaseOffense = BaseAll.TwoRB;
            teamB.Coach.BaseDefense = BaseAll.FourThree;

            Team teamC = new Team("Free Agent", "", "FA");

            Team teamD = new Team("Dallas", "Cowboys", "DAL");

            TeamList teamList = new TeamList();
            teamList.addTeam(teamA);
            teamList.addTeam(teamB);
            teamList.addTeam(teamD);

            Player playerA = new Player();
            playerA.position = PlayerType.QB;
            playerA.injury = new PlayerInjury();

            Player playerB = new Player();
            playerB.position = PlayerType.QB;
            playerB.injury = new PlayerInjury();

            Player playerC = new Player();
            playerC.position = PlayerType.QB;
            playerC.injury = new PlayerInjury();

            Player playerD = new Player();
            playerD.position = PlayerType.QB;
            playerD.injury = new PlayerInjury();

            setting.rosterList.Add("ARI", new Roster(teamA));
            setting.rosterList.Add("BAL", new Roster(teamB));
            setting.rosterList.Add("FA", new Roster(teamC));
            setting.rosterList["FA"].addPlayer(playerA);
            setting.rosterList["FA"].addPlayer(playerB);
            setting.rosterList["FA"].addPlayer(playerC);
            setting.rosterList["FA"].addPlayer(playerD);

            setting.teams = teamList;
            setting.freeAgentsTeam = teamC;
          
            return new FootballSettingsService(setting);
        }

        public FootballSettingsService GetSettingsServiceOnePlayersAddedToMyRoster()
        {
            FootballSettings setting = setting = FootballSettings.setSettings();
            setting.myTeam = "ARI";

            Team teamA = new Team("Arizona", "Cardinals", "ARI");
            teamA.Coach = new Coach();
            teamA.Coach.BaseOffense = BaseAll.TwoRB;
            teamA.Coach.BaseDefense = BaseAll.FourThree;
            
            Team teamB = new Team("Free Agent", "", "FA");

            TeamList teamList = new TeamList();
            teamList.addTeam(teamA);            

            Player playerA = new Player();
            playerA.position = PlayerType.QB;
            playerA.injury = new PlayerInjury();            

            Player playerB = new Player();
            playerB.position = PlayerType.QB;
            playerB.injury = new PlayerInjury();
           
            setting.rosterList.Add("ARI", new Roster(teamA));
            setting.rosterList.Add("FA", new Roster(teamB));
            setting.rosterList["FA"].addPlayer(playerA);
            setting.rosterList["FA"].addPlayer(playerB);

            setting.teams = teamList;
            setting.freeAgentsTeam = teamB;                                          
            
            return new FootballSettingsService(setting);
        }     
        
        public FootballSettingsService GetSettingsServiceCutOnePlayer()
        {
            FootballSettings setting = setting = FootballSettings.setSettings();
            setting.myTeam = "DAL";

            Team teamA = new Team("Arizona", "Cardinals", "ARI");
            teamA.Coach = new Coach();
            teamA.Coach.BaseOffense = BaseAll.TwoRB;
            teamA.Coach.BaseDefense = BaseAll.FourThree;

            Team teamC = new Team("Dallas", "Cowboys", "DAL");

            Team teamB = new Team("Free Agent", "", "FA");

            TeamList teamList = new TeamList();
            teamList.addTeam(teamA);
            teamList.addTeam(teamC);

            Player playerA = new Player();
            playerA.position = PlayerType.RB;
            playerA.injury = new PlayerInjury();

            Player playerB = new Player();
            playerB.position = PlayerType.RB;
            playerB.injury = new PlayerInjury();

            Player playerC = new Player();
            playerC.position = PlayerType.RB;
            playerC.injury = new PlayerInjury();

            Player playerD = new Player();
            playerD.position = PlayerType.RB;
            playerD.injury = new PlayerInjury();
            
            setting.rosterList.Add("ARI", new Roster(teamA));
            setting.rosterList.Add("FA", new Roster(teamB));

            setting.rosterList["FA"].addPlayer(playerA);
            setting.rosterList["FA"].addPlayer(playerB);
            setting.rosterList["FA"].addPlayer(playerC);
            setting.rosterList["FA"].addPlayer(playerD);

            CommonTestData.GetTeamRoster(setting);            

            setting.teams = teamList;
            setting.freeAgentsTeam = teamB;                                 
            
            return new FootballSettingsService(setting);
        }        
    }
}

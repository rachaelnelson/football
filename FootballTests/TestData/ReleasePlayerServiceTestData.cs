﻿using Football.Services.Starter;
using Football.Services;
using Football.Settings;
using Football.League.Players;
using System.Collections.Generic;
using Football.League.Teams;
using Football.League.Coaches;
using Football.League.Injuries;
using Football.UiClasses.ViewModel;
using Football.League.Salary;

namespace FootballTests.TestData
{
    public class ReleasePlayerServiceTestData
    {
        private Player player;

        public ReleasePlayerServiceTestData()
        {
            this.player = new Player()
            {
                id = 1,
                position = PlayerType.QB,
                injury = new PlayerInjury(),
                awareness = 5,
                accuracy = 4,
                distance = 4,
                firstName = "A",
                lastName = "A",
            };
        }

        public ReleasePlayerService TestReleasePlayerView(FootballSettings setting, FootballSettingsService settingsService, RosterService rosterService)
        {            
            setting.myTeam = "DAL";
            
            Team teamA = new Team("Arizona", "Cardinals", "ARI");
            teamA.Coach = new Coach();
            teamA.Coach.BaseOffense = BaseAll.TwoRB;
            teamA.Coach.BaseDefense = BaseAll.FourThree;

            Team teamC = new Team("Free Agent", "", "FA");

            TeamList teamList = new TeamList();
            teamList.addTeam(teamA);
            teamList.addTeam(teamC);

            Player playerA = GetPlayer(teamA);

            setting.rosterList.Add("ARI", new Roster(teamA));
            setting.rosterList.Add("FA", new Roster(teamC));
            setting.rosterList["ARI"].addPlayer(playerA);

            setting.teams = teamList;
            setting.freeAgentsTeam = teamC;            
            
            PlayerTypeValues ptv = new PlayerTypeValues(new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>());            
            CapManagementService capService = new CapManagementService(settingsService, rosterService);
            StarterService starterService = new StarterService(settingsService, ptv, rosterService);
            
            return new ReleasePlayerService(rosterService, starterService, new TransactionService(rosterService, capService, settingsService), settingsService);                  
        }

        private Player GetPlayer(Team teamA)
        {       
            player.team = teamA;
            player.contract = new PlayerContract(teamA, player, new int[] { 100000 });

            return player;
        }

        public ReleasePlayerView TestRelease_GetReleasePlayerView()
        {            
            return new ReleasePlayerView()
            {
                Id = 1,
                Position = PlayerType.QB.ToString(),                
                FirstName = "A",
                LastName = "A",                
                IsOnIR = false,
                TeamAbbr = "ARI"
            };
        }

        public Player TestRelease_GetReleasePlayer()
        {
            Team teamA = new Team("Arizona", "Cardinals", "ARI");

            return GetPlayer(teamA);
        }
    }
}

﻿using System.Collections.Generic;
using Football.League.Coaches;
using Football.League.Players;
using Football.League.Teams;
using Football.Services;
using Football.Settings;

namespace FootballTests.TestData
{
    public class StarterServiceTestData
    {
        public StarterService GetStarterService( BaseAll baseOffense, BaseAll baseDefense )
        {
            Team teamA = new Team("Arizona", "Cardinals", "ARI");
            teamA.Coach = new Coach();
            teamA.Coach.BaseOffense = baseOffense;
            teamA.Coach.BaseDefense = baseDefense;

            TeamList teamList = new TeamList();
            teamList.addTeam(teamA);

            FootballSettings setting = setting = FootballSettings.setSettings();            
            setting.rosterList.Add("ARI", new Roster(teamA));

            setting.teams = teamList;

            FootballSettingsService settingsService = new FootballSettingsService(setting);
            PlayerTypeValues ptv = new PlayerTypeValues(/*new List<PlayerType>() { PlayerType.QB }, new List<PlayerType>(), new List<PlayerType>()*/);
            RosterService rosterService = new RosterService(settingsService);

            return new StarterService(settingsService, ptv, rosterService);
        }       
    }
}

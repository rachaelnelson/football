﻿using System.Collections.Generic;
using Football.League.Conferences;
using Football.League.Playoffs;
using Football.League.Schedules;
using Football.League.Teams;
using Football.League.Teams.TeamRecord;
using Football.Sim.Games;

namespace FootballTests.TestData
{
    public class PlayoffTestData
    {
        public Schedule getHeadToHeadSchedule(Conference conference)
        {
            Schedule schedule = new Schedule();

            GameResult result = new GameResult();
            result.awayTeamScore = 10;
            result.homeTeamScore = 0;
            result.gamePlayed = true;

            ScheduleItem scheduleItem = new ScheduleItem();
            scheduleItem.homeTeam = conference.getTeamByAbbr("GB");
            scheduleItem.awayTeam = conference.getTeamByAbbr("CHI");
            scheduleItem.result = result;

            GameResult result2 = new GameResult();
            result2.awayTeamScore = 7;
            result2.homeTeamScore = 21;
            result2.gamePlayed = true;

            ScheduleItem scheduleItem2 = new ScheduleItem();
            scheduleItem2.awayTeam = conference.getTeamByAbbr("GB");
            scheduleItem2.homeTeam = conference.getTeamByAbbr("CHI");
            scheduleItem2.result = result2;

            schedule.addScheduleItem("1", scheduleItem);
            schedule.addScheduleItem("2", scheduleItem2);

            return schedule;

        }

        public Schedule getDivisionRecordSchedule(Conference conference)
        {
            Schedule schedule = new Schedule();

            GameResult result = new GameResult();
            result.awayTeamScore = 10;
            result.homeTeamScore = 0;
            result.gamePlayed = true;

            ScheduleItem scheduleItem = new ScheduleItem();
            scheduleItem.homeTeam = conference.getTeamByAbbr("GB"); // 0
            scheduleItem.awayTeam = conference.getTeamByAbbr("CHI"); // 10
            scheduleItem.result = result;

            GameResult result2 = new GameResult();
            result2.awayTeamScore = 7;
            result2.homeTeamScore = 21;
            result2.gamePlayed = true;

            ScheduleItem scheduleItem2 = new ScheduleItem();
            scheduleItem2.awayTeam = conference.getTeamByAbbr("MIN"); // 7
            scheduleItem2.homeTeam = conference.getTeamByAbbr("CHI"); // 21
            scheduleItem2.result = result2;

            GameResult result3 = new GameResult();
            result3.awayTeamScore = 10;
            result3.homeTeamScore = 7;
            result3.gamePlayed = true;

            ScheduleItem scheduleItem3 = new ScheduleItem();
            scheduleItem3.awayTeam = conference.getTeamByAbbr("MIN"); // 10
            scheduleItem3.homeTeam = conference.getTeamByAbbr("GB"); // 7
            scheduleItem3.result = result3;

            schedule.addScheduleItem("1", scheduleItem);
            schedule.addScheduleItem("2", scheduleItem2);
            schedule.addScheduleItem("3", scheduleItem3);

            return schedule;
        }

        // Set Chicago and GB teams to tie so it hits Head to Head       
        public Conference getConference()
        {
            List<Division> divisions = new List<Division>();
            List<Team> nfcEast = new List<Team>();
            List<Team> nfcNorth = new List<Team>();
            List<Team> nfcSouth = new List<Team>();
            List<Team> nfcWest = new List<Team>();

            nfcEast.Add(new Team("Philadelphia", "Eagles", "PHI"));
            nfcEast.Add(new Team("Dallas", "Cowboys", "DAL"));
            nfcEast.Add(new Team("Washington", "Redskins", "WAS"));
            nfcEast.Add(new Team("New York", "Giants", "NYG"));

            divisions.Add(new Division("NFC EAST", nfcEast));

            nfcNorth.Add(new Team("Green Bay", "Packers", "GB"));
            nfcNorth.Add(new Team("Chicago", "Bears", "CHI"));
            nfcNorth.Add(new Team("Minnesota", "Vikings", "MIN"));
            nfcNorth.Add(new Team("Detroit", "Lions", "DET"));

            divisions.Add(new Division("NFC North", nfcNorth));

            nfcSouth.Add(new Team("New Orleans", "Saints", "NO"));
            nfcSouth.Add(new Team("Carolina", "Panthers", "CAR"));
            nfcSouth.Add(new Team("Atlanta", "Falcons", "ATL"));
            nfcSouth.Add(new Team("Tampa Bay", "Buccaneers", "TB"));

            divisions.Add(new Division("NFC South", nfcSouth));

            nfcWest.Add(new Team("San Francisco", "49ers", "SF"));
            nfcWest.Add(new Team("Seattle", "Seahawks", "SEA"));
            nfcWest.Add(new Team("St Louis", "Rams", "STL"));
            nfcWest.Add(new Team("Arizona", "Cardinals", "ARI"));

            divisions.Add(new Division("NFC West", nfcWest));

            return new Conference("NFC", divisions);
        }

        /**
         *  Contains the records for the BestRecord test
         */
        public TeamRecordList getBestRecordTeamList(Conference conference)
        {
            TeamRecordList bestRecordList = new TeamRecordList();
            bestRecordList.AddNewRecord(conference.getTeamByAbbr("PHI"), new TeamRecord(null, null, new Record(13, 3, 0)));
            bestRecordList.AddNewRecord(conference.getTeamByAbbr("DAL"), new TeamRecord(null, null, new Record(10, 6, 0)));
            bestRecordList.AddNewRecord(conference.getTeamByAbbr("WAS"), new TeamRecord(null, null, new Record(6, 10, 0)));
            bestRecordList.AddNewRecord(conference.getTeamByAbbr("NYG"), new TeamRecord(null, null, new Record(4, 12, 0)));

            bestRecordList.AddNewRecord(conference.getTeamByAbbr("GB"), new TeamRecord(null, null, new Record(12, 3, 1)));
            bestRecordList.AddNewRecord(conference.getTeamByAbbr("CHI"), new TeamRecord(null, null, new Record(9, 7, 0)));
            bestRecordList.AddNewRecord(conference.getTeamByAbbr("MIN"), new TeamRecord(null, null, new Record(6, 10, 0)));
            bestRecordList.AddNewRecord(conference.getTeamByAbbr("DET"), new TeamRecord(null, null, new Record(4, 12, 0)));

            bestRecordList.AddNewRecord(conference.getTeamByAbbr("NO"), new TeamRecord(null, null, new Record(11, 5, 0)));
            bestRecordList.AddNewRecord(conference.getTeamByAbbr("CAR"), new TeamRecord(null, null, new Record(8, 8, 0)));
            bestRecordList.AddNewRecord(conference.getTeamByAbbr("ATL"), new TeamRecord(null, null, new Record(6, 10, 0)));
            bestRecordList.AddNewRecord(conference.getTeamByAbbr("TB"), new TeamRecord(null, null, new Record(4, 12, 0)));

            bestRecordList.AddNewRecord(conference.getTeamByAbbr("SF"), new TeamRecord(null, null, new Record(14, 2, 0)));
            bestRecordList.AddNewRecord(conference.getTeamByAbbr("SEA"), new TeamRecord(null, null, new Record(7, 9, 0)));
            bestRecordList.AddNewRecord(conference.getTeamByAbbr("STL"), new TeamRecord(null, null, new Record(6, 10, 0)));
            bestRecordList.AddNewRecord(conference.getTeamByAbbr("ARI"), new TeamRecord(null, null, new Record(4, 12, 0)));

            return bestRecordList;

        }

        /**
         * Expected BestRecords results
         */
        public List<PlayoffTeam> getBestRecordExpectedTeams(Conference conference)
        {
            List<PlayoffTeam> bestRecordExpected = new List<PlayoffTeam>();

            bestRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("SF"), 1));
            bestRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("PHI"), 2));
            bestRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("GB"), 3));
            bestRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("NO"), 4));
            bestRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("DAL"), 5));
            bestRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("CHI"), 6));

            return bestRecordExpected;
        }

        /**
         *  Overall record is tied for CHI and GB           
         */
        public TeamRecordList getHeadToHeadRecordTeamList(Conference conference)
        {
            TeamRecordList headToHeadRecordList = new TeamRecordList();
            headToHeadRecordList.AddNewRecord(conference.getTeamByAbbr("PHI"), new TeamRecord(null, null, new Record(13, 3, 0)));
            headToHeadRecordList.AddNewRecord(conference.getTeamByAbbr("DAL"), new TeamRecord(null, null, new Record(9, 7, 0)));
            headToHeadRecordList.AddNewRecord(conference.getTeamByAbbr("WAS"), new TeamRecord(null, null, new Record(6, 10, 0)));
            headToHeadRecordList.AddNewRecord(conference.getTeamByAbbr("NYG"), new TeamRecord(null, null, new Record(4, 12, 0)));

            headToHeadRecordList.AddNewRecord(conference.getTeamByAbbr("GB"), new TeamRecord(null, null, new Record(10, 5, 1)));
            headToHeadRecordList.AddNewRecord(conference.getTeamByAbbr("CHI"), new TeamRecord(null, null, new Record(10, 5, 1)));
            headToHeadRecordList.AddNewRecord(conference.getTeamByAbbr("MIN"), new TeamRecord(null, null, new Record(6, 10, 0)));
            headToHeadRecordList.AddNewRecord(conference.getTeamByAbbr("DET"), new TeamRecord(null, null, new Record(4, 12, 0)));

            headToHeadRecordList.AddNewRecord(conference.getTeamByAbbr("NO"), new TeamRecord(null, null, new Record(11, 5, 0)));
            headToHeadRecordList.AddNewRecord(conference.getTeamByAbbr("CAR"), new TeamRecord(null, null, new Record(8, 8, 0)));
            headToHeadRecordList.AddNewRecord(conference.getTeamByAbbr("ATL"), new TeamRecord(null, null, new Record(6, 10, 0)));
            headToHeadRecordList.AddNewRecord(conference.getTeamByAbbr("TB"), new TeamRecord(null, null, new Record(4, 12, 0)));

            headToHeadRecordList.AddNewRecord(conference.getTeamByAbbr("SF"), new TeamRecord(null, null, new Record(14, 2, 0)));
            headToHeadRecordList.AddNewRecord(conference.getTeamByAbbr("SEA"), new TeamRecord(null, null, new Record(7, 9, 0)));
            headToHeadRecordList.AddNewRecord(conference.getTeamByAbbr("STL"), new TeamRecord(null, null, new Record(6, 10, 0)));
            headToHeadRecordList.AddNewRecord(conference.getTeamByAbbr("ARI"), new TeamRecord(null, null, new Record(4, 12, 0)));

            return headToHeadRecordList;
        }

        public List<PlayoffTeam> getHeadToHeadExpectedTeams(Conference conference)
        {
            List<PlayoffTeam> headToHeadRecordExpected = new List<PlayoffTeam>();

            headToHeadRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("SF"), 1));
            headToHeadRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("PHI"), 2));
            headToHeadRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("NO"), 3));
            headToHeadRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("CHI"), 4));
            headToHeadRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("GB"), 5));
            headToHeadRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("DAL"), 6));

            return headToHeadRecordExpected;
        }

        /**
         *  Three teams tied in NFC North so it hits the Division record handler         
         */
        public TeamRecordList getDivisionRecordTeamList(Conference conference)
        {
            TeamRecordList divisionRecordList = new TeamRecordList();
            divisionRecordList.AddNewRecord(conference.getTeamByAbbr("PHI"), new TeamRecord(null, new Record(4, 0, 0), new Record(13, 3, 0)));
            divisionRecordList.AddNewRecord(conference.getTeamByAbbr("DAL"), new TeamRecord(null, new Record(3, 1, 0), new Record(10, 4, 2)));
            divisionRecordList.AddNewRecord(conference.getTeamByAbbr("WAS"), new TeamRecord(null, new Record(2, 2, 0), new Record(6, 10, 0)));
            divisionRecordList.AddNewRecord(conference.getTeamByAbbr("NYG"), new TeamRecord(null, new Record(0, 4, 0), new Record(4, 12, 0)));

            divisionRecordList.AddNewRecord(conference.getTeamByAbbr("GB"), new TeamRecord(null, new Record(2, 2, 0), new Record(10, 5, 1)));
            divisionRecordList.AddNewRecord(conference.getTeamByAbbr("CHI"), new TeamRecord(null, new Record(3, 1, 0), new Record(10, 5, 1)));
            divisionRecordList.AddNewRecord(conference.getTeamByAbbr("MIN"), new TeamRecord(null, new Record(2, 1, 1), new Record(10, 5, 1)));
            divisionRecordList.AddNewRecord(conference.getTeamByAbbr("DET"), new TeamRecord(null, new Record(0, 4, 0), new Record(4, 12, 0)));

            divisionRecordList.AddNewRecord(conference.getTeamByAbbr("NO"), new TeamRecord(null, new Record(4, 0, 0), new Record(11, 5, 0)));
            divisionRecordList.AddNewRecord(conference.getTeamByAbbr("CAR"), new TeamRecord(null, new Record(3, 1, 0), new Record(8, 8, 0)));
            divisionRecordList.AddNewRecord(conference.getTeamByAbbr("ATL"), new TeamRecord(null, new Record(2, 2, 0), new Record(6, 10, 0)));
            divisionRecordList.AddNewRecord(conference.getTeamByAbbr("TB"), new TeamRecord(null, new Record(0, 4, 0), new Record(4, 12, 0)));

            divisionRecordList.AddNewRecord(conference.getTeamByAbbr("SF"), new TeamRecord(null, new Record(4, 0, 0), new Record(14, 2, 0)));
            divisionRecordList.AddNewRecord(conference.getTeamByAbbr("SEA"), new TeamRecord(null, new Record(3, 1, 0), new Record(7, 9, 0)));
            divisionRecordList.AddNewRecord(conference.getTeamByAbbr("STL"), new TeamRecord(null, new Record(2, 2, 0), new Record(6, 10, 0)));
            divisionRecordList.AddNewRecord(conference.getTeamByAbbr("ARI"), new TeamRecord(null, new Record(0, 4, 0), new Record(4, 12, 0)));

            return divisionRecordList;
        }

        public List<PlayoffTeam> getDivisionRecordExpectedTeams(Conference conference)
        {
            List<PlayoffTeam> divisionRecordExpected = new List<PlayoffTeam>();

            divisionRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("SF"), 1));
            divisionRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("PHI"), 2));
            divisionRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("NO"), 3));
            divisionRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("CHI"), 4));
            divisionRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("DAL"), 6));
            divisionRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("MIN"), 5));

            return divisionRecordExpected;
        }

        /**
         *  Three teams tied for wild card so it hits the conference handler         
         */
        public TeamRecordList getConferenceRecordTeamList(Conference conference)
        {
            TeamRecordList conferenceRecordList = new TeamRecordList();
            conferenceRecordList.AddNewRecord(conference.getTeamByAbbr("PHI"), new TeamRecord(new Record(4, 0, 0), new Record(4, 0, 0), new Record(13, 3, 0)));
            conferenceRecordList.AddNewRecord(conference.getTeamByAbbr("DAL"), new TeamRecord(new Record(1, 3, 0), new Record(3, 1, 0), new Record(10, 6, 0)));
            conferenceRecordList.AddNewRecord(conference.getTeamByAbbr("WAS"), new TeamRecord(new Record(2, 2, 0), new Record(2, 2, 0), new Record(6, 10, 0)));
            conferenceRecordList.AddNewRecord(conference.getTeamByAbbr("NYG"), new TeamRecord(new Record(0, 4, 0), new Record(0, 4, 0), new Record(4, 12, 0)));

            conferenceRecordList.AddNewRecord(conference.getTeamByAbbr("GB"), new TeamRecord(new Record(2, 2, 0), new Record(2, 2, 0), new Record(15, 1, 0)));
            conferenceRecordList.AddNewRecord(conference.getTeamByAbbr("CHI"), new TeamRecord(new Record(3, 1, 0), new Record(3, 1, 0), new Record(10, 6, 0)));
            conferenceRecordList.AddNewRecord(conference.getTeamByAbbr("MIN"), new TeamRecord(new Record(2, 1, 1), new Record(2, 1, 1), new Record(9, 7, 0)));
            conferenceRecordList.AddNewRecord(conference.getTeamByAbbr("DET"), new TeamRecord(new Record(0, 4, 0), new Record(0, 4, 0), new Record(4, 12, 0)));

            conferenceRecordList.AddNewRecord(conference.getTeamByAbbr("NO"), new TeamRecord(new Record(4, 0, 0), new Record(4, 0, 0), new Record(11, 5, 0)));
            conferenceRecordList.AddNewRecord(conference.getTeamByAbbr("CAR"), new TeamRecord(new Record(2, 2, 0), new Record(3, 1, 0), new Record(10, 6, 0)));
            conferenceRecordList.AddNewRecord(conference.getTeamByAbbr("ATL"), new TeamRecord(new Record(2, 2, 0), new Record(2, 2, 0), new Record(6, 10, 0)));
            conferenceRecordList.AddNewRecord(conference.getTeamByAbbr("TB"), new TeamRecord(new Record(0, 4, 0), new Record(0, 4, 0), new Record(4, 12, 0)));

            conferenceRecordList.AddNewRecord(conference.getTeamByAbbr("SF"), new TeamRecord(new Record(4, 0, 0), new Record(4, 0, 0), new Record(14, 2, 0)));
            conferenceRecordList.AddNewRecord(conference.getTeamByAbbr("SEA"), new TeamRecord(new Record(3, 1, 0), new Record(3, 1, 0), new Record(7, 9, 0)));
            conferenceRecordList.AddNewRecord(conference.getTeamByAbbr("STL"), new TeamRecord(new Record(2, 2, 0), new Record(2, 2, 0), new Record(6, 10, 0)));
            conferenceRecordList.AddNewRecord(conference.getTeamByAbbr("ARI"), new TeamRecord(new Record(0, 4, 0), new Record(0, 4, 0), new Record(4, 12, 0)));

            return conferenceRecordList;
        }

        public List<PlayoffTeam> getConferenceRecordExpectedTeams(Conference conference)
        {
            List<PlayoffTeam> conferenceRecordExpected = new List<PlayoffTeam>();

            conferenceRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("GB"), 1));
            conferenceRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("SF"), 2));
            conferenceRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("PHI"), 3));
            conferenceRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("NO"), 4));
            conferenceRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("CHI"), 5));
            conferenceRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("CAR"), 6));

            return conferenceRecordExpected;
        }

        /**
        *  DAL, CHI, SEA have same record.  Need same common games.        
        */
        public TeamRecordList getCommonTeamRecordList(Conference conference)
        {
            TeamRecordList commonTeamRecordList = new TeamRecordList();
            commonTeamRecordList.AddNewRecord(conference.getTeamByAbbr("PHI"), new TeamRecord(new Record(4, 0, 0), new Record(4, 0, 0), new Record(13, 3, 0)));
            commonTeamRecordList.AddNewRecord(conference.getTeamByAbbr("DAL"), new TeamRecord(new Record(1, 3, 0), new Record(3, 1, 0), new Record(10, 6, 0)));
            commonTeamRecordList.AddNewRecord(conference.getTeamByAbbr("WAS"), new TeamRecord(new Record(2, 2, 0), new Record(2, 2, 0), new Record(6, 10, 0)));
            commonTeamRecordList.AddNewRecord(conference.getTeamByAbbr("NYG"), new TeamRecord(new Record(0, 4, 0), new Record(0, 4, 0), new Record(4, 12, 0)));

            commonTeamRecordList.AddNewRecord(conference.getTeamByAbbr("GB"), new TeamRecord(new Record(2, 2, 0), new Record(2, 2, 0), new Record(10, 5, 1)));
            commonTeamRecordList.AddNewRecord(conference.getTeamByAbbr("CHI"), new TeamRecord(new Record(3, 1, 0), new Record(3, 1, 0), new Record(10, 6, 0)));
            commonTeamRecordList.AddNewRecord(conference.getTeamByAbbr("MIN"), new TeamRecord(new Record(2, 1, 1), new Record(2, 1, 1), new Record(9, 7, 0)));
            commonTeamRecordList.AddNewRecord(conference.getTeamByAbbr("DET"), new TeamRecord(new Record(0, 4, 0), new Record(0, 4, 0), new Record(4, 12, 0)));

            commonTeamRecordList.AddNewRecord(conference.getTeamByAbbr("NO"), new TeamRecord(new Record(4, 0, 0), new Record(4, 0, 0), new Record(11, 5, 0)));
            commonTeamRecordList.AddNewRecord(conference.getTeamByAbbr("CAR"), new TeamRecord(new Record(2, 2, 0), new Record(3, 1, 0), new Record(7, 9, 0)));
            commonTeamRecordList.AddNewRecord(conference.getTeamByAbbr("ATL"), new TeamRecord(new Record(2, 2, 0), new Record(2, 2, 0), new Record(6, 10, 0)));
            commonTeamRecordList.AddNewRecord(conference.getTeamByAbbr("TB"), new TeamRecord(new Record(0, 4, 0), new Record(0, 4, 0), new Record(4, 12, 0)));

            commonTeamRecordList.AddNewRecord(conference.getTeamByAbbr("SF"), new TeamRecord(new Record(4, 0, 0), new Record(4, 0, 0), new Record(14, 2, 0)));
            commonTeamRecordList.AddNewRecord(conference.getTeamByAbbr("SEA"), new TeamRecord(new Record(3, 1, 0), new Record(3, 1, 0), new Record(10, 6, 0)));
            commonTeamRecordList.AddNewRecord(conference.getTeamByAbbr("STL"), new TeamRecord(new Record(2, 2, 0), new Record(2, 2, 0), new Record(6, 10, 0)));
            commonTeamRecordList.AddNewRecord(conference.getTeamByAbbr("ARI"), new TeamRecord(new Record(0, 4, 0), new Record(0, 4, 0), new Record(4, 12, 0)));

            return commonTeamRecordList;
        }

        public List<PlayoffTeam> getCommonTeamExpectedTeams(Conference conference)
        {
            List<PlayoffTeam> conferenceRecordExpected = new List<PlayoffTeam>();

            conferenceRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("SF"), 1));
            conferenceRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("PHI"), 2));
            conferenceRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("NO"), 3));
            conferenceRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("GB"), 4));
            conferenceRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("CHI"), 5));
            conferenceRecordExpected.Add(new PlayoffTeam(conference.getTeamByAbbr("SEA"), 6));

            return conferenceRecordExpected;
        }

        public Schedule getCommonTeamSchedule(Conference conference)
        {
            Schedule schedule = new Schedule();

            GameResult result = new GameResult();
            result.awayTeamScore = 10;
            result.homeTeamScore = 0;
            result.gamePlayed = true;

            ScheduleItem scheduleItem = new ScheduleItem();
            scheduleItem.homeTeam = conference.getTeamByAbbr("DAL"); // 0
            scheduleItem.awayTeam = conference.getTeamByAbbr("ARI"); // 10
            scheduleItem.result = result;

            GameResult result2 = new GameResult();
            result2.awayTeamScore = 7;
            result2.homeTeamScore = 21;
            result2.gamePlayed = true;

            ScheduleItem scheduleItem2 = new ScheduleItem();
            scheduleItem2.awayTeam = conference.getTeamByAbbr("ARI"); // 7
            scheduleItem2.homeTeam = conference.getTeamByAbbr("CHI"); // 21
            scheduleItem2.result = result2;

            GameResult result3 = new GameResult();
            result3.awayTeamScore = 10;
            result3.homeTeamScore = 7;
            result3.gamePlayed = true;

            ScheduleItem scheduleItem3 = new ScheduleItem();
            scheduleItem3.awayTeam = conference.getTeamByAbbr("ARI"); // 10
            scheduleItem3.homeTeam = conference.getTeamByAbbr("SEA"); // 7
            scheduleItem3.result = result3;

            GameResult result4 = new GameResult();
            result4.awayTeamScore = 14;
            result4.homeTeamScore = 7;
            result4.gamePlayed = true;

            ScheduleItem scheduleItem4 = new ScheduleItem();
            scheduleItem4.awayTeam = conference.getTeamByAbbr("SEA"); // 14
            scheduleItem4.homeTeam = conference.getTeamByAbbr("ARI"); // 7
            scheduleItem4.result = result4;

            schedule.addScheduleItem("1", scheduleItem);
            schedule.addScheduleItem("2", scheduleItem2);
            schedule.addScheduleItem("3", scheduleItem3);
            schedule.addScheduleItem("4", scheduleItem4);

            return schedule;
        }
    }
}

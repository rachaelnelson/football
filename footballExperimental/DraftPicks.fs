﻿module DraftPicks

type DraftPick = { 
    id: string
    year: int
    round: int
    pick: int
    originalTeam: string
    holdingTeam: string   
}

type YearlyDraft = {
    team: string
    draftPicks: list<DraftPick> 
}
     
let NewDraftYear(newYear, teams) = 
      [ for t in teams do 
         for r in 1 .. 7 do
             yield {
                 id =  String.concat ":" [newYear.ToString(); r.ToString(); t] 
                 year = newYear
                 round = r
                 originalTeam = t
                 holdingTeam = t
                 pick = 0
             } ]
 
let rec AddNewDraftYear( newYear, teams, existingList, increment) = 
    match increment with
    | 0 -> existingList
    | _ -> let newYearList = NewDraftYear( newYear, teams )
           newYearList @ AddNewDraftYear( newYear + 1, teams, existingList, increment - 1)

// i'm sure there's a better way to do this sort code....
// got it from here:  http://www.trelford.com/blog/post/Sorted-with-F-custom-operators.aspx
// this one is slightly better:  using compare stuff, which i still don't totally understand...
// http://diditwith.net/2008/03/14/WhyILoveFARefactoringTale.aspx
// Until I figure it out, the c# code and order the list with linq...lame i know
let GetPicksByTeam( team, picklist ) =         
    List.filter( fun ( pick ) -> pick.holdingTeam = team ) picklist                       
        
let GetPicksByYear( year, picklist )=             
    List.filter( fun ( pick ) -> pick.year = year ) picklist

let GetPickById( id, picklist ) = 
    List.find( fun ( pick ) -> pick.id = id ) picklist

let rec UpdateHoldingTeam( newTeam, picksToUpdate, picklist ) =
    match picklist with
    | [] -> []
    | head::rest -> match List.exists( fun elem -> elem = head ) picksToUpdate with
                    | true -> { head with holdingTeam = newTeam } :: UpdateHoldingTeam( newTeam, picksToUpdate, rest)
                    | false -> head :: UpdateHoldingTeam( newTeam, picksToUpdate, rest) 
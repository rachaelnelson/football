-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 16, 2012 at 06:54 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `footballstats`
--

-- --------------------------------------------------------

--
-- Table structure for table `rushingstats`
--

CREATE TABLE IF NOT EXISTS `rushingstats` (
  `Team` varchar(3) DEFAULT NULL,
  `Name` varchar(16) DEFAULT NULL,
  `Position` varchar(2) DEFAULT NULL,
  `Attempts` int(3) DEFAULT NULL,
  `Yards` int(4) DEFAULT NULL,
  `TDS` int(2) DEFAULT NULL,
  `Year` year(4) DEFAULT NULL,
  KEY `Team` (`Team`),
  KEY `Year` (`Year`),
  KEY `Yards` (`Yards`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rushingstats`
--

INSERT INTO `rushingstats` (`Team`, `Name`, `Position`, `Attempts`, `Yards`, `TDS`, `Year`) VALUES
('BAL', 'Rice', 'RB', 307, 1220, 5, 2010),
('BAL', 'McGahee', 'RB', 100, 380, 5, 2010),
('BAL', 'McClain', 'RB', 28, 85, 0, 2010),
('BAL', 'Flacco', 'QB', 43, 84, 1, 2010),
('BUF', 'Jackson', 'RB', 222, 927, 5, 2010),
('BUF', 'Spiller', 'RB', 74, 283, 0, 2010),
('BUF', 'Fitzpatrick', 'QB', 40, 269, 0, 2010),
('BUF', 'Lynch', 'RB', 37, 164, 0, 2010),
('CIN', 'Benson', 'RB', 321, 1111, 7, 2010),
('CIN', 'Scott', 'RB', 61, 299, 1, 2010),
('CIN', 'Palmer', 'QB', 32, 50, 0, 2010),
('CLE', 'Hillis', 'RB', 270, 1177, 11, 2010),
('CLE', 'McCoy', 'QB', 28, 136, 1, 2010),
('CLE', 'Harrison', 'RB', 31, 91, 0, 2010),
('CLE', 'Bell', 'RB', 31, 71, 0, 2010),
('DEN', 'Moreno', 'RB', 182, 779, 5, 2010),
('DEN', 'Tebow', 'QB', 43, 227, 6, 2010),
('DEN', 'Ball', 'RB', 41, 158, 0, 2010),
('DEN', 'Buckhalter', 'RB', 59, 147, 2, 2010),
('DEN', 'Orton', 'QB', 22, 98, 0, 2010),
('DEN', 'Maroney', 'RB', 36, 74, 0, 2010),
('HOU', 'Foster', 'RB', 327, 1616, 16, 2010),
('HOU', 'Ward', 'RB', 50, 315, 4, 2010),
('HOU', 'Slaton', 'RB', 19, 93, 0, 2010),
('HOU', 'Schaub', 'QB', 22, 28, 0, 2010),
('IND', 'Brown', 'RB', 129, 497, 2, 2010),
('IND', 'Addai', 'RB', 116, 495, 4, 2010),
('IND', 'Hart', 'RB', 43, 185, 1, 2010),
('IND', 'Rhodes', 'RB', 37, 172, 0, 2010),
('IND', 'James', 'RB', 46, 112, 6, 2010),
('IND', 'Manning', 'QB', 18, 18, 0, 2010),
('JAC', 'Jones-Drew', 'RB', 299, 1324, 5, 2010),
('JAC', 'Jennings', 'RB', 84, 459, 4, 2010),
('JAC', 'Garrard', 'QB', 66, 279, 5, 2010),
('JAC', 'Karim', 'RB', 35, 160, 0, 2010),
('KC', 'Charles', 'RB', 230, 1467, 5, 2010),
('KC', 'Jones', 'RB', 245, 896, 6, 2010),
('KC', 'Cassel', 'QB', 33, 125, 0, 2010),
('KC', 'McCluster', 'RB', 18, 71, 0, 2010),
('KC', 'Battle', 'RB', 20, 50, 1, 2010),
('MIA', 'Brown', 'RB', 200, 734, 5, 2010),
('MIA', 'Williams', 'RB', 159, 673, 2, 2010),
('MIA', 'Thigpen', 'QB', 13, 73, 0, 2010),
('MIA', 'Polite', 'RB', 26, 62, 1, 2010),
('MIA', 'Henne', 'QB', 35, 52, 0, 2010),
('NE', 'Green-Ellis', 'RB', 229, 1008, 13, 2010),
('NE', 'Woodhead', 'RB', 97, 547, 5, 2010),
('NE', 'Taylor', 'RB', 43, 155, 0, 2010),
('NE', 'Morris', 'RB', 20, 56, 0, 2010),
('NE', 'Brady', 'QB', 31, 30, 1, 2010),
('NYJ', 'Tomlinson', 'RB', 219, 914, 6, 2010),
('NYJ', 'Greene', 'RB', 185, 766, 2, 2010),
('NYJ', 'Smith', 'WR', 38, 299, 1, 2010),
('NYJ', 'McKnight', 'RB', 39, 189, 0, 2010),
('NYJ', 'Sanchez', 'QB', 30, 105, 3, 2010),
('OAK', 'McFadden', 'RB', 223, 1157, 7, 2010),
('OAK', 'Bush', 'RB', 158, 655, 8, 2010),
('OAK', 'Campbell', 'QB', 47, 222, 1, 2010),
('OAK', 'Ford', 'WR', 10, 155, 2, 2010),
('OAK', 'Reece', 'RB', 30, 122, 1, 2010),
('OAK', 'Gradkowski', 'QB', 12, 41, 0, 2010),
('PIT', 'Mendenhall', 'RB', 324, 1273, 13, 2010),
('PIT', 'Redman', 'RB', 52, 247, 0, 2010),
('PIT', 'Roethlisberger', 'QB', 34, 176, 2, 2010),
('PIT', 'Moore', 'RB', 33, 99, 0, 2010),
('SD', 'Tolbert', 'RB', 182, 735, 11, 2010),
('SD', 'Matthews', 'RB', 158, 678, 7, 2010),
('SD', 'Sproles', 'RB', 50, 267, 0, 2010),
('SD', 'Hester', 'RB', 26, 60, 0, 2010),
('SD', 'Rivers', 'QB', 29, 52, 0, 2010),
('TEN', 'Johnson', 'RB', 316, 1364, 11, 2010),
('TEN', 'Ringer', 'RB', 51, 239, 2, 2010),
('TEN', 'Young', 'QB', 25, 125, 0, 2010),
('ARI', 'Hightower', 'RB', 153, 736, 5, 2010),
('ARI', 'Wells', 'RB', 116, 397, 2, 2010),
('ARI', 'Stephens-Howling', 'RB', 23, 113, 1, 2010),
('ARI', 'Skelton', 'QB', 10, 49, 0, 2010),
('ATL', 'Turner', 'RB', 334, 1371, 12, 2010),
('ATL', 'Snelling', 'RB', 87, 324, 2, 2010),
('ATL', 'Ryan', 'QB', 46, 122, 0, 2010),
('CAR', 'Stewart', 'RB', 178, 770, 2, 2010),
('CAR', 'Goodson', 'RB', 103, 452, 3, 2010),
('CAR', 'Williams', 'RB', 87, 361, 1, 2010),
('CAR', 'Clauson', 'QB', 23, 57, 0, 2010),
('CHI', 'Forte', 'RB', 237, 1069, 6, 2010),
('CHI', 'Taylor', 'RB', 112, 267, 3, 2010),
('CHI', 'Cutler', 'QB', 50, 232, 1, 2010),
('DAL', 'Jones', 'RB', 185, 800, 1, 2010),
('DAL', 'Barber', 'RB', 113, 374, 4, 2010),
('DAL', 'Choice', 'RB', 66, 243, 3, 2010),
('DAL', 'Kitna', 'QB', 31, 147, 1, 2010),
('DET', 'Best', 'RB', 171, 555, 4, 2010),
('DET', 'Morris', 'RB', 90, 336, 5, 2010),
('DET', 'Smith', 'RB', 34, 133, 0, 2010),
('DET', 'Hill', 'QB', 22, 123, 0, 2010),
('DET', 'Stanton', 'QB', 18, 113, 1, 2010),
('DET', 'Logan', 'RB', 15, 95, 0, 2010),
('GB', 'Jackson', 'RB', 190, 703, 3, 2010),
('GB', 'Rodgers', 'QB', 64, 356, 4, 2010),
('GB', 'Kuhn', 'RB', 84, 281, 4, 2010),
('GB', 'Starks', 'RB', 29, 101, 0, 2010),
('GB', 'Nance', 'RB', 36, 95, 0, 2010),
('MIN', 'Peterson', 'RB', 283, 1298, 12, 2010),
('MIN', 'Gerhart', 'RB', 81, 322, 1, 2010),
('MIN', 'Webb', 'QB', 18, 120, 2, 2010),
('NO', 'Ivory', 'RB', 137, 716, 5, 2010),
('NO', 'Thomas', 'RB', 83, 269, 2, 2010),
('NO', 'Jones', 'RB', 48, 193, 0, 2010),
('NO', 'Betts', 'RB', 45, 150, 2, 2010),
('NO', 'Bush', 'RB', 36, 150, 0, 2010),
('NYG', 'Bradshaw', 'RB', 276, 1235, 8, 2010),
('NYG', 'Jacobs', 'RB', 147, 823, 9, 2010),
('NYG', 'Ware', 'RB', 20, 73, 0, 2010),
('NYG', 'Manning', 'QB', 32, 70, 0, 2010),
('PHI', 'McCoy', 'RB', 207, 1080, 7, 2010),
('PHI', 'Vick', 'QB', 100, 676, 9, 2010),
('PHI', 'Harrison', 'RB', 40, 239, 1, 2010),
('STL', 'Jackson', 'RB', 330, 1241, 6, 2010),
('STL', 'Darby', 'RB', 34, 107, 2, 2010),
('STL', 'Bradford', 'QB', 27, 63, 1, 2010),
('SF', 'Gore', 'RB', 203, 853, 3, 2010),
('SF', 'Westbrook', 'RB', 77, 340, 4, 2010),
('SF', 'Dixon', 'RB', 70, 237, 2, 2010),
('SF', 'Smith', 'QB', 23, 121, 1, 2010),
('SF', 'Smith', 'QB', 18, 60, 0, 2010),
('SEA', 'Lynch', 'RB', 165, 573, 6, 2010),
('SEA', 'Forsett', 'RB', 118, 523, 2, 2010),
('SEA', 'Washington', 'RB', 27, 100, 1, 2010),
('SEA', 'Hasselbeck', 'QB', 23, 60, 3, 2010),
('SEA', 'Whitehurst', 'QB', 20, 43, 1, 2010),
('TB', 'Blount', 'RB', 201, 1007, 6, 2010),
('TB', 'Williams', 'RB', 125, 437, 2, 2010),
('TB', 'Freeman', 'QB', 68, 364, 0, 2010),
('TB', 'Graham', 'RB', 20, 99, 1, 2010),
('WAS', 'Torain', 'RB', 164, 742, 4, 2010),
('WAS', 'Williams', 'RB', 65, 261, 3, 2010),
('WAS', 'Portis', 'RB', 54, 227, 2, 2010),
('WAS', 'McNabb', 'QB', 29, 151, 0, 2010),
('BUF', 'Jackson', 'RB', 170, 934, 6, 2011),
('BUF', 'Spiller', 'RB', 107, 561, 4, 2011),
('BUF', 'Fitzpatrick', 'QB', 56, 215, 0, 2011),
('MIA', 'Bush', 'RB', 216, 1086, 6, 2011),
('MIA', 'Thomas', 'RB', 165, 581, 0, 2011),
('MIA', 'Moore', 'QB', 32, 65, 2, 2011),
('NE', 'Green-Ellis', 'RB', 181, 667, 11, 2011),
('NE', 'Ridley', 'RB', 87, 441, 1, 2011),
('NE', 'Woodhead', 'RB', 77, 351, 1, 2011),
('NE', 'Brady', 'QB', 43, 109, 3, 2011),
('NYJ', 'Greene', 'RB', 253, 1054, 6, 2011),
('NYJ', 'Tomlinson', 'RB', 75, 280, 1, 2011),
('NYJ', 'McKnight', 'RB', 43, 134, 0, 2011),
('NYJ', 'Sanchez', 'QB', 37, 103, 6, 2011),
('BAL', 'Rice', 'RB', 291, 1364, 12, 2011),
('BAL', 'Williams', 'RB', 108, 444, 2, 2011),
('BAL', 'Flacco', 'RB', 39, 88, 1, 2011),
('CIN', 'Benson', 'RB', 273, 1067, 6, 2011),
('CIN', 'Scott', 'RB', 112, 380, 3, 2011),
('CIN', 'Dalton', 'QB', 37, 152, 1, 2011),
('CLE', 'Hillis', 'RB', 161, 587, 3, 2011),
('CLE', 'Hardesty', 'RB', 88, 266, 0, 2011),
('CLE', 'McCoy', 'QB', 61, 212, 0, 2011),
('PIT', 'Mendenhall', 'RB', 228, 928, 9, 2011),
('PIT', 'Redman', 'RB', 110, 479, 3, 2011),
('PIT', 'Moore', 'RB', 22, 157, 0, 2011),
('PIT', 'Roethlisberger', 'QB', 31, 70, 0, 2011),
('HOU', 'Foster', 'RB', 278, 1224, 10, 2011),
('HOU', 'Tate', 'RB', 175, 942, 4, 2011),
('HOU', 'Ward', 'RB', 45, 154, 2, 2011),
('IND', 'Brown', 'RB', 134, 645, 5, 2011),
('IND', 'Addai', 'RB', 118, 433, 1, 2011),
('IND', 'Carter', 'RB', 101, 377, 2, 2011),
('IND', 'Painter', 'QB', 17, 107, 0, 2011),
('JAC', 'Jones-Drew', 'RB', 343, 1606, 8, 2011),
('JAC', 'Karim', 'RB', 63, 130, 0, 2011),
('JAC', 'Gabbert', 'QB', 48, 98, 0, 2011),
('TEN', 'Johnson', 'RB', 262, 1047, 4, 2011),
('TEN', 'Ringer', 'RB', 59, 185, 1, 2011),
('TEN', 'Hasselbeck', 'QB', 20, 52, 0, 2011),
('DEN', 'McGahee', 'RB', 249, 1199, 4, 2011),
('DEN', 'Tebow', 'QB', 122, 660, 6, 2011),
('DEN', 'Ball', 'RB', 96, 402, 1, 2011),
('DEN', 'Moreno', 'RB', 37, 179, 0, 2011),
('KC', 'Battle', 'RB', 149, 597, 2, 2011),
('KC', 'McCluster', 'RB', 114, 516, 1, 2011),
('KC', 'Jones', 'RB', 153, 478, 0, 2011),
('KC', 'Cassel', 'QB', 25, 99, 0, 2011),
('OAK', 'Bush', 'RB', 256, 977, 7, 2011),
('OAK', 'McFadden', 'RB', 113, 614, 4, 2011),
('OAK', 'Reece', 'RB', 17, 112, 0, 2011),
('SD', 'Matthews', 'RB', 222, 1091, 6, 2011),
('SD', 'Tolbert', 'RB', 121, 490, 8, 2011),
('SD', 'Brinkley', 'RB', 30, 101, 1, 2011),
('SD', 'Hester', 'RB', 28, 90, 0, 2011),
('SD', 'Rivers', 'QB', 26, 36, 1, 2011),
('DAL', 'Murray', 'RB', 164, 897, 2, 2011),
('DAL', 'Jones', 'RB', 127, 575, 1, 2011),
('DAL', 'Tanner', 'RB', 22, 76, 1, 2011),
('DAL', 'Romo', 'QB', 22, 46, 1, 2011),
('NYG', 'Bradshaw', 'RB', 171, 659, 9, 2011),
('NYG', 'Jacobs', 'RB', 152, 571, 7, 2011),
('NYG', 'Ware', 'RB', 46, 163, 0, 2011),
('NYG', 'Manning', 'QB', 35, 15, 1, 2011),
('PHI', 'McCoy', 'RB', 273, 1309, 17, 2011),
('PHI', 'Vick', 'QB', 76, 589, 1, 2011),
('PHI', 'Brown', 'RB', 42, 136, 1, 2011),
('PHI', 'Lewis', 'RB', 23, 102, 1, 2011),
('WAS', 'Helu', 'RB', 151, 640, 2, 2011),
('WAS', 'Royster', 'RB', 56, 328, 0, 2011),
('WAS', 'Hightower', 'RB', 84, 321, 1, 2011),
('WAS', 'Torain', 'RB', 59, 200, 1, 2011),
('WAS', 'Grossman', 'QB', 20, 11, 1, 2011),
('CHI', 'Forte', 'RB', 203, 997, 3, 2011),
('CHI', 'Barber', 'RB', 114, 422, 6, 2011),
('CHI', 'Bell', 'RB', 79, 337, 0, 2011),
('CHI', 'Cutler', 'QB', 18, 55, 1, 2011),
('DET', 'Best', 'RB', 84, 390, 2, 2011),
('DET', 'Smith', 'RB', 72, 356, 4, 2011),
('DET', 'Morris', 'RB', 80, 316, 1, 2011),
('DET', 'Williams', 'RB', 58, 195, 2, 2011),
('DET', 'Stafford', 'QB', 22, 78, 0, 2011),
('GB', 'Starks', 'RB', 133, 578, 1, 2011),
('GB', 'Grant', 'RB', 134, 559, 2, 2011),
('GB', 'Rodgers', 'QB', 60, 257, 3, 2011),
('GB', 'Kuhn', 'RB', 30, 78, 4, 2011),
('MIN', 'Peterson', 'RB', 208, 970, 12, 2011),
('MIN', 'Gerhart', 'RB', 109, 531, 1, 2011),
('MIN', 'Harvin', 'WR', 52, 345, 2, 2011),
('MIN', 'Ponder', 'QB', 28, 219, 0, 2011),
('MIN', 'Webb', 'QB', 22, 154, 2, 2011),
('ATL', 'Turner', 'RB', 301, 1340, 11, 2011),
('ATL', 'Rodgers', 'RB', 57, 205, 1, 2011),
('ATL', 'Snelling', 'RB', 44, 151, 0, 2011),
('ATL', 'Ryan', 'QB', 37, 84, 2, 2011),
('CAR', 'Williams', 'RB', 155, 836, 7, 2011),
('CAR', 'Stewart', 'RB', 142, 761, 4, 2011),
('CAR', 'Newton', 'QB', 126, 706, 14, 2011),
('NO', 'Sproles', 'RB', 87, 603, 2, 2011),
('NO', 'Thomas', 'RB', 110, 562, 5, 2011),
('NO', 'Ingram', 'RB', 122, 474, 5, 2011),
('NO', 'Ivory', 'RB', 79, 374, 1, 2011),
('NO', 'Brees', 'QB', 21, 86, 1, 2011),
('TB', 'Blount', 'RB', 184, 781, 5, 2011),
('TB', 'Freeman', 'QB', 55, 238, 4, 2011),
('TB', 'Graham', 'RB', 37, 206, 0, 2011),
('TB', 'Lumpkin', 'RB', 31, 105, 0, 2011),
('ARI', 'Wells', 'RB', 245, 1047, 10, 2011),
('ARI', 'Stephens-Howling', 'RB', 43, 167, 0, 2011),
('ARI', 'Skelton', 'QB', 28, 128, 0, 2011),
('ARI', 'Smith', 'RB', 30, 102, 1, 2011),
('ARI', 'Taylor', 'RB', 20, 77, 1, 2011),
('SF', 'Gore', 'RB', 282, 1211, 8, 2011),
('SF', 'Hunter', 'RB', 112, 473, 2, 2011),
('SF', 'Smith', 'QB', 52, 179, 2, 2011),
('SF', 'Dixon', 'RB', 29, 87, 2, 2011),
('SEA', 'Lynch', 'RB', 285, 1204, 12, 2011),
('SEA', 'Washington', 'RB', 53, 248, 1, 2011),
('SEA', 'Forsett', 'RB', 46, 145, 1, 2011),
('SEA', 'Jackson', 'QB', 40, 108, 1, 2011),
('STL', 'Jackson', 'RB', 260, 1145, 5, 2011),
('STL', 'Williams', 'RB', 87, 361, 1, 2011),
('STL', 'Norwood', 'RB', 24, 61, 0, 2011),
('STL', 'Bradford', 'QB', 18, 26, 0, 2011),
('BUF', 'Jackson', 'RB', 237, 1062, 2, 2009),
('BUF', 'Lynch', 'RB', 120, 450, 2, 2009),
('BUF', 'Fitzpatrick', 'QB', 31, 141, 1, 2009),
('BUF', 'Edwards', 'QB', 14, 106, 0, 2009),
('MIA', 'Williams', 'RB', 241, 1121, 11, 2009),
('MIA', 'Brown', 'RB', 147, 648, 8, 2009),
('MIA', 'Polite', 'RB', 37, 123, 0, 2009),
('MIA', 'Hilliard', 'RB', 23, 89, 1, 2009),
('MIA', 'White', 'QB', 21, 81, 0, 2009),
('NE', 'Maroney', 'RB', 194, 757, 9, 2009),
('NE', 'Faulk', 'RB', 62, 335, 2, 2009),
('NE', 'Morris', 'RB', 73, 319, 2, 2009),
('NE', 'Taylor', 'RB', 63, 269, 4, 2009),
('NE', 'Green-Ellis', 'RB', 26, 114, 1, 2009),
('NE', 'Brady', 'QB', 29, 44, 1, 2009),
('NYJ', 'Jones', 'RB', 331, 1402, 14, 2009),
('NYJ', 'Greene', 'RB', 108, 540, 2, 2009),
('NYJ', 'Washington', 'RB', 72, 331, 0, 2009),
('NYJ', 'Smith', 'WR', 18, 207, 1, 2009),
('NYJ', 'Sanchez', 'QB', 36, 106, 3, 2009),
('NYJ', 'Woodhead', 'RB', 15, 64, 0, 2009),
('BAL', 'Rice', 'RB', 254, 1339, 7, 2009),
('BAL', 'McGahee', 'RB', 109, 544, 12, 2009),
('BAL', 'McClain', 'RB', 46, 180, 2, 2009),
('BAL', 'Flacco', 'QB', 35, 56, 0, 2009),
('CIN', 'Benson', 'RB', 301, 1251, 6, 2009),
('CIN', 'Scott', 'RB', 74, 321, 0, 2009),
('CIN', 'Palmer', 'QB', 39, 93, 3, 2009),
('CIN', 'Leonard', 'RB', 27, 84, 0, 2009),
('CLE', 'Harrison', 'RB', 194, 862, 5, 2009),
('CLE', 'Lewis', 'RB', 143, 500, 0, 2009),
('CLE', 'Cribbs', 'WR', 55, 381, 1, 2009),
('CLE', 'Jennings', 'RB', 63, 220, 1, 2009),
('CLE', 'Quinn', 'QB', 20, 98, 1, 2009),
('PIT', 'Mendenhall', 'RB', 242, 1108, 7, 2009),
('PIT', 'Parker', 'RB', 98, 389, 0, 2009),
('PIT', 'Moore', 'RB', 35, 118, 0, 2009),
('PIT', 'Roethlisberger', 'QB', 40, 82, 2, 2009),
('HOU', 'Slaton', 'RB', 131, 437, 3, 2009),
('HOU', 'Moats', 'RB', 101, 390, 4, 2009),
('HOU', 'Brown', 'RB', 79, 267, 3, 2009),
('HOU', 'Foster', 'RB', 54, 257, 3, 2009),
('HOU', 'Schaub', 'QB', 48, 57, 0, 2009),
('IND', 'Addai', 'RB', 219, 828, 10, 2009),
('IND', 'Brown', 'RB', 78, 281, 3, 2009),
('IND', 'Simpson', 'RB', 15, 102, 2, 2009),
('IND', 'Hart', 'RB', 26, 70, 1, 2009),
('IND', 'Manning', 'QB', 19, -13, 0, 2009),
('JAC', 'Jones-Drew', 'RB', 312, 1391, 15, 2009),
('JAC', 'Garrard', 'QB', 77, 323, 3, 2009),
('JAC', 'Jennings', 'RB', 39, 202, 1, 2009),
('JAC', 'Thomas', 'WR', 12, 86, 0, 2009),
('TEN', 'Johnson', 'RB', 358, 2006, 14, 2009),
('TEN', 'Young', 'QB', 55, 281, 2, 2009),
('TEN', 'White', 'RB', 64, 222, 2, 2009),
('DEN', 'Moreno', 'RB', 247, 947, 7, 2009),
('DEN', 'Buckhalter', 'RB', 120, 642, 1, 2009),
('DEN', 'Jordan', 'RB', 25, 86, 0, 2009),
('DEN', 'Orton', 'QB', 24, 71, 0, 2009),
('DEN', 'Hillis', 'RB', 13, 54, 1, 2009),
('KC', 'Charles', 'RB', 190, 1120, 7, 2009),
('KC', 'Johnson', 'RB', 46, 204, 0, 2009),
('KC', 'Cassel', 'QB', 50, 189, 0, 2009),
('OAK', 'Bush', 'RB', 123, 589, 3, 2009),
('OAK', 'Fargas', 'RB', 129, 491, 3, 2009),
('OAK', 'McFadden', 'RB', 104, 357, 1, 2009),
('OAK', 'Gradkowski', 'QB', 18, 108, 0, 2009),
('OAK', 'Russell', 'QB', 18, 44, 0, 2009),
('SD', 'Tomlinson', 'RB', 223, 730, 12, 2009),
('SD', 'Sproles', 'RB', 93, 343, 3, 2009),
('SD', 'Tolbert', 'RB', 25, 148, 1, 2009),
('SD', 'Hester', 'RB', 21, 74, 0, 2009),
('SD', 'Bennett', 'RB', 23, 65, 0, 2009),
('SD', 'Rivers', 'QB', 26, 50, 1, 2009),
('DAL', 'Barber', 'RB', 214, 932, 7, 2009),
('DAL', 'Jones', 'RB', 116, 685, 3, 2009),
('DAL', 'Choice', 'RB', 64, 349, 3, 2009),
('DAL', 'Romo', 'RB', 35, 105, 1, 2009),
('NYG', 'Jacobs', 'RB', 224, 835, 5, 2009),
('NYG', 'Bradshaw', 'RB', 163, 778, 7, 2009),
('NYG', 'Manning', 'QB', 17, 65, 0, 2009),
('PHI', 'McCoy', 'RB', 155, 637, 4, 2009),
('PHI', 'Weaver', 'RB', 70, 323, 2, 2009),
('PHI', 'Westbrook', 'RB', 61, 274, 1, 2009),
('PHI', 'McNabb', 'QB', 37, 140, 2, 2009),
('PHI', 'Vick', 'QB', 24, 95, 2, 2009),
('WAS', 'Portis', 'RB', 124, 494, 1, 2009),
('WAS', 'Campbell', 'QB', 46, 236, 1, 2009),
('WAS', 'Cartwright', 'RB', 64, 228, 0, 2009),
('WAS', 'Betts', 'RB', 56, 210, 2, 2009),
('WAS', 'Ganther', 'RB', 62, 201, 3, 2009),
('WAS', 'Mason', 'RB', 32, 127, 0, 2009),
('CHI', 'Forte', 'RB', 258, 929, 4, 2009),
('CHI', 'Bell', 'RB', 40, 220, 0, 2009),
('CHI', 'Cutler', 'QB', 40, 173, 1, 2009),
('CHI', 'Wolfe', 'RB', 22, 120, 1, 2009),
('DET', 'Smith', 'RB', 217, 747, 4, 2009),
('DET', 'Morris', 'RB', 93, 384, 2, 2009),
('DET', 'Brown', 'RB', 27, 131, 0, 2009),
('DET', 'Stafford', 'QB', 20, 108, 2, 2009),
('DET', 'Culpepper', 'QB', 18, 91, 0, 2009),
('GB', 'Grant', 'RB', 282, 1253, 11, 2009),
('GB', 'Rodgers', 'QB', 58, 316, 5, 2009),
('GB', 'Green', 'RB', 41, 160, 1, 2009),
('GB', 'Jackson', 'RB', 37, 111, 2, 2009),
('MIN', 'Peterson', 'RB', 314, 1383, 18, 2009),
('MIN', 'Taylor', 'RB', 94, 338, 1, 2009),
('MIN', 'Harvin', 'WR', 15, 135, 0, 2009),
('ATL', 'Turner', 'RB', 178, 871, 10, 2009),
('ATL', 'Snelling', 'RB', 142, 613, 4, 2009),
('ATL', 'Norwood', 'RB', 76, 252, 0, 2009),
('ATL', 'Ryan', 'QB', 30, 49, 1, 2009),
('CAR', 'Stewart', 'RB', 221, 1133, 10, 2009),
('CAR', 'Williams', 'RB', 216, 1117, 7, 2009),
('CAR', 'Hoover', 'RB', 20, 52, 1, 2009),
('CAR', 'Goodson', 'RB', 22, 49, 0, 2009),
('CAR', 'Delhomme', 'QB', 17, 60, 0, 2009),
('NO', 'Thomas', 'RB', 147, 793, 6, 2009),
('NO', 'Bell', 'RB', 172, 654, 5, 2009),
('NO', 'Bush', 'RB', 70, 390, 5, 2009),
('NO', 'Hamilton', 'RB', 35, 125, 2, 2009),
('NO', 'Brees', 'QB', 22, 33, 2, 2009),
('TB', 'Williams', 'RB', 211, 823, 4, 2009),
('TB', 'Ward', 'RB', 114, 409, 1, 2009),
('TB', 'Freeman', 'QB', 30, 161, 0, 2009),
('TB', 'Johnson', 'QB', 22, 148, 0, 2009),
('TB', 'Graham', 'RB', 14, 66, 0, 2009),
('ARI', 'Wells', 'RB', 176, 793, 7, 2009),
('ARI', 'Hightower', 'RB', 143, 598, 8, 2009),
('ARI', 'Warner', 'QB', 21, 10, 0, 2009),
('SF', 'Gore', 'RB', 229, 1120, 10, 2009),
('SF', 'Coffee', 'RB', 83, 226, 1, 2009),
('SF', 'Smith', 'QB', 24, 51, 0, 2009),
('SEA', 'Jones', 'RB', 177, 663, 2, 2009),
('SEA', 'Forsett', 'RB', 114, 619, 4, 2009),
('SEA', 'James', 'RB', 46, 125, 0, 2009),
('SEA', 'Hasselbeck', 'QB', 26, 119, 0, 2009),
('STL', 'Jackson', 'RB', 324, 1416, 4, 2009),
('STL', 'Darby', 'RB', 27, 152, 0, 2009),
('STL', 'Boller', 'QB', 13, 76, 0, 2009);

-- --------------------------------------------------------

--
-- Stand-in structure for view `rushingyardsbyteam`
--
CREATE TABLE IF NOT EXISTS `rushingyardsbyteam` (
`TEAM` varchar(3)
,`SumYards` decimal(32,0)
,`year` year(4)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `rushingyardsperccareer`
--
CREATE TABLE IF NOT EXISTS `rushingyardsperccareer` (
`Name` varchar(16)
,`Team` varchar(3)
,`Position` varchar(2)
,`TotalPerc` decimal(39,4)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `rushingyardspercoftotal`
--
CREATE TABLE IF NOT EXISTS `rushingyardspercoftotal` (
`Team` varchar(3)
,`Name` varchar(16)
,`Position` varchar(2)
,`Attempts` int(3)
,`Yards` int(4)
,`TDS` int(2)
,`year` year(4)
,`PercofTeamYards` decimal(17,4)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `rushingyardstotalbyteam`
--
CREATE TABLE IF NOT EXISTS `rushingyardstotalbyteam` (
`team` varchar(3)
,`TotalYards` decimal(54,0)
);
-- --------------------------------------------------------

--
-- Structure for view `rushingyardsbyteam`
--
DROP TABLE IF EXISTS `rushingyardsbyteam`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dev`@`localhost` SQL SECURITY DEFINER VIEW `rushingyardsbyteam` AS select `rushingstats`.`Team` AS `TEAM`,sum(`rushingstats`.`Yards`) AS `SumYards`,`rushingstats`.`Year` AS `year` from `rushingstats` group by `rushingstats`.`Team`,`rushingstats`.`Year`;

-- --------------------------------------------------------

--
-- Structure for view `rushingyardsperccareer`
--
DROP TABLE IF EXISTS `rushingyardsperccareer`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dev`@`localhost` SQL SECURITY DEFINER VIEW `rushingyardsperccareer` AS select `rushingstats`.`Name` AS `Name`,`rushingstats`.`Team` AS `Team`,`rushingstats`.`Position` AS `Position`,((sum(`rushingstats`.`Yards`) / `rushingyardstotalbyteam`.`TotalYards`) * 100) AS `TotalPerc` from (`rushingstats` join `rushingyardstotalbyteam` on((`rushingyardstotalbyteam`.`team` = `rushingstats`.`Team`))) group by `rushingstats`.`Name`,`rushingstats`.`Team`,`rushingstats`.`Position`;

-- --------------------------------------------------------

--
-- Structure for view `rushingyardspercoftotal`
--
DROP TABLE IF EXISTS `rushingyardspercoftotal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dev`@`localhost` SQL SECURITY DEFINER VIEW `rushingyardspercoftotal` AS select `rushingstats`.`Team` AS `Team`,`rushingstats`.`Name` AS `Name`,`rushingstats`.`Position` AS `Position`,`rushingstats`.`Attempts` AS `Attempts`,`rushingstats`.`Yards` AS `Yards`,`rushingstats`.`TDS` AS `TDS`,`rushingstats`.`Year` AS `year`,((`rushingstats`.`Yards` / `rushingyardsbyteam`.`SumYards`) * 100) AS `PercofTeamYards` from (`rushingstats` join `rushingyardsbyteam` on(((`rushingstats`.`Team` = `rushingyardsbyteam`.`TEAM`) and (`rushingstats`.`Year` = `rushingyardsbyteam`.`year`))));

-- --------------------------------------------------------

--
-- Structure for view `rushingyardstotalbyteam`
--
DROP TABLE IF EXISTS `rushingyardstotalbyteam`;

CREATE ALGORITHM=UNDEFINED DEFINER=`dev`@`localhost` SQL SECURITY DEFINER VIEW `rushingyardstotalbyteam` AS select `rushingyardsbyteam`.`TEAM` AS `team`,sum(`rushingyardsbyteam`.`SumYards`) AS `TotalYards` from `rushingyardsbyteam` group by `rushingyardsbyteam`.`TEAM`;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

﻿using System;
using System.IO;
using Football.FileIO.NewGame;
using Football.FileIO.NewGame.Imports;
using Football.Settings;
using Football.Sim.Games;
using Football.Sim.Penalties;
using Football.Sim.Situations;
using System.Collections.Generic;
using Football.League.Players;
using Football.League.Draft;
using Football.League.Transaction;

namespace Football.FileIO
{
    public class FileImporter
    {
        private FileSettings fileSettings;
        private FootballSettings setting;
        private SituationHandler situationHandler;
        private GameStateHandler gameStateHandler;
        private string assemblyDir;

        public FileImporter(FootballSettings setting, SituationHandler situationHandler,GameStateHandler gameStateHandler)
        {            
            this.setting = setting;
            this.situationHandler = situationHandler;
            this.gameStateHandler = gameStateHandler;

            fileSettings = new FileSettings(setting);
            assemblyDir = Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(typeof(FootballSettings)).Location);
        }
         
        /**
        *   Load the teams from the csv file.  Called by initNewGame
        */
        public bool loadTeams()
        {
            String fileName = assemblyDir+"/Config/teams.csv";
            FileImport fileImport = new TeamImport(fileName, setting);
            return fileImport.import();
        }

        /**
         *  Loads Rosters 
         */
        public bool loadRoster()
        {
            String fileName = assemblyDir + "/Config/rosters.csv";
            FileImport fileImport = new RosterImport(fileName, setting);
            return fileImport.import();
        }// end method       

        public bool loadConferences()
        {
            String fileName = assemblyDir+"/Config/conferences.csv";
            FileImport fileImport = new ConferenceImport(fileName, setting);
            return fileImport.import();
        }

        public bool loadSchedule()
        {
            String fileName = assemblyDir+"/Config/schedule.csv";
            FileImport fileImport = new ScheduleImport(fileName, setting);
            return fileImport.import();
        }

        /**
         *  Load game situations 
         */
        public bool loadSituations()
        {
            String fileName = assemblyDir+"/Config/situations.csv";
            FileImport fileImport = new SituationImport(fileName,situationHandler,gameStateHandler);
            return fileImport.import();
        }

        public bool loadCoaches()
        {
            String fileName = assemblyDir+"/Config/coaches.csv";
            FileImport fileImport = new CoachImport(fileName, setting);
            return fileImport.import();
        }

        public bool loadPenalties(PenaltyHandler penaltyHandler)
        {
            String fileName = assemblyDir+"/Config/penalties.csv";
            FileImport fileImport = new PenaltyImport(fileName, setting, penaltyHandler);
            return fileImport.import();
        }

        public bool loadTradeValues()
        {
            String fileName = assemblyDir + "/Config/playerTradeValueChart.csv";
            PlayerTradeValueChartImport fimport = new PlayerTradeValueChartImport( fileName );
            Dictionary<PlayerType, List<PlayerDraftBand>> playerBands = fimport.import();

            String fileName2 = assemblyDir + "/Config/draftValueChart.csv";
            DraftValueChartImport dimport = new DraftValueChartImport( fileName2 );
            Dictionary<int, decimal> draftValueChart = dimport.import();

            setting.TradeValue = new TradeValue( draftValueChart, playerBands );

            return true;
        }

    } // end class
} // end namespace
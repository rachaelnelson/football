﻿using System;
using System.IO;
using Football.Settings;

namespace Football.FileIO
{
    class GameLogIO
    {
        private FileSettings fileSettings;
        
        public GameLogIO(FootballSettings setting)
        {
            this.fileSettings = new FileSettings(setting);
        }
        
        public bool saveLog( String fileName, String outputString )
        {            
            String logPath = this.getSaveFilePath(fileName);
            File.WriteAllText(logPath, outputString);
            return File.Exists(logPath);
        }

        private String getSaveFilePath(String fileName)
        {            
            return String.Format( @"{0}\{1}",fileSettings.getTempLogFilePath(),fileName);
        }       

        public String loadLog(string fileName)
        {
            string log = "";

            if (fileName != "" && fileName != null)
            {
                String filePath = this.getLogFilePath(fileName);
                log = this.getLogFile(filePath);
            }
            return log;
        }

        private String getLogFilePath(String fileName)
        {
            String saveFilePath = this.getSaveFilePath(fileName);
            String rootPath = this.fileSettings.getRootPath(fileName);

            String finalPath = null;

            if (File.Exists(saveFilePath))
            {
                finalPath = saveFilePath;
            }
            else if (File.Exists(rootPath))
            {
                finalPath = rootPath;
            }

            return finalPath;
        }

        private String getLogFile(String fileText)
        {
            return (File.Exists(fileText)) ? File.ReadAllText(fileText) : null;            
        }
    }
}

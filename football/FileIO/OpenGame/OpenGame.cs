﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Football.Settings;
using Football.Sim.Games;
using Football.Sim.Penalties;
using Football.Sim.Situations;

namespace Football.FileIO.OpenGame
{
    public class OpenGame
    {       
        private FileSettings fileSettings;
        private FileImporter importer;
        private FootballSettings setting;
        private PenaltyHandler penaltyHandler;

        public OpenGame(FootballSettings setting, PenaltyHandler penaltyHandler,SituationHandler situationHandler, GameStateHandler gameStateHandler)
        {
            this.setting = setting;
            this.fileSettings = new FileSettings(setting);
            this.importer = new FileImporter(setting,situationHandler,gameStateHandler);
            this.penaltyHandler = penaltyHandler;
        }
        
        public bool openGame(String fileName)
        {
            try
            {
                return this.openBinary(fileName)
                      && this.importer.loadPenalties(this.penaltyHandler)
                      && this.importer.loadSituations();
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Error opening game: {0}", e.Message));
            }
        }

        private bool openBinary(String fileName)
        {
            try
            {
                String openGamePath = fileSettings.getSaveGameBinaryPath(fileName);
                Stream stream = File.Open(openGamePath, FileMode.Open);
                BinaryFormatter bFormatter = new BinaryFormatter();
                stream.Position = 0;
                // May need to return this back out somehow
                setting = FootballSettings.setSettings((FootballSettings)bFormatter.Deserialize(stream));                                 
                stream.Close();
                
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }        
    }
}

﻿using System;
using System.IO;
using Football.Settings;

namespace Football.FileIO
{
    public class FileSettings
    {
        private FootballSettings setting;

        public FileSettings(FootballSettings setting)
        {
            this.setting = setting;            
        }

        public bool copyFile(FileInfo from, String to)
        {
            FileInfo newFile = from.CopyTo(to, true);
            return newFile.Exists;
        }

        public bool copyFolder(DirectoryInfo fromDir, String to)
        {            
            FileInfo[] fileList = fromDir.GetFiles();

            if (!Directory.Exists(to))
            {
                Directory.CreateDirectory(to);
            }

            foreach(FileInfo file in fileList)
            {
                String copyToPath = String.Format(@"{0}\{1}",to,file);
                this.copyFile( file, copyToPath );
            }
            return true; // epic fail...
        }

        public String getTempPath()
        {
            String tempPath = String.Format(@"{0}FootballSimulator", System.IO.Path.GetTempPath());

            if (!Directory.Exists(tempPath))
            {
                Directory.CreateDirectory(tempPath);
            }

            return tempPath;
        }
        
        public String getTempLogFilePath()
        {               
            String logPath;

            if (setting.tempLogDirectory.Length == 0)
            {
                logPath = String.Format(@"{0}\Logs_{1}", this.getTempPath(), DateTime.UtcNow.ToFileTimeUtc());

                if (!Directory.Exists(logPath))
                {
                    Directory.CreateDirectory(logPath);
                    setting.tempLogDirectory = logPath;
                }
            }
            else
            {
                logPath = setting.tempLogDirectory;
            }
            return logPath;
        }          

        /**
         *   Root path such as \FootballSimulator\GB\
         */
        public String getRootPath(String fileName)
        {            
            return String.Format(@"{0}{1}", setting.appPath, fileName);
        }

        public String getSaveGameBinaryPath(String fileName)
        {
            String rootPath = this.getRootPath(fileName);
            return String.Format(@"{0}\game.sav", rootPath);
        }
    }
}

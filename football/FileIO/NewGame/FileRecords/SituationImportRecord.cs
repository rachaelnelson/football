﻿
using FileHelpers;
using Football.FileIO.NewGame.Converters;
using Football.Sim.Settings;
using Football.Sim.Situations;

namespace Football.FileIO.NewGame.FileRecords
{
    [DelimitedRecord(",")]
    [IgnoreEmptyLines()]
    [IgnoreFirst(1)]
    public class SituationImportRecord
    {        
        [FieldTrim(TrimMode.Both)]
        [FieldConverter(typeof(SituationConverter))] 
        public Situation situation;

        [FieldTrim(TrimMode.Both)]
        public PlayType playType;

        [FieldTrim(TrimMode.Both)]
        public int odds;        
    }
}

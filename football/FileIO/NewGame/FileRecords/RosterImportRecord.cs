﻿using System;
using FileHelpers;
using Football.FileIO.NewGame.Converters;
using Football.League.Injuries;
using Football.League.Players;

namespace Football.FileIO.NewGame.FileRecords
{
    [DelimitedRecord(",")]
    [IgnoreEmptyLines()]    
    [IgnoreFirst(1)]
    public class RosterImportRecord
    {
        [FieldTrim(TrimMode.Both)]
        public String firstName;

        [FieldTrim(TrimMode.Both)]
        public String lastName;

        [FieldTrim(TrimMode.Both)]
        public String abbreviation;

        [FieldTrim(TrimMode.Both)]
        [FieldConverter(ConverterKind.Date, "M/d/yy")]
        public DateTime birthDate;

        [FieldTrim(TrimMode.Both)]
        [FieldConverter(typeof(PlayerTypeConverter))] 
        public PlayerType position;

        [FieldTrim(TrimMode.Both)]
        public int accuracy;

        [FieldTrim(TrimMode.Both)]
        public int distance;

        [FieldTrim(TrimMode.Both)]
        public int awareness;

        [FieldTrim(TrimMode.Both)]
        public int speed;

        [FieldTrim(TrimMode.Both)]
        public int receiving;

        [FieldTrim(TrimMode.Both)]
        public int passBlocking;

        [FieldTrim(TrimMode.Both)]
        public int runBlocking;

        [FieldTrim(TrimMode.Both)]
        public int passDefense;

        [FieldTrim(TrimMode.Both)]
        public int runDefense;

        [FieldTrim(TrimMode.Both)]
        public int sack;
        
        [FieldTrim(TrimMode.Both)]
        public int injuryDuration;

        [FieldTrim(TrimMode.Both)]
        public Injury injury;

        [FieldTrim(TrimMode.Both)]
        public int salaryCurrYear;

        [FieldTrim(TrimMode.Both)]
        public string salary;

        [FieldTrim(TrimMode.Both)]
        public int yearsExperience;
    }
}

﻿using System;

using FileHelpers;

using Football.FileIO.NewGame.Converters;
using Football.League.Players;

namespace Football.FileIO.NewGame.FileRecords
{
    [DelimitedRecord(",")]
    [IgnoreEmptyLines()]
    [IgnoreFirst(1)]
    public class PenaltyImportRecord
    {
        [FieldTrim(TrimMode.Both)]
        public String name;

        [FieldTrim(TrimMode.Both)]
        public int yards;

        [FieldTrim(TrimMode.Both)]
        public String side;

        [FieldTrim(TrimMode.Both)]
        public String postPlay;

        [FieldTrim(TrimMode.Both)]
        public bool isFirstDown;

        [FieldTrim(TrimMode.Both)]
        [FieldConverter(typeof(PlayerTypeConverter))] 
        public PlayerType position;        
    }
}

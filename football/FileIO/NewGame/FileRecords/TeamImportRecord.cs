﻿using System;

using FileHelpers;

namespace Football.FileIO.NewGame.FileRecords
{
    [DelimitedRecord(",")]
    [IgnoreEmptyLines()]    
    public class TeamImportRecord
    {
        [FieldTrim(TrimMode.Both)]
        public String name;

        [FieldTrim(TrimMode.Both)]
        public String nickname;

        [FieldTrim(TrimMode.Both)]
        public String abbreviation;       
    }
}

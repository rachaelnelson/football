﻿using System;

using FileHelpers;

namespace Football.FileIO.NewGame.FileRecords
{
    [DelimitedRecord(",")]
    [IgnoreEmptyLines()]
    [IgnoreFirst(1)]
    public class ScheduleImportRecord
    {
        [FieldTrim(TrimMode.Both)]
        public String week;

        [FieldTrim(TrimMode.Both)]
        public String away;

        [FieldTrim(TrimMode.Both)]
        public String home;
    }
}

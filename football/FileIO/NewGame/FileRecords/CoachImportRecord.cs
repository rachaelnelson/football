﻿using System;

using FileHelpers;
using Football.Settings;

namespace Football.FileIO.NewGame.FileRecords
{
    [DelimitedRecord(",")]
    [IgnoreEmptyLines()]
    [IgnoreFirst(1)]
    class CoachImportRecord
    {
        [FieldTrim(TrimMode.Both)]
        public String team = null;

        [FieldTrim(TrimMode.Both)]
        public BaseAll baseOffense = BaseAll.TwoTE;

        [FieldTrim(TrimMode.Both)]
        public BaseAll baseDefense = BaseAll.TwoTE;

        [FieldTrim(TrimMode.Both)]
        public int Balance = 0;

        [FieldTrim(TrimMode.Both)]
        public int Behind = 0;

        [FieldTrim(TrimMode.Both)]
        public int TwoPointAttempt = 0;
    }
}

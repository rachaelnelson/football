﻿using System;

using FileHelpers;

namespace Football.FileIO.NewGame.FileRecords
{
    [DelimitedRecord(",")]
    [IgnoreEmptyLines()]    
    public class ConferenceImportRecord
    {
       [FieldTrim(TrimMode.Both)]
       public String teamAbbr;   

       [FieldTrim(TrimMode.Both)]
       public String conference;        

       [FieldTrim(TrimMode.Both)]
       public String division;                 
    }
}

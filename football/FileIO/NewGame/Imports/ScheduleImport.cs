﻿using System;
using System.Collections.Generic;
using System.IO;
using FileHelpers;
using Football.FileIO.NewGame.FileRecords;
using Football.League.Schedules;
using Football.League.Teams;
using Football.Settings;


namespace Football.FileIO.NewGame.Imports
{
    class ScheduleImport : FileImport
    {
        private FootballSettings setting;
        private FileHelperEngine engine = new FileHelperEngine(typeof(ScheduleImportRecord));
        private String fileName;
        private String message;

        Dictionary<string, Team> tList;

        public ScheduleImport(String file, FootballSettings setting)
        {
            this.fileName = file;
            this.setting = setting;
            tList = this.setting.getTeams();
        }

        public bool import()
        {
            ScheduleImportRecord[] records = this.getRecords();
            return this.processRecords(records);
        }

        private ScheduleImportRecord[] getRecords()
        {
            try
            {
                if (File.Exists(fileName))
                {
                    return engine.ReadFile(fileName) as ScheduleImportRecord[];
                }
                else
                {
                    this.message = string.Format("File {0} doesn't exist", fileName);
                    System.ArgumentException exc = new System.ArgumentException(this.message);
                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.ToString());
            }
            return null;
        }

        private bool processRecords(ScheduleImportRecord[] records)
        {
            // Create Schedule Item
            ScheduleItem schedItem;
            Schedule schedule = new Schedule();

            foreach( ScheduleImportRecord record in records )
            {
                schedItem = new ScheduleItem();
                schedItem.awayTeam = tList[record.away]; // fetches away team       
                schedItem.homeTeam = tList[record.home]; // fetches home team                                                     
                schedule.addScheduleItem(record.week,schedItem);
            }
            this.setting.schedule = schedule;
            return true;                                                                                                          
        }
    }
}

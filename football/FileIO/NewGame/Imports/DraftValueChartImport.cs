﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Football.Settings;
using System.IO;
using Football.League.Draft;
using Football.League.Players;

namespace Football.FileIO.NewGame.Imports
{
    class DraftValueChartImport
    {
        private String fileName;
        
        public DraftValueChartImport(String file)
        {
            this.fileName = file;           
        }

        // special file so rolling by hand
        public Dictionary<int,decimal> import()
        {
            return loadDraftValueChart();
        }

        private Dictionary<int,decimal> loadDraftValueChart()
        {
            Dictionary<int, decimal> draftValue = new Dictionary<int,decimal>();

            using (StreamReader reader = new StreamReader(fileName))
            {
                int lineNum = 0;

                string line;                

                while ((line = reader.ReadLine()) != null)
                {                    
                    // 3000,580,265,112,43,27,14.2              
                    //    1, 33, 65, 97, 
                    string[] parts = line.Split(',');
                    
                    for (int i = 0; i < parts.Length; i++)
                    {                        
                        int position = (i * 32) + lineNum + 1;
                        decimal value = Convert.ToDecimal( parts[i] );
                        draftValue.Add( position, value );                         
                    }

                    lineNum++;
                }
            }

            return draftValue;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using FileHelpers;
using Football.FileIO.NewGame.FileRecords;
using Football.League.Conferences;
using Football.League.Teams;
using Football.Settings;

namespace Football.FileIO.NewGame.Imports
{
    public class ConferenceImport : FileImport
    {
        private FileHelperEngine engine = new FileHelperEngine(typeof(ConferenceImportRecord));
        private String fileName;
        private String message;
        private FootballSettings setting;

        public ConferenceImport(String file, FootballSettings setting)
        {
            this.fileName = file;
            this.setting = setting;
        }

        public bool import()
        {
            ConferenceImportRecord[] records = this.getRecords();
            return this.processRecords(records);
        }

        private ConferenceImportRecord[] getRecords()
        {
            try
            {
                if (File.Exists(fileName))
                {
                    return engine.ReadFile(fileName) as ConferenceImportRecord[];
                }
                else
                {
                    this.message = string.Format("File {0} doesn't exist", fileName);
                    System.ArgumentException exc = new System.ArgumentException(this.message);
                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.ToString());
            }
            return null;
        }

        // if conference doesn't exist, create it
        // if division doesn't exit, create it
        // add teams to proper division
        // add divisions to correct conference

        // this code is pretty absurd lol
        private bool processRecords(ConferenceImportRecord[] records)
        {
            Dictionary<string, Dictionary<string, List<Team>>> confDict = new Dictionary<string, Dictionary<string, List<Team>>>();            

            foreach (ConferenceImportRecord record in records)
            {
                String conf = record.conference;
                String div = record.division;
                String abbr = record.teamAbbr;

                Team team = setting.teams.getTeam(abbr);                
                
                if (!confDict.ContainsKey(conf))
                {
                    confDict.Add(conf, new Dictionary<string,List<Team>>());
                }
                
                if (!confDict[conf].ContainsKey(div))
                {
                    confDict[conf].Add(div, new List<Team>());
                }

                confDict[conf][div].Add( team );                                                                               
            }

            setting.leagueStructure = new Structure();

            foreach (var item in confDict)
            {
                List<Division> divisions = new List<Division>();
                // NFC, Dict
                foreach (var division in item.Value)
                {
                    divisions.Add(new Division(division.Key, division.Value));                    
                }

                setting.leagueStructure.addConference( new Conference(item.Key, divisions) );
            }

            confDict = null;

            return true;
        }
    }
}

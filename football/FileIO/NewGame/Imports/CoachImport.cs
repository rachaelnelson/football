﻿using System;
using System.Collections.Generic;
using System.IO;

using FileHelpers;

using Football.FileIO.NewGame.FileRecords;
using Football.League.Coaches;
using Football.League.Teams;
using Football.Settings;

namespace Football.FileIO.NewGame.Imports
{
    class CoachImport : FileImport
    {
        private FootballSettings setting;
        private FileHelperEngine engine = new FileHelperEngine(typeof(CoachImportRecord));
        private String fileName;
        private String message;

        public CoachImport(String file, FootballSettings setting)
        {
            this.fileName = file;
            this.setting = setting;
        }

        public bool import()
        {
            CoachImportRecord[] records = this.getRecords();
            return this.processRecords(records);
        }

        private CoachImportRecord[] getRecords()
        {
            try
            {
                if (File.Exists(this.fileName))
                {
                    return engine.ReadFile(this.fileName) as CoachImportRecord[];                    
                }
                else
                {
                    this.message = string.Format("File {0} doesn't exist", fileName);
                    System.ArgumentException exc = new System.ArgumentException(this.message);
                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.ToString());
            }
            return null;
        }

        private bool processRecords(CoachImportRecord[] records)
        {
            Dictionary<string, Team> tList = setting.getTeams();
            

            foreach(CoachImportRecord record in records)
            {
                Coach coachObj = new Coach();
                coachObj.BaseOffense = record.baseOffense;
                coachObj.BaseDefense = record.baseDefense;
                coachObj.Balance = record.Balance;
                coachObj.Behind = record.Behind;
                coachObj.TwoPoint = record.TwoPointAttempt;
                tList[record.team].Coach = coachObj;        
            }
            return true;
        }
    }
}

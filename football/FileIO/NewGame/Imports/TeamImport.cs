﻿using System;
using System.IO;
using FileHelpers;
using Football.FileIO.NewGame.FileRecords;
using Football.League.Teams;
using Football.League.Teams.TeamRecord;
using Football.Settings;

namespace Football.FileIO.NewGame.Imports
{
    class TeamImport : FileImport
    {
        private FootballSettings setting;
        private FileHelperEngine engine = new FileHelperEngine(typeof(TeamImportRecord));
        private TeamList teamList = new TeamList();         
        private String fileName;
        private String message;

        public TeamImport(String file, FootballSettings setting)
        {
            this.fileName = file;
            this.setting = setting;
        }

        public bool import()
        {
           TeamImportRecord[] records = this.getRecords();
           return this.processRecords(records);
        }

        private TeamImportRecord[] getRecords()
        {          
            try
            {
                if (File.Exists(fileName))
                {
                    return engine.ReadFile(fileName) as TeamImportRecord[];
                }
                else
                {
                    this.message = string.Format("File {0} doesn't exist", fileName);
                    System.ArgumentException exc = new System.ArgumentException(this.message);                    
                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.ToString());                
            }
            return null;
        }

        private bool processRecords(TeamImportRecord[] records)
        {
            TeamRecordList recordList = new TeamRecordList();

            foreach (TeamImportRecord record in records)
            {                
                Team team = new Team(record.name, record.nickname, record.abbreviation);                
                teamList.addTeam( team );
                this.setting.rosterList[team.Abbr] = new Roster(team);                
                
                recordList.AddNewRecord( team, new TeamRecord(new Record(),new Record(), new Record()) );
            }
            this.setting.teams = teamList;
            this.setting.teamRecordList = recordList;
            this.setting.freeAgentsTeam = new Team("Free", "Agents", "FA");
            this.setting.rosterList["FA"] = new Roster(this.setting.freeAgentsTeam);            
                        
            return true;
        }        
    }
}

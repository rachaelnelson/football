﻿using System;
using System.Collections.Generic;
using System.IO;
using FileHelpers;
using Football.FileIO.NewGame.FileRecords;
using Football.Sim.Games;
using Football.Sim.Situations;

namespace Football.FileIO.NewGame.Imports
{
    class SituationImport : FileImport
    {        
        private FileHelperEngine engine = new FileHelperEngine(typeof(SituationImportRecord));        
        private List<Situation> situationList;
        private String fileName;
        private String message;
        private SituationHandler situationHandler;
        private GameStateHandler gameStateHandler;

        public SituationImport(String file, SituationHandler situationHandler, GameStateHandler gameStateHandler)
        {
            this.fileName = file;
            this.situationHandler = situationHandler;
            this.gameStateHandler = gameStateHandler;
            this.situationList = new List<Situation>();   
        }

        public bool import()
        {
            SituationImportRecord[] records = this.getRecords();
            return this.processRecords(records);
        }

        private SituationImportRecord[] getRecords()
        {
            try
            {
                if (File.Exists(this.fileName))
                {
                    return engine.ReadFile(this.fileName) as SituationImportRecord[];
                }
                else
                {
                    this.message = string.Format("File {0} doesn't exist", fileName);
                    System.ArgumentException exc = new System.ArgumentException(this.message);
                }
            }                
            catch (Exception e)
            {
                Console.Out.WriteLine(e.ToString());
            }
                  
            return null;
        }

        private bool processRecords(SituationImportRecord[] records)
        {                       
            foreach (SituationImportRecord record in records)
            {
                record.situation.gameHandler = this.gameStateHandler;
                this.addSituation(record);                
            }
            this.calculateOdds();
            return true;
        }

        private void addSituation(SituationImportRecord record)
        {
            SituationOdds oddsObj;
            Situation situation;

            oddsObj = new SituationOdds();
            oddsObj.odds = record.odds;
            oddsObj.type = record.playType;
            situation = record.situation;            

            int listIndex = situationList.IndexOf(situation);

            if (listIndex < 0) // -1
            {
                situationList.Add(situation);
            }
            else
            {
                situation = this.situationList[listIndex];
            }
            situation.OddsList.add(oddsObj);
        }

        private void calculateOdds()
        {
            foreach (var item in this.situationList)
            {
                if (item.OddsList.verifyOddsSum())
                {
                    item.OddsList.buildOddsArray();
                }
                else
                {
                    this.message = string.Format("Error:  Situation odds do not add to 100 for {0}", item.ToString());
                    System.ArgumentException exc = new System.ArgumentException(this.message);
                    throw exc;
                }
            }
            this.situationHandler.SituationList = this.situationList;            
        }
    }
}

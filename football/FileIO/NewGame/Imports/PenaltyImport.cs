﻿using System;
using System.Collections.Generic;
using System.IO;
using FileHelpers;
using Football.FileIO.NewGame.FileRecords;
using Football.Settings;
using Football.Sim.Penalties;


namespace Football.FileIO.NewGame.Imports
{
    class PenaltyImport : FileImport
    {
        private FootballSettings setting;
        private FileHelperEngine engine = new FileHelperEngine(typeof(PenaltyImportRecord));
        private String fileName;
        private String message;
        private PenaltyHandler penaltyHandler;

        public PenaltyImport(String file, FootballSettings setting, PenaltyHandler penaltyHandler)
        {
            this.fileName = file;
            this.setting = setting;
            this.penaltyHandler = penaltyHandler;
        }

        public bool import()
        {
            PenaltyImportRecord[] records = this.getRecords();
            return this.processRecords(records);
        }

        private PenaltyImportRecord[] getRecords()
        {
            try
            {
                if (File.Exists(this.fileName))
                {
                    return engine.ReadFile(this.fileName) as PenaltyImportRecord[];
                }
                else
                {
                    this.message = string.Format("File {0} doesn't exist", fileName);
                    System.ArgumentException exc = new System.ArgumentException(this.message);
                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.ToString());
            }
            return null;
        }

        private bool processRecords(PenaltyImportRecord[] records)
        {            
            Penalty penaltyObj;

            Dictionary<String, Dictionary<string, List<Penalty>>> tempPenaltyList = new Dictionary<String, Dictionary<string, List<Penalty>>>() 
                            { 
                                {
                                    "preplay",
                                    new Dictionary<string,List<Penalty>>()
                                    {
                                        {"offense",new List<Penalty>()},
                                        {"defense",new List<Penalty>()}
                                    }
                                },
                                {
                                    "postplay",
                                    new Dictionary<string,List<Penalty>>()
                                    {
                                        {"offense",new List<Penalty>()},
                                        {"defense",new List<Penalty>()}
                                    }
                                }
                            };

            foreach (PenaltyImportRecord record in records)
            {
                // Create Penalty
                penaltyObj = new RegularPenalty();
                penaltyObj.Name = record.name;
                penaltyObj.Yards = record.yards;
                penaltyObj.FirstDown = record.isFirstDown;
                penaltyObj.Position = record.position;

                tempPenaltyList[record.postPlay][record.side].Add(penaltyObj);    
            }
            penaltyHandler.PenaltyList = tempPenaltyList;
            return true;
        }
    }
}

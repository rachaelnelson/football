﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Football.Settings;
using System.IO;
using Football.League.Draft;
using Football.League.Players;

namespace Football.FileIO.NewGame.Imports
{
    class PlayerTradeValueChartImport
    {
        private String fileName;              

        public PlayerTradeValueChartImport(String file)
        {
            this.fileName = file;                    
        }

        // special file so rolling by hand
        public Dictionary<PlayerType, List<PlayerDraftBand>> import()
        {
            return loadPlayerTradeValue();                                             
        }        

        private Dictionary<PlayerType, List<PlayerDraftBand>> loadPlayerTradeValue()
        {
            Dictionary<PlayerType, List<PlayerDraftBand>> bands = new Dictionary<PlayerType, List<PlayerDraftBand>>();

            using (StreamReader reader = new StreamReader(fileName))
            {
                string line;              
                
                while ((line = reader.ReadLine()) != null)
                {   
                    List<PlayerDraftBand> blist = new List<PlayerDraftBand>();
                    
                    // QB,4.0:5.0,3.0:4.0,2.5:3.0,2.2:2.5,1.9:2.2,1.7:1.9,1.5:1.7                 
                    string[] parts = line.Split(',');

                    PlayerType ptype = PlayerType.GetPlayerTypeByString(parts[0]);                                        

                    for( int i=1; i < parts.Length; i++ )
                    {
                        if (parts[i] != "")
                        {
                            string[] bandparts = parts[i].Split(':');
                            blist.Add(new PlayerDraftBand() { LowerBound = Convert.ToDecimal(bandparts[0]), UpperBound = Convert.ToDecimal(bandparts[1]), Round = i });
                        }
                    }

                    bands.Add( ptype, blist );
                }
            }      
            
            return bands;
        }
    }
}

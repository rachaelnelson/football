﻿using FileHelpers;
using Football.League.Players;

namespace Football.FileIO.NewGame.Converters
{
    public class PlayerTypeConverter : ConverterBase
    {
        public override object StringToField(string from)
        {            
            return PlayerType.GetPlayerTypeByString(from);           
        }

        public override string FieldToString(object fieldValue)
        {
            return fieldValue.ToString();
        }
    }
}

﻿using System;

using FileHelpers;

using Football.Sim.Situations;

namespace Football.FileIO.NewGame.Converters
{
    class SituationConverter : ConverterBase
    {
        public override object StringToField(string from)
        {
            Type situationType = Type.GetType("Football.Sim.Situations.GameSituations." + from, false);
            return (Situation)Activator.CreateInstance(situationType); // have to use full name with namespace                                       
        }

        public override string FieldToString(object fieldValue)
        {
            return fieldValue.ToString();
        }
    }
}

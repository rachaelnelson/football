﻿using System;
using System.Collections.Generic;
using Football.League.Draft;
using Football.League.Teams;
using Football.Settings;
using Football.Sim.Games;
using Football.Sim.Penalties;
using Football.Sim.Situations;
using Football.Stats.Records;

namespace Football.FileIO.NewGame
{    
    public class NewGame
    {       
        private FileImporter importer;
        private FootballSettings setting;
        private PenaltyHandler penaltyHandler;       

        public NewGame(FootballSettings setting, PenaltyHandler penaltyHandler,SituationHandler situationHandler, GameStateHandler gameStateHandler )
        {
            this.setting = setting;
            this.penaltyHandler = penaltyHandler;
            this.importer = new FileImporter(setting,situationHandler, gameStateHandler);            
        }

        /**
         *  Initializes new game.  Called by ChooseTeam 
         */
        public bool initNewGame()
        {            
            setting.thisWeek = 1;
            setting.year = 2012;

            return this.loadSettings(penaltyHandler);
        }

        private bool loadSettings(PenaltyHandler penaltyHandler)
        {
            return this.importer.loadTeams()  // load teams                 
                   && this.importer.loadRoster()   // load rosters  
                   && this.importer.loadConferences()
                   && this.importer.loadSchedule()
                   && this.importer.loadSituations()
                   && this.importer.loadCoaches()
                   && this.importer.loadPenalties(penaltyHandler)
                   && this.loadDraftPicks()
                   && this.loadStats()
                   && this.importer.loadTradeValues();
        }        

        private bool loadStats()
        {
            /*
                 May need to decouple this....
             */
            Dictionary<String, List<StatsRecord>> statsList = new Dictionary<String, List<StatsRecord>>()
            {
                {"Passing",new List<StatsRecord>()},            
                {"Rushing", new List<StatsRecord>()},            
                {"Receiving",new List<StatsRecord>()},           
                {"Defense", new List<StatsRecord>()},           
                {"Kicking",new List<StatsRecord>()},            
                {"Punting",new List<StatsRecord>()}           
            };

            setting.statsList = statsList;
            return true;
        }

        // Add 3 years of draft picks
        private bool loadDraftPicks()
        {
            setting.DraftList = new DraftPickList();
            setting.DraftList.AddNewDraftYear(setting.year, setting.teams.getTeamAbbrs(), 3);            
            return true;
        }       
    }
}

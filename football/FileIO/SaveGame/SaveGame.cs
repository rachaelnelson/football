﻿using System;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Football.Settings;

namespace Football.FileIO.SaveGame
{
    public class SaveGame
    {
        private FileSettings fileSettings;
        private FootballSettings setting;

        public SaveGame(FootballSettings setting)
        {
            this.setting = setting;
            this.fileSettings = new FileSettings(setting);
        }

        public bool saveGame(String fileName)
        {
            try
            {
                return this.saveBinary(fileName)
                    && this.saveLogs(fileName);
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Error saving game: {0}", e.Message));
            }
        }

        private bool saveLogs(String fileName)
        {                     
            DirectoryInfo tempFolder = new DirectoryInfo(fileSettings.getTempLogFilePath());            
            String logPath = String.Format(@"{0}\Logs", fileSettings.getRootPath(fileName));

            fileSettings.copyFolder(tempFolder, logPath);
            return Directory.Exists(logPath);
        }

        private bool saveBinary(String fileName)
        {            
            String saveGamePath = fileSettings.getSaveGameBinaryPath(fileName);
            Stream stream = File.Open(saveGamePath, FileMode.Create);
            BinaryFormatter bFormatter = new BinaryFormatter();
            bFormatter.Serialize(stream, setting);
            stream.Close();

            return File.Exists(saveGamePath);
        }       
    }
}

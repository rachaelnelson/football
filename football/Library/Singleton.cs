﻿
// Not currently used - here for example
namespace Library
{
    public sealed class Singleton
    {
        private static Singleton s_value = new Singleton();

        // Private constructor prevents any code outside
        // this class form creating an instance
        private Singleton() { }

        // Public, static property that returns the singleton object
        public static Singleton Value
        {
            get
            {
                // Return a reference to the single object
                return s_value;
            }
        }
    }
}

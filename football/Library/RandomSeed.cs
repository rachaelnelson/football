﻿using System;

namespace Football.Library
{
    /**
     *  Used for generating random numbers.  According to the documentation, one random number instance will produce the best results:
     *  http://msdn.microsoft.com/en-us/library/system.random.aspx
     */   
    class RandomSeed
    {
        private static RandomSeed instance = RandomSeed.setSeed();
        private Random rand;

        // Public, static property that returns the singleton object
        public static RandomSeed Instance
        {
            get
            {
                // Return a reference to the single object                
                return instance;
            }
        } 
         
        public static RandomSeed setSeed(RandomSeed newSeed=null)
        {
            if (newSeed == null)
            {
                instance = new RandomSeed();
            }
            else
            {
                instance = newSeed;
            }
            return instance;
        }

        private RandomSeed()
        {
            this.rand = new Random();
        }

        // return random number
        public int Next(int max)
        {
            return this.rand.Next(max);
        }

        // return random number
        public int Next(int start,int max)
        {
            if (start > max)
            {
                start = max;
            }
            return this.rand.Next(start,max);
        }
    }
}

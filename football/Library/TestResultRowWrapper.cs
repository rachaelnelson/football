﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Football.Library;

// http://wraithnath.blogspot.com/2011/01/implementing-icustomtypedescriptor-for.html

namespace Football.Library
{
    public class TestResultRowWrapper : Dictionary<string, TestResultValue>, ICustomTypeDescriptor
    {
        //- METHODS -----------------------------------------------------------------------------------------------------------------

        #region Methods

        /// <summary>
        /// Gets the Attributes for the object
        /// </summary>
        AttributeCollection ICustomTypeDescriptor.GetAttributes()
        {
            return new AttributeCollection(null);
        }

        /// <summary>
        /// Gets the Class name
        /// </summary>
        string ICustomTypeDescriptor.GetClassName()
        {
            return null;
        }

        /// <summary>
        /// Gets the component Name
        /// </summary>
        string ICustomTypeDescriptor.GetComponentName()
        {
            return null;
        }

        /// <summary>
        /// Gets the Type Converter
        /// </summary>
        TypeConverter ICustomTypeDescriptor.GetConverter()
        {
            return null;
        }

        /// <summary>
        /// Gets the Default Event
        /// </summary>
        /// <returns></returns>
        EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
        {
            return null;
        }

        /// <summary>
        /// Gets the Default Property
        /// </summary>
        PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
        {
            return null;
        }

        /// <summary>
        /// Gets the Editor
        /// </summary>
        object ICustomTypeDescriptor.GetEditor(Type editorBaseType)
        {
            return null;
        }

        /// <summary>
        /// Gets the Events
        /// </summary>
        EventDescriptorCollection ICustomTypeDescriptor.GetEvents(Attribute[] attributes)
        {
            return new EventDescriptorCollection(null);
        }

        /// <summary>
        /// Gets the events
        /// </summary>
        EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
        {
            return new EventDescriptorCollection(null);
        }

        /// <summary>
        /// Gets the properties
        /// </summary>
        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties(Attribute[] attributes)
        {
            List<PropertyDescriptor> properties = new List<PropertyDescriptor>();

            //Add property descriptors for each entry in the dictionary
            foreach (string key in this.Keys)
            {
                properties.Add(new TestResultPropertyDescriptor(key));
            }

            //Get properties also belonging to this class also
            PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(this.GetType(), attributes);

            foreach (PropertyDescriptor oPropertyDescriptor in pdc)
            {
                properties.Add(oPropertyDescriptor);
            }

            return new PropertyDescriptorCollection(properties.ToArray());
        }

        /// <summary>
        /// gets the Properties
        /// </summary>
        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
        {
            return ((ICustomTypeDescriptor)this).GetProperties(null);
        }

        /// <summary>
        /// Gets the property owner
        /// </summary>
        object ICustomTypeDescriptor.GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }

        #endregion Methods
    }
}


/// <summary>
/// Property Descriptor for Test Result Row Wrapper
/// </summary>
public class TestResultPropertyDescriptor : PropertyDescriptor
{
    //- PROPERTIES --------------------------------------------------------------------------------------------------------------

    #region Properties

    /// <summary>
    /// Component Type
    /// </summary>
    public override Type ComponentType
    {
        get { return typeof(Dictionary<string, TestResultValue>); }
    }

    /// <summary>
    /// Gets whether its read only
    /// </summary>
    public override bool IsReadOnly
    {
        get { return false; }
    }

    /// <summary>
    /// Gets the Property Type
    /// </summary>
    public override Type PropertyType
    {
        get { return typeof(string); }
    }

    #endregion Properties

    //- CONSTRUCTOR -------------------------------------------------------------------------------------------------------------

    #region Constructor

    /// <summary>
    /// Constructor
    /// </summary>
    public TestResultPropertyDescriptor(string key)
        : base(key, null)
    {

    }

    #endregion Constructor

    //- METHODS -----------------------------------------------------------------------------------------------------------------

    #region Methods

    /// <summary>
    /// Can Reset Value
    /// </summary>
    public override bool CanResetValue(object component)
    {
        return true;
    }

    /// <summary>
    /// Gets the Value
    /// </summary>
    public override object GetValue(object component)
    {
        return ((Dictionary<string, TestResultValue>)component)[base.Name];
    }

    /// <summary>
    /// Resets the Value
    /// </summary>
    public override void ResetValue(object component)
    {
        ((Dictionary<string, TestResultValue>)component)[base.Name] = null;
    }

    /// <summary>
    /// Sets the value
    /// </summary>
    public override void SetValue(object component, object value)
    {
        ((Dictionary<string, TestResultValue>)component)[base.Name] = (TestResultValue)value;
    }

    /// <summary>
    /// Gets whether the value should be serialized
    /// </summary>
    public override bool ShouldSerializeValue(object component)
    {
        return false;
    }

    #endregion Methods

    //---------------------------------------------------------------------------------------------------------------------------
}
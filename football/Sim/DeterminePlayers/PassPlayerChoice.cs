﻿using System.Collections.Generic;
using Football.League.Players;
using Football.Sim.Games;

namespace Football.Sim.DeterminePlayers
{    
    class PassPlayerChoice:PlayerChoice
    {
        private GameStateHandler gameHandler;  

        public PassPlayerChoice(GameStateHandler gameHandler)
        {
            this.gameHandler = gameHandler;
        }

        public GameTeam getOffense()
        {
            return this.gameHandler.Offense;
        }

        public GameTeam getDefense()
        {
            return this.gameHandler.Defense;
        }

        public override List<Player> getPlayers()
        {
            List<Player> playerList = new List<Player>();
            // logic to fetch players for a run play
            playerList.Add(this.getQB());
            playerList.Add(this.getWR());
            playerList.Add(this.getTackler());
            return playerList;
        }
        
        private Player getQB()
        {
            return this.getPlayer(this.getOffense().offStarters, new PlayerType[] { PlayerType.QB });
        }

        private Player getWR()
        {
            return this.getPlayer(this.getOffense().offStarters, new PlayerType[] { PlayerType.WR, PlayerType.TE, PlayerType.RB });
        }

        private Player getTackler()
        {
            return this.getPlayer(this.getDefense().defStarters, new PlayerType[] { PlayerType.DL, PlayerType.LB, PlayerType.DB });
        }        
    }
}

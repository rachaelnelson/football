﻿using System.Collections.Generic;
using Football.League.Players;
using Football.Sim.Games;

namespace Football.Sim.DeterminePlayers
{
    class FGPlayerChoice : PlayerChoice
    {
        private GameStateHandler gameHandler;

        public FGPlayerChoice(GameStateHandler gameHandler)
        {
            this.gameHandler = gameHandler;
        }

        public GameTeam getOffense()
        {
            return this.gameHandler.Offense;
        }

        public GameTeam getDefense()
        {
            return this.gameHandler.Defense;
        }

        public override List<Player> getPlayers()
        {
            List<Player> playerList = new List<Player>();
            playerList.Add(this.getKicker());
            return playerList;
        }

        private Player getKicker()
        {
            return this.getPlayer(this.getOffense().offStarters, new PlayerType[] { PlayerType.K });
        }      
    }
}

﻿using System.Collections.Generic;
using Football.League.Players;
using Football.Sim.Games;

namespace Football.Sim.DeterminePlayers
{
    class RunPlayerChoice:PlayerChoice
    {
        private GameStateHandler gameHandler;   

        public RunPlayerChoice(GameStateHandler gameHandler)
        {
            this.gameHandler = gameHandler;
        }

        public GameTeam getOffense()
        {
            return this.gameHandler.Offense;
        }

        public GameTeam getDefense()
        {
            return this.gameHandler.Defense;
        }

        public override List<Player> getPlayers()
        {
            List<Player> playerList = new List<Player>();
            // logic to fetch players for a run play
            playerList.Add(this.getRB());
            playerList.Add(this.getTackler());
            return playerList;
        }

        // TODO:  similar logic in penaltyHandler - might be best to consolidate somewhere  
        private Player getRB()
        {            
            return this.getPlayer(this.getOffense().offStarters, new PlayerType[] { PlayerType.RB });
        }

        private Player getTackler()
        {              
            return this.getPlayer( this.getDefense().defStarters, new PlayerType[] { PlayerType.DL, PlayerType.LB, PlayerType.DB } );
        }        
    }
}

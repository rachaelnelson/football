﻿using System.Collections.Generic;
using System.Linq;
using Football.League.Players;
using Football.Library;

namespace Football.Sim.DeterminePlayers
{
    public abstract class PlayerChoice
    {       
        private RandomSeed rand = RandomSeed.Instance;                   
        
        protected Player getPlayer(Dictionary<PlayerType, List<Player>> playerList, PlayerType[] positions)
        {
            List<Player> filtered = new List<Player>();

            /* Pull all the players from the dictionary that are in the passed in positions list.
               This should only look at the keys to determine this, not the player position because
               injury substitutions may be playing out of position.
             */ 

            foreach(var item in playerList.Keys.Where(item=>(positions.Contains(item))))
            {
                filtered.AddRange(playerList[item]);
            }            
           
            // probably can assign probabilities to players and use that on the line below to pull the player
            // ** Good idea, but where are the probabilities pulled from?

            int rp = this.rand.Next(filtered.Count);
            return filtered[rp];
        }
        
        abstract public List<Player> getPlayers();        
    }
}

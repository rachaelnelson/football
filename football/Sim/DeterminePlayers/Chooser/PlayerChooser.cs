﻿using System.Collections.Generic;
using Football.Sim.Games;
using Football.Sim.Settings;

namespace Football.Sim.DeterminePlayers.Chooser
{
    public class PlayerChooser : IPlayerChooser 
    {
        private Dictionary<PlayType, PlayerChoice> playDict;

        public PlayerChooser(GameStateHandler gameHandler)
        {
            this.initDict( gameHandler );
        }

        public void initDict(GameStateHandler gameHandler)
        {
            playDict = new Dictionary<PlayType, PlayerChoice>()
            {
                {PlayType.Run,new RunPlayerChoice(gameHandler)},
                {PlayType.Pass,new PassPlayerChoice(gameHandler)},
                {PlayType.Kickoff,new KickoffPlayerChoice(gameHandler)},
                {PlayType.Punt,new PuntPlayerChoice(gameHandler)},
                {PlayType.FG,new FGPlayerChoice(gameHandler)},
                {PlayType.ExtraPoint,new ExtraPointPlayerChoice(gameHandler)}
            };
        }
       
        public PlayerChoice getChooser( PlayType type )
        {
            return this.playDict[type];            
        }
    }
}

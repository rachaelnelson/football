﻿using Football.Sim.Settings;

namespace Football.Sim.DeterminePlayers.Chooser
{
    interface IPlayerChooser
    {
        // TODO:  Make this a non enum so it is configurable.
        PlayerChoice getChooser(PlayType type);
    }
}

﻿
namespace Football.Sim.Plays.PlayRunner
{
    interface IPlayRunner
    {
        void callPlay(ExecutePlay play, GamePlay gamePlay);
    }
}

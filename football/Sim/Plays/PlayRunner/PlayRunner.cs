﻿using System;
using System.Collections.Generic;
using Football.Sim.Penalties;

namespace Football.Sim.Plays.PlayRunner
{   
    // Handles the logic for running a play
    public class PlayRunner : IPlayRunner
    {        
        private Dictionary<PenaltyType, Delegate> playCallDelegate;
        private ExecutePlay executePlay;
        private GamePlay gamePlay;           
        
        public PlayRunner(GamePlay gamePlay)
        {
            this.gamePlay = gamePlay;

            this.playCallDelegate = new Dictionary<PenaltyType, Delegate>()
            {
                {PenaltyType.None,new Action(this.noPenaltyPlay)},                
                {PenaltyType.Preplay,new Action(this.preplayPenalty)},    
                {PenaltyType.Postplay,new Action(this.postplayPenalty)} 
            };  
        }

        /*
        * Processes the necessary steps to run any given play
        */
        public void callPlay(ExecutePlay executePlay, GamePlay gamePlay)
        {
            this.executePlay = executePlay;
            PenaltyType penalty = this.checkForPenalty();
            this.playCallDelegate[penalty].DynamicInvoke();            
        }

        private void noPenaltyPlay()
        {
            this.executePlay.execute();
        }

        /**
         *  Pre play penalties stop the play  
         */
        private void preplayPenalty()
        {            
            this.gamePlay.PenaltyHandler.executePrePlayPenalty();            
        }

        /**
         *  preplay penalties let the play continue or happen during play 
         */
        private void postplayPenalty()
        {
            this.gamePlay.PenaltyHandler.save();
            this.noPenaltyPlay();
            this.gamePlay.PenaltyHandler.executePostPlayPenalty(this.gamePlay.Result);           
        }        
        
        private PenaltyType checkForPenalty()
        {
            return this.gamePlay.PenaltyHandler.getPenaltyType();
        }       
    }
}

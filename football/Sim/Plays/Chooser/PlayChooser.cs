﻿using System.Collections.Generic;
using Football.Sim.Plays.GamePlays;
using Football.Sim.Settings;

namespace Football.Sim.Plays.Chooser
{
    public class PlayChooser:IPlayChooser
    {
        private Dictionary<PlayType, ExecutePlay> playDict;

        public PlayChooser(GamePlay gamePlay)
        {
            this.initDict(gamePlay);
        }    

        /**
         *  Returns the appropriate play object.  This keeps the code clean by having one place to send back the object without worrying about details. 
         *  Factory pattern
         */
        public void initDict(GamePlay gamePlay)
        {
            playDict = new Dictionary<PlayType, ExecutePlay>()
            {
                {PlayType.Pass,new PassPlay(gamePlay)},
                {PlayType.Run,new RunPlay(gamePlay)},
                {PlayType.FG,new FGPlay(gamePlay)},
                {PlayType.Punt,new PuntPlay(gamePlay)},
                {PlayType.Kickoff,new KickoffPlay(gamePlay)},
                {PlayType.KickoffReturn,new KickoffReturnPlay(gamePlay)},
                {PlayType.Return,new ReturnPlay(gamePlay)},
                {PlayType.PuntReturn,new PuntReturnPlay(gamePlay)},
                {PlayType.Safety,new SafetyPlay(gamePlay)},
                {PlayType.ExtraPoint,new ExtraPointPlay(gamePlay)},
                {PlayType.EOG,new EOGPlay(gamePlay)},
                {PlayType.Touchback,new TouchbackPlay(gamePlay)},
                {PlayType.None,new NonePlay(gamePlay)},
            };
        }        

        public ExecutePlay choosePlay( PlayType type )
        {            
            return playDict[type]; 
        }
    }
}

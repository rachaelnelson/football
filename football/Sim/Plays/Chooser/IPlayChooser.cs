﻿
using Football.Sim.Settings;

namespace Football.Sim.Plays.Chooser
{
    interface IPlayChooser
    {
        // TODO:  Make this a non enum so it is configurable.
        ExecutePlay choosePlay(PlayType type); 
    }
}

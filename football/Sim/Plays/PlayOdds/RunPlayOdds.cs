﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.Library;

namespace Football.Sim.Plays.PlayOdds
{
    class RunPlayOdds
    {
        private int[] runOddsArray;
        private Delegate[] runDelegateArray;

        private RandomSeed rand;

        public RunPlayOdds()
        {
            rand = RandomSeed.Instance;
            this.buildRunOddsList();
            this.buildRunDelegateList();
        }

        public int getResult()
        {
            int r = rand.Next(100);
            return (int)this.runDelegateArray[r].DynamicInvoke(r);
        }

        private void buildRunOddsList()
        {
            Dictionary<int, int> oddsDict = new Dictionary<int, int>()
            {
                {-5,1}, // run yardage, odds range
                {-4,1},
                {-3,1},
                {-2,4},
                {-1,7},
                {0,10},
                {1,12},
                {2,12},
                {3,12},
                {4,11},   
                {5,5},
                {6,5},
                {7,4},
                {8,4},
                {9,4}
            };

            List<int> tempInjuryList = new List<int>();
            this.runOddsArray = new int[92];

            foreach (var item in oddsDict)
            {
                tempInjuryList.AddRange(Enumerable.Repeat<int>(item.Key, item.Value));
            }
            this.runOddsArray = tempInjuryList.ToArray();
        }

        private void buildRunDelegateList()
        {
            Dictionary<Delegate, int> oddsDict = new Dictionary<Delegate, int>()
            {
                {new Func<int,int>(this.runUnderTenYards),92},
                {new Func<int,int>(this.runFromTenToTwentyYards),5},
                {new Func<int,int>(this.runFromTwentyToFortyYards),2},
                {new Func<int,int>(this.runOverFortyYards),1}
            };

            List<Delegate> tempDelegateList = new List<Delegate>();

            foreach (var item in oddsDict)
            {
                tempDelegateList.AddRange(Enumerable.Repeat<Delegate>(item.Key, item.Value));
            }
            this.runDelegateArray = tempDelegateList.ToArray<Delegate>();
        }

        private int runUnderTenYards(int result)
        {
            return this.runOddsArray[result];
        }

        private int runFromTenToTwentyYards(int result)
        {
            return 10 + rand.Next(11);
        }

        private int runFromTwentyToFortyYards(int result)
        {
            return (20 + rand.Next(21));
        }

        private int runOverFortyYards(int result)
        {
            int n = rand.Next(11);

            if (n <= 1)
                n = 2;

            return (n * 20) + rand.Next(11);
        }        
    }
}

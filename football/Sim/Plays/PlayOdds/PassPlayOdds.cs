﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.Library;

namespace Football.Sim.Plays.PlayOdds
{
    class PassPlayOdds
    {
        private int[] passOddsArray;
        private Delegate[] passDelegateArray;

        private RandomSeed rand;

        public PassPlayOdds()
        {
            rand = RandomSeed.Instance;
            this.buildPassOddsList();
            this.buildPassDelegateList();
        }

        public int getResult()
        {
            int pass = 0;
            int i = rand.Next(101);

            //	 impact increases numbers by up to 10%            
            //int x = rand.Next(11);            

            //  Decide how much of the impact score to apply on every down
            //i = i - (int)(impact * x);

            if (i <= 39)//incomplete
            {
                pass = -800;
            }
            else
            {
                int r = rand.Next(100);
                pass = (int)this.passDelegateArray[r].DynamicInvoke(r);
            }
            return pass;
        }

        private void buildPassOddsList()
        {
            Dictionary<int, int> oddsDict = new Dictionary<int, int>()
            {
                {-5,1}, // pass yardage, odds range
                {-4,1},
                {-3,1},
                {-2,1},
                {-1,2},
                {0,6},
                {1,6},
                {2,6},
                {3,6},
                {4,6},   
                {5,8},
                {6,16},
                {7,8},
                {8,4},
                {9,4}
            };

            List<int> tempList = new List<int>();
            this.passOddsArray = new int[75]; //new int[100];

            foreach (var item in oddsDict)
            {
                tempList.AddRange(Enumerable.Repeat<int>(item.Key, item.Value));
            }
            this.passOddsArray = tempList.ToArray();
        }

        private void buildPassDelegateList()
        {
            Dictionary<Delegate, int> oddsDict = new Dictionary<Delegate, int>()
            {
                {new Func<int,int>(this.passUnderTenYards),75},
                {new Func<int,int>(this.passFromTenToTwentyYards),10},
                {new Func<int,int>(this.passFromTwentyToFortyYards),12},
                {new Func<int,int>(this.passOverFortyYards),3}
            };

            List<Delegate> tempDelegateList = new List<Delegate>();

            foreach (var item in oddsDict)
            {
                tempDelegateList.AddRange(Enumerable.Repeat<Delegate>(item.Key, item.Value));
            }
            this.passDelegateArray = tempDelegateList.ToArray<Delegate>();
        }

        private int passUnderTenYards(int result)
        {
            return this.passOddsArray[result];
        }

        private int passFromTenToTwentyYards(int result)
        {
            return 10 + rand.Next(11);
        }

        private int passFromTwentyToFortyYards(int result)
        {
            return (20 + rand.Next(21));
        }

        private int passOverFortyYards(int result)
        {
            int n = rand.Next(11);

            if (n <= 1)
                n = 2;

            return (n * 20) + rand.Next(11);
        }        
    }
}

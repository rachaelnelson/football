﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.Library;

namespace Football.Sim.Plays.PlayOdds
{
    public class KickReturnPlayOdds
    {
        private RandomSeed rand;
        private Delegate[] returnDelegateArray;

        public KickReturnPlayOdds()
        {
            rand = RandomSeed.Instance;
            this.buildReturnDelegateList();
        }

        public int getResult()
        {
            int r = rand.Next(100);
            return (int)this.returnDelegateArray[r].DynamicInvoke();
        }

        private void buildReturnDelegateList()
        {
            Dictionary<Delegate, int> oddsDict = new Dictionary<Delegate, int>()
            {
                {new Func<int>(this.returnUnderTenYards),10},
                {new Func<int>(this.returnFromTenToTwentyYards),40},
                {new Func<int>(this.returnFromTwentyToThirtyYards),20},
                {new Func<int>(this.returnFromThirtyToFortyYards),20},
                {new Func<int>(this.returnOverFortyYards),10}
            };

            List<Delegate> tempDelegateList = new List<Delegate>();

            foreach (var item in oddsDict)
            {
                tempDelegateList.AddRange(Enumerable.Repeat<Delegate>(item.Key, item.Value));
            }
            this.returnDelegateArray = tempDelegateList.ToArray<Delegate>();
        }

        private int returnUnderTenYards()
        {
            return rand.Next(11);
        }

        private int returnFromTenToTwentyYards()
        {            
            return 10 + rand.Next(11);
        }

        private int returnFromTwentyToThirtyYards()
        {
            return 20 + rand.Next(11);
        }

        private int returnFromThirtyToFortyYards()
        {
            return 30 + rand.Next(11);
        }

        private int returnOverFortyYards()
        {
            int n = rand.Next(11);
            int p = rand.Next(11);

            if (n <= 4)
            {
                n = 4;
            }

            return (n * 10) + p;
        }
    }
}

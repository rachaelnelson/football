﻿using Football.Sim.Settings;

namespace Football.Sim.Plays.GamePlays
{
    /** 
     *   Interception and Fumble returns
     */
    class ReturnPlay : ExecutePlay
    {
        private GamePlay gamePlay;

        public ReturnPlay(GamePlay gamePlay)
        {
            this.gamePlay = gamePlay;            
        }

        public void execute()
        {
            this.gamePlay.PlayType = PlayType.Return;

            this.gamePlay.Result = this.gamePlay.Play.regularReturn();

            this.gamePlay.GameHandler.checkResultDistance(ref this.gamePlay.Result);

            this.gamePlay.GameHandler.updateYardline(this.gamePlay.Result);
            this.gamePlay.GameHandler.setClock();
            this.gamePlay.GameHandler.firstDown();

            this.gamePlay.Display.displayReturn(this.gamePlay.Result);

            DownResult downRes = this.gamePlay.GameHandler.updateDown(this.gamePlay.Result);
            this.gamePlay.Display.displayDownResult(downRes);
            this.gamePlay.GameHandler.firstDown();
        }        
    }
}
﻿using Football.Sim.Settings;

namespace Football.Sim.Plays.GamePlays
{
    class RunPlay:ExecutePlay
    {
        private bool isFumble;
        private int isKept;       
        private GamePlay gamePlay;

        // TODO:  Is it possible to seperate fumble?
        public RunPlay(GamePlay gamePlay)
        {
            this.gamePlay = gamePlay;            
        }

        public void execute()
        {
            this.gamePlay.PlayType = PlayType.Run;

            // preplay
            this.gamePlay.Display.qty();

            //run
            this.gamePlay.Result = gamePlay.Play.run(); 

            //check result
            this.gamePlay.GameHandler.checkResultDistance(ref gamePlay.Result);
            this.isFumble = this.gamePlay.Play.isFumble();

            if (this.isFumble)
            {
                this.isKept = this.gamePlay.Play.isKept();
                this.gamePlay.Result = this.gamePlay.Result / 3;
            }      

            // spot result
            this.gamePlay.GameHandler.updateYardline(this.gamePlay.Result);
            this.gamePlay.GameHandler.setClock();     
 
            // players
            this.gamePlay.Players = this.gamePlay.getPlayers();     

            // display
            this.gamePlay.Display.displayRun(this.gamePlay.Result, this.gamePlay.Players);     

            //post play
            DownResult downRes = this.gamePlay.GameHandler.updateDown(this.gamePlay.Result);
            this.gamePlay.Display.displayDownResult(downRes);
            this.gamePlay.GameHandler.updateForTurnoverOnDowns(this.gamePlay.Result);

            if (this.isFumble)
            {
                this.gamePlay.Display.displayFumble(this.isKept);

                if (this.isKept == 1)
                {
                    this.gamePlay.GameHandler.IsFumbleLost = true;
                }
            }

            this.gamePlay.InjuryHandler.checkForInjury(); 

            // update stats
            this.gamePlay.StatsHandler.updateRushingStats(this.gamePlay.Result, this.gamePlay.Players);
        }       
    }   
          
}
﻿using Football.Sim.Settings;

namespace Football.Sim.Plays.GamePlays
{
    class FGPlay : ExecutePlay
    {
        private GamePlay gamePlay;
        private bool kickMade;
        private int distance;

        public FGPlay(GamePlay gamePlay)
        {
            this.gamePlay = gamePlay;           
        }

        public void execute()
        {
            this.gamePlay.PlayType = PlayType.FG;

            this.gamePlay.Display.qty();

            this.distance = this.gamePlay.GameHandler.DistanceToGoal + 17;
            this.kickMade = this.gamePlay.Play.FG(this.distance);

            this.gamePlay.GameHandler.handleFG(this.kickMade);

            this.gamePlay.Players = this.gamePlay.getPlayers();
            this.gamePlay.Display.displayFG(this.kickMade, this.distance, this.gamePlay.Players);
            this.gamePlay.StatsHandler.updateKickingStats(this.kickMade, this.gamePlay.Players, true);
        }        
    }
}
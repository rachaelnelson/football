﻿using Football.Sim.Settings;

namespace Football.Sim.Plays.GamePlays
{
    class PassPlay : ExecutePlay
    {
        private GamePlay gamePlay;               
        
        // TODO:  should this be broken down into Sack, Interception, Incomplete, and Complete Pass??  
        public PassPlay(GamePlay gamePlay)
        {
            this.gamePlay = gamePlay;            
        }

        public void execute()
        {
            this.gamePlay.PlayType = PlayType.Pass;

            // preplay
            this.gamePlay.Display.qty();

            // result
            this.gamePlay.Result = this.gamePlay.Play.pass();

            // check result
            this.gamePlay.GameHandler.IsIncomplete = false;
            this.gamePlay.GameHandler.IsInterception = this.gamePlay.Play.isInterception();
            this.gamePlay.GameHandler.IsSack = this.gamePlay.Play.isSack();

            if (this.gamePlay.Result == -800)
            {
                this.gamePlay.GameHandler.IsIncomplete = true;
                this.gamePlay.Result = 0;
                this.gamePlay.GameHandler.IsInterception = false;
            }
            else if (this.gamePlay.GameHandler.IsSack)
            {
                this.gamePlay.Result = this.gamePlay.Play.calcSackYds();
            }
            this.gamePlay.GameHandler.checkResultDistance(ref this.gamePlay.Result);

            // spot result
            this.gamePlay.GameHandler.updateYardline(this.gamePlay.Result);
            this.gamePlay.GameHandler.setClock();

            // get players
            this.gamePlay.Players = this.gamePlay.getPlayers();

            // display 
            if (this.gamePlay.GameHandler.IsIncomplete)
            {
                this.gamePlay.Display.displayIncompletePass();
            }
            else if (this.gamePlay.GameHandler.IsInterception)
            {
                this.gamePlay.Display.displayInterception();
            }
            else if (this.gamePlay.GameHandler.IsSack)
            {
                this.gamePlay.Display.displaySack(this.gamePlay.Result);
            }
            else
            {
                this.gamePlay.Display.displayPass(this.gamePlay.Result, this.gamePlay.Players);
            }

            // post play
            if (!this.gamePlay.GameHandler.IsInterception)
            {
                DownResult downRes = this.gamePlay.GameHandler.updateDown(this.gamePlay.Result);
                this.gamePlay.Display.displayDownResult(downRes);
                this.gamePlay.GameHandler.updateForTurnoverOnDowns(this.gamePlay.Result);
            }

            gamePlay.InjuryHandler.checkForInjury();   

            // update stats
            this.gamePlay.StatsHandler.updatePassingStats(this.gamePlay.Result, this.gamePlay.Players);

            // cleanup
            this.gamePlay.GameHandler.IsSack = false;
            this.gamePlay.GameHandler.IsIncomplete = false; 
        }
    }    
}

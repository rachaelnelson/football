﻿using Football.Sim.Settings;

namespace Football.Sim.Plays.GamePlays
{
    class PuntReturnPlay : ExecutePlay
    {
        private GamePlay gamePlay;
        private int adjustedResult;

        public PuntReturnPlay(GamePlay gamePlay)
        {
            this.gamePlay = gamePlay;            
        }

        public void execute()
        {
            this.gamePlay.PlayType = PlayType.PuntReturn;

            this.gamePlay.Result = this.gamePlay.Play.puntReturn();
            this.gamePlay.GameHandler.checkResultDistance(ref this.gamePlay.Result);

            this.adjustedResult = this.gamePlay.Result;
            if (this.gamePlay.Result != -200)
            {
                this.gamePlay.GameHandler.setClock();
            }
            else
            {
                this.adjustedResult = 0;
            }
            this.gamePlay.GameHandler.updateYardline(this.adjustedResult);

            if (this.gamePlay.Result == -200)
            {
                this.gamePlay.Display.displayFairCatch();
            }
            else
            {
                this.gamePlay.Display.displayReturn(this.gamePlay.Result);
            }

            DownResult downRes = this.gamePlay.GameHandler.updateDown(this.adjustedResult);
            this.gamePlay.Display.displayDownResult(downRes);
            this.gamePlay.GameHandler.firstDown();
        }        
    }   
}

﻿using Football.Sim.Settings;

namespace Football.Sim.Plays.GamePlays
{
    class NonePlay : ExecutePlay
    {
        private GamePlay gamePlay;

        public NonePlay(GamePlay gamePlay)
        {
            this.gamePlay = gamePlay;            
        }

        public void execute()
        {
            this.gamePlay.PlayType = PlayType.None;
        }       
    }
}

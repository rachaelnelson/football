﻿using Football.Sim.Settings;

namespace Football.Sim.Plays.GamePlays
{
    class ExtraPointPlay : ExecutePlay
    {
        private GamePlay gamePlay;
        private bool isKickMade;

        public ExtraPointPlay(GamePlay gamePlay)
        {
            this.gamePlay = gamePlay;            
        }

        public void execute()
        {
            this.gamePlay.PlayType = PlayType.ExtraPoint;
            this.isKickMade = this.gamePlay.Play.extraPoint();
            this.gamePlay.GameHandler.extraPointMade(this.isKickMade);
            this.gamePlay.Players = this.gamePlay.getPlayers();
            this.gamePlay.Display.displayExtraPoint(this.isKickMade, this.gamePlay.Players);
            this.gamePlay.StatsHandler.updateKickingStats(this.isKickMade, this.gamePlay.Players, false);
        }        
    }
}
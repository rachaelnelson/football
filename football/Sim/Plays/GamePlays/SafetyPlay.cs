﻿using Football.Sim.Settings;

namespace Football.Sim.Plays.GamePlays
{
    class SafetyPlay: ExecutePlay
    {
        private GamePlay gamePlay;
    
        public SafetyPlay(GamePlay gamePlay)
        {
            this.gamePlay = gamePlay;            
        }

        public void execute()
        {
            this.gamePlay.PlayType = PlayType.Safety;
            this.gamePlay.GameHandler.score(ScoreType.Safety);
            this.gamePlay.Display.displaySafety();
        }       
    }
}

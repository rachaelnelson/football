﻿using Football.Sim.Settings;

namespace Football.Sim.Plays.GamePlays
{
    class KickoffPlay : ExecutePlay
    {
        private GamePlay gamePlay;

        public KickoffPlay(GamePlay gamePlay)
        {
            this.gamePlay = gamePlay;            
        }

        public void execute()
        {
            this.gamePlay.PlayType = PlayType.Kickoff;

            this.gamePlay.Result = this.gamePlay.Play.kickoff(80); // TODO:  Player ability to mod this

            this.gamePlay.GameHandler.updateYardline(this.gamePlay.Result);

            this.gamePlay.Players = this.gamePlay.getPlayers();

            this.gamePlay.Display.displayKickoff(this.gamePlay.Result, this.gamePlay.Players, this.gamePlay.GameHandler.isTouchback());
        }
    }       
}
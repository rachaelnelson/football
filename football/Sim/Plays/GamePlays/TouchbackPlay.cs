﻿using Football.Sim.Settings;

namespace Football.Sim.Plays.GamePlays
{
    class TouchbackPlay : ExecutePlay
    {
        private GamePlay gamePlay;

        public TouchbackPlay(GamePlay gamePlay)
        {
            this.gamePlay = gamePlay;            
        }

        public void execute()
        {
            this.gamePlay.PlayType = PlayType.Touchback;
            this.gamePlay.GameHandler.setupTouchback();
            this.gamePlay.Display.displayTouchback();
            this.gamePlay.GameHandler.firstDown();
        }
    } 
}
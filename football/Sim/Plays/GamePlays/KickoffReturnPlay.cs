﻿using Football.Sim.Settings;

namespace Football.Sim.Plays.GamePlays
{
    class KickoffReturnPlay : ExecutePlay
    {
        private GamePlay gamePlay;

        public KickoffReturnPlay(GamePlay gamePlay)
        {
            this.gamePlay = gamePlay;            
        }

        public void execute()
        {
            this.gamePlay.PlayType = PlayType.KickoffReturn;

            this.gamePlay.Result = this.gamePlay.Play.kickReturn();

            this.gamePlay.GameHandler.checkResultDistance(ref this.gamePlay.Result);

            this.gamePlay.GameHandler.updateYardline(this.gamePlay.Result);
            this.gamePlay.GameHandler.setClock();

            this.gamePlay.Display.displayReturn(this.gamePlay.Result);

            DownResult downRes = this.gamePlay.GameHandler.updateDown(this.gamePlay.Result);
            this.gamePlay.Display.displayDownResult(downRes);
            this.gamePlay.GameHandler.firstDown();
        }      
    }
}
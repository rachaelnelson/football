﻿using Football.Sim.Settings;

namespace Football.Sim.Plays.GamePlays
{
    class PuntPlay : ExecutePlay
    {
        private GamePlay gamePlay;

        public PuntPlay(GamePlay gamePlay)
        {
            this.gamePlay = gamePlay;            
        }

        public void execute()
        {
            this.gamePlay.PlayType = PlayType.Punt;

            this.gamePlay.Display.qty();

            this.gamePlay.Result = this.gamePlay.Play.punt(80); // TODO:  adjust to player

            this.gamePlay.GameHandler.updateYardline(this.gamePlay.Result);
            this.gamePlay.GameHandler.setClock();

            this.gamePlay.Players = this.gamePlay.getPlayers();

            this.gamePlay.Display.displayPunt(this.gamePlay.Result, this.gamePlay.Players);

            this.gamePlay.StatsHandler.updatePuntingStats(this.gamePlay.Result, this.gamePlay.Players);
        }
    }
}
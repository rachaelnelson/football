﻿using Football.Sim.Settings;

namespace Football.Sim.Plays.GamePlays
{
    class EOGPlay:ExecutePlay
    {
        private GamePlay gamePlay;

        public EOGPlay(GamePlay gamePlay)
        {
            this.gamePlay = gamePlay;            
        }

        public void execute()
        {
            this.gamePlay.PlayType = PlayType.EOG;
            this.gamePlay.Display.displayEndOfGame();
            this.gamePlay.GameHandler.resetGameState(); // TODO:  maybe this can be called in between each game instead
        }        
    }
}

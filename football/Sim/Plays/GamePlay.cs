﻿using System.Collections.Generic;
using Football.League.Players;
using Football.Sim.DeterminePlayers.Chooser;
using Football.Sim.Displays;
using Football.Sim.Games;
using Football.Sim.Injuries;
using Football.Sim.Penalties;
using Football.Sim.Settings;
using Football.Sim.Situations;
using Football.Stats;

namespace Football.Sim.Plays
{
    /**
     *  Base class that controls the actions each play must utilize        
     */
    public class GamePlay
    {        
        public Display Display { get; set; }
        public Play Play { get; set; } 
        public GameStateHandler GameHandler { get; set; } 
        public FootballStats StatsHandler { get; set;}         
        public PlayType PlayType = PlayType.None;
        public PlayerChooser PlayerChooser { get; set;} // TODO:  reduce access level.  Currently public due to assignment in GameManager
        public List<Player> Players {get; set;}
        public PenaltyHandler PenaltyHandler { get; set; }
        public InjuryHandler InjuryHandler { get; set; }
        public SituationHandler SituationHandler { get; set; }
        
        public int Result = -500;   // TODO:  used in PlayRunner...need to find a way to decouple                                    

        public GamePlay(Display display, GameStateHandler gameHandler, FootballStats statsHandler, PenaltyHandler penaltyHandler, InjuryHandler injuryHandler, SituationHandler situationHandler, Play play = null, PlayerChooser playerChooser = null)
        {
            this.Display = display;
            this.Play = play;
            this.GameHandler = gameHandler;
            this.StatsHandler = statsHandler;
            this.PlayerChooser = playerChooser;
            this.PenaltyHandler = penaltyHandler;
            this.InjuryHandler = injuryHandler;
            this.SituationHandler = situationHandler;
        }
        
        //  TODO:  Is this method really necessary here?
        public List<Player> getPlayers()
        {
            return this.PlayerChooser.getChooser(this.PlayType).getPlayers();            
        }              
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using Football.Library;
using Football.Sim.Plays.PlayOdds;

namespace Football.Sim.Plays
{
    /*  Plays found in a game such as sack, interception, punt, kick return 
    Uses random number generation and odds to calculate specific outcomes */
    public class Play
    {        
        private RandomSeed rand;
        private PassPlayOdds passPlay;
        private RunPlayOdds runPlay;
        private KickReturnPlayOdds kickReturnPlay;
        private PuntReturnPlayOdds puntReturnPlay;
        private RegularReturnPlayOdds regularReturnPlay;
        
        /*  Constructor takes game object  */
        public Play()
        {            
            rand = RandomSeed.Instance;
            passPlay = new PassPlayOdds();
            runPlay = new RunPlayOdds();
            kickReturnPlay = new KickReturnPlayOdds();
            puntReturnPlay = new PuntReturnPlayOdds();
            regularReturnPlay = new RegularReturnPlayOdds();
        }

        /* Made up #s, need real data
           50% runs => 4 yds    50% < 4 yds

           20% 4-8              10% 3
           10% 9-13             10% 2
           10% 14-19            10% 1
           10% 20 and up        10% 0
                                10% < 0
        */
        /*  Returns length of run  */
        public int run(/*double impact*/)
        {
            return this.runPlay.getResult();              
        }

        /* Returns true if sack on play */
        public bool isSack()
        {           
            return rand.Next(101) < 11; // 36 sacks per 327 completions (since sim doesn't count incomplete for sacks)
        }

        /* Calculates sack yardage */
        public int calcSackYds()
        {          
            return -4-(rand.Next(11) / 2); // Set min sack length to 4 yards
        }

        /*  Returns true if ball is fumbled on the play */
        public bool isFumble()
        {           
            return rand.Next(101) < 5;
        }
       
        /* Returns 0 if ball is retained after fumble, 1 if defense recovers, and 2 if fumbled out of bounds */
        public int isKept()
        {
           List<int> tempList = new List<int>();
           int[] fumbleOddsArray = new int[100];
           Dictionary<int, int> oddsDict = new Dictionary<int, int>()
           {
                {0,48}, 
                {1,47},
                {2,5}                
            };           

           foreach (var item in oddsDict)
           {
               tempList.AddRange(Enumerable.Repeat<int>(item.Key, item.Value));
           }
           fumbleOddsArray = tempList.ToArray();

           int r = rand.Next(100);
           return fumbleOddsArray[r];    
        }

        /*  Returns true if pass is intercepted */
        public bool isInterception()
        { 	       
           // since the sim doesn't do interceptions on incomplete, this is the % of interceptions based on completions
           // which is 16 interceptions per 327 completions           
            return rand.Next(101) < 5;
        }

        /* Returns the distance of the completed pass or -800 for incomplete pass  */
        public int pass(/*double impact*/)
        {
            return this.passPlay.getResult();            
        }

        public int kickReturn()
        {
            return this.kickReturnPlay.getResult();
        }

        public int puntReturn()
        {
            return this.puntReturnPlay.getResult();
        }

        public int regularReturn()
        {
            return this.regularReturnPlay.getResult();
        }

        /* Returns distance of kick   */
        private int kick(int leg, int k)
        {            
            int r = rand.Next(11);
            int kick = k;

            if (leg < 80)
            {
                r = r * -1;  //shortens kick distance for weak legs
            }

            kick = kick + r;

            return kick;
        }

        /*  Sets avg length of kick and starting yardline
            Passes leg and avg kick values to kick and returns final value */
        public int kickoff(int leg)
        {
            int k = 65;
            return kick(leg, k);
        }

        /*  Sets avg length of kick 
            Passes leg and avg kick values to kick and returns final value */
        public int punt(int leg)
        {
            int k = 43;            
            return kick(leg, k);
        }

        /*  Extra Point.  Returns true for made, false for missed  */
        public bool extraPoint()
        {
            return rand.Next(101) < 98;            
        }

        /*  Field Goal.  Returns true for made, false for missed  */
        public bool FG(int distance)
        {
            Dictionary<int, int> kickDict = new Dictionary<int, int>()
            {
                {0,98},
                {1,98},// 10 yards to 19
                {2,96},// 20 to 29
                {3,86},// 30 to 39
                {4,70},// 40 to 49
                {5,60} // 50+ 
            };
            
            int keyValue = distance / 10; // don't need the fraction, just the first number
            return rand.Next(101) < kickDict[keyValue];           
        }   
    }
}

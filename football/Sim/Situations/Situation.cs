﻿using System;
using Football.Sim.Games;
using Football.Sim.Settings;

namespace Football.Sim.Situations
{
    [Serializable()]
    public abstract class Situation : IEquatable<Situation>
    {
        public SituationOddList OddsList {get; set;}
        public GameStateHandler gameHandler { get; set; }
        
        public abstract bool isMatch(); // implemented in specific situation classes
        public abstract void preSituationActions();            

        protected Situation()
        {
            OddsList = new SituationOddList();            
        }

        public PlayType getPlay()
        {
            return OddsList.getPlay();
        }

        public bool Equals(Situation other)
        {
            return this.ToString() == other.ToString();
        }
    }
}

﻿using System.Collections.Generic;
using Football.Sim.Settings;

namespace Football.Sim.Situations
{
    public class SituationHandler
    {
        public List<Situation> SituationList { get; set; }
        //private static SituationHandler instance = SituationHandler.setSettings();        

        /*
        public static SituationHandler Instance
        {
            get
            {
                // Return a reference to the single object                
                return instance;
            }
        }

        public static SituationHandler setSettings(SituationHandler newSetting = null)
        {
            if (newSetting == null)
            {
                instance = new SituationHandler();
            }
            else
            {
                instance = newSetting;
            }
            return instance;
        }        
        */
         
 
        /**
         *  Runs through the list of Situations and returns the play type back to GameManager 
         */
        public PlayType runSituations()
        {
            PlayType play = PlayType.None; // default play ...

            foreach (Situation item in this.SituationList)
            {
                if (item.isMatch())
                {
                    item.preSituationActions();
                    play = item.getPlay();                    
                    break;
                }                         
            }
            return play;
        }
    }
}

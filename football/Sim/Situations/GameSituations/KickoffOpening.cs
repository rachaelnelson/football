﻿
namespace Football.Sim.Situations.GameSituations
{
    class KickoffOpening : Situation
    {
        public override bool isMatch()
        {            
            return this.gameHandler.isBegGame();
        }

        public override void preSituationActions() 
        {
            this.gameHandler.setupKickoffOpening();       
        }               
    }
}

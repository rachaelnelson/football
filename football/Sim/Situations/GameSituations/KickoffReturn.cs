﻿
namespace Football.Sim.Situations.GameSituations
{
    class KickoffReturn:Situation
    {
        public override bool isMatch()
        {
            return this.gameHandler.isKickoffReturn();            
        }

        public override void preSituationActions()
        {
            this.gameHandler.changePoss();	
        }
    }
}

﻿
namespace Football.Sim.Situations.GameSituations
{
    class EndOfGame:Situation
    {
        public override bool isMatch()
        {            
            return this.gameHandler.isEnd();
        }

        public override void preSituationActions() {}
    }
}

﻿
namespace Football.Sim.Situations.GameSituations
{
    class Touchback:Situation
    {
        public override bool isMatch()
        {            
            return this.gameHandler.isTouchback();              
        }

        public override void preSituationActions()
        {
            this.gameHandler.changePoss();	
        }
    }
}

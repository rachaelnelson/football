﻿
namespace Football.Sim.Situations.GameSituations
{
    class KickoffRegular : Situation
    {
        public override bool isMatch()
        {
            return this.gameHandler.isKickoffRegular(); 
        }

        public override void preSituationActions() 
        {
            this.gameHandler.setupKickoffRegular();
        }               
    }
}

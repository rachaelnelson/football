﻿
namespace Football.Sim.Situations.GameSituations
{
    class PuntReturn:Situation
    {
        public override bool isMatch()
        {        
            return this.gameHandler.isPuntReturn();
        }

        public override void preSituationActions()
        {
            this.gameHandler.changePoss();
        }
    }
}

﻿
namespace Football.Sim.Situations.GameSituations
{
    class FieldGoalAttempt : Situation
    {
        public override bool isMatch()
        {
            return this.gameHandler.isFieldGoalAttempt();            
        }
        
        public override void preSituationActions() { }
    }
}

﻿
namespace Football.Sim.Situations.GameSituations
{
    class KickoffSecondHalf : Situation
    {
        public override bool isMatch()
        {            
            return this.gameHandler.isBegSecondHalf();
        }

        public override void preSituationActions() 
        {
            this.gameHandler.setupKickoffSecondHalf();
        }               
    }
}

﻿
namespace Football.Sim.Situations.GameSituations
{
    class Punt: Situation
    {
        public override bool isMatch()
        {
            return this.gameHandler.isPunt();
        }

        public override void preSituationActions() { }
    }
}

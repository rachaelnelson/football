﻿
namespace Football.Sim.Situations.GameSituations
{
    class ExtraPointAttempt : Situation
    {
        public override bool isMatch()
        {
            return this.gameHandler.isExtraPointAttempt();            
        }

        public override void preSituationActions() 
        {
            this.gameHandler.setupExtraPointAttempt();           
        }               
    }
}

﻿
namespace Football.Sim.Situations.GameSituations
{
    class Behind : Situation
    {        
        public override bool isMatch()
        {           
            return this.gameHandler.isBehind();            
        }

        public override void preSituationActions() 
        {
            this.OddsList.Odds = this.gameHandler.getBehindOdds();
        }        
    }
}
  
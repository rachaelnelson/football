﻿
namespace Football.Sim.Situations.GameSituations
{
    class KickoffOT : Situation
    {
        public override bool isMatch()
        {            
            return this.gameHandler.isBegOT();
        }

        public override void preSituationActions() 
        {
            this.gameHandler.setupKickoffOT();
        }               
    }
}

﻿
namespace Football.Sim.Situations.GameSituations
{
    class FumbleInterceptionReturn:Situation
    {
        public override bool isMatch()
        {
            return this.gameHandler.isFumbleInterceptionReturn();
        }

        public override void preSituationActions()
        {
            this.gameHandler.setupFumbleInterceptionReturn();            
        }
    }
}

﻿
namespace Football.Sim.Situations.GameSituations
{
    class TwoPointAttempt : Situation
    {
        public override bool isMatch()
        {
            return this.gameHandler.isTwoPointAttempt();
        }

        public override void preSituationActions() 
        {
            this.OddsList.Odds = this.gameHandler.getTwoPointAttemptOdds();            
        }               
    }
}

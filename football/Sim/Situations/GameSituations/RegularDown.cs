﻿
namespace Football.Sim.Situations.GameSituations
{
    class RegularDown:Situation
    {
        public override bool isMatch()
        {
            return this.gameHandler.isRegularDown();
        }

        public override void preSituationActions() {
            this.OddsList.Odds = this.gameHandler.getRegularDownOdds();
        }    
    }
}

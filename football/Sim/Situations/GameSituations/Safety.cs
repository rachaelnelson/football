﻿
namespace Football.Sim.Situations.GameSituations
{
    class Safety : Situation
    {
        public override bool isMatch()
        {
            return this.gameHandler.isSafety();
        }

        public override void preSituationActions() { }               
    }
}

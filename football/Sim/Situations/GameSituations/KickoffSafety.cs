﻿
namespace Football.Sim.Situations.GameSituations
{
    class KickoffSafety : Situation
    {
        public override bool isMatch()
        {
            return this.gameHandler.isKickoffSafety();
        }

        public override void preSituationActions() 
        {
            this.gameHandler.setupKickoffSafety();
        }               
    }
}

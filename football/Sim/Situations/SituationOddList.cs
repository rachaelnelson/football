﻿using System.Collections.Generic;
using System.Linq;
using Football.Library;
using Football.Sim.Settings;

namespace Football.Sim.Situations
{
    public class SituationOddList
    {
        public List<SituationOdds> oddsList;  // holds all of the plays and odds for a given situation
        private PlayType[] oddsArray; // an array of 100 elements used to randomly pick out the play based on odds
        private RandomSeed rand;

        public PlayType[] Odds
        {
            get
            {
                return this.oddsArray;
            }
            set
            {
                this.oddsArray = value;
            }
        }

        public SituationOddList()
        {
            oddsList = new List<SituationOdds>();
            oddsArray = new PlayType[100];
            rand = RandomSeed.Instance;            
        }
        
        /**
         *  Builds 100 count array that stores the play types.
         *  Should only be built on loading of odds in Util
         *  and on specific situations
         */ 
        public void buildOddsArray()
        {            
            List<PlayType> tempPlayList = new List<PlayType>();            
            foreach (SituationOdds item in this.oddsList)
            {
                tempPlayList.AddRange( Enumerable.Repeat<PlayType>(item.type, item.odds).ToList<PlayType>() );              
            }
            this.oddsArray = tempPlayList.ToArray<PlayType>();
        }

        /**
         *  Sum the odds to make sure they equal 100.  Otherwise return false
         *  Only used by Util
         */
        public bool verifyOddsSum()
        {            
            return (oddsList.Sum(item => item.odds)==100)?true:false;            
        }

        /**
         *  Fetches play type to use based on the odds
         */ 
        public PlayType getPlay()
        {
            int randNum = rand.Next(100);  
            return this.oddsArray[randNum];
        }

        /**
         *  Add odds to list 
         */
        public void add( SituationOdds oddsObj )
        {
            //  Add to list if not exists                        
            if (!oddsList.Contains(oddsObj))
            {
                oddsList.Add(oddsObj);
            }                                    
        }        
    }
}
﻿using System;
using Football.League.Teams;

namespace Football.Sim.Games
{
    [Serializable()]
    public class GameResult
    {
        public Team AwayTeam { get; set; }
        public Team HomeTeam { get; set; }
        public int homeTeamScore { get; set; }
        public int awayTeamScore { get; set; }
        public Boolean gamePlayed { get; set; }
        public string gamePath { get; set; } // path to log file

        public GameResult()
        {
            this.gamePlayed = false;
        }
    }
}

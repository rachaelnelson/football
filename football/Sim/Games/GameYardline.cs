﻿
namespace Football.Sim.Games
{
    public class GameYardline
    {
        private int internalYardline;
        
        public int getInternalYardline()
        {
            return this.internalYardline;
        }

        public void setInternalYardline(int newYardline)
        {
            this.internalYardline = newYardline;
        }

        /* Calculates internal ydline (0-100)  Anything 1-49 signals own side of field, 51-99 signals opponents side of field
           50 = neutral, 0 or less = safety and 100+ = TD
        */
        public void calcYdline(int result)
        {
            this.internalYardline += result;
        }

         /* Changes possession of ball  */
        public void changePoss()
        {
            this.internalYardline = 100 - this.internalYardline; //converts internal ydline
        }

        /*  yds to goal line  */
        public int distanceToGoal()
        {
            return 100 - this.internalYardline;
        }
        
        /*  Formats internal ydline into acceptable "football" format  */
        public int getYardline()
        {
            int yardline = this.internalYardline;

            if (this.internalYardline > 50)
            {
                yardline = 100 - this.internalYardline;
            }
            return yardline;
        }
        
        public bool inNeutralEndZone()
        {
            return this.internalYardline <= 0 || this.internalYardline >= 100;
        }
        
        public bool inOpponentEndZone(int result = 0)
        {
            return this.internalYardline + result >= 100;
        }

        public bool inOwnEndZone()
        {
            return this.internalYardline <= 0;
        } 

    }
}

﻿
namespace Football.Sim.Games
{
    /* This class determines the impact players have on the baseline probability.  */

    //  Will eventually need to modify for use with three+ WRs sets, 3-4 defense, etc
    /*
    class Impact
    {
        public double calcImpact(int play_type, Roster offense, Roster defense)
        {
            double score = 0.0;

            //System.out.println(offense);
            //System.out.println(defense);

            // If a run play (may need to enumerate these instead)
            if (play_type == 1)
            {
                List<Player> offense_line = offense.getPositionList(PlayerType.OL, false);

                //int count = 0;
                double ol_ability = 0.0, dl_ability = 0.0, lb_ability = 0.0, db_ability = 0.0;

                // May need to set a flag on the Players method to only return starters (pass 0 or 1)
                List<Player> rb_array = offense.getPositionList(PlayerType.RB, false);
                List<Player> te_array = offense.getPositionList(PlayerType.TE, false);

                // Grab starting running back
                Player rb = rb_array[0];
                Player te = te_array[0];

                //  Awareness is 60%, Speed 40%
                double rb_ability = (.6) * rb.awareness + rb.speed * (.4);
                double te_ability = (.6) * te.awareness + te.runBlocking * (.4);

                //System.out.println("Run Play....");
                //System.out.println("RB_ability: " + rb_ability);
                //System.out.println("TE_ability: " + te_ability);

                //  Grab the sum of the offensive line's run blocking ability
                //	Awareness is 60%, Blocking 40%
                //for ( Iterator it = offense_line.iterator(); it.hasNext(); )

                //  Only return the top 5 lineman.  Currently working on the assumption that the roster Array lists the 
                // players in order of starting, so the top 5 OL are all starters and number 6 would be the top backup 

                //count = 0;
                //for (Iterator it = offense_line.iterator(); count < offense_line.size(); )
                foreach ( Player item in offense_line )
                {
                    //OL o_line = (OL)it.next();
                    ol_ability = +item.awareness * (.6) + item.runBlocking * (.4);                    
                }
                // Grab the OL score
                ol_ability = ol_ability / 5;

                //System.out.println("OL_ability: " + ol_ability);

                //  OL = 60%, RB = 35%, TE=5%
                double total_offense_score = ol_ability * (.6) + rb_ability * (.35) + te_ability * (.05);
                //System.out.println("total offense score: " + total_offense_score);

                List<Player> dl_array = defense.getPositionList(PlayerType.DL, false);
                List<Player> lb_array = defense.getPositionList(PlayerType.LB, false);
                List<Player> db_array = defense.getPositionList(PlayerType.DB, false);

                //count = 0;
               //for (Iterator it = dl_array.iterator(); count < dl_array.size(); )
                foreach (Player item in dl_array)
                {
                    //DL d_line = (DL)it.next();
                    dl_ability = +item.runDefense * (.6) + item.awareness * (.4);
                    //count++;
                }
                // Grab the OL score
                dl_ability = dl_ability / 4;
                //System.out.println("DL Ability: " + dl_ability);
                //count = 0;
                //for (Iterator it = lb_array.iterator(); count < lb_array.size(); )
                foreach (Player item in lb_array)
                {
                    //LB lb_corp = (LB)it.next();
                    lb_ability = +item.runDefense * (.6) + item.awareness * (.4);
                    //count++;
                }
                // Grab the OL score
                lb_ability = lb_ability / 3;
                //System.out.println("LB Ability: " + lb_ability);

               // count = 0;
               // for (Iterator it = db_array.iterator(); count < db_array.size(); )
                foreach (Player item in db_array)
                {
                    //DB secondary = (DB)it.next();
                    db_ability = +item.runDefense * (.6) + item.awareness * (.4);
                    //count++;
                }
                // Grab the OL score
                db_ability = db_ability / 4;
                //System.out.println("DB Ability: " + db_ability);

                //  OL = 60%, RB = 35%, TE=5%
                double total_defense_score = dl_ability * (.7) + lb_ability * (.25) + db_ability * (.05);
                //System.out.println("Total Defense Score: " + total_defense_score);

                score = total_offense_score - total_defense_score;
                //System.out.println("Score: " + score);
            }

            // if pass....probably should be used to help calculate the odds of a sack
            if (play_type == 2)
            {
                List<Player> offense_line = offense.getPositionList(PlayerType.OL, false);
                List<Player> receivers = offense.getPositionList(PlayerType.WR, false);

                //int count = 0;
                double wr_ability = 0.0, ol_ability = 0.0, dl_ability = 0.0, lb_ability = 0.0, db_ability = 0.0;

                // May need to set a flag on the Players method to only return starters (pass 0 or 1)
                List<Player> qb_array = offense.getPositionList(PlayerType.QB, false);
                List<Player> rb_array = offense.getPositionList(PlayerType.RB, false);
                List<Player> te_array = offense.getPositionList(PlayerType.TE, false);

                // Grab starting running back
                Player qb = qb_array[0];
                Player rb = rb_array[0];
                Player te = te_array[0];

                //  Awareness is 60%, Speed 40%
                double qb_ability = (.6) * qb.awareness + qb.accuracy * (.3) + qb.distance * (.1);
                double rb_ability = rb.awareness * (.5) + rb.receiving * (.3) + rb.speed * (.2);
                double te_ability = te.awareness * (.5) + te.receiving * (.3) + te.speed * (.2);

                //System.out.println("Pass....");
                //System.out.println("qb_ability: " + qb_ability);
                //System.out.println("rb_ability: " + rb_ability);
                //System.out.println("qb_ability: " + te_ability);		

                //  Grab the sum of the offensive line's run blocking ability
                //	Awareness is 60%, Blocking 40%
                //for ( Iterator it = offense_line.iterator(); it.hasNext(); )

                //  Only return the top 5 lineman.  Currently working on the assumption that the roster Array lists the 
                // players in order of starting, so the top 5 OL are all starters and number 6 would be the top backup 

                //count = 0;
                //for (Iterator it = offense_line.iterator(); count < offense_line.size(); )
                foreach (Player item in offense_line)
                {
                    //OL o_line = (OL)it.next();
                    ol_ability = +item.awareness * (.6) + item.runBlocking * (.4);
                    //count++;
                }
                // Grab the OL score
                ol_ability = ol_ability / 5;

                //System.out.println("ol_ability: " + ol_ability);

                //count = 0;
                //for (Iterator it = receivers.iterator(); count < receivers.size(); )
                foreach (Player item in receivers)
                {
                    //WR rec = (WR)it.next();
                    wr_ability = +item.awareness * (.5) + item.receiving * (.3) + item.speed * (.2);
                    //count++;
                }
                wr_ability = wr_ability / 2;

                //System.out.println("wr_ability: " + wr_ability);

                //  OL = 60%, RB = 35%, TE=5%
                double total_offense_score = ol_ability * (.35) + rb_ability * (.05) + te_ability * (.05) + qb_ability * (.35) + wr_ability * (.20);
                //System.out.println("total offense score: " + total_offense_score);

                List<Player> dl_array = defense.getPositionList(PlayerType.DL, false);
                List<Player> lb_array = defense.getPositionList(PlayerType.LB, false);
                List<Player> db_array = defense.getPositionList(PlayerType.DB, false);

                //count = 0;                
                //for (Iterator it = dl_array.iterator(); count < dl_array.size(); )
                foreach (Player item in dl_array) 
                {
                    //DL d_line = (DL)it.next();
                    dl_ability = +item.runDefense * (.6) + item.awareness * (.4);
                    //count++;
                }
                // Grab the OL score
                dl_ability = dl_ability / 4;
                //System.out.println("dl_ability: " + dl_ability);
                //count = 0;
                //for (Iterator it = lb_array.iterator(); count < lb_array.size(); )
                foreach (Player item in lb_array) 
                {
                    //LB lb_corp = (LB)it.next();
                    lb_ability = +item.runDefense * (.6) + item.awareness * (.4);
                    //count++;
                }
                // Grab the OL score
                lb_ability = lb_ability / 3;
                //System.out.println("lb_ability: " + lb_ability);

                //count = 0;
                //for (Iterator it = db_array.iterator(); count < db_array.size(); )
                foreach (Player item in db_array) 
                {
                    //DB secondary = (DB)it.next();
                    db_ability = +item.runDefense * (.6) + item.awareness * (.4);
                    //count++;
                }
                // Grab the OL score
                db_ability = db_ability / 4;
                //System.out.println("db_ability: " + db_ability);

                double total_defense_score = dl_ability * (.4) + lb_ability * (.2) + db_ability * (.4);
                //System.out.println("total defense score: " + total_defense_score);

                score = total_offense_score - total_defense_score;
                //System.out.println("score: " + score);
            }
            // return play impact
            return score;
        } // end calcImpact
    }
    */
}

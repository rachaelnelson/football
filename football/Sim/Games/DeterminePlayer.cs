﻿using System.Collections.Generic;
using Football.League.Players;
using Football.League.Teams;

namespace Football.Sim.Games
{
    class DeterminePlayer
    {
        //  need to pass play type so it knows which method to call
        public List<Player> getInPlay(int result, Roster offense, Roster defense)
        {
            List<Player> involved = new List<Player>();

            List<Player> qb_start = offense.getPositionList(PlayerType.QB);
            List<Player> rb_start = offense.getPositionList(PlayerType.RB);
            List<Player> wr_start = offense.getPositionList(PlayerType.WR);
            List<Player> te_start = offense.getPositionList(PlayerType.TE);

            List<Player> dl_start = defense.getPositionList(PlayerType.DL);
            List<Player> lb_start = defense.getPositionList(PlayerType.LB);
            List<Player> db_start = defense.getPositionList(PlayerType.DB);

            //this.rankReceivers(rb_start, wr_start, te_start);

            return involved;
        }

        /**
         *   Since I didn't originally write comments, will have to guess what was going on here....
         *   It appears to take the stats of anyone that can catch the ball and do a composite score based on specific criteria.
         *   The player and his average are put into the ranking list
         *   The score for each player is put into score_array list
         *   
         *   ** IDEA **
         *   sort by avg like this:
         *   WR1 8.0
         *   WR2 7.0
         *   TE1 6.0
         *   RB1 6.0
         *   RB2 5.5         
         *   ---------
         *   sum = 32
         *   
         *   WR1 8.0/32 = 25% chance of getting the passing play
         *   Get a random number and compare that against a dynamically built set
         *   
         *   x < 25 = WR1
         *   25> x < 46 = WR2 (7.0/32=.21875 will need to be consistent on rounding)
         *   46>= x < 65 = TE1 ( 6.0/32=.1875 )
         *   64>= x 83< = RB1 ( 6.0/32=.1875 )
         *   83>= x  = RB2 ( 5.5/32=.1718 )
         *   
         *   This could be built for each play set (3 receiver, 2 rec 2 rb, etc) and loaded once at the beginning of the game.  On roster changes/injury, reload and recalculate.
         *   The load once should save a bit of processing time because it won't be called on every play.
         *   
         *   A function will need to be built for defense to cover the various play types mentioned above.  For example, short yardage plays and run
         *   plays are more likely to be stopped by the front 7 though the safeties may have a few tackles
         *   
         *   // Update 1/1/12
         *   Looked at stats and most have these 4 (not necessarily in this order) in the top 5 on the team
         *   WR
         *   WR
         *   TE
         *   RB
         *   X
         *   5th is determined by the formation (RB, TE, WR)
         */

        /*
        private HashMap rankReceivers(List<RB> rb_list, List<WR> wr_list, List<TE> te_list)
        {
	       HashMap<Object,Double> ranking = new HashMap<Object,Double>();
	       List<Double> score_array = new List<Double>();
	   
	       int count=0;
	       for (Iterator it=rb_list.iterator(); count<2;)
	       {
		      RB temp = (RB)it.next();
		      Double score = temp.getAwareness()*(.5) + temp.getReceiving()*(.3) + temp.getSpeed()*(.2);	
		   		   		   		   
		      ranking.put(temp,score);
		      score_array.add(score);	   
		      count++;
	       }
	 
	       count=0;
	       for (Iterator it=wr_list.iterator(); count<2;)
	       {
		      WR temp = (WR)it.next();
		      Double score = temp.getAwareness()*(.5) + temp.getReceiving()*(.3) + temp.getSpeed()*(.2);
		   
		      //ranking.put((Object)temp, score);
		      ranking.put(temp,score);
		      score_array.add(score);		   
		      count++;
	       }   
           count=0;
           for (Iterator it=te_list.iterator(); count<1;)
           {
	          TE temp = (TE)it.next();
	          Double score = temp.getAwareness()*(.5) + temp.getReceiving()*(.3) + temp.getSpeed()*(.2);
	   
	          //ranking.put((Object)temp, score);
	          ranking.put(temp,score);
	          score_array.add(score);	   
	          count++;	   
          }      
          //   Horrible, but best I can do for now.....
          Collections.sort(score_array);   
      
          for( Double d : score_array)
          {
	         for ( Object k : ranking.keySet() )
	         {
		        if( ranking.get(k) == d  )
		        {			  
		        }
	         }
          }     
          return ranking;
      } */
    }          
}

﻿using System;
using System.Collections.Generic;
using Football.League.Players;
using Football.League.Teams;
using Football.Sim.Settings;
using Football.Sim.Situations;

namespace Football.Sim.Games
{    
    public class GameTeam
    {
        private byte score=0;
        public Team Team { get; set; }
        public byte Score { get { return score; } }
        public Boolean HasBall { get; set; }        
        public Boolean IsKicking { get; set; }

        public Dictionary<String, List<SituationOddList>> oddsDict;       
        public Dictionary<PlayerType, List<Player>> offStarters;
        public Dictionary<PlayerType, List<Player>> defStarters;

        public GameTeam(Team team, Dictionary<String, Roster> rosterList)
        {
            this.Team = team;
            this.initDictionary();
            this.setStarters(rosterList);
        }

        public void addPoints(byte points){
            this.score += points;
        }        

        /* Insane yet awesome at the same time */
        // Load odds of plays based on the coach.
        // TODO:  place this elsewhere
        public void initDictionary(){            
            oddsDict = new Dictionary<String, List<SituationOddList>>()
            { 
                {
                    "Behind", new List<SituationOddList>()
                    {
                        new SituationOddList()
                        { 
                            oddsList=new List<SituationOdds>
                            {
                                new SituationOdds(){ odds=this.Team.Coach.Behind, type=PlayType.Pass },
                                new SituationOdds(){ odds=(100 - this.Team.Coach.Behind), type=PlayType.Run }
                            }
                        },                        
                    }
                },
                {
                    "RegularDown", new List<SituationOddList>()
                    {
                        new SituationOddList()
                        { 
                            oddsList=new List<SituationOdds>
                            {
                                new SituationOdds(){ odds=this.Team.Coach.Balance, type=PlayType.Pass },
                                new SituationOdds(){ odds=(100 - this.Team.Coach.Balance), type=PlayType.Run }
                            }
                        }
                    }
                },
                {
                    "TwoPointAttempt", new List<SituationOddList>()
                    {
                        new SituationOddList()
                        { 
                            oddsList=new List<SituationOdds>
                            {
                                new SituationOdds(){ odds=this.Team.Coach.TwoPoint, type=PlayType.Pass },
                                new SituationOdds(){ odds=(100 - this.Team.Coach.TwoPoint), type=PlayType.Run }
                            }
                        }
                    }
                }
            };     

            // build odds array
            this.oddsDict["Behind"][0].buildOddsArray();
            this.oddsDict["RegularDown"][0].buildOddsArray();
            this.oddsDict["TwoPointAttempt"][0].buildOddsArray();
        }

        // Used in Penalty rollback.
        public GameTeam shallowCopy()
        {
            return (GameTeam)this.MemberwiseClone();
        }
        
        public void setStarters(Dictionary<String, Roster> rosterList)
        {               
            offStarters = rosterList[this.Team.Abbr].GetStartersOffense();
            defStarters = rosterList[this.Team.Abbr].GetStartersDefense();
        }
    }
}

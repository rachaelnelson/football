﻿using Football.Sim.Clock;
using Football.Sim.DeterminePlayers.Chooser;
using Football.Sim.Plays;
using Football.Sim.Plays.Chooser;
using Football.Sim.Plays.PlayRunner;
using Football.Sim.Settings;

namespace Football.Sim.Games
{
    /*  Calls the plays.  This is where situation specific logic needs to be put in, such as last minute field goals to win the game  */
    class GameManager
    {      
        private Game game;
        private GamePlay gamePlay;
        private PlayChooser playChooser;                        
        private PlayerChooser playerChooser;
        private PlayRunner playRunner;
        private GameClock gameClock;           
        private ExecutePlay executePlay;
        
        /* Initialize variables  */
        public GameManager(Game game, GamePlay gamePlay)        
        {
            this.game = game;
            this.gamePlay = gamePlay;

            gameClock = new GameClock(new FootballClockSettings());
            gamePlay.GameHandler.init(game, gameClock);                        
                     
            playerChooser = new PlayerChooser(gamePlay.GameHandler);            
            playRunner = new PlayRunner(gamePlay);            

            gamePlay.Play = new Play();
            gamePlay.PlayerChooser = this.playerChooser;

            playChooser = new PlayChooser(gamePlay);   

            this.gamePlay.Display.newLog();  
        }       

        /*  Runs simulation.  Uses down, distance, ydline to control plays  */
        public PlayType callPlay()
        {
            PlayType playType = this.gamePlay.SituationHandler.runSituations();
            gamePlay.GameHandler.PreviousPlay = playType;
            executePlay = this.playChooser.choosePlay(playType);
            gamePlay.PlayerChooser = this.playerChooser;
            playRunner.callPlay(this.executePlay, this.gamePlay);        
            
            return playType;
        }  
    }   
}
﻿using System;
using System.Collections.Generic;
using Football.League.Teams;

namespace Football.Sim.Games
{  
    /*  The Game class runs the game.  It keeps track of the score, who has possession, and the down,distance, and yard line.
  An object of this class is used to run a game   */
    
    public class Game
    {               
        public GameTeam homeTeam { get; set;}
        public GameTeam awayTeam { get; set; }          

        public GameTeam Offense
        { 
           get
           {
               return (homeTeam.HasBall) ? homeTeam : awayTeam;  
           }
       }

       public GameTeam Defense
       {
           get
           {
               return (homeTeam.HasBall) ? awayTeam : homeTeam;
           }
       }

        public Game(Team homeTeam, Team awayTeam, Dictionary<String, Roster> rosterList)
        {
            this.homeTeam = new GameTeam(homeTeam, rosterList);
            this.awayTeam = new GameTeam(awayTeam, rosterList);            
        }

        /*
        public Team getWinner()
        {
            return (this.homeTeam.Score > this.awayTeam.Score) ? homeTeam.Team : awayTeam.Team;
        }
         */ 
    }
}

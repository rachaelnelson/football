﻿using System;
using Football.FileIO;
using Football.League.Schedules;
using Football.Settings;
using Football.Sim.Plays;
using Football.Sim.Settings;
using Football.Stats;

namespace Football.Sim.Games
{
    public class SimGame
    {        
        private GameManager manager;        
        private PlayType end;
        private Game game;
        private ScheduleItem schedItem;
        private FootballSettings setting;
        private FootballStats statsHandler;
        private GamePlay gamePlay;

        /* Sims game  */
        public SimGame(ScheduleItem item, FootballSettings setting, GamePlay gamePlay)
        {
            this.schedItem = item;
            this.setting = setting;
            this.gamePlay = gamePlay;
            this.statsHandler = gamePlay.StatsHandler;
            this.game = new Game( item.homeTeam, item.awayTeam, setting.rosterList );
            this.manager = new GameManager(this.game, gamePlay);                 
        }

        public GameResult simGame()
        {          

            this.end = PlayType.EOG;     //merely to initialize the variable                
            
            /*  Runs game Manager */
            do
            {
                end = this.manager.callPlay();
            } while (end != PlayType.EOG);        

            this.processPostGame();
         
            return schedItem.result;
        }
        
        private void processPostGame()
        {
            schedItem.result.AwayTeam = this.game.awayTeam.Team;
            schedItem.result.HomeTeam = this.game.homeTeam.Team;
            schedItem.result.homeTeamScore = this.game.homeTeam.Score;
            schedItem.result.awayTeamScore = this.game.awayTeam.Score;
            schedItem.result.gamePlayed = true;
            schedItem.result.gamePath = this.saveGameLog();
           
            this.statsHandler.updateStats();            
        }
        
        /**
         * Saves game log
         */        
        private string saveGameLog()
        {                      
            GameLogIO gameLog = new GameLogIO(setting);
            String fileName = String.Format("{0}-{1}-{2}-{3}.log ", setting.year, setting.thisWeek, this.game.homeTeam.Team.Abbr, this.game.awayTeam.Team.Abbr);
            String outputString = this.gamePlay.Display.outputString.ToString();
            gameLog.saveLog( fileName, outputString );

            return fileName;                    
        }        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.Library;
using Football.Sim.Clock;
using Football.Sim.Settings;

namespace Football.Sim.Games
{    
    public class GameStateHandler
    {
        private RandomSeed rand;
        private GameYardline gameYardline;

        // maybe these two aren't needed?
        private Game Game { get;set; }        
        private GameClock GameClock { get; set; }
        
        public PlayType PreviousPlay{ get; set; }
        public bool IsScoringPlay { get; set; }
        public bool IsTouchdown { get; set; }
        public bool IsTwoPointSuccess { get; set; }
        public bool IsTwoPointAttempt { get; set; }
        public bool IsInterception { get; set; }
        public bool IsFumbleLost { get; set; }
        public bool IsIncomplete { get; set; }
        public bool IsSack { get; set; }

        public Dictionary<String, Boolean> kickDict;

        private int down;
        private int distance;
        
        private Dictionary<ScoreType, byte> scoringDictionary;

        //private FootballClockSettings footballClockSettings;

        // Used in Rollback and GameStateHandler
        public GameTeam HomeTeam 
        {
            get
            {
                return this.Game.homeTeam;
            }
            set
            {
                this.Game.homeTeam = value;
            }
        }

        // Used in Rollback and GameStateHandler
        public GameTeam AwayTeam
        {
            get
            {
                return this.Game.awayTeam;
            }
            set
            {
                this.Game.awayTeam = value;
            }
        }        

        // Used in Display and GameStateHandler
        public int Yardline  
        {
            get 
            {
                return this.gameYardline.getYardline();
            }            
        }

        // Used in Rollback and GameStateHandler
        public int InternalYardline
        {
            get
            {
                return this.gameYardline.getInternalYardline();
            }

            set
            {
                this.gameYardline.setInternalYardline(value);
            }
        }

        // Used in Display
        public string Side
        {
            get
            {
                return this.getSide();
            }      
        }

        // Used in Display and GameStateHandler
        public int Quarter
        {
            get
            {
                return this.GameClock.Period;
            }

        }

        // Used in Display and GameStateHandler
        public int Minutes
        {
            get
            {
                return this.GameClock.Minutes;
            }

        }

        // Used in Display and GameStateHandler
        public int Seconds
        {
            get
            {
                return this.GameClock.Seconds;
            }

        }
       
        // Used in GameStateHandler, PlayerChoice Classes, PenaltyHandler, InjuryHandler, Display
        public GameTeam Offense
        {
            get
            {
                return this.Game.Offense;
            }            
        }

        // Used in GameStateHandler, PlayerChoice Classes, PenaltyHandler, InjuryHandler, Display
        public GameTeam Defense
        {
            get
            {
                return this.Game.Defense;
            }            
        }

        public int ScoreDiff 
        {
            get
            {
                return this.getScoreDiff();
            }
        }

        public int DistanceToGoal
        {
            get
            {
                return this.gameYardline.distanceToGoal();
            }
        }

        public int Distance
        {
            get
            {
                return this.distance;
            }
            set
            {
                this.distance = value;
            }
        }

        public int Down
        {
            get
            {
                return this.down;
            }

            set
            {
                this.down = value;
            }
        }
      
        public GameStateHandler()
        {
            this.gameYardline = new GameYardline();

            this.rand = RandomSeed.Instance;
            
            this.down = 0;
            this.distance = 0;
            this.gameYardline.setInternalYardline(0);                                                    

            this.initScoringDict();            
        }

        public void init(Game game, GameClock gameClock)
        {            
            this.kickDict = new Dictionary<String, Boolean>() { 
                { "FirstHalf", true },
                { "SecondHalf", true },
                { "OT", true }
            };
            this.Game = game;
            // TODO:  game clock, score, and overtime need to be set from outside or managed in another way            
            this.GameClock = gameClock;
        }             
                
        public void updateYardline(int result)
        {
            this.gameYardline.calcYdline(result);
        }            

        /**
         *  Situation.KickoffOpening
         */ 
        public bool isBegGame()
        {
            bool retValue = false;
            if (this.GameClock.isBeginningOfGame() && this.kickDict["FirstHalf"])
            {
                this.kickDict["FirstHalf"] = false;
                retValue = true;
            }
            return retValue;
        }

        /**
         *  Situation.KickoffOT
         */ 
        public bool isBegOT()
        {
            bool retValue = false;
            if (this.GameClock.isBeginningOfOvertime() && this.kickDict["OT"] && this.ScoreDiff == 0)
            {
                this.kickDict["OT"] = false;
                retValue = true;
            }
            return retValue;            
        }        
       
        public bool isBegSecondHalf()
        {
            bool retValue = false;
            if (this.GameClock.isAfterIntermission() && this.kickDict["SecondHalf"])
            {
                this.kickDict["SecondHalf"] = false;
                retValue = true;
            }
            return retValue;   
        }
        
        public void setClock()
        {
            if (!this.IsScoringPlay) // Do not tick clock if two point conversion
            {          
                //ticks play off the clock                    
                this.GameClock.incrementClock( (byte)(20 + this.rand.Next(11)) );
            }
        }

        public bool isNearEndOfHalf()
        {          
            int qtr = this.GameClock.Period;
            int min = this.GameClock.Minutes;
            int sec = this.GameClock.Seconds;            
           
            return ((qtr == 2 || qtr >= 4) && min == 0 && sec <= 6);
        }
        
        /**
         * Situation.EndOfGame
         */ 
        public bool isEnd()
        {
            return this.Quarter > 4 && (!this.isOT() || !this.GameClock.inOT());                            
        }

        private bool isOT()
        {   
           // means quarter 5 is the ot quarter.  TODO:  Need to mod for playoff OT situations which have infinite OT               
            return (this.Quarter > 4 && this.Quarter < 6) && (this.ScoreDiff == 0); 
        }
        
        
        private bool updateForTouchdown()
        {
            bool isTD = this.gameYardline.inOpponentEndZone();
            if (isTD)
            {               
                this.score(ScoreType.TD);               
                this.IsTouchdown = true;
            }
            return isTD;
        }     
             
              
        // called externally from penalty handler
        public bool updateForFirst(int result)
        {
            bool isFirst = this.checkForFirst(result);
            if (isFirst)
            {
                this.firstDown();
            }
            else
            {
                this.setNonFirstDown(result);
            }
            return isFirst;
        }
                            
        private bool checkForTurnoverOnDowns(int result)
        {
            return this.turnoverOnDowns(result);            
        }

        public void updateForTurnoverOnDowns(int result)
        {
            if (this.checkForTurnoverOnDowns(result))
            {
                this.changePoss();
            }
        }

        private bool updateForTwoPoint()
        {
            bool twoPoint = false;
            if (this.IsTwoPointAttempt)
            {
                if (this.IsTouchdown)
                {
                    this.IsTwoPointSuccess = this.gameYardline.inOpponentEndZone();  // Effectively checks if crossed the goal line
                    this.score(ScoreType.TwoPoint);
                    twoPoint = true;
                }                
            }
            return twoPoint;
        }

        public DownResult updateDown(int result)
        {
            DownResult downRes = DownResult.NextDown;
            // check TD, First Down, and TOD        
            if (this.updateForTwoPoint())
            {
                downRes = DownResult.TwoPoint;
            }
            else if (this.updateForTouchdown())
            {
                downRes = DownResult.TD;
            }
            else if (this.updateForFirst(result))
            {
                downRes = DownResult.FirstDown;
            }
            else if (this.checkForTurnoverOnDowns(result))
            {
                downRes = DownResult.TOD;
            }           
            return downRes;
        }        

        public void checkResultDistance(ref int result)
        {
            if (this.gameYardline.inOpponentEndZone(result))
            {
                result = this.DistanceToGoal;
            }
        }
        
        /**
         *  Situation.Behind
         */ 
        public bool isBehind()
        {
            return this.isLastQuarter() && this.isBehindByLargeMargin();
        }

        private bool isLastQuarter()
        {
            return this.Quarter == 4;
        }

        private bool isBehindByLargeMargin()
        {
            return this.ScoreDiff <= -17 || // losing by 17+
                            ((this.ScoreDiff < -10 && this.ScoreDiff > -17) && this.Minutes <= 8) || // down by 10  - 17 with 8 minutes
                            ((this.ScoreDiff < -7 && this.ScoreDiff > -10) && this.Minutes <= 5); // down by 7 to 10 with 5 minutes
        }
        
        public bool attemptTwoPointConversion()
        {
            bool result = false;
            int[] winning = new int[] { 1, 4, 5, 12, 15, 19 };
            int[] losing = new int[] { 1, 2, 5, 9, 11, 12, 16, 19 };

            if (this.ScoreDiff > 0)
            {
                result = winning.Contains(Math.Abs(this.ScoreDiff));                
            }
            else 
            {
                result = losing.Contains(Math.Abs(this.ScoreDiff));                
            }
            return result;
        }

        public void resetGameState()
        {            
             this.IsScoringPlay = false;
             this.IsTouchdown = false;
             this.IsTwoPointSuccess = false;
             this.IsTwoPointAttempt = false;
             this.IsInterception = false;
             this.IsFumbleLost = false;
             this.IsIncomplete = false;
             this.IsSack = false;
        }

        //----------Game Methods below here---------------------        
                
        private void initScoringDict()
        {
            this.scoringDictionary = new Dictionary<ScoreType, byte>() 
            { 
                {ScoreType.TD,6},
                {ScoreType.FG,3},
                {ScoreType.XP,1},
                {ScoreType.Safety,2}
            };
        }

        /* Calculates who kicks ball based on random #  */
        public void coinToss()
        {
            int toss = rand.Next(11); // 11-1 = 10 so should return a number between 0 and 10                        

            this.HomeTeam.HasBall = true;
            this.AwayTeam.HasBall = false;

            this.HomeTeam.IsKicking = true;
            this.AwayTeam.IsKicking = false;

            if (toss < 5)
            {
                this.HomeTeam.HasBall = false;
                this.AwayTeam.HasBall = true;

                this.HomeTeam.IsKicking = false;
                this.AwayTeam.IsKicking = true;
            }
        }

        /**
         *  Called for second half kickoff 
         */
        public void switchKickingTeam()
        {
            if (this.HomeTeam.IsKicking)
            {
                this.HomeTeam.HasBall = false;
                this.HomeTeam.IsKicking = false;
                this.AwayTeam.HasBall = true;
                this.AwayTeam.IsKicking = true;
            }
            else
            {
                this.HomeTeam.HasBall = true;
                this.HomeTeam.IsKicking = true;
                this.AwayTeam.HasBall = false;
                this.AwayTeam.IsKicking = false;
            }
        }

        /*  Adds points to scoreboard  */
        public void score(ScoreType type)
        {
            GameTeam scoringTeam = this.Offense; ;

            if (type == ScoreType.Safety)
            {
                scoringTeam = this.Defense;
            }

            byte points = this.scoringDictionary[type];

            scoringTeam.addPoints(points);

            this.IsScoringPlay = true;
        }

        /* score differential  */
        private int getScoreDiff()
        {
            return Offense.Score - Defense.Score;
        }

        /* Checks for first  */
        private bool checkForFirst(int result)
        {
            return result >= this.distance;
        }
        
        /*  Returns the side of the field based on ydline  */
        public String getSide()
        {
            String side = " ";
            if (this.gameYardline.getInternalYardline() < 50)
            {
                side = Offense.Team.Abbr;
            }
            else if (this.gameYardline.getInternalYardline() > 50)
            {
                side = Defense.Team.Abbr;
            }
            return side;
        }

        

        public void firstDown()
        {
            this.distance = 10;
            this.down = 1;
        }        
        
        
        
        /* Changes possession of ball  */
        public void changePoss()
        {
            this.gameYardline.changePoss();

            /* swaps possession of ball  */
            if (this.HomeTeam.HasBall)
            {
                this.HomeTeam.HasBall = false;
                this.AwayTeam.HasBall = true;
            }
            else
            {
                this.HomeTeam.HasBall = true;
                this.AwayTeam.HasBall = false;
            }
            this.firstDown();
        }
       
        // candidate for a helper class
        private bool turnoverOnDowns(int result)
        {
            bool isTurnover = false;
            if (this.down > 4)
            {
                this.distance = this.distance - result;
                isTurnover = true;
            }
            return isTurnover;
        }

        // candidate for a helper class
        private void setNonFirstDown(int result)
        {
            this.down++;
            this.distance = this.distance - result;
        }

        public bool isEligibleForPenalty()
        {
            return this.isNotFourthDown()
                 && this.isPenaltyEligiblePlay()
                 && this.isNotInsideTwenty();
        }

        private bool isNotFourthDown()
        {
            return this.Down < 4;
        }

        private bool isNotInsideTwenty()
        {
            return this.Yardline >= 20 && this.Yardline <= 80;
        }

        private bool isPenaltyEligiblePlay()
        {
            return this.PreviousPlay != PlayType.Kickoff
                 && this.PreviousPlay != PlayType.ExtraPoint
                 && this.PreviousPlay != PlayType.TwoPoint;
        }

        /**
         *  Situation.Safety 
         */
        public bool isSafety()
        {
            return (this.PreviousPlay != PlayType.Kickoff 
                    || this.PreviousPlay != PlayType.Punt 
                    || this.PreviousPlay != PlayType.Return)
                   && this.gameYardline.inOwnEndZone();
        }

        /**
         *  Situation.Touchback
         */
        public bool isTouchback()
        {
            return (this.PreviousPlay == PlayType.Kickoff 
                    || this.PreviousPlay == PlayType.Punt 
                    ||this.PreviousPlay == PlayType.Return)
                   && this.gameYardline.inNeutralEndZone();
        }

        /**
         *   Situation.PuntReturn
         */ 
        public bool isPuntReturn()
        {
            return this.PreviousPlay == PlayType.Punt
                && !this.gameYardline.inNeutralEndZone();
        }

        /**
         *  Situation.KickoffReturn
         */
        public bool isKickoffReturn()
        {
            return this.PreviousPlay == PlayType.Kickoff
                && !this.gameYardline.inNeutralEndZone();
        }

        /**
         *  Situation.Behind
         *  TODO:  Encapsulate this more...
         */ 
        public PlayType[] getBehindOdds()
        {
             return this.Offense.oddsDict["Behind"][0].Odds;
        }

        /**
         * Situation.isExtraPointAttempt
         */
        public bool isExtraPointAttempt()
        {
            return this.IsTouchdown 
                && !this.attemptTwoPointConversion();
        }

        /**
         * Situation.isExtraPointAttempt
         */
        public void setupExtraPointAttempt()
        {
            this.InternalYardline = 98;
            this.IsScoringPlay = false;
            this.IsTouchdown = false;
        }

        /**
         * Situation.FieldGoalAttempt
         */
        public bool isFieldGoalAttempt()
        {            
            return !this.isBehind()
                 && this.Down == 4
                 && this.DistanceToGoal <= 33;              
        }

        /**
         * Situation.FumbleInterceptionReturn
         */ 
        public bool isFumbleInterceptionReturn()
        {
            return this.IsInterception || this.IsFumbleLost;
        }

        /**
         * Situation.FumbleInterceptionReturn
         */ 
        public void setupFumbleInterceptionReturn()
        {
            this.changePoss();
            this.IsInterception = false;
            this.IsFumbleLost = false;
        }

        /**
         *  Situation.KickoffOpening
         */ 
        public void setupKickoffOpening()
        {
            this.coinToss();
            this.InternalYardline = 35;
            this.IsScoringPlay = false;     
        }

        /**
         * Situation.KickoffOT
         */ 
        public void setupKickoffOT()
        {
            this.coinToss();
            this.setupKickoffRegular();
        }                

        /**
         * Situation.KickoffRegular
         */
        public bool isKickoffRegular()
        {
            return (this.PreviousPlay == PlayType.FG
                    || this.PreviousPlay == PlayType.ExtraPoint
                    || this.IsTwoPointAttempt)
                 && !this.GameClock.inOT()
                 && this.IsScoringPlay;
                   
        }

        /**
         * Situation.KickoffRegular
         */
        public void setupKickoffRegular()
        {
            this.InternalYardline = 35;
            this.IsScoringPlay = false;
            this.IsTwoPointSuccess = false;
            this.IsTwoPointAttempt = false;
            this.IsTouchdown = false;
        }

        /**
         * Situation.KickoffSafety
         */
        public bool isKickoffSafety()
        {
            return this.IsScoringPlay 
                && this.PreviousPlay == PlayType.Safety; 
        }

        /**
         * Situation.KickoffSafety
         */
        public void setupKickoffSafety()
        {
            this.setupKickoffRegular();
            this.InternalYardline = 20;            
        }

        /**
         * Situation.KickoffSecondHalf
         */
        public void setupKickoffSecondHalf()
        {
            this.switchKickingTeam();
            this.setupKickoffRegular();
        }

        /**
         * Situation.Punt
         */ 
        public bool isPunt()
        {
            return !this.isBehind()
                 && this.Down == 4
                 && this.DistanceToGoal >= 50;
        }

        /**
         * Situation.RegularDown
         */
        public bool isRegularDown()
        {
            return !this.isBehind()                 
                 && this.PreviousPlay != PlayType.Kickoff;
        }

        /**
         * Situation.RegularDown 
         */
        public PlayType[] getRegularDownOdds()
        {           
             return this.Offense.oddsDict["RegularDown"][0].Odds; 
        }

        /**
         *  Situation.TwoPointAttempt
         */
        public bool isTwoPointAttempt()
        {
            return this.IsTouchdown
                   && !this.IsTwoPointAttempt
                   && this.attemptTwoPointConversion(); 
        }

        /**
        *  Situation.TwoPointAttempt
        */
        public void setupTwoPointAttempt()
        {
            this.InternalYardline = 98;
            this.IsTwoPointAttempt = true;
            this.IsScoringPlay = false;
            this.IsTouchdown = false;
        }

         /**
         * Situation.TwoPointAttempt 
         */
        public PlayType[] getTwoPointAttemptOdds()
        {
            return this.Offense.oddsDict["TwoPointAttempt"][0].Odds;
        }

        /**
         * Play.ExtraPointPlay
         */ 
        public void extraPointMade(bool result)
        {
            if (result)  //made FG
            {
                this.score(ScoreType.XP);                
            }     
        }

        /**
         *  Play.FGPlay
         */
        public void handleFG(bool result)
        {            
            this.setClock();

            if (result)  //made FG
            {
                this.score(ScoreType.FG);
            }
            else	 //missed FG
            {
                // Preserve the following order so that the spot is correct after a miss
                this.changePoss();
                this.updateYardline(this.Yardline + 7);
            }
        }

        /**
         * Play.TouchbackPlay
         */
        public void setupTouchback()
        {
            this.InternalYardline = 20;
        }
    }    
}

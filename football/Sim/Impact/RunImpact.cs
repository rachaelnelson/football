﻿using Football.League.Players;

namespace Football.Sim.Impact
{
    class RunImpact : PlayImpact
    {
        /*
         * idea of things needed to implement this:
         * 
         * Offensive and Defensive Formation          
         * Offensive and Defensive starters
         * 
         */

        public override int getImpact()
        {
            return 0;
        }

        private float getRBAbility(Player player)
        {
            return (((float).6 * player.awareness) + ((float).4 * player.speed));
        }

        private float getTEAbility(Player player)
        {
            return (((float).6 * player.awareness) + ((float).4 * player.runBlocking));
        }

        private float getOLAbility(Player player)
        {
            return (((float).6 * player.awareness) + ((float).4 * player.runBlocking));
        }
    }
}

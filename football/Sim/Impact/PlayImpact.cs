﻿
namespace Football.Sim.Impact
{
    abstract class PlayImpact
    {
        public abstract int getImpact();
    }
}

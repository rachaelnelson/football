﻿
namespace Football.Sim.Penalties
{
    public enum PenaltySide { Offense, Defense }
    public enum PenaltyType { Postplay, Preplay, None }
}

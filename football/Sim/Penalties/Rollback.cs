﻿using System;
using Football.Sim.Games;
using Football.Sim.Settings;

namespace Football.Sim.Penalties
{    
    //TODO:  see if there is a way to use memento pattern here
    public class Rollback
    {
        GameStateHandler gameHandler;
        
        private PlayType PreviousPlay { get; set; }
        Boolean IsScoringPlay { get; set; }
        private Boolean IsTouchdown { get; set; }
        private Boolean IsTwoPointSuccess { get; set; }
        private Boolean IsTwoPointAttempt { get; set; }
        private Boolean IsInterception { get; set; }
        private Boolean IsSack { get; set; }
        private Boolean IsFumbleLost { get; set; }
        private int InternalYardline { get; set; }
        private int Down { get; set; }
        private int Distance { get; set; }
        private GameTeam HomeTeam { get; set; }
        private GameTeam AwayTeam { get; set; } 

        public Rollback(GameStateHandler gameHandler)
        {
            this.gameHandler = gameHandler;
        }

        /**
         *  Saves all the pertinent items to rollback after a post play penalty 
         */
        public void savePoint()
        {
            this.PreviousPlay = this.gameHandler.PreviousPlay;
            this.IsScoringPlay = this.gameHandler.IsScoringPlay;
            this.IsTouchdown = this.gameHandler.IsTouchdown;
            this.IsTwoPointSuccess = this.gameHandler.IsTwoPointSuccess;
            this.IsTwoPointAttempt = this.gameHandler.IsTwoPointAttempt;
            this.IsInterception = this.gameHandler.IsInterception;
            this.IsSack = this.gameHandler.IsSack;
            this.IsFumbleLost = this.gameHandler.IsFumbleLost;
            this.InternalYardline = this.gameHandler.InternalYardline;
            this.Down = this.gameHandler.Down;
            this.Distance = this.gameHandler.Distance;
            this.HomeTeam = this.gameHandler.HomeTeam.shallowCopy();
            this.AwayTeam = this.gameHandler.AwayTeam.shallowCopy();
        }

        public void rollbackPlay()
        {
            this.gameHandler.PreviousPlay = this.PreviousPlay;
            this.gameHandler.IsScoringPlay = this.IsScoringPlay;
            this.gameHandler.IsTouchdown = this.IsTouchdown;
            this.gameHandler.IsTwoPointSuccess = this.IsTwoPointSuccess;
            this.gameHandler.IsTwoPointAttempt = this.IsTwoPointAttempt;
            this.gameHandler.IsInterception = this.IsInterception;
            this.gameHandler.IsSack = this.IsSack;
            this.gameHandler.IsFumbleLost = this.IsFumbleLost;
            this.gameHandler.InternalYardline = this.InternalYardline;
            this.gameHandler.Down = this.Down;
            this.gameHandler.Distance = this.Distance;
            this.gameHandler.HomeTeam = this.HomeTeam;
            this.gameHandler.AwayTeam = this.AwayTeam;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.League.Players;
using Football.Library;
using Football.Sim.Displays;
using Football.Sim.Games;

namespace Football.Sim.Penalties
{    
    public class PenaltyHandler
    {
        private Dictionary<String, Dictionary<string, List<Penalty>>> penaltyList = new Dictionary<String, Dictionary<string, List<Penalty>>>();
        private RandomSeed rand;
        private Rollback rollbackObj;
        private GameStateHandler gameHandler;
        private Display display;

        public Dictionary<String, Dictionary<string, List<Penalty>>> PenaltyList 
        {
            get
            {
                return this.penaltyList;
            }
            set
            {
                this.penaltyList = value;
            }
        }
        
        public PenaltyHandler(GameStateHandler gameHandler,Display display)
        {
            this.gameHandler = gameHandler;
            this.display = display;
            this.rollbackObj = new Rollback(this.gameHandler);
        }

        /**
         *  Determines if there is a penalty.  
         *  Returns the PenaltyType
         */ 
        public PenaltyType getPenaltyType()
        {
            PenaltyType penaltyType = PenaltyType.None;

            if (this.gameHandler.isEligibleForPenalty())
            {
                penaltyType = this.isPenalty();
            }

            return penaltyType;                 
        }
        
        private PenaltyType isPenalty()
        {
            PenaltyType penaltyType = PenaltyType.None;

            this.rand = RandomSeed.Instance;            

            if (this.rand.Next(101) >= 92)
            {                
                penaltyType = (this.rand.Next(11) < 5) ? PenaltyType.Preplay : PenaltyType.Postplay;
            }
            return penaltyType;
        }
        
        /**
         *  Wrapper to fetch preplay penalties 
         */
        public Penalty getPreplayPenalty()
        {
            return this.getPenalty("preplay");
        }

        /**
         *  Wrapper to fetch postplay penalties 
         */
        public Penalty getPostplayPenalty()
        {
            Penalty gamePenalty;                      
            
            if ( this.gameHandler.IsIncomplete && this.rand.Next(11) < 5 )
            {
                Penalty piPenalty = new PassInterference(this.gameHandler);
                piPenalty.Name = "Pass Interference";
                piPenalty.FirstDown = true;
                piPenalty.Position = PlayerType.DB;
                gamePenalty = piPenalty;
            }
            else
            {
                gamePenalty = this.getPenalty("postplay");
            }
            return gamePenalty;
        }

        /**
         *  Returns penalty
         */ 
        private Penalty getPenalty(String key)
        {
            Penalty gamePenalty;                    
            int p = this.rand.Next(11);
            String pt = (p < 5) ? "offense" : "defense";
            List<Penalty> penaltyList = this.penaltyList[key][pt];
            int rp = this.rand.Next(penaltyList.Count);
            gamePenalty = penaltyList[rp];
            gamePenalty.OnOffense = (p < 5);
            
            return gamePenalty;
        }

        public void save()
        {
            this.rollbackObj.savePoint();
        }

        public void rollback()
        {
            this.rollbackObj.rollbackPlay();
        }

        /**
         *  Updates down and distance based on the penalty 
         */
        public void updateForPenalty(Penalty gamePenalty)
        {
            // store penalty yards because the yards changes based on the yardline (hence values can change between calls)                              
            int penaltyYards = gamePenalty.Yards;  

            if (gamePenalty.OnOffense)
            {
                this.gameHandler.updateYardline(penaltyYards * -1);
                this.gameHandler.Distance += penaltyYards;
            }
            else
            {
                int down = this.gameHandler.Down;
                this.gameHandler.updateYardline(penaltyYards);

                if (!this.gameHandler.updateForFirst(penaltyYards)) 
                {
                    this.gameHandler.Down = down;
                }
                
                // Force first down if penalty is automatic first down
                if (gamePenalty.FirstDown)
                {
                    this.gameHandler.firstDown();
                }                 
            }
        }

        /**
         *  Returns the player the penalty is on
         *  TODO:  Replace part of this with Starter object
         */ 
        public Player getPlayer(Penalty gamePenalty)
        {            
            List<Player> filtered = new List<Player>();
            Dictionary<PlayerType, List<Player>> starters = (gamePenalty.OnOffense) ? this.gameHandler.Offense.offStarters : this.gameHandler.Defense.defStarters;   

            // filter starter list to those in the penalty
            foreach (var item in starters.Keys.Where(item => (item.Equals(gamePenalty.Position))))
            {
                filtered.AddRange(starters[item]);
            }                    
         
            int rp = this.rand.Next(filtered.Count);
            return filtered[rp];
        }

        public void executePrePlayPenalty()
        {
            Penalty gamePenalty = this.getPreplayPenalty();
            Player penaltyPlayer = this.getPlayer(gamePenalty);
            this.display.displayPenalty(gamePenalty, penaltyPlayer);
            this.updateForPenalty(gamePenalty);
        }

        public void executePostPlayPenalty(int result)
        {
            Penalty gamePenalty = this.getPostplayPenalty();
            Player penaltyPlayer = this.getPlayer(gamePenalty);
            this.display.displayPenalty(gamePenalty, penaltyPlayer);

            // if penalty is on offense, always accept             
            // if penalty is on defense and result yardage is < penalty yardage, accept penalty.  Otherwise use result.
            if (this.shouldAcceptPenalty(gamePenalty, result))
            {
                this.display.displayDeclinePenalty();
            }
            else
            {
                this.rollback();
                this.display.displayAcceptPenalty();
                this.updateForPenalty(gamePenalty);
            }
        }

        private bool shouldAcceptPenalty(Penalty gamePenalty, int result)
        {
            return !gamePenalty.OnOffense && result >= gamePenalty.Yards;
        }
    }
}
﻿using Football.Library;
using Football.Sim.Games;

namespace Football.Sim.Penalties
{
    public class PassInterference : Penalty
    {
        private RandomSeed rand;
        private GameStateHandler gameHandler;

        public PassInterference(GameStateHandler gameHandler)
        {
            this.gameHandler = gameHandler;
        }

        public override int getYards()
        {
            if ( this.yards <= 0 ) // Holds the yardage for this penalty so that the numbers don't change on display
            {
                this.rand = RandomSeed.Instance;

                int distanceToGoal = this.gameHandler.DistanceToGoal / 2;
                int start = 15;

                if (start > distanceToGoal)
                {
                    start = distanceToGoal;
                }

                this.yards = this.rand.Next(start, distanceToGoal);
            }
            return this.yards;            
        }
    }
}

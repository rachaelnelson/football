﻿using System;
using Football.League.Players;

namespace Football.Sim.Penalties
{
    public abstract class Penalty
    {
        public String Name { get; set; }
        protected int yards = 0;        
        public bool FirstDown { get; set; }
        public PlayerType Position { get; set; }
        public bool OnOffense { get; set; }        

        public int Yards
        {
            get
            {
                return this.getYards();
            }
            set
            {
                this.yards = value;
            }
        }
        public abstract int getYards();
    }
}
   
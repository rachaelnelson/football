﻿
namespace Football.Sim.Clock
{      
    public class GameClock
    {           
        private byte second;
        private byte minute;
        private byte period;  

        public byte Period { get { return this.period; } }
        public byte Minutes { get { return this.getMinutes(); } }
        public byte Seconds { get { return this.getSeconds(); } }
            
        public ClockSettings clock { get; set; }
                
        public GameClock(ClockSettings clock)
        {
            this.clock = clock;            

            this.period = 1; 
            this.second = 0; 
            this.minute = 0;                       
        }

        public void incrementClock(byte increment)
        {
            this.second += increment;

            /* Increments minute by one if seconds exceeds 60;  resets seconds to 0  */
            if (this.second >= 60)
            {
                this.minute++;
                this.second = (byte)(this.second - 60);  //10-14-07 Fix rounding of seconds when over 60
            }

            /* Increments quarter when minutes over period length  */
            if (this.minute >= this.clock.PeriodLength)
            {
                this.period++;
                this.minute = 0;
                this.second = 0;
            }
        }

        /**
         *  Is beginning of second half? 
         */
        public bool isAfterIntermission()
        {
            return this.isIntermissionPeriod() && this.isBeginningOfPeriod();
        }

        /**
        *  Is beginning of game? 
        */
        public bool isBeginningOfGame()
        {
            return this.Period == 1 && this.isBeginningOfPeriod();
        }
        
        /* Calculates the minutes for the clock.  Returns Minutes  */
        private byte getMinutes()
        {
            if (this.second == 0)
            {
                return (byte)(this.clock.PeriodLength - this.minute);
            }
            else
            {
                return (byte)(this.clock.PeriodLength - 1 - this.minute);
            }
        }     

        /* Calculates the seconds for the clock.  Returns seconds  */
        private byte getSeconds()
        {          
            if (this.second == 0)
            {
                return this.second;
            }
            else
            {
                return (byte)(60 - this.second);
            }              
        }                                                          
       
        private bool isBeginningOfPeriod()
        {
            return this.Minutes == this.clock.PeriodLength;
        }
        
        private bool isIntermissionPeriod()
        {
            bool result = false;

            if( this.period-1 > 0 )
            {
                result = (this.period - 1) % this.clock.IntermissionInterval == 0;
            }

            return result;
        }

        /**
         *  Is beginning of OT? 
         */
        public bool isBeginningOfOvertime()
        {
            return this.isOvertime() && this.isBeginningOfPeriod();
        }

        public bool inOT()
        {           
           return this.Period > this.clock.NumberOfPeriods && this.Minutes < this.clock.PeriodLength;
        }
        
        private bool isOvertime()
        {
            return this.Period > this.clock.NumberOfPeriods;
        }
    }
}

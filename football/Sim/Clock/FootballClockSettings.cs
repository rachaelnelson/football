﻿
namespace Football.Sim.Clock
{
    public class FootballClockSettings : ClockSettings
    {               
        public FootballClockSettings()
            : base(Football.Config.ClockSettings.Default.PeriodLength, Football.Config.ClockSettings.Default.NumberOfPeriods, Football.Config.ClockSettings.Default.IntermissionInterval)
        {
                             
        }        
    }
}

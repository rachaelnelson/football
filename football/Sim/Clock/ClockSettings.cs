﻿
namespace Football.Sim.Clock
{
    public abstract class ClockSettings
    {
        private byte periodLength;
        private byte numberOfPeriods;
        private byte intermissionInterval;

        public byte IntermissionInterval { get { return this.intermissionInterval; } }
        public byte PeriodLength { get { return this.periodLength; } }
        public byte NumberOfPeriods { get { return this.numberOfPeriods; } }

        protected ClockSettings(byte periodLength, byte numberOfPeriods, byte intermissionInterval)
        {
            this.periodLength = periodLength;
            this.numberOfPeriods = numberOfPeriods;
            this.intermissionInterval = intermissionInterval;
        }
    }
}

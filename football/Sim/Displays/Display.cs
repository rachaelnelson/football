﻿using System;
using System.Collections.Generic;
using System.Text;
using Football.League.Players;
using Football.Sim.Games;
using Football.Sim.Penalties;
using Football.Sim.Settings;

namespace Football.Sim.Displays
{
    /* Command line output  */
    public class Display
    {       
        private bool enableLogging = false;
        private GameStateHandler gameHandler;
        public StringBuilder outputString {get;set;}

        private Dictionary<DownResult, Delegate> displayDownDelegate;        

        public Display(GameStateHandler gameHandler)
        {
            this.gameHandler = gameHandler;

            this.newLog();

            this.displayDownDelegate = new Dictionary<DownResult, Delegate>()
            {
                {DownResult.FirstDown,new Action(this.displayFirstDownDelegate)},                
                {DownResult.NextDown,new Action(this.displayNextDownDelegate)},   
                {DownResult.TD,new Action(this.displayTDDelegate)},   
                {DownResult.TOD,new Action(this.displayTODDelegate)},   
                {DownResult.TwoPoint,new Action(this.displayTwoPointDelegate)}   
            };  
        }

        public void newLog()
        {
            this.outputString = new StringBuilder();
        }

        /*
        public void finalStats()
        {
           this.outputString.AppendLine();
           this.outputString.AppendLine();
           this.outputString.AppendLine(String.Format("{0} run attempts: {1} {2} run yds: {3} ", g.homeTeam, s.getT1RAtt().ToString(), g.homeTeam, s.getT1RunYds().ToString()));
           this.outputString.AppendLine(String.Format("{0} pass attempts: {1} {2} pass completions: {3} pass yards: {4}", g.homeTeam, s.getT1PAtt().ToString(), g.homeTeam, s.getT1PComp().ToString(), s.getT1PassYds().ToString()));
           this.outputString.AppendLine(String.Format("{0} run attempts: {1} {2} run yds: {3} ", g.awayTeam, s.getT2RAtt().ToString(), g.awayTeam, s.getT2RunYds().ToString()));
           this.outputString.AppendLine(String.Format("{0} pass attempts: {1} {2} pass completions: {3} pass yards: {4}", g.awayTeam, s.getT2PAtt().ToString(), g.awayTeam, s.getT2PComp().ToString(), s.getT2PassYds().ToString()));		 
        }
         */
        
        public void qty()
        {
            int down = this.gameHandler.Down;
            int distance = this.gameHandler.Distance;
            int ydline = this.gameHandler.Yardline;

            string distanceString = distance.ToString();
            if (this.gameHandler.DistanceToGoal <= 10)
            {
                distanceString = "Goal";
            }

            // seconds formatting
            int u = this.gameHandler.Seconds;
            String us = u.ToString();

            us = u.ToString();
            if (u == 0)
            {
                us = us + "0";
            }
            else if (us.Length == 1)
            {
                us = "0" + us;
            }
            String output = "";
            this.outputString.AppendLine();
            String time = String.Format("Quarter {0} {1}:{2}", this.gameHandler.Quarter, this.gameHandler.Minutes, us);
            output = "\n" + output + time;
            this.outputString.AppendLine(time);
            String score = String.Format("{0} {1} {2} {3}",
                this.gameHandler.Offense.Team.Nickname,
                this.gameHandler.Offense.Score,
                this.gameHandler.Defense.Team.Nickname,
                this.gameHandler.Defense.Score);
            output = output + "\t" + score;
            this.outputString.AppendLine(score);
            String poss = String.Format("Poss: {0} \t {1} and {2} from {3} {4}",
                this.gameHandler.Offense.Team.Nickname,
                down,
                distanceString,
                this.gameHandler.Side,
                ydline);
            output = output + "\n"  + poss;
            this.display(poss);         
        }
        
        /**
         *   Displays punt code
         */        
        public void displayPunt(int result,List<Player> players)
        {
            String side = this.gameHandler.Side;
            int y = this.gameHandler.Yardline;        
            
            String place = (y < 0) ? "endzone" : y.ToString();                                            
            this.display( String.Format("Punt for {0} yds by {1} {2} to the {3} {4}",
                result,
                this.gameHandler.Offense.Team.Nickname,
                players[0].lastName,
                side,
                place) );            
        }
        
        /**
         *  Display Extra Point 
         */
        public void displayExtraPoint(bool result, List<Player> players)
        {
            String output = (result) ? "Extra point Good!" : "Missed extra point!";
            this.display(String.Format("\n{0} {1} {2}",
                this.gameHandler.Offense.Team.Nickname,
                players[0].lastName,
                output));
        }
        
        /**
         *  Display FG
         */        
        public void displayFG(bool result, int distance, List<Player> players)
        {
            String resultString = (result) ? "Made" : "Missed";
            this.display( String.Format("{0} {1} {2} a {3} yard FG!", 
                this.gameHandler.Offense.Team.Nickname,
                players[0].lastName,
                resultString, 
                distance.ToString()) );            
        }         

        /**
         *  Display Fumble 
         */        
        public void displayFumble(int kept)
        {            
            String recovered=null;
            String output=null;

            this.display( String.Format("Ball Fumbled!") );            
            
            if (kept < 2) // if not out of bounds
            {
                recovered = (kept == 0) ? this.gameHandler.Offense.Team.Name : this.gameHandler.Defense.Team.Name;
                output = String.Format("Recovered by {0} at {1} {2}", recovered, this.gameHandler.Side, this.gameHandler.Yardline);                
            }
            else // out of bounds
            {
                output = String.Format("Bounces out of bounds at {0} {1}", this.gameHandler.Side, this.gameHandler.Yardline);                
            }
            this.display(output);
        }        
        
        public void displaySafety()
        {
            this.display( String.Format("Safety!") );                        
        }                    

        public void displayRun(int result, List<Player> players)
        {
            this.display( String.Format("Run by {0} {1} for {2} yards.  Tackled by {3} {4}.", 
                this.gameHandler.Offense.Team.Nickname, 
                players[0].lastName, 
                result, 
                this.gameHandler.Defense.Team.Nickname, 
                players[1].lastName));                  
        }

        public void displayPass(int result, List<Player> players)
        {
            this.display( String.Format("Pass by {0} {1} for {2} yards to {3}.  Tackled by {4} {5}", 
                this.gameHandler.Offense.Team.Nickname,
                players[0].lastName, 
                result,
                players[1].lastName,
                this.gameHandler.Defense.Team.Nickname,
                players[2].lastName));            
        }

        public void displayKickoff(int result, List<Player> players, bool touchback)
        {
            string area = (touchback) ? "end zone" : this.gameHandler.Yardline.ToString();
            this.display( String.Format("\nKickoff of {0} yards by {1} {2} to the {3} {4}\n", 
                result, 
                this.gameHandler.Offense.Team.Nickname, 
                players[0].lastName, 
                this.gameHandler.Side, 
                area) );                   
        }                        
        
        public void displayReturn(int result)
        {
            this.display( String.Format("Return by {0} for {1} yards", this.gameHandler.Offense.Team.Nickname, result) );            
        }

        
        private void displayTurnoverOnDowns()
        {
            this.display( String.Format("Turnover on Downs!") );            
        }                
        
        public void displayInterception(){
            this.display( String.Format("Pass Intercepted at {0} {1}!", this.gameHandler.Side, this.gameHandler.Yardline) );            
        }
        
        public void displaySack(int result)
        {
            this.display( String.Format("Sack for {0} yards! ", result) );            
        }        

        public void displayEndOfGame()
        {
            this.display( String.Format("End of Game") );            
        }    

        public void displayIncompletePass()
        {
            this.display( String.Format("Incomplete Pass") );            
        }        

        public void displayFairCatch()
        {
            this.display( String.Format("Fair Catch") );            
        }

        public void displayTouchback()
        {
            this.display( String.Format("Touchback") );            
        }

        private void displayTouchdown()
        {
            this.display( String.Format("Touchdown!") );            
        }

        private void displayFirstDown()
        {
            this.display( String.Format("First Down!") );            
        }

        private void displayTwoPointConversion()
        {
            this.display( String.Format("Conversion {0}!", (this.gameHandler.IsTwoPointSuccess ? "Successful" : "Failed")) );            
        }

        private void displayYardline()
        {
            String output = String.Format("{0} ball on the {1} {2}", this.gameHandler.Offense.Team.Nickname, this.gameHandler.Side, this.gameHandler.Yardline.ToString());
            if (this.gameHandler.Side.Equals(" "))
            {
                output = String.Format("{0} ball on the {1}", this.gameHandler.Offense.Team.Nickname, this.gameHandler.Yardline.ToString());
            }
            this.display(output);
        }

        public void displayPenalty(Penalty gamePenalty, Player penaltyPlayer)
        {
            String team = (gamePenalty.OnOffense) ? this.gameHandler.Offense.Team.Abbr : this.gameHandler.Defense.Team.Abbr;
            this.display( String.Format("Penalty!  {0} on {1} of {2} for {3} yards", gamePenalty.Name, penaltyPlayer.lastName, team, gamePenalty.getYards()) );                              
        }

        public void displayAcceptPenalty()
        {            
            this.display( String.Format("Penalty Accepted") );            
        }

        public void displayDeclinePenalty()
        {
            this.display( String.Format("Penalty Declined") );            
        }

        public void displayInjury(String name, String injury, String duration)
        {
            this.display( String.Format("Injured Player.  {0} leaves with a {1} injury of {2} weeks",name, injury, duration) );            
        }

        private void display( String output )
        {
            if (this.enableLogging) { System.Console.Out.WriteLine(output); }
            this.outputString.AppendLine(output);
        }


        public void displayDownResult(DownResult downRes)
        {
            this.displayDownDelegate[downRes].DynamicInvoke();
        }

        private void displayFirstDownDelegate()
        {
            this.displayFirstDown();
            this.displayYardline();       
        }

        private void displayTwoPointDelegate()
        {
            this.displayTwoPointConversion();
        }

        private void displayTDDelegate()
        {
            this.displayTouchdown();
        }

        private void displayNextDownDelegate()
        {
            this.displayYardline();
        }

        private void displayTODDelegate()
        {
            this.displayYardline();
            this.displayTurnoverOnDowns();
        }
    }
}

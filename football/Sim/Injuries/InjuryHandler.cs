﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.League.Injuries;
using Football.League.Players;
using Football.Library;
using Football.Settings;
using Football.Sim.Displays;
using Football.Sim.Games;

namespace Football.Sim.Injuries
{    
    public class InjuryHandler
    {
        private RandomSeed rand;        
        private FootballSettings setting;
        private GameStateHandler gameHandler;
        private Display display;

        private int[] oddsArray;

        public InjuryHandler(FootballSettings setting, GameStateHandler gameHandler, Display display)
        {
            rand = RandomSeed.Instance;
            this.setting = setting;
            this.gameHandler = gameHandler;
            this.display = display;
            this.buildOddsList();
        }

        private void buildOddsList()
        {
            Dictionary<int, int> oddsDict = new Dictionary<int, int>()
            {
                {1,30}, // weeks, odd range
                {2,15},
                {3,15},
                {4,10},
                {5,5},
                {6,5},
                {7,5},
                {8,5},
                {9,5},
                {10,5}
            };        

            List<int> tempInjuryList = new List<int>();  
            this.oddsArray = new int[100];

            foreach (var item in oddsDict)
            {
                tempInjuryList.AddRange(Enumerable.Repeat<int>(item.Key, item.Value));
            }            
            this.oddsArray = tempInjuryList.ToArray();
        }

        public bool isInjury()
        {            
            int r = rand.Next(101);
            return (r < 2) ? true : false;   
        }

        public Player getInjuredPlayer(GameTeam Offense, GameTeam Defense)
        {
            PlayerInjury injury = this.getPlayerInjury();
            Player player = this.determinePlayer(Offense, Defense);            
            player.injury = injury;
            return player;
        }

        private PlayerInjury getPlayerInjury()
        {
            Injury injury = this.getInjury();
            int duration = this.getDuration();
            return new PlayerInjury( injury, duration );
        }

        private Injury getInjury()
        {
            string[] names = Enum.GetNames(typeof(Injury));            
            int randomEnum = rand.Next(names.Length-1);
            return (Injury)Enum.Parse(typeof(Injury), names[randomEnum]);                    
        }

        // Need to account for the num weeks remaining.
        private int getDuration()
        {
            int r = rand.Next(100);            
            return this.oddsArray[r];                       
        }

        private Player determinePlayer(GameTeam Offense, GameTeam Defense)
        {
            int r = rand.Next(101);
            Dictionary<PlayerType, List<Player>> starters = (r < 50) ? Offense.offStarters : Defense.defStarters;
           
            List<PlayerType> positionList = starters.Keys.ToList();            

            int injuredPosition = rand.Next(starters.Count-2); // randomly select injured position   (-2 removes K and P).. need to fix this...         
            List<Player> players = starters[positionList[injuredPosition]]; //  eewwww....

            int injuredIndex = rand.Next(players.Count); // randomly select injured player
            return players[injuredIndex];  // return selected player
        }
               
        /* 
         * Handles injuries for run and pass         
         */
        public void checkForInjury()
        {           
            if (this.setting.injuriesEnabled && this.isInjury())
            {
                Player player = this.getInjuredPlayer(this.gameHandler.Offense, this.gameHandler.Defense);
                this.display.displayInjury(player.lastName, player.injury.injury.ToString(), player.injury.duration.ToString());
                this.gameHandler.Offense.setStarters(setting.rosterList);
                this.gameHandler.Defense.setStarters(setting.rosterList);
            }        
        }
    }
}

﻿
namespace Football.Sim.Settings
{
    public enum PlayType { Pass, Run, Punt, FG, Kickoff, KickoffReturn, Return, PuntReturn, EOG, None, Safety, Touchback, ExtraPoint, TwoPoint }
    public enum ScoreType { FG, TD, Safety, XP, TwoPoint }
    public enum DownResult { TD, FirstDown, TOD, NextDown, TwoPoint }    
}

﻿using System.Collections.Generic;
using Football.League.Schedules;
using Football.League.Standings;
using Football.League.Teams;
using Football.Settings;
using Football.Sim.Games;

namespace Football.League.Playoffs
{
    public class PlayoffHandler
    {
        FootballSettings setting;

        public PlayoffHandler(FootballSettings setting)
        {
            this.setting = setting;
        }

        public bool isStartOfPlayoffs(byte week)
        {
            return (week == 18) ? true : false;
        }
        
        public bool isPlayoffs(int week)
        {
            return (week >= 18) ? true : false;
        }          

        public void setupBracket()
        {
            // Create playoff bracket           
            if (setting.thisWeek == 18 && setting.PlayoffBracket.Count==0)
            {
                Playoff playoff = new Playoff(new FootballStandings(setting.teamRecordList), setting.schedule, setting.teamRecordList, setting.leagueStructure);
                List<PlayoffTeam> conf1 = playoff.getPlayoffTeams(setting.leagueStructure.getConferences()[0]);
                List<PlayoffTeam> conf2 = playoff.getPlayoffTeams(setting.leagueStructure.getConferences()[1]);
                setting.PlayoffBracket.Add(new FootballPlayoffBracket(conf1, setting.rosterList,setting.schedule));
                setting.PlayoffBracket.Add(new FootballPlayoffBracket(conf2, setting.rosterList,setting.schedule));
            }
        }

        private void generateGames()
        {
            // add games for the week to the schedule
            if (setting.thisWeek >= 18 && setting.PlayoffBracket.Count>0)
            {
                // grab team matchups
                List<PlayoffGame> conf1Round = setting.PlayoffBracket[0].getRoundGames();
                List<PlayoffGame> conf2Round = setting.PlayoffBracket[1].getRoundGames();

                // add games to schedule
                foreach (var item in conf1Round)
                {
                    Game game = item.getGame();
                    ScheduleItem schedItem = new ScheduleItem();
                    schedItem.awayTeam = game.awayTeam.Team;
                    schedItem.homeTeam = game.homeTeam.Team;
                    setting.schedule.addScheduleItem(setting.thisWeek.ToString(), schedItem);
                }

                foreach (var item in conf2Round)
                {
                    Game game = item.getGame();
                    ScheduleItem schedItem = new ScheduleItem();
                    schedItem.awayTeam = game.awayTeam.Team;
                    schedItem.homeTeam = game.homeTeam.Team;
                    setting.schedule.addScheduleItem(setting.thisWeek.ToString(), schedItem);
                }
            }
        }

        private void generateSuperBowl()
        {
            // grab champions from both brackets and play
            PlayoffTeam conf1Team = setting.PlayoffBracket[0].getBracketWinner();
            PlayoffTeam conf2Team = setting.PlayoffBracket[1].getBracketWinner();

            Team homeTeam = conf2Team.Team;
            Team awayTeam = conf1Team.Team;

            // TODO:  probably need to account for same seeds, but that's a nice to have right now
            if(conf1Team.Seed <= conf2Team.Seed)
            {
                homeTeam = conf1Team.Team;
                awayTeam = conf2Team.Team;
            }                   

            Game game = new Game(homeTeam,awayTeam,setting.rosterList);
            ScheduleItem schedItem = new ScheduleItem();
            schedItem.awayTeam = game.awayTeam.Team;
            schedItem.homeTeam = game.homeTeam.Team;
            setting.schedule.addScheduleItem(setting.thisWeek.ToString(), schedItem);
        }    

        public void nextRound(Schedule schedule)
        {
            // Inits bracket
            if (this.isStartOfPlayoffs(setting.thisWeek))
            {
                this.setupBracket();
                this.generateGames(); // generates the games
            }
            else if (setting.thisWeek >= 19 && setting.thisWeek <= 20) // Round 1 = 19, Round 2 = 20, Round 3 = 21, Round 4 = 22
            {
                setting.PlayoffBracket[0].nextRound();
                setting.PlayoffBracket[1].nextRound();
                this.generateGames(); // generates the games
            }
            else if (setting.thisWeek == 21) // super bowl
            {               
                this.generateSuperBowl();
            }            
        }
    }
}

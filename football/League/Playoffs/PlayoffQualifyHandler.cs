﻿using System.Collections.Generic;
using Football.League.Conferences;
using Football.League.Playoffs.PlayoffHandlers;
using Football.League.Schedules;
using Football.League.Teams;
using Football.League.Teams.TeamRecord;

namespace Football.League.Playoffs
{
    public class PlayoffQualifyHandler
    {
        private LinkedList<IPlayoffHandler> handlers;

        public PlayoffQualifyHandler(Schedule schedule, TeamRecordList teamRecordList, Structure leagueStructure)
        {           
            this.handlers = this.initHandlers(schedule, teamRecordList, leagueStructure);                
        }

        // http://sports.espn.go.com/nfl/news/story?page=tiebreakers
        // http://boards.straightdope.com/sdmb/showthread.php?t=231843
        private LinkedList<IPlayoffHandler> initHandlers(Schedule schedule, TeamRecordList teamRecordList,Structure leagueStructure)
        {            
            LinkedListNode<IPlayoffHandler> bestRecord = new LinkedListNode<IPlayoffHandler>(new BestRecordHandler(teamRecordList));
            LinkedListNode<IPlayoffHandler> headToHead = new LinkedListNode<IPlayoffHandler>(new HeadToHeadHandler(schedule));
            LinkedListNode<IPlayoffHandler> divisionRecord = new LinkedListNode<IPlayoffHandler>(new DivisionRecordHandler(leagueStructure, teamRecordList));
            LinkedListNode<IPlayoffHandler> commonGames = new LinkedListNode<IPlayoffHandler>(new CommonGamesHandler(schedule));
            LinkedListNode<IPlayoffHandler> conferenceRecord = new LinkedListNode<IPlayoffHandler>(new ConferenceRecordHandler(leagueStructure, teamRecordList));
            LinkedListNode<IPlayoffHandler> randomTeam = new LinkedListNode<IPlayoffHandler>(new RandomTeamHandler());

            LinkedList<IPlayoffHandler> list = new LinkedList<IPlayoffHandler>();
            list.AddFirst(bestRecord);
            list.AddAfter(bestRecord, headToHead);
            list.AddAfter(headToHead, divisionRecord);
            list.AddAfter(divisionRecord, commonGames);
            list.AddAfter(commonGames, conferenceRecord);
            list.AddAfter(conferenceRecord, randomTeam);  

            return list;
        }       

        private Team getWinner(List<Team> teams, LinkedListNode<IPlayoffHandler> node = null)
        {    
            if (teams.Count == 1 || node==null)
            {                
                return teams[0];
            }

            int origCount = teams.Count;
            teams = node.Value.handleRequest(teams);
            
            // Resets so the 2 can go through the handlers again, per the NFL tie breaking rules.
            if (teams.Count == 2 && teams.Count < origCount)
            {
                node = this.resetList();
                origCount = teams.Count;
            }           
            else
            {
                node = node.Next;
            }
            
            return getWinner( teams, node );            
        }

        public Team getWinner(List<Team> teams)
        {
            return this.getWinner(teams, this.handlers.First);
        }
        
        private LinkedListNode<IPlayoffHandler> resetList()
        {
            return this.handlers.First;
        }
    }
}

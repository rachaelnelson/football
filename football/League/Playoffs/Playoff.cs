﻿using System.Collections.Generic;
using System.Linq;
using Football.League.Conferences;
using Football.League.Playoff.Comparer;
using Football.League.Schedules;
using Football.League.Standings;
using Football.League.Teams;
using Football.League.Teams.TeamRecord;


namespace Football.League.Playoffs
{
    public class Playoff
    {       
        private PlayoffQualifyHandler handler;
        private FootballStandings standings;

        public Playoff( FootballStandings standings, Schedule schedule, TeamRecordList teamRecordList, Structure leagueStructure )
        {        
               this.handler = new PlayoffQualifyHandler(schedule, teamRecordList, leagueStructure);
               this.standings = standings;  
        }
        
         /**
         *   Gets the playoff teams by conference            
         */ 
        public List<PlayoffTeam> getPlayoffTeams(Conference conference)
        {
            List<Team> divisionWinners = this.getDivisionWinners( conference );
            List<Team> wildCardWinners = this.getWildCardTeams( conference, divisionWinners );            
            
            List<PlayoffTeam> playoffTeams = this.seedTeams( divisionWinners, wildCardWinners );

            return playoffTeams;                                      
        }

        private List<PlayoffTeam> seedTeams( List<Team> divisionWinners, List<Team> wildCardWinners )
        {
            List<PlayoffTeam> playoffTeams = new List<PlayoffTeam>();
            byte seed = 1;

            // seed division winners
            while (divisionWinners.Count > 0)
            {
                Team winner = this.handler.getWinner(divisionWinners);
                playoffTeams.Add( new PlayoffTeam(winner, seed) );
                divisionWinners.Remove( winner );
                seed++;
            }

            // seed wild card teams
            while (wildCardWinners.Count > 0)
            {
                Team winner = this.handler.getWinner( wildCardWinners );
                playoffTeams.Add(new PlayoffTeam(winner, seed));
                wildCardWinners.Remove(winner);
                seed++;
            }

            return playoffTeams;
        }

        private List<Team> getWildCardTeams( Conference conference, List<Team> divisionWinners )
        {
            //  (1)  get wildcard teams               

            List<Team> wildcard = new List<Team>();
            List<Team> allConferenceTeams = this.standings.getConference(conference);
            List<Team> remainingConfTeams = allConferenceTeams.Except<Team>(divisionWinners, new TeamListExceptComparer()).ToList<Team>();
            // may want to filter further to only include the top n teams with records either exactly the same, or the next best win records

            // inefficient but should work.  look to make this slicker, such as sorting by record.  Actually...items should already be sorted with best record on top.

            while (wildcard.Count < 2)
            {
                Team winner = this.handler.getWinner(remainingConfTeams);
                remainingConfTeams.Remove(winner);
                wildcard.Add(winner);                
            }

            return wildcard;
        }

        private List<Team> getDivisionWinners( Conference conference )
        {
            // loop through divisions and grab the division teams
            // determine the div winner                                   
           
            List<Team> winners = new List<Team>();

            foreach (var division in conference.Divisions)
            {                                                   
                    winners.Add(this.handler.getWinner(division.Teams));                     
            }

            return winners;
        }
    }
}

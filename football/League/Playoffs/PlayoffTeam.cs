﻿using Football.League.Teams;

namespace Football.League.Playoffs
{
    public class PlayoffTeam
    {
        private Team team;
        public Team Team  { get { return this.team; } }

        private byte seed;
        public byte Seed { get { return this.seed; } }

        public PlayoffTeam(Team team, byte seed)
        {
            this.team = team;
            this.seed = seed;
        }
    }
}

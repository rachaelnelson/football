﻿using System;
using System.Collections.Generic;
using Football.League.Teams;

namespace Football.League.Playoff.Comparer
{
    public class TeamListExceptComparer:IEqualityComparer<Team>
    {
        public bool Equals(Team x, Team y)
        {
            //Check whether the compared objects reference the same data. 
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null. 
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                return false;

            //Check whether the teams' properties are equal. 
            return x.Name == y.Name && x.Name == y.Name;
        }

        public int GetHashCode(Team team)
        {
            //Check whether the object is null 
            if (Object.ReferenceEquals(team, null)) return 0;

            //Get hash code for the Name field if it is not null. 
            int hashTeamName = team.Name == null ? 0 : team.Name.GetHashCode();
            
            //Calculate the hash code for the team. 
            return hashTeamName * 27;
        }
    }
}

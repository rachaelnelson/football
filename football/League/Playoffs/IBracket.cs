﻿using System.Collections.Generic;

namespace Football.League.Playoffs
{
    interface IBracket
    {
        //void createBracket();

        List<PlayoffGame> getRoundGames();

        void nextRound();
    }
}

﻿using System;
using System.Collections.Generic;

namespace Football.League.Playoffs
{
    /**
     *  A playoff round
     */ 
    public class Round
    {
        private String roundName;
        private List<PlayoffGame> roundGames;

        public List<PlayoffGame> RoundGames { get { return this.roundGames; } }
        public String RoundName { get { return roundName; } }        

        public Round(String roundName, List<PlayoffGame> roundGames)
        {
            this.roundName = roundName;
            this.roundGames = roundGames;
        }        
    }
}

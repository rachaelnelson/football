﻿using System;
using System.Collections.Generic;
using Football.League.Teams;
using Football.Sim.Games;

namespace Football.League.Playoffs
{
    public class PlayoffGame
    {
        private PlayoffTeam home;
        private PlayoffTeam away;
        private Game game;

        public PlayoffTeam Away { get { return this.away; } }
        public PlayoffTeam Home { get { return this.home; } }

        public PlayoffGame(PlayoffTeam home, PlayoffTeam away, Dictionary<String, Roster> rosterList)
        {
            this.home = home;
            this.away = away;

            this.game = new Game(home.Team,away.Team,rosterList);
        }
        
        public Game getGame()
        {
            return this.game;
        }
    }
}

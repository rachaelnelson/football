﻿using System.Collections.Generic;
using Football.League.Teams;
using Football.Library;

namespace Football.League.Playoffs.PlayoffHandlers
{
    public class RandomTeamHandler : IPlayoffHandler
    {
        private RandomSeed rand;

        public List<Team> handleRequest(List<Team> teams)
        {
            return this.getRandomTeam(teams);
        }

        public List<Team> getRandomTeam(List<Team> teams)
        {
            this.rand = RandomSeed.Instance;           
            int rt = this.rand.Next(teams.Count);

            return new List<Team>(){teams[rt]};         
        }
    }
}

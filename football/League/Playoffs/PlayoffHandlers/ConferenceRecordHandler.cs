﻿using System.Collections.Generic;
using Football.League.Conferences;
using Football.League.Teams;
using Football.League.Teams.TeamRecord;

namespace Football.League.Playoffs.PlayoffHandlers
{
    public class ConferenceRecordHandler : IPlayoffHandler
    {
        private Structure leagueStructure;
        private TeamRecordList teamRecordList;

        public ConferenceRecordHandler(Structure leagueStructure, TeamRecordList teamRecordList)
        {            
            this.leagueStructure = leagueStructure;
            this.teamRecordList = teamRecordList;
        }

        public List<Team> handleRequest(List<Team> teams)
        {             
            return this.getConferenceRecord(teams);
        }

        public List<Team> getConferenceRecord( List<Team> teams )
        {
            // verify all teams have the same division

            Team team1 = null;

            if (teams.Count > 1)
            {
                team1 = teams[0];
            }
            else
            {
                return teams;
            }

            foreach (var team in teams)
            {
                if (!leagueStructure.areTeamsinSameConference(team1, team))
                {
                    return teams;
                }
            }

            List<Team> conferenceList = new List<Team>();
            int points = 0;

            foreach (var team in teams)
            {
                TeamRecord teamRecord = this.teamRecordList.GetTeamRecord(team);
                int teamPoints = teamRecord.ConferenceRecord.getPoints();

                if (teamPoints > points)
                {
                    points = teamPoints;
                    conferenceList.Clear();
                    conferenceList.Add(team);
                }
                else if (teamPoints == points)
                {
                    conferenceList.Add(team);
                }
            }

            return conferenceList;
        }

    }
}

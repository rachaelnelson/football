﻿using System.Collections.Generic;
using Football.League.Schedules;
using Football.League.Teams;

namespace Football.League.Playoffs.PlayoffHandlers
{
    public class HeadToHeadHandler : IPlayoffHandler
    {
        private Schedule schedule;

        public HeadToHeadHandler( Schedule schedule )
        {
            this.schedule = schedule;
        }


        // http://java.dzone.com/articles/design-patterns-uncovered-chain-of-responsibility
        public List<Team> handleRequest(List<Team> teams)
        {
            // first check for one team with best record
            // if more than one, follow team tie breaker rules
            // TODO:  need to calculate head to head and within division/conference records
            
            return this.getHeadToHead(teams);            
        }

        /**
        *   Returns the teams left after applying head to head         
         *  TODO:  Verify each team played the others because this looks more like a divisional check comparison.
         *  Actually, it's incorrect for divisional too.
        */
        private List<Team> getHeadToHead(List<Team> teams)
        {
            List<Team> headToHeadWinners = new List<Team>();
            int highestWinningPoints = 0;

            // Only compare two teams.  Otherwise, return for other handlers to figure out.
            if( teams.Count>2 )
            {
                return teams;
            }

            // Compare each team in the division
            foreach (Team team in teams)
            {                              
                int points = this.getHeadtoHeadWinningPoints(team, teams);
                
                if (highestWinningPoints < points)
                {
                    highestWinningPoints = points;
                    headToHeadWinners.Clear();
                    headToHeadWinners.Add(team);
                }
                else if (highestWinningPoints == points)
                {
                    headToHeadWinners.Add(team);
                }

            }
            return headToHeadWinners;
        }

        /**
         *  Calculates head to head winning points            
         */
        private int getHeadtoHeadWinningPoints(Team team, List<Team> division)
        {            
            List<Team> opponents = new List<Team>();

            // build opponents list
            foreach (var item in division)
            {
                if (!item.Abbr.Equals(team.Abbr))
                {
                    opponents.Add(item);
                }
            }

            int wins = 0;            
            int ties = 0;            
             
            // GB - CHI
            // Grab team's schedule and look for the opponents in the division
            foreach ( var item in this.schedule.getScheduleByTeam(team.Abbr) )
            {
                // TODO:  refactor and simplify                                
                if (item.Value!=null && (opponents.Exists(t => t.Abbr.Equals(item.Value.homeTeam.Abbr)) || opponents.Exists(t => t.Abbr.Equals(item.Value.awayTeam.Abbr))))     
                {
                    if (item.Value.getWinner().Abbr.Equals(team.Abbr))
                    {
                        wins++;
                    }
                    else if (item.Value.getWinner() == null)
                    {
                        ties++;
                    }
                }
            }

            // TODO:  see if there's a way to reuse the formula in TeamRecord
            return (wins * 2) + (ties);            
        }       
    }
}

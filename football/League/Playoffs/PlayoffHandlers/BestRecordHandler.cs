﻿using System.Collections.Generic;
using Football.League.Teams;
using Football.League.Teams.TeamRecord;

namespace Football.League.Playoffs.PlayoffHandlers
{
    public class BestRecordHandler : IPlayoffHandler
    {
        private TeamRecordList teamRecordList;

        public BestRecordHandler( TeamRecordList teamRecordList )
        {
            this.teamRecordList = teamRecordList;
        }    

        // http://java.dzone.com/articles/design-patterns-uncovered-chain-of-responsibility
        public List<Team> handleRequest(List<Team> teams)
        {
            // first check for one team with best record
            // if more than one, follow team tie breaker rules
            // TODO:  need to calculate head to head and within division/conference records           

            return this.getBestRecord(teams);                     
        }


        /**
         *  Returns the team(s) with the best record in the division 
         */
        private List<Team> getBestRecord(List<Team> division)
        {
            int maxPoints = 0;
            List<Team> bestRecordList = new List<Team>();

            //TeamRecordHandler recordHandler = new TeamRecordHandler();            

            foreach (Team team in division)
            {
                TeamRecord record = this.teamRecordList.GetTeamRecord(team);
                int teamPoints = record.OverallRecord.getPoints();
                
                if (teamPoints > maxPoints)
                {
                    bestRecordList.Clear();
                    bestRecordList.Add(team);
                    maxPoints = teamPoints;
                }
                else if (teamPoints == maxPoints)
                {
                    bestRecordList.Add(team);
                }                
            }
            return bestRecordList;
        }        
    }
}

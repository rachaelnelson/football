﻿using System.Collections.Generic;
using Football.League.Schedules;
using Football.League.Teams;
using Football.League.Teams.TeamRecord;

namespace Football.League.Playoffs.PlayoffHandlers
{
    public class CommonGamesHandler : IPlayoffHandler
    {
        private Schedule schedule;

        public CommonGamesHandler(Schedule schedule)
        {
            this.schedule = schedule;
        }

        public List<Team> handleRequest(List<Team> teams)
        {
            List<HashSet<Team>> teamSetList = this.getTeamSets(teams);
            HashSet<Team> commonTeams = this.getCommonGames(teamSetList, teamSetList[teamSetList.Count - 1]);

            // Find the games played for each team the from the commonTeams list and sum up a record.  
            // Use points to figure out, because a team could play a common opponent more than once while the other teams only played that opponent once
            return this.getCommonGamesWinner( teams, commonTeams );
        }
       
        private List<HashSet<Team>> getTeamSets(List<Team> teams)
        {           
            List<HashSet<Team>> teamSets = new List<HashSet<Team>>(); // may need to make this an array instead

            foreach( Team team in teams )
            {
                teamSets.Add( this.schedule.getScheduleSet(team.Abbr) );                
            }

            return teamSets;
        }

        /* Returns a set of the common opponents
          
           (1) all team sets - need to loop through
           (2) set to compare against
           (3) final set
         */
        private HashSet<Team> getCommonGames( List<HashSet<Team>> teamSetList, HashSet<Team> teamSet )
        {
            // if all teams have been looped through, or there are no matches in the teamset, return
            if ( teamSetList.Count == 1 || teamSet.Count == 0)
            {
                return teamSet;
            }

            // removes current item passed in.
            teamSetList.Remove(teamSetList[teamSetList.Count - 1]);

            // grabs new end of list and intersects it with the current list.
            teamSet.IntersectWith(teamSetList[teamSetList.Count - 1]);

            return this.getCommonGames(teamSetList, teamSet);           
        }

        private List<Team> getCommonGamesWinner( List<Team> teams, HashSet<Team> commonTeams )
        {
            // if no common games among the teams...
            if (commonTeams.Count == 0)
            {
                return teams;
            }

            Dictionary<Team, Record> teamCommonRecords = new Dictionary<Team, Record>();

            // nasty loops but works....
            foreach (Team team in teams)
            {
                teamCommonRecords.Add(team, new Record());

                foreach (Team opponent in commonTeams)
                {
                    Record gameRecord = schedule.getRecordByOpponent(team, opponent);
                    
                    teamCommonRecords[team].addWin(gameRecord.getWin());
                    teamCommonRecords[team].addLoss(gameRecord.getLoss());
                    teamCommonRecords[team].addTie(gameRecord.getTie());
                }
            }
                        
            // Uses winning percentage in case one of the teams has played the opponent more than once whereas the other teams only played one game
            float winPerc = 0.0F;
            List<Team> commonGamesWinner = new List<Team>();

            foreach( KeyValuePair<Team,Record> item in teamCommonRecords )
            {
                if (item.Value.getWinPercentage() > winPerc)
                {
                    commonGamesWinner.Clear();
                    commonGamesWinner.Add(item.Key);
                }
                else if (item.Value.getWinPercentage() == winPerc)
                {
                    commonGamesWinner.Add(item.Key);
                }
            }

            return commonGamesWinner;
        }   
    }
}

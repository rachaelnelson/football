﻿using System.Collections.Generic;
using Football.League.Teams;

namespace Football.League.Playoffs.PlayoffHandlers
{
    public interface IPlayoffHandler
    {     
        // http://java.dzone.com/articles/design-patterns-uncovered-chain-of-responsibility
        List<Team> handleRequest(List<Team> teams);
    }
}

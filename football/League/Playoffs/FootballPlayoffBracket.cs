﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.League.Schedules;
using Football.League.Teams;

namespace Football.League.Playoffs
{
    [Serializable()]
    public class FootballPlayoffBracket : IBracket
    {
        // List of teams with seeding
        private List<PlayoffTeam> playoffTeams;

        // Holds games indexed by round
        private Dictionary<int, Round> playoffGames;
        private Dictionary<String, Roster> rosterList;
        private Schedule schedule;

        private int currentRound;

        public FootballPlayoffBracket(List<PlayoffTeam> playoffTeams, Dictionary<String, Roster> rosterList, Schedule schedule)
        {
            this.currentRound = 1;
            this.playoffTeams = playoffTeams;
            this.rosterList = rosterList;
            this.schedule = schedule;
            this.playoffGames = this.buildBracket(); 
        }

        public List<PlayoffGame> getRoundGames()
        {
            return this.playoffGames[currentRound].RoundGames;
        }

        public void nextRound()
        {
            List<PlayoffTeam> winners = this.getWinners();

            if (currentRound == 1) // Before division round
            {
                this.createDivisionRound(winners);
            }
            else if (currentRound == 2)
            {
                this.createConferenceRound(winners);  // before conference round
            }           

            this.currentRound++;
        }

        // Specific logic...may need to move elsewhere
        private void createDivisionRound(List<PlayoffTeam> winners)
        {
            List<PlayoffGame> divisionGames = new List<PlayoffGame>();

            if (winners[0].Seed > winners[1].Seed)
            {
                divisionGames.Add(new PlayoffGame(this.getSeed(1), this.getSeed(winners[0].Seed), rosterList));
                divisionGames.Add(new PlayoffGame(this.getSeed(2), this.getSeed(winners[1].Seed), rosterList));
            }
            else
            {
                divisionGames.Add(new PlayoffGame(this.getSeed(1), this.getSeed(winners[1].Seed), rosterList));
                divisionGames.Add(new PlayoffGame(this.getSeed(2), this.getSeed(winners[0].Seed), rosterList));
            }

            Round divisionRound = new Round("Division", divisionGames);
            this.playoffGames[2] = divisionRound;           
        }

        private void createConferenceRound(List<PlayoffTeam> winners)
        {
            List<PlayoffGame> conferenceGames = new List<PlayoffGame>();
                       
            if (winners[0].Seed < winners[1].Seed)
            {
                conferenceGames.Add(new PlayoffGame(this.getSeed(winners[0].Seed), this.getSeed(winners[1].Seed), rosterList));   
            }
            else
            {
                conferenceGames.Add(new PlayoffGame(this.getSeed(winners[1].Seed), this.getSeed(winners[0].Seed), rosterList)); 
            }

            Round conferenceRound = new Round("Conference", conferenceGames);
            this.playoffGames[3] = conferenceRound;                        
        }

        // awful convaluted mess...
        private List<PlayoffTeam> getWinners()
        {
            List<PlayoffTeam> winners = new List<PlayoffTeam>();

            List<ScheduleItem> sched = schedule.getScheduleByWeek((17 + currentRound).ToString());

            List<PlayoffGame> roundGames = this.getRoundGames();

            foreach (var game in roundGames)
            {
                foreach (var item in sched)
                {
                    // checks if games match
                    if(game.getGame().awayTeam.Team.Equals(item.awayTeam))
                    {
                        Team winner = item.getWinner();

                        if (winner.Abbr.Equals(game.getGame().awayTeam.Team.Abbr))
                        {
                            winners.Add(game.Away);
                        }
                        else if (winner.Abbr.Equals(game.getGame().homeTeam.Team.Abbr))
                        {
                            winners.Add(game.Home);
                        }
                        break;
                    }
                }
            }
            
            return winners;
        }

        public PlayoffTeam getBracketWinner()
        {
            if (this.currentRound == 3) // if superbowl
            {
                return this.getWinners()[0];
            }
            else
            {
                return null;
            }            
        }

        private Dictionary<int, Round> buildBracket()
        { 
            // WildCard
            List<PlayoffGame> wildcardGames = new List<PlayoffGame>();
            wildcardGames.Add(new PlayoffGame(this.getSeed(3), this.getSeed(6), rosterList));

            // WildCard
            wildcardGames.Add(new PlayoffGame(this.getSeed(4), this.getSeed(5), rosterList));
            
            Round wildCardRound = new Round( "WildCard", wildcardGames );            

            return new Dictionary<int,Round>() {
                { 1, wildCardRound }                
            };            
        }

        private PlayoffTeam getSeed(int seed)
        {
            //return (PlayoffTeam)this.playoffTeams.Where<PlayoffTeam>(item => item.Seed == seed);
            return this.playoffTeams.Where<PlayoffTeam>(item => item.Seed == seed).ToList<PlayoffTeam>()[0];
        }
    }
}

﻿
namespace Football.League.Injuries
{
    public class InjuryView
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string Injury { get; set; }
        public int InjuryDuration { get; set; }
        public bool IsOnIR { get; set; }
    }
}

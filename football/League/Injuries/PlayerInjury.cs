﻿using System;

namespace Football.League.Injuries
{
    [Serializable()]
    public class PlayerInjury
    {
        public Injury injury { get; set; }
        public int duration { get; set; }

        public PlayerInjury(Injury inj = Injury.None, int dur = 0)
        {
            this.injury = inj;
            this.duration = dur;
        }
    }
}

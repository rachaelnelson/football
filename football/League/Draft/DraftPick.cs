﻿/*
using Football.League.Teams;
namespace Football.League.Draft
{
    public class DraftPick
    {        
        public int Year { get; private set; }
        public int Round { get; private set; }
        public Team OriginalTeam { get; private set; }
        public Team HoldingTeam { get; private set; }

        // Could add player drafted here or link player to this

        public DraftPick(int year, int round, Team oTeam)
        {
            Year = year;
            Round = round;
            OriginalTeam = oTeam;
            HoldingTeam = oTeam;
        }

        // needed for making pick when a trade has occurred
        public DraftPick(int year, int round, Team oTeam, Team hTeam)
        {
            Year = year;
            Round = round;
            OriginalTeam = oTeam;
            HoldingTeam = hTeam;
        }        
    }
}
*/
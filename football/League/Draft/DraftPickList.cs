﻿using System.Collections.Generic;
using System.Linq;

using Microsoft.FSharp.Core;
using Microsoft.FSharp.Collections;

namespace Football.League.Draft
{    
    public class DraftPickList
    {
        private List<DraftPicks.DraftPick> picklist;
        
        public DraftPickList(List<DraftPicks.DraftPick> list)
        {
            picklist = list;
        }

        public DraftPickList()
        {
            picklist = new List<DraftPicks.DraftPick>();
        }    
        
        // TODO: test if this works ok
        public void UpdateHoldingTeam(string currentTeam, string newTeamAbbr, List<DraftPicks.DraftPick> picksToUpdate)   
        {           
            picklist = DraftPicks.UpdateHoldingTeam(newTeamAbbr, ListModule.OfSeq(picksToUpdate), ListModule.OfSeq(picklist)).ToList();
        }        

        // Appends new draft year to existing list
        public void AddNewDraftYear( int year, List<string> teams, int increment = 1 )
        {
            picklist = DraftPicks.AddNewDraftYear( year, teams.AsEnumerable<string>(), ListModule.OfSeq<DraftPicks.DraftPick>(picklist), increment ).ToList();
        }

        public List<DraftPicks.DraftPick> GetPicksByTeam( string teamAbbr )
        {
            return DraftPicks.GetPicksByTeam( teamAbbr, ListModule.OfSeq<DraftPicks.DraftPick>(picklist) ).ToList();
        }

        public DraftPicks.DraftPick GetPickById(string id)
        {
            return DraftPicks.GetPickById(id, ListModule.OfSeq<DraftPicks.DraftPick>(picklist));
        }
    }    
}

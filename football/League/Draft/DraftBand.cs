﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Football.League.Draft
{
    public class PlayerDraftBand
    {
        public decimal LowerBound { get; set; }
        public decimal UpperBound { get; set; }
        public int Round { get; set; }        
    }
}

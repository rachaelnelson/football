﻿using Football.League.Players;
using Football.League.PlayerValue.BaseValue;

namespace Football.League.PlayerValue
{
    public class QBBaseValue : IBaseValue
    {
        public decimal GetBaseValue(Player player)
        {
            /*
             *  Accuracy 8
             *  Distance 8
             *  Awareness 8
             *  Speed 8
             *  
             * Total:  8
             

            
             *  Accuracy 7    2.1
             *  Distance 6    1.2
             *  Awareness 8   3.2  
             *  Speed 5        .5  
             *  
             *  Total:  7
             *  
             *  Accuracy 5    1.5 
             *  Distance 8    1.6
             *  Awareness 3   1.2  
             *  Speed 7        .7    
             *  
             *  Total:   5
             * 
             * 
             *  It's obvious the first player will have a much bigger impact, but his score is only 1 grade higher.  This means there should be
             *  some sort of logarithmic/exponential scale for each leap in grade.  
             *  
             *  Might need to make these percentages system dependent too
             */

            // Base scale                      
            return (decimal)((player.awareness * .4) + (player.accuracy * .3) + (player.distance * .2) + (player.speed * .1));   
        }

        public int GetBaseValuePoints(Player player)
        {
            return player.awareness + player.accuracy + player.distance + player.speed;
        }
    }
}

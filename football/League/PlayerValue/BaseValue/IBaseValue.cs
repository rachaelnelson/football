﻿using Football.League.Players;

namespace Football.League.PlayerValue.BaseValue
{
    interface IBaseValue
    {
        decimal GetBaseValue(Player player);

        int GetBaseValuePoints(Player player);
    }
}

﻿using Football.League.Players;

namespace Football.League.PlayerValue.BaseValue
{
    public class RBBaseValue : IBaseValue
    {
        public decimal GetBaseValue(Player player)
        {
            return (decimal)((player.awareness * .4) + (player.speed * .3) + (player.receiving * .2) + (player.passBlocking * .1));
        }

        public int GetBaseValuePoints(Player player)
        {
            return player.awareness + player.speed + player.receiving + player.passBlocking;
        }
    }
}

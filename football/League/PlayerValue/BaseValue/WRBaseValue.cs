﻿using Football.League.Players;

namespace Football.League.PlayerValue.BaseValue
{
    public class WRBaseValue : IBaseValue
    {
        public decimal GetBaseValue(Player player)
        {
            return (decimal)((player.awareness * .3) + (player.receiving * .3) + (player.speed * .3) + (player.runBlocking * .1));
        }

        public int GetBaseValuePoints(Player player)
        {
            return player.awareness + player.receiving + player.speed + player.runBlocking;
        }
    }
}

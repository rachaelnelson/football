﻿using Football.League.Players;

namespace Football.League.PlayerValue.BaseValue
{
    public class KPBaseValue : IBaseValue
    {
        public decimal GetBaseValue(Player player)
        {
            return (decimal)((player.awareness * .4) + (player.distance * .3) + (player.accuracy * .3));
        }

        public int GetBaseValuePoints(Player player)
        {
            return player.awareness + player.distance + player.accuracy;
        }
    }
}
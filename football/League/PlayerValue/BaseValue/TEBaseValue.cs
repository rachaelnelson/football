﻿using Football.League.Players;

namespace Football.League.PlayerValue.BaseValue
{
    public class TEBaseValue : IBaseValue
    {
        public decimal GetBaseValue(Player player)
        {
            return (decimal)((player.awareness * .2) + (player.receiving * .2) + (player.speed * .2) + (player.runBlocking * .2) + (player.passBlocking * .2));
        }

        public int GetBaseValuePoints(Player player)
        {
            return player.awareness + player.receiving + player.speed + player.runBlocking + player.passBlocking;
        }
    }
}


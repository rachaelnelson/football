﻿using Football.League.Players;

namespace Football.League.PlayerValue.BaseValue
{
    public class DLBaseValue : IBaseValue
    {
        public decimal GetBaseValue(Player player)
        {
            return (decimal)((player.awareness * .3) + (player.runDefense * .3) + (player.sack * .3) + (player.speed * .1));
        }

        public int GetBaseValuePoints(Player player)
        {
            return player.awareness + player.runDefense + player.sack + player.speed;
        }
    }
}

﻿using Football.League.Players;

namespace Football.League.PlayerValue.BaseValue
{
    public class LBBaseValue : IBaseValue
    {
        public decimal GetBaseValue(Player player)
        {
            return (decimal)((player.awareness * .3) + (player.runDefense * .2) + (player.passDefense * .2) + (player.speed * .2) + (player.sack * .1) );
        }

        public int GetBaseValuePoints(Player player)
        {
            return player.awareness + player.runDefense + player.passDefense + player.speed + player.sack;
        }
    }
}
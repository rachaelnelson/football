﻿using Football.League.Players;

namespace Football.League.PlayerValue.BaseValue
{
    public class OLBaseValue : IBaseValue
    {
        public decimal GetBaseValue(Player player)
        {
            return (decimal)((player.awareness * .3) + (player.passBlocking * .3) + (player.runBlocking * .3) + (player.speed * .1));
        }

        public int GetBaseValuePoints(Player player)
        {
            return player.awareness + player.passBlocking + player.runBlocking + player.speed;
        }
    }
}

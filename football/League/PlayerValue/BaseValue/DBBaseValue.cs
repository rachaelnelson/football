﻿using Football.League.Players;

namespace Football.League.PlayerValue.BaseValue
{
    public class DBBaseValue : IBaseValue
    {
        public decimal GetBaseValue(Player player)
        {
            return (decimal)((player.awareness * .3) + (player.passDefense * .3) + (player.speed * .3) + (player.runDefense * .1));
        }

        public int GetBaseValuePoints(Player player)
        {
            return player.awareness + player.passDefense + player.speed + player.runDefense;
        }
    }
}
﻿using System.Collections.Generic;
using Football.League.Players;
using Football.League.PlayerValue.PointsValue;

namespace Football.League.PlayerValue
{
    public class PlayerPoints
    {
        private static Dictionary<PlayerType, IPointsValue> pointsList = new Dictionary<PlayerType, IPointsValue>()
        {
            {PlayerType.QB, new QBPointsValue()},
            {PlayerType.RB, new RBPointsValue()},
            {PlayerType.WR, new WRPointsValue()},
            {PlayerType.TE, new TEPointsValue()},
            {PlayerType.OL, new OLPointsValue()},
            {PlayerType.DL, new DLPointsValue()},
            {PlayerType.LB, new LBPointsValue()},
            {PlayerType.DB, new DBPointsValue()},
            {PlayerType.K, new KPPointsValue()},
            {PlayerType.P, new KPPointsValue()},
        };

        public static int GetMaxPoints(Player player)
        {
            return pointsList[player.position].GetMaxPoints();
        }

        public static int GetMinPoints(Player player)
        {
            return pointsList[player.position].GetMinPoints();
        }

        public static int GetMidPoints(Player player)
        {
            return pointsList[player.position].GetMidPoints();
        }
    }
}

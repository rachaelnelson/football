﻿using System.Collections.Generic;
using Football.League.Players;
using Football.League.PlayerValue.BaseValue;

namespace Football.League.PlayerValue
{
    // Intent is for this class to return the value of players.
    public static class PlayerValues
    {
        private static Dictionary<PlayerType, IBaseValue> valueList = new Dictionary<PlayerType, IBaseValue>()
        {
            {PlayerType.QB, new QBBaseValue()},
            {PlayerType.RB, new RBBaseValue()},
            {PlayerType.WR, new WRBaseValue()},
            {PlayerType.TE, new TEBaseValue()},
            {PlayerType.OL, new OLBaseValue()},
            {PlayerType.DL, new DLBaseValue()},
            {PlayerType.LB, new LBBaseValue()},
            {PlayerType.DB, new DBBaseValue()},
            {PlayerType.K, new KPBaseValue()},
            {PlayerType.P, new KPBaseValue()},
        };        
        
        public static decimal GetPlayerValue(Player player)
        {
            return GetPlayerBaseValue(player);
        }

        public static int GetPlayerBaseValuePoints(Player player)
        {
            return valueList[player.position].GetBaseValuePoints(player);
        }

        // TODO:  this might need to have some relevance on the in-game impact, which should probably be logarithmic scale, like Richter Scale.
        private static decimal GetPlayerBaseValue(Player player)
        {
            return valueList[player.position].GetBaseValue(player);
        }

        public static List<Player> SortByLowestPlayerValue(List<Player> initialPlayerList)
        {
            initialPlayerList.Sort(ComparePlayersByValueLowest);

            return initialPlayerList;
        }

        // TODO:  move to different class
        private static int ComparePlayersByValueLowest(Player x, Player y)
        {
            if (PlayerValues.GetPlayerValue(x) < PlayerValues.GetPlayerValue(y))
            {
                return -1;
            }
            else if (PlayerValues.GetPlayerValue(x) > PlayerValues.GetPlayerValue(y))
            {
                return 1;
            }

            return 0;
        }
    }
}

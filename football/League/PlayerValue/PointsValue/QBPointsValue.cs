﻿

namespace Football.League.PlayerValue.PointsValue
{
    public class QBPointsValue : IPointsValue
    {
        public int GetMaxPoints()
        {
            // 5 is max points * the 4 categories
            return 5 * 4;
        }

        public int GetMinPoints()
        {
            // 1 is min points * the 4 categories
            return 1 * 4;
        }

        public int GetMidPoints()
        {
            // 3 is mid points * the 4 categories
            return 3 * 4;
        }
    }
}

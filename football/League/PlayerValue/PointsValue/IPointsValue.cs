﻿

namespace Football.League.PlayerValue.PointsValue
{
    interface IPointsValue
    {
        int GetMaxPoints();
        int GetMinPoints();
        int GetMidPoints();
    }
}

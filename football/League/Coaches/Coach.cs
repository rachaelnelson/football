﻿using System;
using Football.Settings;

namespace Football.League.Coaches
{
    [Serializable()]
    public class Coach
    {
        public BaseAll BaseOffense {get; set;}
        public BaseAll BaseDefense { get; set; }
        public int Balance { get; set; } // 0 is all Run, 100 is all pass
        public int Behind{ get; set; } // 0 is all Run, 100 is all pass
        public int TwoPoint { get; set; } // 0 is all Run, 100 is all pass
    }
}

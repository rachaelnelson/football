﻿
namespace Football.League.Players
{
    public class OLPositionAttributesChooser : AttributeChooser
    {
        public override PlayerAttributes getAttributes(Player player)
        {          
            return new OLPositionAttributes(player);
        }
    }
}

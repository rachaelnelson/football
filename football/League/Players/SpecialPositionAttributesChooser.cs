﻿
namespace Football.League.Players
{
    public class SpecialPositionAttributesChooser : AttributeChooser
    {
        public override PlayerAttributes getAttributes(Player player)
        {          
            return new SpecialPositionAttributes(player);
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Football.League.Players
{    
    public class PlayerTypeValues
    {   
        private List<PlayerType> offTypes;
        private List<PlayerType> defTypes;
        private List<PlayerType> spTypes;

        public PlayerTypeValues()
        {
            this.offTypes = new List<PlayerType>()
            {
                PlayerType.QB,
                PlayerType.RB,
                PlayerType.WR,
                PlayerType.TE,
                PlayerType.OL                
            };

            this.defTypes = new List<PlayerType>()
            {
                PlayerType.DL,
                PlayerType.LB,
                PlayerType.DB                 
            };

            this.spTypes = new List<PlayerType>()
            {
                PlayerType.K,
                PlayerType.P               
            };
        }

        public PlayerTypeValues(List<PlayerType> offTypes, List<PlayerType> defTypes, List<PlayerType> spTypes)
        {
            this.offTypes = offTypes;
            this.defTypes = defTypes;
            this.spTypes = spTypes;
        }

        public Array GetPlayerOffenseValues()
        {            
            return offTypes.ToArray();
        }
         
        public Array GetPlayerDefenseValues()
        {
            return defTypes.ToArray();
        }

        public Array GetPlayerSpecialValues()
        {
            return spTypes.ToArray();
        }
       
        public bool IsOffense(PlayerType type)
        {
            bool result = false;

            Array offValues = GetPlayerOffenseValues();

            foreach (PlayerType item in offValues)
            {
                if (item == type)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        public bool IsDefense(PlayerType type)
        {
            bool result = false;

            Array defValues = GetPlayerDefenseValues();

            foreach (PlayerType item in defValues)
            {
                if (item == type)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        public bool IsSpecial(PlayerType type)
        {
            bool result = false;

            Array spValues = GetPlayerSpecialValues();

            foreach (PlayerType item in spValues)
            {
                if (item == type)
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

    }
}
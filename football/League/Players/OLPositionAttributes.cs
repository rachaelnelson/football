﻿using System.ComponentModel;

namespace Football.League.Players
{
    class OLPositionAttributes : PlayerAttributes
    {
        [CategoryAttribute("Attributes"), ReadOnlyAttribute(true)]
        public int PassBlocking { get; set; }

        [CategoryAttribute("Attributes"), ReadOnlyAttribute(true)]
        public int RunBlocking { get; set; }

        public OLPositionAttributes(Player player)
        {
            this.Awareness = player.awareness;            
            this.PassBlocking = player.passBlocking;
            this.RunBlocking = player.runBlocking;     
            this.Speed = player.speed;        
        }
    }
}

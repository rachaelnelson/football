﻿using System.ComponentModel;

namespace Football.League.Players
{
    class DefensePositionAttributes : PlayerAttributes
    {
        [CategoryAttribute("Attributes"),ReadOnlyAttribute(true)]
        public int PassDefense{get;set;}

        [CategoryAttribute("Attributes"), ReadOnlyAttribute(true)]
        public int RunDefense{get;set;}

        [CategoryAttribute("Attributes"), ReadOnlyAttribute(true)]
        public int Sack{get;set;}

        public DefensePositionAttributes(Player player)
        {
            this.Awareness = player.awareness;
            this.PassDefense = player.passDefense;
            this.RunDefense = player.runDefense;
            this.Sack = player.sack;
            this.Speed = player.speed;        
        }
    
    }
}

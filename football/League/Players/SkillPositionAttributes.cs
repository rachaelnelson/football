﻿using System.ComponentModel;

namespace Football.League.Players
{
    class SkillPositionAttributes : PlayerAttributes
    {
        [CategoryAttribute("Attributes"), ReadOnlyAttribute(true)]
        public int Receiving { get; set; }

        [CategoryAttribute("Attributes"), ReadOnlyAttribute(true)]
        public int PassBlocking { get; set; }

        [CategoryAttribute("Attributes"), ReadOnlyAttribute(true)]
        public int RunBlocking { get; set; }

        public SkillPositionAttributes(Player player)
        {
            this.Awareness = player.awareness;
            this.Receiving = player.receiving;
            this.PassBlocking = player.passBlocking;
            this.RunBlocking = player.runBlocking;     
            this.Speed = player.speed;        
        }
    }
}

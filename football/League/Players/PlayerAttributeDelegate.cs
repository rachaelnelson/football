﻿using System.Collections.Generic;

namespace Football.League.Players
{
    // TODO:  PlayerAttributeFactory is used in PlayerViewer.  This Delegate class is really new 
    //        and had a purpose, but I'm can't remember what...
    //        I think it might be for grabbing attributes for use in Roster signing, etc
    //        Also, I think this is supposed to be called inside the specific PVMAttributes so it is uniform across the board 
    //        rather than the data being pulled form multiple spots.
    public class PlayerAttributeDelegate
    {
        private Dictionary<PlayerType, AttributeChooser> attributeDict = new Dictionary<PlayerType, AttributeChooser>()
        {
            {PlayerType.QB,new SpecialPositionAttributesChooser()},
            {PlayerType.RB,new SkillPositionAttributesChooser()},
            {PlayerType.TE,new SkillPositionAttributesChooser()},
            {PlayerType.WR,new SkillPositionAttributesChooser()},
            {PlayerType.OL,new OLPositionAttributesChooser()},
            {PlayerType.DL,new DefenseAttributesChooser()},
            {PlayerType.LB,new DefenseAttributesChooser()},
            {PlayerType.DB,new DefenseAttributesChooser()},
            {PlayerType.K,new SpecialPositionAttributesChooser()},
            {PlayerType.P,new SpecialPositionAttributesChooser()}
        };        

        private Player player;

        public PlayerAttributeDelegate(Player player)
        {
            this.player = player;            
        }

        public PlayerAttributes getAttributes()
        {
            return this.attributeDict[this.player.position].getAttributes(this.player);
        }

    }
}

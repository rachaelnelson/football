﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Football.League.Players
{
    interface IPlayerTypeValues
    {
        Array GetPlayerOffenseValues();
        Array GetPlayerDefenseValues();
        Array GetPlayerSpecialValues();
    }
}

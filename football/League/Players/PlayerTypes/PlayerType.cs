﻿using System;
using System.Collections.Generic;

namespace Football.League.Players
{
    public class PlayerType
    {       
        private static Dictionary<string, PlayerType> typeDict = new Dictionary<string, PlayerType>()
        {
           {"QB", new PlayerType("QB",0,"Passing")},
           {"RB", new PlayerType("RB",1,"Rushing")},
           {"WR", new PlayerType("WR",2,"Receiving")},
           {"TE", new PlayerType("TE",3,"Receiving")},
           {"OL", new PlayerType("OL",4,"")},
           {"DL", new PlayerType("DL",5,"Defense")},
           {"LB", new PlayerType("LB",6,"Defense")},
           {"DB", new PlayerType("DB",7,"Defense")},
           {"K", new PlayerType("K",8,"Kicking")},
           {"P", new PlayerType("P",9,"Punting")},
        };

        public static PlayerType QB { get { return typeDict["QB"]; } }
        public static PlayerType RB { get { return typeDict["RB"]; } }
        public static PlayerType WR { get { return typeDict["WR"]; } }
        public static PlayerType TE { get { return typeDict["TE"]; } }
        public static PlayerType OL { get { return typeDict["OL"]; } }
        public static PlayerType DL { get { return typeDict["DL"]; } }
        public static PlayerType LB { get { return typeDict["LB"]; } }
        public static PlayerType DB { get { return typeDict["DB"]; } }
        public static PlayerType K { get { return typeDict["K"]; } }
        public static PlayerType P { get { return typeDict["P"]; } }

                
        private string abbr;
        private string statDesc;
        private int value;

        public int Value { get { return this.value; } }
        public string StatDesc { get { return this.statDesc; } }

        public PlayerType(string abbr, int val, string statDesc)
        {
            this.abbr = abbr;            
            this.value = val;
            this.statDesc = statDesc;
        }

        // Need to load the roster file in RosterImportRecord.cs
        // TODO:  this will obviously need better error handling for user files
        public static PlayerType GetPlayerTypeByString(string type)
        {
            return typeDict[type];
        }
        
        public static Array GetPlayerTypes()
        {
            string[] typeArray = new string[typeDict.Keys.Count];
            typeDict.Keys.CopyTo(typeArray,0);
            return typeArray;
        }

        public static List<PlayerType> GetPlayerTypesList()
        {
            List<PlayerType> listType = new List<PlayerType>();

            foreach (var item in typeDict)
            {
                listType.Add(item.Value);
            }

            return listType;
        }

        public override string ToString()
        {
            return this.abbr;
        }
    }
}

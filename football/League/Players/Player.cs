﻿using System;
using System.ComponentModel;
using Football.League.Injuries;
using Football.League.Salary;
using Football.League.Teams;

namespace Football.League.Players
{
    [Serializable()]
    public class Player
    {
        //  Constant variables to determine various player scores
        // Why use decimal over float:  http://gregs-blog.com/2007/12/10/dot-net-decimal-type-vs-float-type/
        /*
        private const decimal REC_AWARE_PERC = .5m;
        private const decimal REC_PERC = .3m;
        private const decimal REC_SPEED_PERC = .2m;
        */

        // needed for stats rows for uniqueness
        public int id { get; set; }

        [DisplayName("First Name")]
        public String firstName { get; set; }

        [DisplayName("Last Name")]
        public String lastName { get; set; }

        [DisplayName("DOB")]
        public DateTime birthDate { get; set; }
        [DisplayName("Accuracy")]
        public int accuracy { get; set; }

        [DisplayName("Distance")]
        public int distance { get; set; }

        [DisplayName("Awareness")]
        public int awareness { get; set; }

        [DisplayName("Speed")]
        public int speed { get; set; }

        [DisplayName("Receiving")]
        public int receiving { get; set; }

        [DisplayName("Pass Blocking")]
        public int passBlocking { get; set; }

        [DisplayName("Run Blocking")]
        public int runBlocking { get; set; }

        [DisplayName("Pass Defense")]
        public int passDefense { get; set; }

        [DisplayName("Run Defense")]
        public int runDefense { get; set; }

        [DisplayName("Position")]
        public PlayerType position { get; set; }

        [DisplayName("Sack")]
        public int sack { get; set; }
        
        [DisplayName("Team")]
        public Team team { get; set; }        

        // TODO:  why is this here?  
        [DisplayName("TeamAbbr")]
        public String teamAbbr 
        {
            get
            {
                return this.team.Abbr;
            }
        }  

        [DisplayName("Injured")]
        public bool isInjured 
        {
            get
            {
                return (this.injury.duration>0)?true:false;
            }
        }

        public PlayerInjury injury { get; set; }
        public PlayerContract contract { get; set; }
        
        [DisplayName("Contract Years Remaining")]
        public String currentYear
        {
            get
            {
                // 5 1
                return ((this.contract.Salary.Length - this.contract.Year) + 1).ToString();
            }
        }
        
        [DisplayName("Salary")]
        public String salary
        {
            get
            {
                int year = this.contract.Year - 1;
                return this.contract.Salary[year].ToString();
            }
        }

        [DisplayName("Experience")]
        public int yearsExperience { get; set; }

        // Used as a cheap hack instead of implementing a salary cache.  this is a quicker way.
        // Shouldn't display this anywhere
        public int expectedSalary { get; set; }

        public override string ToString()
        {
            return firstName + " " + lastName + " " + position;
        }


        /*
        public float recScore;
        public float sackScore;
        public float interScore;
         */ 

        // Dictionary definition used for player import
        //public Player(Dictionary<string, string> player = null)
        //{
          //  if (player != null)
          //  {
        //        this.importPlayer(player);                
          //  }    
            /*        
            this.calculateRecScore();
            this.calculateSackScore();
            this.calculateInterceptionScore();
             */ 
        //}

        //  Lame, lame, lame but dynamically assigning is a pita!
        /*
        private void importPlayer(Dictionary<string, string> player)
        {            
            try
            {
                this.firstName = player["fname"];
                this.lastName = player["lname"];
                this.birthDate = Convert.ToDateTime(player["bdate"]);
                this.position = (PlayerType)Enum.Parse(typeof(PlayerType), player["pos"]);
                this.accuracy = Convert.ToInt32(player["acc"]);
                this.distance = Convert.ToInt32(player["dist"]);
                this.awareness = Convert.ToInt32(player["aware"]);
                this.speed = Convert.ToInt32(player["sp"]);
                this.receiving = Convert.ToInt32(player["rec"]);
                this.passBlocking = Convert.ToInt32(player["passb"]);
                this.runBlocking = Convert.ToInt32(player["runb"]);
                this.passDefense = Convert.ToInt32(player["passd"]);
                this.runDefense = Convert.ToInt32(player["rund"]);
                this.sack = Convert.ToInt32(player["sk"]);
                //  Nasty but here's why... http://msdn.microsoft.com/en-us/library/86hw82a3.aspx
                this.isInjured = Convert.ToBoolean(Convert.ToInt32(player["inj"]));
                this.injury = new PlayerInjury(); // May need to import injuries as well                  
            }
            catch (Exception e)
            {
                 // export errors to file 
                Console.Out.WriteLine(e.ToString());
            }                        
        }
        */
        /**
         *  Needed for saving player hash to stats tables in db
         *  
         *  http://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-an-overridden-system-object-gethashcode
         */
        /*
        public int getUniqueHash()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                // Suitable nullity checks etc, of course :)
                hash = hash * 29 + this.firstName.GetHashCode();
                hash = hash * 29 + this.lastName.GetHashCode();
                hash = hash * 29 + this.birthDate.GetHashCode();
                return hash;
            }

        }
         */ 
        /*
        //   Calculate the receiving score to rank who gets the most catches        
        public void calculateRecScore()
        {
             this.recScore = (float)((this.awareness * REC_AWARE_PERC) + (this.receiving * REC_PERC) + (this.speed * REC_SPEED_PERC));
        }

        //   Calculate the sack score to rank who gets the most sacks
        public void calculateSackScore()
        {
             this.sackScore = (float)((this.sack * .50) + ( this.speed * .20) + ( this.awareness * .20 ));
        }

        //    Calculate the interception score to determine who gets the most interceptions
        public void calculateInterceptionScore()
        {
            this.interScore = (float)((this.passDefense * .5) + (this.awareness * .5));
        }
         */ 
    }
}
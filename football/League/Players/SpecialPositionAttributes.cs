﻿using System.ComponentModel;

namespace Football.League.Players
{
    class SpecialPositionAttributes:PlayerAttributes
    {
        [CategoryAttribute("Attributes"), ReadOnlyAttribute(true)]
        public int Accuracy { get; set; }

        [CategoryAttribute("Attributes"), ReadOnlyAttribute(true)]
        public int Distance { get; set; }

        public SpecialPositionAttributes(Player player)
        {
            this.Awareness = player.awareness;
            this.Accuracy = player.accuracy;
            this.Distance = player.distance;            
            this.Speed = player.speed;        
        }
    }
}

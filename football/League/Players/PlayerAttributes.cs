﻿using System.ComponentModel;

namespace Football.League.Players
{
    public abstract class PlayerAttributes
    {
        [CategoryAttribute("Attributes"), ReadOnlyAttribute(true)]
        public int Awareness{get;set;}

        [CategoryAttribute("Attributes"), ReadOnlyAttribute(true)]
        public int Speed { get; set; }        
    }
}

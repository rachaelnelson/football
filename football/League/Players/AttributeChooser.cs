﻿
namespace Football.League.Players
{    
    public abstract class AttributeChooser
    {       
        public abstract PlayerAttributes getAttributes(Player player);
    }
}

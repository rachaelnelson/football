﻿
namespace Football.League.Players
{
    public class DefenseAttributesChooser : AttributeChooser
    {
        public override PlayerAttributes getAttributes(Player player)
        {            
            return new DefensePositionAttributes(player);
        }
    }
    
}

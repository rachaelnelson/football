﻿
namespace Football.League.Players
{
    public class SkillPositionAttributesChooser : AttributeChooser
    {
        public override PlayerAttributes getAttributes(Player player)
        {            
            return new SkillPositionAttributes(player);
        }    
    }
}

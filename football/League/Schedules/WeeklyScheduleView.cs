﻿
namespace Football.League.Schedules
{
    public class WeeklyScheduleView
    {
        public string HomeTeam;
        public string AwayTeam;
        public string Score;
        public string GameLog;
    }
}

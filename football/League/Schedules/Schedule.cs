﻿using System;
using System.Collections.Generic;
using Football.League.Teams;
using Football.League.Teams.TeamRecord;

namespace Football.League.Schedules
{
    [Serializable()]
    public class Schedule
    {
        private Dictionary<string, List<ScheduleItem>> schedule;
        
        public Schedule()
        {
            this.schedule = new Dictionary<string, List<ScheduleItem>>();
        }        

        public List<ScheduleItem> getScheduleByWeek(string week)
        {            
            if ( Convert.ToInt16( week ) > this.schedule.Keys.Count )
            {
                week = this.schedule.Keys.Count.ToString();
            }
            return schedule[week];
        }

        /**
         *  Fetch the Schedule for the selected team 
         *  TODO:  can this be replaced with linq lol
         */
        public Dictionary<string, ScheduleItem> getScheduleByTeam(string abbr)
        {
            Dictionary<string, ScheduleItem> teamSchedule = new Dictionary<string, ScheduleItem>();
            foreach (var weekItem in schedule) // loop through each week
            {
                teamSchedule.Add(weekItem.Key,null); // inits list for this week
                foreach (var scheduleItem in schedule[weekItem.Key]) // loop through each game in the week
                {
                    if (scheduleItem.awayTeam.Abbr.Equals(abbr) || scheduleItem.homeTeam.Abbr.Equals(abbr)) // if team is home or away team, place in list
                    {
                        teamSchedule[weekItem.Key]=scheduleItem; // add scheduleItem to list to return
                    }
                }
            }
            return teamSchedule;
        }

        public void addScheduleItem(string week,ScheduleItem item)
        {
            if (!schedule.ContainsKey(week))
            {
                schedule.Add(week, new List<ScheduleItem>());                 
            }
            schedule[week].Add(item);
        }

        public HashSet<Team> getScheduleSet(string abbr)
        {
            HashSet<Team> teamSchedule = new HashSet<Team>();
            foreach (var weekItem in schedule) // loop through each week
            {                
                foreach (var scheduleItem in schedule[weekItem.Key]) // loop through each game in the week
                {
                    if (scheduleItem.awayTeam.Abbr.Equals(abbr) )
                    {
                        if (!teamSchedule.Contains(scheduleItem.homeTeam))
                        {
                            teamSchedule.Add(scheduleItem.homeTeam);
                        }
                    }
                    else if (scheduleItem.homeTeam.Abbr.Equals(abbr))
                    {
                        if (!teamSchedule.Contains(scheduleItem.awayTeam))
                        {
                            teamSchedule.Add(scheduleItem.awayTeam);
                        }
                    }                    
                }
            }
            return teamSchedule;
        }

        // yeah i know...pretty gross...
        public Record getRecordByOpponent(Team team, Team opponent)       
        {
            Record teamRecord = new Record();

            foreach (var weekItem in schedule) // loop through each week
            {
                foreach (var scheduleItem in schedule[weekItem.Key]) // loop through each game in the week
                {
                    Team winner = null;

                    if ( (scheduleItem.awayTeam.Abbr.Equals(team.Abbr) && scheduleItem.homeTeam.Abbr.Equals(opponent.Abbr)) 
                         || (scheduleItem.homeTeam.Abbr.Equals(team.Abbr) && scheduleItem.awayTeam.Abbr.Equals(opponent.Abbr))
                        )
                    {
                        winner = scheduleItem.getWinner();
                        
                        if (winner.Abbr == team.Abbr)
                        {
                            teamRecord.addWin();
                        }
                        else if (winner.Abbr!=team.Abbr)
                        {
                            teamRecord.addLoss();
                        }                        
                        else
                        {
                            teamRecord.addTie();
                        }
                    }                             
                }
            }

            return teamRecord;
        }

        public int GetNumberOfWeeks()
        {
            return schedule.Keys.Count;
        }
        
    }
}

﻿
namespace Football.League.Schedules
{
    public class TeamScheduleView
    {
        public string Week;
        public string Opponent;
        public string Score;
        public string GameLog;
    }
}

﻿using System;
using Football.League.Teams;
using Football.Sim.Games;

namespace Football.League.Schedules
{
    [Serializable()]
    public class ScheduleItem
    {
        public Team homeTeam { get; set; }
        public Team awayTeam { get; set; }       
        // TODO:  add game time      
        public GameResult result { get; set; }

        public ScheduleItem()
        {
            this.result = new GameResult();
        }

        public Team getWinner()
        {
            Team winner = null;
            int scoreDiff = this.result.homeTeamScore - this.result.awayTeamScore;

            if (scoreDiff>0)
            {
                winner = this.homeTeam;
            }
            else if (scoreDiff < 0)
            {
                winner = this.awayTeam;
            }

            return winner;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Football.League.Teams;


namespace Football.League.Conferences
{
    [Serializable]
    public class Conference
    {
        private List<Division> divisions;
        private string name;

        public List<Division> Divisions { get { return this.divisions; } }
        public string Name { get { return this.name; } }
       
        public Conference(string name, List<Division> divisions)
        {
            this.name = name;
            this.divisions = divisions;
        }

        public Division getTeamDivision( Team team )
        {
            return divisions.Find(d => d.Teams.Contains(team));
        }

        /**
         *  Terribad...but works for now....
         *  Currently used in unit tests
         */ 
        public Team getTeamByAbbr(string abbr)
        {
            Team found = null;

            foreach (Division division in divisions)
            {
                foreach (Team divTeam in division.Teams)
                {
                    if (divTeam.Abbr.Equals(abbr))
                    {
                        found = divTeam;
                    }
                }
            }

            return found;
        }
    }
}

﻿using System;
using System.Collections.Generic;

using Football.League.Teams;

namespace Football.League.Conferences
{
    [Serializable]
    public class Division
    {
        private string name;
        private List<Team> teams;

        public string Name { get { return this.name; } }
        public List<Team> Teams { get { return this.teams; } }        

        public Division(string name, List<Team> teams)
        {
            this.name = name;
            this.teams = teams;
        }
    }
}

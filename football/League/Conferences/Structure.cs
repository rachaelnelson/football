﻿using System;
using System.Collections.Generic;
using Football.League.Teams;

namespace Football.League.Conferences
{
    [Serializable]
    public class Structure
    {    
        private List<Conference> conferences;

        public Structure()
        {
            this.conferences = new List<Conference>();
        }

        public void addConference( Conference conf )
        {
            conferences.Add( conf );
        }

        public List<String> getAllConferenceNames()
        {
            List<String> nameList = new List<String>();

            foreach (var conference in this.conferences)
            {
                nameList.Add(conference.Name);
            }
            nameList.Sort();

            return nameList;
        }
        
        public List<String> getAllDivisionNames()
        {
            List<String> nameList = new List<String>();

            foreach (var conference in this.conferences)
            {
                foreach (var division in conference.Divisions)
                {
                    nameList.Add(division.Name);
                }
            }
            nameList.Sort();

            return nameList;
        }

        public List<Conference> getConferences()
        {                      
            return this.conferences;
        }

        public bool areTeamsinSameConference(Team homeTeam, Team awayTeam)
        {
            Division homeDiv = null;
            Division awayDiv = null;

            Conference conf = this.conferences[0];
            
            homeDiv = conf.getTeamDivision(homeTeam);
            awayDiv = conf.getTeamDivision(homeTeam);

            // only need first conference to determine if teams are in same conference
            return ((homeDiv == null && awayDiv == null) || (homeDiv != null && awayDiv != null)); 
        }

        public bool areTeamsinSameDivison(Team homeTeam, Team awayTeam)
        {
            Division homeDiv = null; 
            Division awayDiv = null;

            foreach (Conference conf in this.conferences)
            {
                if (homeDiv == null)
                {
                    homeDiv = conf.getTeamDivision(homeTeam);
                }
                if (awayDiv == null)
                {
                    awayDiv = conf.getTeamDivision(awayTeam);
                }                
            }

            return homeDiv.Equals(awayDiv);
        }
    }
}

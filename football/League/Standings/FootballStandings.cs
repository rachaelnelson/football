﻿using System.Collections.Generic;
using System.Linq;
using Football.League.Conferences;
using Football.League.Teams;
using Football.League.Teams.TeamRecord;

namespace Football.League.Standings
{
    public class FootballStandings
    {
        private TeamRecordList teamRecordList;

        public FootballStandings(TeamRecordList teamRecordList)
        {
            this.teamRecordList = teamRecordList;
        }
    

        public List<Team> getConference( Conference conference )
        {            
            List<Team> confList = new List<Team>();

            List<Division> divisions = conference.Divisions;
                       
            foreach (var item in divisions)
            {                
                confList.AddRange( item.Teams );
            }                        

            List<TeamRecord> recordList = new List<TeamRecord>();

            foreach (Team team in confList)
            {
                recordList.Add(this.teamRecordList.GetTeamRecord(team));
            }

            return recordList.OrderByDescending(t => t.OverallRecord.getPoints()).Select(t => t.Team).ToList<Team>();
        }

        /**
         *  Returns division
         */        
        public List<Team> getDivisionTeams( Division division )
        {          
            List<TeamRecord> recordList = new List<TeamRecord>();

            foreach( Team team in division.Teams )
            {
                recordList.Add( this.teamRecordList.GetTeamRecord(team) );
            }

            return recordList.OrderByDescending(t => t.OverallRecord.getPoints()).Select(t=>t.Team).ToList<Team>();
        }          
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.League.Players;
using Football.League.Teams;

namespace Football.League.Salary
{
    [Serializable]
    public class PlayerContract
    {
        public int signingBonus { get; set; }
        //public int Length { get; set; } // maybe the salary should be indexed by year so this wouldn't be needed.
        public int[] Salary { get; set; } // each index is a year so it can vary per year.  Will need its own calculation.
        
        // might could add certain bonuses like a roster bonus, etc.

        // will need a method for calculating the cap hit per year.

        public Team Team { get; set; }
        public Player Player { get; set; }
        public int Year { get; set; }  // current year of deal.  Ex:  yr 2 of 4 yr deal

        public PlayerContract( Team team, Player player, int[] salary, int year=1 )
        {
            this.Team = team;
            this.Player = player;
            this.Salary = salary;            
            this.Year = year;
        }

        public IEnumerable<int> getSalaryView()
        {
            List<int> salaryList = new List<int>();
            
            int[] salaryArray = new int[10];

            for (int i = Year - 1; i < Salary.Length; i++)
            {
                salaryList.Add( Salary[i] );
            }

            return salaryList.AsEnumerable();
        }        
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace Football.League.Salary.PayBands
{
    public abstract class IPayBand
    {
        abstract public List<PayBand> GetPayBand();

        public int GetBasePayAmount(decimal ranking)
        {
            List<PayBand> band = GetPayBand();

            PayBand playerBand = band.Where(x => (ranking >= x.LowerBound) && (ranking < x.UpperBound)).FirstOrDefault();

            // in theory bands could be null...and in theory it should only return one result...but not gonna check muwhahaha

            // for example assume 2.7.   .7 is leftover.  Add one for 1.7.  This can be used in the multiplication to get the salary.
            decimal leftover = 1 + (ranking - playerBand.LowerBound);

            int playerSalary = (int)(leftover * (decimal)playerBand.LowerPay);

            // if calculated salary is greater than the upper band (though this should never happen....), 
            // set to upper band
            if (playerSalary > playerBand.UpperPay)
            {
                playerSalary = playerBand.UpperPay;
            }

            // TODO:  add randomization, though perhaps this is done in the calling method.  
            // UPDATE:  it is randomized in calling method
            return playerSalary;
        }
    }
}

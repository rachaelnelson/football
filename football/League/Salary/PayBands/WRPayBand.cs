﻿using System.Collections.Generic;

namespace Football.League.Salary.PayBands
{
    public class WRPayBand : IPayBand
    {
        // Overall average:
        // 1.0 to 2.0  bottom 25%  Backup, Borderline
        // 2.0 to 3.0  middle 50%  Average player
        // 3.0 to 4.0  next 15% Pro Bowlers
        // 4.0 to 5.0  top 10%  Hall of Famers

        // pulling salary numbers out of my "database"...      
        public override List<PayBand> GetPayBand()
        {
            return new List<PayBand>
            {
                { new PayBand { LowerBound = 1.0M, UpperBound = 2.0M, LowerPay=400000, UpperPay=2000000 } },
                { new PayBand { LowerBound = 2.0M, UpperBound = 3.0M, LowerPay=2000000, UpperPay=5300000 } },
                { new PayBand { LowerBound = 3.0M, UpperBound = 4.0M, LowerPay=5300000, UpperPay=9000000 } },
                { new PayBand { LowerBound = 4.0M, UpperBound = 5.0M, LowerPay=9000000, UpperPay=14600000} }
            };
        }
    }
}

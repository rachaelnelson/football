﻿
namespace Football.League.Salary.PayBands
{
    public class PayBand
    {       
        public decimal LowerBound { get; set; }
        public decimal UpperBound { get; set; }
        public int LowerPay { get; set; }
        public int UpperPay { get; set; }
    }
}

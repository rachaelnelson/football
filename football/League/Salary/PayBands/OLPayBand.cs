﻿using System.Collections.Generic;

namespace Football.League.Salary.PayBands
{
    public class OLPayBand : IPayBand
    {
        public override List<PayBand> GetPayBand()
        {
            return new List<PayBand>
            {
                { new PayBand { LowerBound = 1.0M, UpperBound = 2.0M, LowerPay=400000, UpperPay=1500000 } },
                { new PayBand { LowerBound = 2.0M, UpperBound = 3.0M, LowerPay=1500000, UpperPay=4000000 } },
                { new PayBand { LowerBound = 3.0M, UpperBound = 4.0M, LowerPay=4000000, UpperPay=9000000 } },
                { new PayBand { LowerBound = 4.0M, UpperBound = 5.0M, LowerPay=9000000, UpperPay=11400000} }
            };
        }
    }
}

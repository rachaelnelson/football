﻿using System.Collections.Generic;

namespace Football.League.Salary.PayBands
{
    public class DLPayBand : IPayBand
    {
        public override List<PayBand> GetPayBand()
        {
            return new List<PayBand>
            {
                { new PayBand { LowerBound = 1.0M, UpperBound = 2.0M, LowerPay=400000, UpperPay=2000000 } },
                { new PayBand { LowerBound = 2.0M, UpperBound = 3.0M, LowerPay=2000000, UpperPay=4000000 } },
                { new PayBand { LowerBound = 3.0M, UpperBound = 4.0M, LowerPay=4000000, UpperPay=11000000 } },
                { new PayBand { LowerBound = 4.0M, UpperBound = 5.0M, LowerPay=11000000, UpperPay=17000000} }
            };
        }
    }
}

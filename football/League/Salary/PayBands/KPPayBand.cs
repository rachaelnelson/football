﻿using System.Collections.Generic;

namespace Football.League.Salary.PayBands
{
    public class KPPayBand : IPayBand
    {
        public override List<PayBand> GetPayBand()
        {
            return new List<PayBand>
            {
                { new PayBand { LowerBound = 1.0M, UpperBound = 2.0M, LowerPay=400000, UpperPay=800000 } },
                { new PayBand { LowerBound = 2.0M, UpperBound = 3.0M, LowerPay=800000, UpperPay=1600000 } },
                { new PayBand { LowerBound = 3.0M, UpperBound = 4.0M, LowerPay=1600000, UpperPay=3000000 } },
                { new PayBand { LowerBound = 4.0M, UpperBound = 5.0M, LowerPay=3000000, UpperPay=5000000} }
            };
        }
    }
}

﻿using System.Collections.Generic;

namespace Football.League.Salary.PayBands
{
    public class LBPayBand : IPayBand
    {
        public override List<PayBand> GetPayBand()
        {
            return new List<PayBand>
            {
                { new PayBand { LowerBound = 1.0M, UpperBound = 2.0M, LowerPay=400000, UpperPay=1000000 } },
                { new PayBand { LowerBound = 2.0M, UpperBound = 3.0M, LowerPay=1000000, UpperPay=2700000 } },
                { new PayBand { LowerBound = 3.0M, UpperBound = 4.0M, LowerPay=2700000, UpperPay=8000000 } },
                { new PayBand { LowerBound = 4.0M, UpperBound = 5.0M, LowerPay=8000000, UpperPay=15500000} }
            };
        }
    }
}

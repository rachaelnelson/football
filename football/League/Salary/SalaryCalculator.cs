﻿using System.Collections.Generic;
using Football.League.Players;
using Football.League.PlayerValue;
using Football.League.Salary.PayBands;
using Football.Library;

namespace Football.League.Salary
{
    public class SalaryCalculator
    {        
        public static int CalculateExpectedSalary(Player player)
        {
            if (player.expectedSalary == 0)
            {                                        
                RandomSeed rand = RandomSeed.Instance;
                decimal randomAdjustment = 1 + (((decimal)rand.Next(-20, 20)) / 100);

                player.expectedSalary = CalculateExpectedSalary(player, randomAdjustment);
            }
            return player.expectedSalary;
        }

        public static int CalculateExpectedSalary(Player player, decimal randomAdjustment)
        {    
            // 1 2 3 4 5

            // 1 1 1 1
            // 1 2 2 1
            // 2 2 2 2
            // 2 3 3 2
            // 3 3 3 3
            // 3 4 4 3
            // 4 4 4 4
            // 4 5 5 4
            // 4 5 5 4
            
            // Overall average:
            // 1.0 to 2.0  bottom 25%  Borderline to Backup
            // 2.0 to 3.0  middle 50%  Average player
            // 3.0 to 4.0  next 20% Above Average and Good player
            // 4.0 to 5.0  top 5%  Pro Bowlers and Hall of Famers

            // 1.0 to 1.5  bottom 10%  Borderline
            // 1.5 to 2.0  bottom 15%  Backup
            // 2.0 to 2.5  middle 25%  Solid Backup
            // 2.5 to 3.0  middle 25%  Starter
            // 3.0 to 3.5  next 10% Solid Starter
            // 3.5 to 4.0  next 10% Good Starter
            // 4.0 to 4.5  top 4%  Pro Bowlers
            // 4.5 to 5.0  top 1%  Hall of Famers
            
            // QBS 20M 10% (3) - 56% increase,  25% 12.8M (8) - 24% increase, 50% 10.3M (16)
            // RBS 12M 10% (3) - 76% increase,25% 6.8M (8), 50% 3.75M (16) - 81% increase
            // TE  11M - 109% increase,  5.25M - 51% increase, 3.47M

            Dictionary<PlayerType, IPayBand> bands = new Dictionary<PlayerType, IPayBand>
            {
                {PlayerType.QB, new QBPayBand()},
                {PlayerType.RB, new RBPayBand()},
                {PlayerType.WR, new WRPayBand()},
                {PlayerType.TE, new TEPayBand()},
                {PlayerType.OL, new OLPayBand()},
                {PlayerType.DL, new DLPayBand()},
                {PlayerType.LB, new LBPayBand()},
                {PlayerType.DB, new DBPayBand()},
                {PlayerType.K, new KPPayBand()},
                {PlayerType.P, new KPPayBand()},
            };

            // IDEA:  Salary Range bands for each position.  
            // For example, TEs with a BasePlayerValue of 2.0-3.0 fall within a certain pay band, with a bit of variability, of course.            
            decimal playerValue = PlayerValues.GetPlayerValue(player);

            // TODO:  Make sure they can't go below the salary minimum.
            //        Also need to adjust for years of service, etc for minimum salaries
            return (int)(randomAdjustment * bands[player.position].GetBasePayAmount(playerValue));            
        }
    }
}

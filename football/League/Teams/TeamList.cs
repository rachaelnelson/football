﻿using System;
using System.Collections.Generic;
using System.Linq;

/**
 *   Modified to use a Dictionary approach with abbreviation being the key. 
 *   We'll see if this works ok for the game's needs - otherwise may have to rethink it
 */ 
namespace Football.League.Teams
{
    [Serializable()]
    public class TeamList
    {        
        private Dictionary<string, Team> teamList;
        private const int MAX_TEAMS=32;

        public TeamList()
        {            
            teamList = new Dictionary<string, Team>(MAX_TEAMS);
        }

        public bool addTeam(Team team)
        { 
           bool is_added=false;
           if(teamList.Count()<MAX_TEAMS) 
           {
              teamList.Add(team.Abbr,team);
              is_added=true; 
           }
           return is_added;
       }

       public bool removeTeam(Team team)
       {  
           return teamList.Remove(team.Abbr); 
       }

       public Dictionary<string, Team> getTeamList()
       {  
           return teamList;  
       }

       public Team getTeam(string abbr)
       {
           return teamList[abbr];
       }

       public List<string> getTeamAbbrs()
       {
           return teamList.Keys.ToList();
       }
    }
}

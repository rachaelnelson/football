﻿using System;

namespace Football.League.Teams.TeamRecord
{
    /* Holds the team record information  */
    [Serializable()]
    public class Record
    {
        private int win;
        private int loss;
        private int tie;
        
        public Record()
        {
            win = 0;
            loss = 0;
            tie = 0;               
        }

        public Record(int win, int loss, int tie)
        {
            this.win = win;
            this.loss = loss;
            this.tie = tie;
        }

        public void addWin()
        { 
            win++; 
        }

        public void addWin( int wins )
        {
            this.win += wins;
        } 

        public void addLoss()
        {
            loss++; 
        }

        public void addLoss(int losses)
        {
            this.loss += losses;
        } 

        public void addTie()
        {
            tie++; 
        }

        public void addTie(int ties)
        {
            this.tie += ties;
        } 

        public int getWin()
        {
            return win; 
        }

        public int getLoss()
        {
            return loss; 
        }
        
        public int getTie()
        { 
            return tie; 
        }
       
        // returns the winning percentage
        public float getWinPercentage()
        {
            int totalGames = win+loss+tie;
            float winsTies = (float)(win + (tie / 2));
            // formula = (wins + 1/2 of ties) / total # games played
            return (totalGames > 0) ? winsTies / totalGames : 0.0F;                           
        }

        public int getPoints()
        {
            return (win * 2) + (tie); 
        }
    }
}

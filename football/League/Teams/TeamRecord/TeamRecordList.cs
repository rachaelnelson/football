﻿using System;
using System.Collections.Generic;

namespace Football.League.Teams.TeamRecord
{
    [Serializable()]
    public class TeamRecordList
    {
        private Dictionary<String, TeamRecord> teamRecordList;

        public TeamRecordList()
        {
            this.teamRecordList = new Dictionary<String, TeamRecord>();
        }       

        public void AddNewRecord(Team team, TeamRecord teamRecord)
        {
            this.teamRecordList[team.Abbr] = teamRecord;
            this.teamRecordList[team.Abbr].Team = team;
        }

        public void AddWin(Team team, bool isInConference, bool isInDivision)
        {
            this.teamRecordList[team.Abbr].AddWin( isInConference, isInDivision );
        }

        public void AddLoss(Team team, bool isInConference, bool isInDivision)
        {
            this.teamRecordList[team.Abbr].AddLoss(isInConference, isInDivision);
        }

        public void AddTie(Team team, bool isInConference, bool isInDivision)
        {
            this.teamRecordList[team.Abbr].AddTie(isInConference, isInDivision);
        }

        public TeamRecord GetTeamRecord(Team team)
        {
            return this.teamRecordList[team.Abbr];
        }

        public void UpdateRecord(Team homeTeam, Team awayTeam, int scoreDiff, bool isInConference, bool isInDivision)
        {           
            if (scoreDiff < 0) // Home Team Lost
            {
                this.AddWin(awayTeam, isInConference, isInDivision);
                this.AddLoss(homeTeam, isInConference, isInDivision);
            }
            else if (scoreDiff > 0) // Home Team won
            {
                this.AddWin(homeTeam, isInConference, isInDivision);
                this.AddLoss(awayTeam, isInConference, isInDivision);
            }
            else // Tie
            {
                this.AddTie(awayTeam, isInConference, isInDivision);
                this.AddTie(homeTeam, isInConference, isInDivision);
            }
        }
    }
}

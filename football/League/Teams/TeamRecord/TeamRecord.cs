﻿using System;

namespace Football.League.Teams.TeamRecord
{
    [Serializable()]
    public class TeamRecord
    {
        public Team Team { get; set; }      
        public Record ConferenceRecord { get; set; }
        public Record DivisionRecord { get; set; }
        public Record OverallRecord { get; set; }

        public TeamRecord( Record conferenceRecord, Record divisionRecord, Record overallRecord )
        {
            this.ConferenceRecord = conferenceRecord;
            this.DivisionRecord = divisionRecord;
            this.OverallRecord = overallRecord;           
        }

        /**
         *   The folowing methods maybe should be in TeamRecordHandler
         */ 
        public void AddWin(bool isConference, bool isDivision)
        {
            this.OverallRecord.addWin();

            if (isConference)
            {
                this.ConferenceRecord.addWin();
            }

            if (isDivision)
            {
                this.DivisionRecord.addWin();
            }
        
        }

        public void AddLoss(bool isConference, bool isDivision)
        {
            this.OverallRecord.addLoss();

            if (isConference)
            {
                this.ConferenceRecord.addLoss();
            }

            if (isDivision)
            {
                this.DivisionRecord.addLoss();
            }
        }

        public void AddTie(bool isConference, bool isDivision)
        {
            this.OverallRecord.addTie();

            if (isConference)
            {
                this.ConferenceRecord.addTie();
            }

            if (isDivision)
            {
                this.DivisionRecord.addTie();
            }
        }
    }
}

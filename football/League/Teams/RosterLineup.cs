﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.League.Players;
using Football.League.Teams.FormationFilters;
using Football.Settings;


namespace Football.League.Teams
{
    [Serializable()]
    public class RosterLineup
    {
        private Dictionary<BaseAll, Lineup> lineupDict;        

        public RosterLineup()
        {
            this.initLineup();            
        }
       
        public Lineup getLineup( BaseAll type )
        {
            return lineupDict[type];
        }

        private void initLineup()
        {
            this.lineupDict = new Dictionary<BaseAll, Lineup>()
            {
                {BaseAll.TwoRB,new Lineup()},
                {BaseAll.TwoTE,new Lineup()},
                {BaseAll.ThreeWR,new Lineup()},
                {BaseAll.FourThree,new Lineup()},
                {BaseAll.ThreeFour,new Lineup()},
                {BaseAll.KP,new Lineup()}
            };
        }

        /**
         *  Add every player to the formation - easier that way. 
         */
        public void addPlayer(Player player)
        {            
            foreach (var item in lineupDict)
            {
                item.Value.addPlayer(player);
            }
        }

        public void removePlayer(Player player)
        {
            foreach (var item in lineupDict)
            {
                item.Value.removePlayer(player);                
            }
        }

        public Dictionary<PlayerType, List<Player>> getStarters(FormationFilter filter, BaseAll formation, List<Player> availablePlayers)
        {
            // may want to use helper here
            Dictionary<PlayerType, List<Player>> starters = new Dictionary<PlayerType, List<Player>>();                                               
            Lineup lineup = this.lineupDict[formation]; // pulls all players by position            

            foreach (var item in filter.starterDictionary.Where(item => item.Value > 0)) // where depth is 1 or more
            {
                starters.Add(item.Key,new List<Player>());

                // List of all players available at the position, filtering out IR, inactives, and injured
                List<Player> posList = lineup.getPositionList(item.Key,availablePlayers);
                List<Player> currentAlternates = new List<Player>();

                while (posList.Count < item.Value) // if less players available for the position than required
                {
                    Player player = this.getAlternatePlayer(item.Key, lineup, item.Value, availablePlayers.Except(currentAlternates).ToList());

                    if (player == null)
                    {
                       throw new Exception("Error:  Alternate player can't be found.  Returns null in RosterHelper.getAlternatePlayer.");
                    }
                    
                    starters[item.Key].Add(player); 
                }
                
                starters[item.Key].AddRange(posList.GetRange(0, item.Value)); // add players to starters list
            }

            return starters;
        }       
        
        // TODO:  verify the player isn't playing another alternate position already....
        private Player getAlternatePlayer(PlayerType type, Lineup lineup, int depth, List<Player> availablePlayers)
        {
            Player player = null;
            PlayerType[] typeArray = new PlayerType[6] { PlayerType.RB, PlayerType.WR, PlayerType.DB, PlayerType.LB, PlayerType.DL, PlayerType.OL };

            foreach (PlayerType playerType in typeArray)
            {
                List<Player> posList = lineup.getPositionList(playerType,availablePlayers);

                if (posList.Count > depth)
                {
                    player = posList[depth];
                    break;
                }
            }
            
            return player;
        }

        public int PromotePlayer(Player player, BaseAll formation)
        {
            return lineupDict[formation].PromotePlayer(player);
        }

        public int DemotePlayer(Player player, BaseAll formation)
        {
            return lineupDict[formation].DemotePlayer(player);
        }

        public void ClearPosition(PlayerType position)
        {
            foreach (var item in lineupDict)
            {
                item.Value.ClearPosition(position);
            }
        }
          
    }
}

﻿using System;
using System.Collections.Generic;

namespace Football.League.Teams
{
    public class PlayerRosterView
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string DOB { get; set; }
        public int Experience { get; set; }
        public bool IsInactive { get; set; }
        public bool IsInjured { get; set; }
        public bool IsOnIR { get; set; }
        public int Accuracy { get; set; }
        public int Distance { get; set; }
        public int Awareness { get; set; }
        public int Speed { get; set; }
        public int Receiving { get; set; }
        public int PassBlocking { get; set; }
        public int RunBlocking { get; set; }
        public int PassDefense { get; set; }
        public int RunDefense { get; set; }
        public int Sack { get; set; }
        public IEnumerable<int> Salaries  { get; set; }
        public decimal PlayerValue { get; set; }
        public int InjuryDuration { get; set; }

        public override string ToString()
        {
            return String.Format("{0} {1} {2}", Position, FirstName, LastName);
        }

    }      
}

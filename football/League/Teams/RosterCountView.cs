﻿using System.ComponentModel;

namespace Football.League.Teams
{
    public class RosterCountView : INotifyPropertyChanged
    {
        // http://msdn.microsoft.com/en-us/library/ms133020%28v=vs.100%29.aspx
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        // Number of Active Players on team
        private int activeCount;
        public int ActiveCount
        {
            get
            {
                return activeCount;
            }
            set
            {
                if (value != activeCount)
                {
                    activeCount = value;
                    NotifyPropertyChanged("ActiveCount");
                }
            }
        }

        // Number of Inactives for the week
        private int inactiveCount;
        public int InactiveCount
        {
            get
            {
                return inactiveCount;
            }
            set
            {
                if (value != inactiveCount)
                {
                    inactiveCount = value;
                    NotifyPropertyChanged("InactiveCount");
                }
            }
        }

        // Number of players on IR
        private int irCount;
        public int IRCount
        {
            get
            {
                return irCount;
            }
            set
            {
                if (value != irCount)
                {
                    irCount = value;
                    NotifyPropertyChanged("IRCount");
                }
            }
        }

        // Total NON IR players (active, inactive, injured, no IR)
        private int nonIRCount;
        public int NonIRCount
        {
            get
            {
                return nonIRCount;
            }
            set
            {
                if (value != nonIRCount)
                {
                    nonIRCount = value;
                    NotifyPropertyChanged("NonIRCount");
                }
            }
        }

        // Total players (active, inactive, injured, IR)
        private int totalCount;
        public int TotalCount
        {
            get
            {
                return totalCount;
            }
            set
            {
                if (value != totalCount)
                {
                    totalCount = value;
                    NotifyPropertyChanged("TotalCount");
                }
            }
        }

        //  Total non injured roster size - 53
        private int maxRosterCount;
        public int MaxRosterCount
        {
            get
            {
                return maxRosterCount;
            }
            set
            {
                if (value != maxRosterCount)
                {
                    maxRosterCount = value;
                    NotifyPropertyChanged("MaxRosterCount");
                }
            }
        }

        private int minRosterCount;
        public int MinRosterCount
        {
            get
            {
                return minRosterCount;
            }
            set
            {
                if (value != minRosterCount)
                {
                    minRosterCount = value;
                    NotifyPropertyChanged("MinRosterCount");
                }
            }
        }

        // 46 player limit
        private int activeLimit;
        public int ActiveLimit
        {
            get
            {
                return activeLimit;
            }
            set
            {
                if (value != activeLimit)
                {
                    activeLimit = value;
                    NotifyPropertyChanged("ActiveLimit");
                }
            }
        }
    }
}

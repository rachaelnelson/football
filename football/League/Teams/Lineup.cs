﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.League.Players;


namespace Football.League.Teams
{
    [Serializable()]
    public class Lineup
    {
        // tracks depth charts by position     
        private Dictionary<PlayerType, List<Player>> playerDict = new Dictionary<PlayerType, List<Player>>()
        {
            {PlayerType.QB,new List<Player>()},
            {PlayerType.RB,new List<Player>()},
            {PlayerType.WR,new List<Player>()},
            {PlayerType.TE,new List<Player>()},
            {PlayerType.OL,new List<Player>()},
            {PlayerType.DL,new List<Player>()},
            {PlayerType.LB,new List<Player>()},
            {PlayerType.DB,new List<Player>()},
            {PlayerType.K,new List<Player>()},
            {PlayerType.P,new List<Player>()}
        };         
        
        /*  Adds Player to roster  */
        public bool addPlayer(Player player)
        {           
            this.playerDict[player.position].Add(player);                                    
            return true;
        }
        
        public bool removePlayer(Player player)
        {            
            return this.playerDict[player.position].Remove(player);            
        }
       
        public List<Player> fetchList(PlayerType type)
        {
            return this.playerDict[type];
        }        
        
        public List<Player> getPositionList(PlayerType type, List<Player> players)
        {
           List<Player> temp = new List<Player>();

           IEnumerable<Player> player = playerDict[type].Where(p => players.Contains(p));                                     

           temp.AddRange(player); 

           return temp;
        }

        public int PromotePlayer(Player player)
        {
            int index = playerDict[player.position].FindIndex(p => p.id == player.id);
            int newIndex = 0;

            if (index > 0)
            {
                Player a = playerDict[player.position][index];
                Player b = playerDict[player.position][index-1];

                Player temp = b;
                playerDict[player.position][index-1] = a;
                playerDict[player.position][index] = temp;

                newIndex = index - 1;
            }

            return newIndex;
        }

        public int DemotePlayer(Player player)
        {
            int index = playerDict[player.position].FindIndex(p => p.id == player.id);
            int newIndex = playerDict[player.position].Count - 1;

            if (index < (playerDict[player.position].Count-1))
            {
                Player a = playerDict[player.position][index];
                Player b = playerDict[player.position][index+1];

                Player temp = b;
                playerDict[player.position][index+1] = a;
                playerDict[player.position][index] = temp;

                newIndex = index + 1;
            }

            return newIndex;
        }

        public void ClearPosition(PlayerType position)
        {
            playerDict[position].Clear();
        }
    }    
}

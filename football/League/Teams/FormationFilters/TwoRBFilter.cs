﻿using Football.League.Players;

namespace Football.League.Teams.FormationFilters
{
    public class TwoRBFilter:FormationFilter
    {
        public TwoRBFilter()
        {
            starterDictionary[PlayerType.QB] = 1;
            starterDictionary[PlayerType.RB] = 2;
            starterDictionary[PlayerType.WR] = 2;
            starterDictionary[PlayerType.TE] = 1;
            starterDictionary[PlayerType.OL] = 5;
            starterDictionary[PlayerType.K] = 1;
            starterDictionary[PlayerType.P] = 1;

            // 22
            minimumDepthDictionary[PlayerType.QB] = 2;
            minimumDepthDictionary[PlayerType.RB] = 4;
            minimumDepthDictionary[PlayerType.WR] = 4;
            minimumDepthDictionary[PlayerType.TE] = 2;
            minimumDepthDictionary[PlayerType.OL] = 8;
            minimumDepthDictionary[PlayerType.K] = 1;
            minimumDepthDictionary[PlayerType.P] = 1;
        }
    }
}

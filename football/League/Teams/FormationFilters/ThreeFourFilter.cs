﻿using Football.League.Players;

namespace Football.League.Teams.FormationFilters
{
    public class ThreeFourFilter:FormationFilter
    {
        public ThreeFourFilter()
        {
            starterDictionary[PlayerType.DL] = 3;
            starterDictionary[PlayerType.LB] = 4;
            starterDictionary[PlayerType.DB] = 4;

            // 21
            minimumDepthDictionary[PlayerType.DL] = 6;
            minimumDepthDictionary[PlayerType.LB] = 7;
            minimumDepthDictionary[PlayerType.DB] = 8;
        }
    }
}

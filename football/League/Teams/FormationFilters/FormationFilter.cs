﻿using System.Collections.Generic;
using Football.League.Players;

namespace Football.League.Teams.FormationFilters
{
    public abstract class FormationFilter
    {
        // TODO:  Make an accessor for this and mindepth dict for RosterLineup class
        public Dictionary<PlayerType, int> starterDictionary = new Dictionary<PlayerType, int>()
        {
            {PlayerType.QB,0},
            {PlayerType.RB,0},
            {PlayerType.WR,0},
            {PlayerType.TE,0},
            {PlayerType.OL,0},
            {PlayerType.DL,0},
            {PlayerType.LB,0},
            {PlayerType.DB,0},
            {PlayerType.K,1},
            {PlayerType.P,1},
        };

        public Dictionary<PlayerType, int> minimumDepthDictionary = new Dictionary<PlayerType, int>()
        {
            {PlayerType.QB,0},
            {PlayerType.RB,0},
            {PlayerType.WR,0},
            {PlayerType.TE,0},
            {PlayerType.OL,0},
            {PlayerType.DL,0},
            {PlayerType.LB,0},
            {PlayerType.DB,0},
            {PlayerType.K,1},
            {PlayerType.P,1},
        };

        public int GetRequiredCountByPosition(PlayerType position)
        {
            return minimumDepthDictionary[position];
        }

        public int GetStarterCountByPosition(PlayerType position)
        {
            return starterDictionary[position];
        }
    }
}

﻿using Football.League.Players;

namespace Football.League.Teams.FormationFilters
{
    public class FourThreeFilter:FormationFilter
    {
        public FourThreeFilter()
        {
            starterDictionary[PlayerType.DL] = 4;
            starterDictionary[PlayerType.LB] = 3;
            starterDictionary[PlayerType.DB] = 4;

            // 21
            minimumDepthDictionary[PlayerType.DL] = 7;
            minimumDepthDictionary[PlayerType.LB] = 6;
            minimumDepthDictionary[PlayerType.DB] = 8;
        }
    }
}

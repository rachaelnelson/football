﻿using Football.League.Players;

namespace Football.League.Teams.FormationFilters
{
    public class ThreeWRFilter:FormationFilter
    {
        public ThreeWRFilter()
        {
            starterDictionary[PlayerType.QB] = 1;
            starterDictionary[PlayerType.RB] = 1;
            starterDictionary[PlayerType.WR] = 3;
            starterDictionary[PlayerType.TE] = 1;
            starterDictionary[PlayerType.OL] = 5;
            starterDictionary[PlayerType.K] = 1;
            starterDictionary[PlayerType.P] = 1;

            // 22
            minimumDepthDictionary[PlayerType.QB] = 2;
            minimumDepthDictionary[PlayerType.RB] = 3;
            minimumDepthDictionary[PlayerType.WR] = 5;
            minimumDepthDictionary[PlayerType.TE] = 2;
            minimumDepthDictionary[PlayerType.OL] = 8;
            minimumDepthDictionary[PlayerType.K] = 1;
            minimumDepthDictionary[PlayerType.P] = 1;
        }
    }
}

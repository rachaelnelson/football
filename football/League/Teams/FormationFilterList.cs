﻿using System.Collections.Generic;
using System.Linq;

using Football.League.Coaches;
using Football.League.Teams.FormationFilters;
using Football.Settings;

namespace Football.League.Teams
{
    public static class FormationFilterList
    {
        private static Dictionary<BaseAll, FormationFilter> formationList = new Dictionary<BaseAll, FormationFilter>() 
        { 
            {BaseAll.TwoRB,new TwoRBFilter()},
            {BaseAll.TwoTE,new TwoTEFilter()},                
            {BaseAll.ThreeWR,new ThreeWRFilter()},     
            {BaseAll.FourThree,new FourThreeFilter()},     
            {BaseAll.ThreeFour,new ThreeFourFilter()}   
        };

        public static FormationFilter GetFormationFilter(BaseAll formationValue)
        {
            return formationList[formationValue];
        }

        // factor the coach base all crap to another object
        public static FormationFilter GetFormationFilter(Coach coach)
        {
            FormationFilter offenseFilter = formationList[coach.BaseOffense];
            FormationFilter defenseFilter = formationList[coach.BaseDefense];

            FormationFilter coachFilter = new CoachFormationFilter();

            
            // Bug:  these may overwrite each other

            foreach (var item in offenseFilter.starterDictionary.Where(x=>x.Value > 0))
            {                
                coachFilter.starterDictionary[item.Key] = item.Value;
            }

            foreach (var item in defenseFilter.starterDictionary.Where(x => x.Value > 0))
            {
                coachFilter.starterDictionary[item.Key] = item.Value;
            }

            foreach (var item in offenseFilter.minimumDepthDictionary.Where(x => x.Value > 0))
            {
                coachFilter.minimumDepthDictionary[item.Key] = item.Value;
            }

            foreach (var item in defenseFilter.minimumDepthDictionary.Where(x => x.Value > 0))
            {
                coachFilter.minimumDepthDictionary[item.Key] = item.Value;
            }

            return coachFilter;
        }
    }
}

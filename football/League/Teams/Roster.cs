﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.League.Players;
using Football.League.Teams.FormationFilters;
using Football.Settings;


namespace Football.League.Teams
{
    [Serializable()]    
    public class Roster 
    {
        private const int MIN_ROSTER_SIZE = 43;  //TODO:  pass in as config
        private const int MAX_ROSTER_SIZE = 53;  //TODO:  pass in as config
        private const int MAX_ACTIVE_SIZE = 46;  //TODO:  pass in as config
        private List<Player> all;
        private List<Player> inactiveList;
        private List<Player> IRList;
        private Lineup perPosition;
        private RosterLineup helper;        
        private Team team;
             
        public Roster(Team team)
        {
            this.team = team;

            this.all = new List<Player>();  // tracks every player on roster                  
            this.IRList = new List<Player>();  // tracks every player on IR 
            this.inactiveList = new List<Player>();
            this.perPosition = new Lineup(); // tracks every player on a per position basis
            this.helper = new RosterLineup();  // tracks every player per formation
        }
       
        public List<Player>fetchList(BaseAll formation,PlayerType position)
        {            
            return this.helper.getLineup(formation).fetchList(position);
        }

        private bool checkRosterSize()
        {
            return (all.Count - IRList.Count) < MAX_ROSTER_SIZE;            
        }

        public int activeRosterSize()
        {
            return all.Count - IRList.Count - inactiveList.Count;
        }

        public int inactiveSize()
        {
            return inactiveList.Count;
        }
        
        public int IRSize()
        {
            return IRList.Count;
        }

        public int NonIRSize()
        {
            return all.Count - IRList.Count;
        }

        public int maxSize()
        {
            return MAX_ROSTER_SIZE;
        }

        public int minSize()
        {
            return MIN_ROSTER_SIZE;
        }

        public int activeLimit()
        {
            return MAX_ACTIVE_SIZE;
        }

        public int totalSize()
        {
            return all.Count;
        }

        /*  Adds Player to roster  */
        public bool addPlayer(Player player)
        {
            bool value = false;

            if (checkRosterSize())
            {                
                all.Add(player); // add player to all and the individual depth chart
                perPosition.addPlayer(player);
                helper.addPlayer(player);
                value = true;
            }

            return value;
        }

        /* Removes Player from roster 
           TODO:  Will need to add a check for injured players  */
        public bool removePlayer(Player player)
        {            
            perPosition.removePlayer(player);
            helper.removePlayer(player);
            inactiveList.Remove(player);
            IRList.Remove(player);

            return this.all.Remove(player);
        }
        
        public int GetAvailableCountByPosition(PlayerType type)
        {
            return GetAvailablePlayersByPosition(type).Count;
        }

        public int GetAvailableNonInactiveCountByPosition(PlayerType type)
        {
            return GetAvailableNonInactivePlayersByPosition(type).Count;
        }

        // Currently here to stop compilation errors in impact and determine player        
        public List<Player> getPositionList(PlayerType type)
        {
            return perPosition.getPositionList( type, GetAvailablePlayers() );            
        }         

        private Dictionary<PlayerType, List<Player>> GetStarters(BaseAll formation)
        {
            FormationFilter filter = FormationFilterList.GetFormationFilter(formation);

            return helper.getStarters(filter,formation,GetAvailablePlayers());            
        }

        public Dictionary<PlayerType, List<Player>> GetStartersOffense()
        {          
            return GetStarters(team.Coach.BaseOffense);           
        }

        public Dictionary<PlayerType, List<Player>> GetStartersDefense()
        {
            return GetStarters(team.Coach.BaseDefense);    
        }

        private List<Player> GetAvailablePlayers()
        {          
            return all.Where(p=>p.injury.duration==0).Except(IRList).Except(inactiveList).ToList<Player>();
        }

        // Used in RosterService Inactive determination
        public List<Player> GetNonIRPlayers()
        {
            return all.Except(IRList).ToList<Player>();
        }

        private List<Player> GetAvailablePlayersByPosition(PlayerType type)
        {
            return all.Where(p => p.injury.duration == 0 && p.position == type).Except(IRList).Except(inactiveList).ToList<Player>();
        }

        private List<Player> GetAvailableNonInactivePlayersByPosition(PlayerType type)
        {
            return all.Where(p => p.position == type).Except(IRList).Except(inactiveList).ToList<Player>();
        }   

        public List<Player> GetAllPlayersByPosition(PlayerType type)
        {
            return perPosition.getPositionList(type, all);
        }

        /**
         *  Returns all the injured players on the roster 
         */
        public List<Player> getInjuredPlayers()
        {
            List<Player> temp = new List<Player>();
            temp.AddRange(all.Where(p => p.isInjured));

            return temp;
        }

        public List<Player> getIRPlayers()
        {
            return IRList;
        }

        public List<Player> getInactivePlayers()
        {
            return inactiveList;
        }
        
        public bool releasePlayer( Player player )
        {
            return removePlayer( player );
        }

        public Player getPlayerById(int playerId)
        {
            Player player = null;
            List<Player> players = all.Select(x => x).Where(x => x.id == playerId).ToList<Player>();           

            if (players.Count==1)
            {
                player = players[0];
            }

            return player;
        }

        // sums salaries of current year
        public int getSumSalaries()
        {        
            return all.Sum(x => x.contract.getSalaryView().FirstOrDefault());
        }

        public void addPlayerToIR(Player player)
        {
            if (!IRList.Contains(player))
            {
                IRList.Add(player);
                inactiveList.Remove(player);
            }
        }

        public void removePlayerFromIR(Player player)
        {
            IRList.Remove(player);
        }
        
        public void clearInactiveList()
        {
            inactiveList.Clear();
        }

        public int PromotePlayer(Player player, BaseAll formation)
        {
            return helper.PromotePlayer(player,formation);
        }

        public int DemotePlayer(Player player, BaseAll formation)
        {
            return helper.DemotePlayer(player, formation);
        }

        public void RemoveFromInactive(Player player)
        {
            inactiveList.Remove(player);
        }

        public void AddToInactive(Player player)
        {
            inactiveList.Add(player);
        }

        public Team GetTeam()
        {
            return team;
        }

        // removes all players from a position.  Used in unit tests for cutting players in filldepth
        public void ClearPosition(PlayerType position)
        {
            helper.ClearPosition(position);
        }

        public List<Player> GetAll()
        {
            return all;
        }

        // this should probably be in roster service....
        public decimal GetAveragePlayerValue()
        {
            return all.Average(x => PlayerValue.PlayerValues.GetPlayerValue(x));
        }
    }   
}
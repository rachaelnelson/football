﻿using System;

using Football.League.Coaches;

namespace Football.League.Teams
{
    [Serializable()]
    public class Team
    {        
        private string abbr;
       
        private string name;

        private string nickname;
        
        public String Abbr { get { return this.abbr; } }

        public Coach Coach { get; set; }               
                
        public String Name { get { return this.name; } }

        public String Nickname { get { return this.nickname; } }

        public Team(string name, string nickname, string abbreviation)
        {
            this.name = name;
            this.nickname = nickname;
            this.abbr = abbreviation;
        }
    }
}
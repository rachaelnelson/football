﻿using System.Collections.Generic;
using Football.League.Players;

namespace Football.League.Transaction
{
    public class TransactionView
    {
        public string Team1 { get; set; }
        public int Team1PlayerCount { get; set; }
        public int Team1SalarySum{get;set;}
        public int Team1PickCount { get; set; }

        public string Team2 { get; set; }
        public int Team2PlayerCount { get; set; }
        public int Team2SalarySum { get; set; }
        public int Team2PickCount { get; set; }
    }
}

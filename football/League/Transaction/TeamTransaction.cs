﻿using System;
using System.Collections.Generic;
using System.Linq;

using Football.League.Players;
using Football.League.Teams;

namespace Football.League.Transaction
{
    public class TeamTransaction
    {      
        private Dictionary<string, List<Player>> tradeDict;
        private Dictionary<string, List<DraftPicks.DraftPick>> draftDict;
       
        public TeamTransaction()
        {
            tradeDict = new Dictionary<string, List<Player>>();
            draftDict = new Dictionary<string, List<DraftPicks.DraftPick>>();
        }

        public void AddTeamToTransaction(Team team)
        {
            AddTeamToTransaction(team.Abbr);
        }

        public void AddTeamToTransaction(string team)
        {
            tradeDict.Add(team, new List<Player>());            
        }
        
        public void AddPickToTransaction( DraftPicks.DraftPick pick )
        {
            string team = pick.holdingTeam;

            if (!draftDict.ContainsKey(team))
            {
                draftDict.Add(team, new List<DraftPicks.DraftPick>());
            }

            if (!draftDict[team].Contains(pick))
            {
                draftDict[team].Add(pick);
            }
        }

        public void AddPlayerToTransaction(Player player)
        {
            string team = player.teamAbbr;

            if (!tradeDict.ContainsKey(team))
            {
                AddTeamToTransaction(team);
            }

            if (!tradeDict[team].Contains(player))
            {
                tradeDict[team].Add(player);
            }
        }

        public void RemovePickFromTransaction(DraftPicks.DraftPick pick)
        {
            draftDict[pick.holdingTeam].Remove(pick);
        }

        public void RemovePlayerFromTransaction(Player player)
        {
            tradeDict[player.teamAbbr].Remove(player);
        }

        public void ClearTransaction()
        {
            tradeDict.Clear();
            draftDict.Clear();
        }

        public List<Player> GetPlayerTransaction( string teamAbbr )
        {
            if ( tradeDict.ContainsKey( teamAbbr ) )
                return tradeDict[teamAbbr];
            else
                return new List<Player>();
        }

        public List<DraftPicks.DraftPick> GetPickTransaction( string teamAbbr )
        {
            if ( draftDict.ContainsKey( teamAbbr ) )
                return draftDict[teamAbbr];
            else
                return new List<DraftPicks.DraftPick>();
        }

        public Dictionary<string,List<Player>> GetPlayerTransaction()
        {
            return tradeDict;
        }

        public Dictionary<string, List<DraftPicks.DraftPick>> GetPickTransaction()
        {
            return draftDict;
        }

        public TransactionView GetTransactionView()
        {            
            TransactionView view = new TransactionView();

            if (tradeDict.Keys.Count > 0)
            {
                foreach (var item in tradeDict.Keys)
                {
                    if (view.Team1 == null)
                    {
                        view.Team1 = item;
                        view.Team1PlayerCount = tradeDict[item].Count;
                        view.Team1SalarySum = tradeDict[item].Sum(p => Convert.ToInt32(p.salary));
                        // view.Team1PickCount = draftDict[item].Count;
                    }
                    else if (view.Team2 == null)
                    {
                        view.Team2 = item;
                        view.Team2PlayerCount = tradeDict[item].Count;
                        view.Team2SalarySum = tradeDict[item].Sum(p => Convert.ToInt32(p.salary));
                        //view.Team2PickCount = draftDict[item].Count;
                    }
                }
            }

            if (draftDict.Keys.Count > 0)
            {
                foreach (var item in draftDict.Keys)
                {
                    if (view.Team1 == null)
                    {
                        view.Team1 = item;
                        //view.Team1PlayerCount = tradeDict[item].Count;
                        //view.Team1SalarySum = tradeDict[item].Sum(p => Convert.ToInt32(p.salary));
                        view.Team1PickCount = draftDict[item].Count;
                    }
                    else if (view.Team2 == null)
                    {
                        view.Team2 = item;
                        //view.Team2PlayerCount = tradeDict[item].Count;
                        //view.Team2SalarySum = tradeDict[item].Sum(p => Convert.ToInt32(p.salary));
                        view.Team2PickCount = draftDict[item].Count;
                    }
                }
            }

            return view;
        }  
    }
}
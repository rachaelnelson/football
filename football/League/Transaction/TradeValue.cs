﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Football.League.Players;
using Football.League.Draft;

namespace Football.League.Transaction
{
    public class TradeValue
    {
        private Dictionary<int, decimal> draftValueChart;
        private Dictionary<PlayerType, List<PlayerDraftBand>> playerBands;

        public TradeValue(Dictionary<int, decimal> draftValueChart, Dictionary<PlayerType, List<PlayerDraftBand>> playerBands)
        {
            this.draftValueChart = draftValueChart;
            this.playerBands = playerBands;
        }
    }
}

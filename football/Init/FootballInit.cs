﻿using System.IO;
using Football.League.Players;
using Football.League.Playoffs;
using Football.League.Teams;
using Football.Services;
using Football.Services.Depth;
using Football.Services.Sim;
using Football.Services.Starter;
using Football.Settings;
using Football.Sim.Displays;
using Football.Sim.Games;
using Football.Sim.Injuries;
using Football.Sim.Penalties;
using Football.Sim.Plays;
using Football.Sim.Situations;
using Football.Stats;


namespace Football.Init
{
    public class FootballInit
    {
        public FootballSettings Setting { get; set; }
        public FootballStats StatsHandler { get; set; }
        public InjuryHandler InjuryHandler { get; set; }
        public PenaltyHandler PenaltyHandler { get; set; }
        public GamePlay GamePlay { get; set; }
        public GameStateHandler GameHandler { get; set; }
        public Display Display { get; set; }
        public SituationHandler SituationHandler { get; set; }
        public PlayoffHandler PlayoffHandler { get; set; }
        public FootballSettingsService SettingsService { get; set; }
        public RosterService RosterService { get; set; }
        public CapManagementService CapService { get; set; }
        public ScheduleService ScheduleService { get; set; }
        public SimService SimService { get; set; }
        public InjuryService InjuryService { get; set; }
        public TransactionService TransactionService { get; set; }
        public ReleasePlayerService ReleasePlayerService { get; set; }
        public SignPlayerService SignPlayerService { get; set; }
        public StarterService StarterService { get; set; }        
        public PlayerTypeValues PlayerTypeValues { get; set; }
        public DepthSign DepthSign { get; set; }
        public DepthService DepthService { get; set; }
        public InactiveService InactiveService { get; set; }
        public PreSim PreSim { get; set; }
        public PostSim PostSim { get; set; }
        public PositionPriorityService PositionPriority { get; set; }

        public FootballInit()
        {
            Setting = FootballSettings.setSettings();
            SettingsService = new FootballSettingsService(Setting);

            GameHandler = new GameStateHandler();
            SituationHandler = new SituationHandler();
            Display = new Display(GameHandler);
            PenaltyHandler = new PenaltyHandler(GameHandler, Display);  
        }

        public void InitRest()
        {           
            PlayerTypeValues = new PlayerTypeValues();
            PlayoffHandler = new PlayoffHandler(Setting);
            InjuryHandler = new InjuryHandler(Setting, GameHandler, Display);
            StatsHandler = new FootballStats(Setting, GameHandler);
            GamePlay = new GamePlay(Display, GameHandler, StatsHandler, PenaltyHandler, InjuryHandler, SituationHandler);
            RosterService = new RosterService(SettingsService);
            CapService = new CapManagementService(SettingsService, RosterService);
            ScheduleService = new ScheduleService(SettingsService);
            InjuryService = new InjuryService(SettingsService, RosterService);

            StarterService = new StarterService(SettingsService, PlayerTypeValues, RosterService);
            TransactionService = new TransactionService(RosterService, CapService, SettingsService);    
            SignPlayerService = new SignPlayerService(RosterService, StarterService, TransactionService, SettingsService);
            ReleasePlayerService = new ReleasePlayerService(RosterService, StarterService, TransactionService, SettingsService);
            PositionPriority = new PositionPriorityService(PlayerTypeValues, StarterService);

            // Depth Service is really turning some things into dependency hell....the tests are a kludge now...
            DepthSign = new DepthSign( SignPlayerService, CapService );
            DepthService = new DepthService(SettingsService,PositionPriority, DepthSign);
            InactiveService = new InactiveService(SettingsService,PlayerTypeValues);
            PreSim = new PreSim(InjuryService, DepthService, InactiveService);
            PostSim = new PostSim(SettingsService, PlayoffHandler, InjuryService);                                
            SimService = new SimService(SettingsService, GamePlay, PreSim, PostSim);
        }

        public void Exit()
        {
            cleanTempFiles();
        }

        private void cleanTempFiles()
        {
            if (Setting != null && File.Exists(Setting.tempLogDirectory))
            {
                File.Delete(Setting.tempLogDirectory);
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Data;

using Football.League.Schedules;

namespace Football.UiClasses.DataViews
{
    public class TeamScheduleDataView
    {
        private DataView dataView;
        private DataTable table;

        public TeamScheduleDataView()
        {
            this.table = new DataTable();
            this.dataView = this.initDataView();  
        }

        public DataView getDataView()
        {
            return this.dataView;
        }

        private DataView initDataView()
        {
            // Add Data Table columns
            table.Columns.Add("Week",typeof(int));
            table.Columns.Add("Opponent");
            table.Columns.Add("Score");
            table.Columns.Add("Game Log");

            return new DataView(table);
        }

        public void updateDataTable(IEnumerable<TeamScheduleView> scheduleView)
        {
            table.Rows.Clear();

            foreach (var item in scheduleView)
            {
                List<object> rowList = new List<object>() 
                { 
                    item.Week,
                    item.Opponent,
                    item.Score,
                    item.GameLog
                };

                table.Rows.Add(rowList.ToArray());
            }
        }           
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Football.League.Teams;

namespace Football.UiClasses.DataViews
{
    public class RosterDataView
    {
        private DataView dataView;
        private DataTable table;        

        public RosterDataView(int currentYear)
        {
            this.table = new DataTable();
            this.dataView = this.initDataView(currentYear);  
        }

        public DataView getDataView()
        {
            return this.dataView;
        }
        
        private DataView initDataView(int currentYear)
        {            
            // Add Data Table columns
            table.Columns.Add("Id",typeof(int));
            table.Columns.Add("First Name",typeof(string));
            table.Columns.Add("Last Name",typeof(string));
            table.Columns.Add("Position",typeof(string));
            table.Columns.Add("DOB", typeof(DateTime));
            table.Columns.Add("Experience", typeof(int));
            table.Columns.Add("IsInactive", typeof(bool));
            table.Columns.Add("IsInjured",typeof(bool));
            table.Columns.Add("IsOnIR", typeof(bool));
            table.Columns.Add("Accuracy",typeof(int));
            table.Columns.Add("Distance", typeof(int));
            table.Columns.Add("Awareness", typeof(int));
            table.Columns.Add("Speed", typeof(int));
            table.Columns.Add("Receiving", typeof(int));
            table.Columns.Add("Pass Blocking", typeof(int));
            table.Columns.Add("Run Blocking", typeof(int));
            table.Columns.Add("Pass Defense", typeof(int));
            table.Columns.Add("Run Defense", typeof(int));
            table.Columns.Add("Sack", typeof(int));

            for (int i = 0; i < 5; i++)
            {
                int year = currentYear + i;
                table.Columns.Add("Salary for "+ year.ToString(),typeof(string));
            }

            return new DataView(table);
        }

        public void updateDataTable(IEnumerable<PlayerRosterView> rosterList)
        {
            table.Rows.Clear();

            foreach (var item in rosterList)
            {
                List<object> rowList = new List<object>() 
                { 
                    item.Id,
                    item.FirstName, 
                    item.LastName, 
                    item.Position,
                    item.DOB,
                    item.Experience,
                    item.IsInactive,
                    item.IsInjured,
                    item.IsOnIR,
                    item.Accuracy,
                    item.Distance,
                    item.Awareness,
                    item.Speed,
                    item.Receiving,
                    item.PassBlocking,
                    item.RunBlocking,
                    item.PassDefense,
                    item.RunDefense,
                    item.Sack
                };

                // prolly can combine these two loops....
                foreach (var sal in item.Salaries)
                {
                    rowList.Add(sal.ToString("0,0"));
                }
                
                // pads if salary doesn't exist
                for (int i = item.Salaries.Count(); i < 5; i++)
                {
                    rowList.Add(null);
                }                                  

                table.Rows.Add(rowList.ToArray());                  
            }
        }

        public void Clear()
        {
            table.Rows.Clear();
        }
    }
}

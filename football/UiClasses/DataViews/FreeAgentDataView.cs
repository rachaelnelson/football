﻿using System;
using System.Collections.Generic;
using System.Data;
using Football.League.Teams;

namespace Football.UiClasses.DataViews
{
    public class FreeAgentDataView
    {
        private DataView dataView;
        private DataTable table;        

        public FreeAgentDataView()
        {
            this.table = new DataTable();
            this.dataView = this.initDataView();  
        }

        public DataView getDataView()
        {
            return this.dataView;
        }
        
        private DataView initDataView()
        {            
            // Add Data Table columns
            table.Columns.Add("Id",typeof(int));
            table.Columns.Add("First Name",typeof(string));
            table.Columns.Add("Last Name",typeof(string));
            table.Columns.Add("Position",typeof(string));
            table.Columns.Add("DOB", typeof(DateTime));
            table.Columns.Add("Experience", typeof(int));
            table.Columns.Add("Salary Request", typeof(string));
            table.Columns.Add("Salary Request Hidden", typeof(int));
            table.Columns.Add("Injured",typeof(bool));
            table.Columns.Add("Accuracy",typeof(int));
            table.Columns.Add("Distance", typeof(int));
            table.Columns.Add("Awareness", typeof(int));
            table.Columns.Add("Speed", typeof(int));
            table.Columns.Add("Receiving", typeof(int));
            table.Columns.Add("Pass Blocking", typeof(int));
            table.Columns.Add("Run Blocking", typeof(int));
            table.Columns.Add("Pass Defense", typeof(int));
            table.Columns.Add("Run Defense", typeof(int));
            table.Columns.Add("Sack", typeof(int));                        

            return new DataView(table);
        }

        public void updateDataTable(IEnumerable<FreeAgentView> freeAgentList)
        {
            table.Rows.Clear();

            foreach (var item in freeAgentList)
            {
                List<object> rowList = new List<object>() 
                { 
                    item.Id,
                    item.FirstName, 
                    item.LastName, 
                    item.Position,
                    item.DOB,
                    item.Experience,
                    item.SalaryRequest.ToString("0,0"),
                    item.SalaryRequest,
                    item.IsInjured,
                    item.Accuracy,
                    item.Distance,
                    item.Awareness,
                    item.Speed,
                    item.Receiving,
                    item.PassBlocking,
                    item.RunBlocking,
                    item.PassDefense,
                    item.RunDefense,
                    item.Sack                   
                };                                         

                table.Rows.Add(rowList.ToArray());                  
            }
        }           
    }
}

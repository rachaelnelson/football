﻿using System.Collections.Generic;
using System.Data;
using Football.League.Schedules;

namespace Football.UiClasses.DataViews
{
    public class WeeklyScheduleDataView
    {
        private DataView dataView;
        private DataTable table;

        public WeeklyScheduleDataView()
        {
            this.table = new DataTable();
            this.dataView = this.initDataView();  
        }

        public DataView getDataView()
        {
            return this.dataView;
        }

        private DataView initDataView()
        {
            // Add Data Table columns
            table.Columns.Add("Home Team");
            table.Columns.Add("Away Team");            
            table.Columns.Add("Score");
            table.Columns.Add("Game Log"); 

            return new DataView(table);
        }

        public void updateDataTable(IEnumerable<WeeklyScheduleView> scheduleView)
        {
            table.Rows.Clear();

            foreach (var item in scheduleView)
            {
                List<object> rowList = new List<object>() 
                { 
                    item.HomeTeam,
                    item.AwayTeam,
                    item.Score,
                    item.GameLog
                };

                table.Rows.Add(rowList.ToArray());
            }
        }           
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;

using Football.League.Teams;
using Football.League.Players;

namespace Football.UiClasses.DataViews
{   
    public class TradePlayerDataView
    {
        private DataView dataView;
        private DataTable table;

        public TradePlayerDataView()
        {
            this.table = new DataTable();
            this.dataView = this.initDataView();
        }

        public DataView getDataView()
        {
            return this.dataView;
        }

        private DataView initDataView()
        {
            // Add Data Table columns
            table.Columns.Add("Id", typeof(int));
            table.Columns.Add("First Name", typeof(string));
            table.Columns.Add("Last Name", typeof(string));
            table.Columns.Add("Position", typeof(string));
            table.Columns.Add("DOB", typeof(DateTime));
            table.Columns.Add("Value", typeof(decimal));

            return new DataView(table);
        }

        public void updateDataTable(List<Player> tradeViewList)
        {
            table.Rows.Clear();

            foreach (var item in tradeViewList)
            {
                List<object> rowList = new List<object>() 
                { 
                    item.id,
                    item.firstName, 
                    item.lastName, 
                    item.position,
                    item.birthDate,
                    3 // TODO:  put actual player value here.          
                };

                table.Rows.Add(rowList.ToArray());
            }
        }

        public void Clear()
        {
            table.Rows.Clear();
        }
    }
}

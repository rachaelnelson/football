﻿using System.Collections.Generic;
using System.Data;
using Football.League.Injuries;

namespace Football.UiClasses.DataViews
{
    public class InjuryDataView
    {
        private DataView injuryDataView;
        private DataTable injuryTable;        

        public InjuryDataView()
        {
            this.injuryTable = new DataTable();
            this.injuryDataView = this.initInjuryDataView();  
        }

        public DataView getInjuryDataView()
        {
            return injuryDataView;
        }

        private DataView initInjuryDataView()
        {
            injuryTable.Columns.Add("Id",typeof(int));
            injuryTable.Columns.Add("First Name",typeof(string));
            injuryTable.Columns.Add("Last Name",typeof(string));
            injuryTable.Columns.Add("Position",typeof(string));
            injuryTable.Columns.Add("Injury",typeof(string));
            injuryTable.Columns.Add("Duration in Weeks",typeof(int));
            injuryTable.Columns.Add("Is On IR?",typeof(bool));

            return new DataView(injuryTable);
        }

        public void updateInjuryDataTable(IEnumerable<InjuryView> injuryList)
        {
            injuryTable.Rows.Clear();
            
            foreach (var item in injuryList)
            {
                List<object> rowList = new List<object>() 
                { 
                    item.Id,
                    item.FirstName, 
                    item.LastName, 
                    item.Position,
                    item.Injury,
                    item.InjuryDuration,
                    item.IsOnIR
                };                

                injuryTable.Rows.Add(rowList.ToArray());
            }            
        }           
    }
}

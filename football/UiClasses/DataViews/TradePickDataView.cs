﻿using System;
using System.Collections.Generic;
using System.Data;

using Football.League.Teams;
using Football.League.Players;

namespace Football.UiClasses.DataViews
{
    public class TradePickDataView
    {
        private DataView dataView;
        private DataTable table;

        public TradePickDataView()
        {
            this.table = new DataTable();
            this.dataView = this.initDataView();
        }

        public DataView getDataView()
        {
            return this.dataView;
        }

        private DataView initDataView()
        {
            // Add Data Table columns
            table.Columns.Add("Id", typeof(string));
            table.Columns.Add("Year", typeof(int));
            table.Columns.Add("Round", typeof(int));
            table.Columns.Add("Pick", typeof(int));     

            return new DataView(table);
        }

        public void updateDataTable(List<DraftPicks.DraftPick> draftList)
        {
            table.Rows.Clear();

            foreach (var item in draftList)
            {
                table.Rows.Add(item.id, item.year, item.round, item.pick);
            }
        }

        public void Clear()
        {
            table.Rows.Clear();
        }
    }
}


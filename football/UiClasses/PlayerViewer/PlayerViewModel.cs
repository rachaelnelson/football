﻿using System;
using System.Collections.Generic;


namespace Football.UiClasses.PlayerViewer
{
    public class PlayerViewModel
    {
        public PVMPersonal playerInfo { get; set; }
        public PVMAttributes playerAttributes{ get; set; }
        public object playerStats { get; set; }
        public object playerSalary { get; set; }

        public List<PVMPersonal> GetPlayerInfo()
        {            
            return new List<PVMPersonal>{ playerInfo };
        }

        // http://www.java2s.com/Code/CSharp/Reflection/Getsastringwithallofthepropertiesthatarenotnull.htm
        public List<PVMAttributes> GetPlayerAttributes()
        {            
            return new List<PVMAttributes> { playerAttributes };
        }

        public object GetPlayerStats()
        {
            return playerStats;
        }

        public object GetPlayerSalary()
        {
            return playerSalary;
        }
    }

    public class PVMPersonal
    {
        public string Name { get; set; }
        public string Position { get; set; }
        public DateTime DOB { get; set; }
        public string Team { get; set; }
    }    
}

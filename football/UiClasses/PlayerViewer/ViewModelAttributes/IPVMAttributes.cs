﻿using Football.League.Players;

namespace Football.UiClasses.PlayerViewer.ViewModelAttributes
{
    interface IPVMAttributes
    {
        PVMAttributes GetAttributes(Player player);
    }
}

﻿using Football.League.Players;

namespace Football.UiClasses.PlayerViewer.ViewModelAttributes
{
    public class LBPVMAttributes : IPVMAttributes
    {
        public PVMAttributes GetAttributes(Player player)
        {
            return new PVMAttributes
            {
                Awareness = player.awareness.ToString(),
                Speed = player.speed.ToString(),
                RunDefense = player.runDefense.ToString(),
                PassDefense = player.passDefense.ToString(),
                Sack = player.sack.ToString()
            };
        }
    }
}

﻿using Football.League.Players;

namespace Football.UiClasses.PlayerViewer.ViewModelAttributes
{
    public class DLPVMAttributes : IPVMAttributes
    {
        public PVMAttributes GetAttributes(Player player)
        {
            return new PVMAttributes
            {
                Awareness = player.awareness.ToString(),
                Speed = player.speed.ToString(),
                RunDefense = player.runDefense.ToString(),
                Sack = player.sack.ToString()
            };
        }
    }
}

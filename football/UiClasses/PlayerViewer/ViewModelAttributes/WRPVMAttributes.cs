﻿using Football.League.Players;

namespace Football.UiClasses.PlayerViewer.ViewModelAttributes
{
    public class WRPVMAttributes : IPVMAttributes
    {
        public PVMAttributes GetAttributes(Player player)
        {
            return new PVMAttributes
            {
                Awareness = player.awareness.ToString(),
                Speed = player.speed.ToString(),
                Receiving = player.receiving.ToString(),
                RunBlocking = player.runBlocking.ToString()
            };
        }
    }
}

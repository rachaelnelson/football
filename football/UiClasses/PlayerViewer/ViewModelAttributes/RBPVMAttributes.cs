﻿using Football.League.Players;

namespace Football.UiClasses.PlayerViewer.ViewModelAttributes
{
    public class RBPVMAttributes : IPVMAttributes
    {
        public PVMAttributes GetAttributes(Player player)
        {
            return new PVMAttributes
            {
                Awareness = player.awareness.ToString(),
                Speed = player.speed.ToString(),
                Receiving = player.receiving.ToString(),
                PassBlocking = player.passBlocking.ToString()
            };
        }
    }
}

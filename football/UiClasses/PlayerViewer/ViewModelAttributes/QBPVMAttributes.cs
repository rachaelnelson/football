﻿using Football.League.Players;

namespace Football.UiClasses.PlayerViewer.ViewModelAttributes
{
    public class QBPVMAttributes : IPVMAttributes
    {
        public PVMAttributes GetAttributes(Player player)
        {
            return new PVMAttributes 
                   {
                        Awareness = player.awareness.ToString(),
                        Speed = player.speed.ToString(),
                        Accuracy = player.accuracy.ToString(),
                        Distance = player.distance.ToString()
                   };                    
        }
    }
}

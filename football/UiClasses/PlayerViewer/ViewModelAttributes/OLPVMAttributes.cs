﻿using Football.League.Players;

namespace Football.UiClasses.PlayerViewer.ViewModelAttributes
{
    public class OLPVMAttributes : IPVMAttributes
    {
        public PVMAttributes GetAttributes(Player player)
        {
            return new PVMAttributes
            {
                Awareness = player.awareness.ToString(),
                Speed = player.speed.ToString(),
                PassBlocking = player.passBlocking.ToString(),
                RunBlocking = player.runBlocking.ToString()
            };
        }
    }
}

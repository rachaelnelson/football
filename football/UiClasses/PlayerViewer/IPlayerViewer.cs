﻿
namespace Football.UiClasses.PlayerViewer
{
    public interface IPlayerViewer
    {
        void Display(PlayerViewModel player);
    }
}

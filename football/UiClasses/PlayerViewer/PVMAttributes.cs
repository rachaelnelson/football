﻿namespace Football.UiClasses.PlayerViewer
{
    public class PVMAttributes
    {
        public string Accuracy { get; set; }
        public string Distance { get; set; }
        public string Awareness { get; set; }
        public string Speed { get; set; }
        public string Receiving { get; set; }
        public string PassBlocking { get; set; }
        public string RunBlocking { get; set; }
        public string PassDefense { get; set; }
        public string RunDefense { get; set; }
        public string Position { get; set; }
        public string Sack { get; set; }
    }    
}

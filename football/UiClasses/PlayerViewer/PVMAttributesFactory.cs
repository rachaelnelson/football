﻿using System.Collections.Generic;

using Football.League.Players;
using Football.UiClasses.PlayerViewer.ViewModelAttributes;


namespace Football.UiClasses.PlayerViewer
{
    // Used in Player Viewer
    public static class PVMAttributesFactory
    {
        private static Dictionary<PlayerType, IPVMAttributes> attDict = new Dictionary<PlayerType, IPVMAttributes>()
        {
           {PlayerType.QB, new QBPVMAttributes() },
           {PlayerType.RB, new RBPVMAttributes() },
           {PlayerType.WR, new WRPVMAttributes() },
           {PlayerType.TE, new TEPVMAttributes() },
           {PlayerType.OL, new OLPVMAttributes() },
           {PlayerType.DL, new DLPVMAttributes() },
           {PlayerType.LB, new LBPVMAttributes() },
           {PlayerType.DB, new DBPVMAttributes() },
           {PlayerType.K, new KPPVMAttributes() },
           {PlayerType.P, new KPPVMAttributes() },
        }; 

        public static PVMAttributes GetAttributes(Player player)
        {
            return attDict[player.position].GetAttributes(player);
        }
    }
}

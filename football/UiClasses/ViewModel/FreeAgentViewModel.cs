﻿using System;
using System.ComponentModel;
using System.Data;
using Football.League.Players;

namespace Football.UiClasses.ViewModel
{
    public class FreeAgentViewModel : INotifyPropertyChanged
    {
        // http://msdn.microsoft.com/en-us/library/ms133020%28v=vs.100%29.aspx

        // http://www.dreamincode.net/forums/topic/208833-using-the-inotifypropertychanged-functionality/
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private PlayerType selectedPosition;
        public PlayerType SelectedPosition
        {
            get
            {
                return this.selectedPosition;
            }

            set
            {
                if (value != this.selectedPosition)
                {
                    this.selectedPosition = value;
                    NotifyPropertyChanged("SelectedPosition");
                }
            }
        }

        public Array Positions { get; set; }

        private DataView freeAgentData;
        public DataView FreeAgentData
        {
            get
            {
                return this.freeAgentData;
            }
            set
            {
                if (value != this.freeAgentData)
                {
                    this.freeAgentData = value;
                    NotifyPropertyChanged("FreeAgentData");
                }
            }
        }

        private SignPlayerView signPlayer;
        public SignPlayerView SignPlayer
        {
            get
            {
                return this.signPlayer;
            }
            set
            {
                if (value != this.signPlayer)
                {
                    this.signPlayer = value;
                    NotifyPropertyChanged("SignPlayer");
                }
            }
        }

        private string capSpace;
        public string CapSpace
        {
            get
            {
                return this.capSpace;
            }
            set
            {
                if (value != this.capSpace)
                {
                    this.capSpace = value;
                    NotifyPropertyChanged("CapSpace");
                }
            }
        }        
    }
}

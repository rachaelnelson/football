﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using Football.League.Teams;
using System.Collections;
using Football.League.Players;

namespace Football.UiClasses.ViewModel
{
    public class TradeViewModel
    {
        // http://msdn.microsoft.com/en-us/library/ms133020%28v=vs.100%29.aspx
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        
        private List<string> teams;
        public List<string> Teams
        {
            get
            {
                return this.teams;
            }
            set
            {
                if (value != this.teams)
                {
                    this.teams = value;
                    NotifyPropertyChanged("Teams");
                }
            }
        }

        private string selectedTeamAbbr;
        public string SelectedTeamAbbr
        {
            get
            {
                return this.selectedTeamAbbr;
            }
            set
            {
                if (value != this.selectedTeamAbbr)
                {
                    this.selectedTeamAbbr = value;
                    NotifyPropertyChanged("SelectedTeamAbbr");
                }
            }
        }

        private string team1;
        public string Team1
        {
            get
            {
                return this.team1;
            }
            set
            {
                this.team1 = value;
            }
        }

        private string team2;
        public string Team2
        {
            get
            {
                return this.team2;
            }
            set
            {
                this.team2 = value;
            }
        }

        private DataView team1RosterData;
        public DataView Team1RosterData
        {
            get
            {
                return this.team1RosterData;
            }
            set
            {
                if (value != this.team1RosterData)
                {
                    this.team1RosterData = value;
                    NotifyPropertyChanged("Team1RosterData");
                }
            }
        }

        private DataView team2RosterData;
        public DataView Team2RosterData
        {
            get
            {
                return this.team2RosterData;
            }
            set
            {
                if (value != this.team2RosterData)
                {
                    this.team2RosterData = value;
                    NotifyPropertyChanged("Team2RosterData");
                }
            }
        }

        private DataView team1PlayerTradeData;
        public DataView Team1PlayerTradeData
        {
            get
            {
                return this.team1PlayerTradeData;
            }
            set
            {
                if (value != this.team1PlayerTradeData)
                {
                    this.team1PlayerTradeData = value;
                    NotifyPropertyChanged("Team1PlayerTradeData");
                }
            }
        }

        private DataView team2PlayerTradeData;
        public DataView Team2PlayerTradeData
        {
            get
            {
                return this.team2PlayerTradeData;
            }
            set
            {
                if (value != this.team2PlayerTradeData)
                {
                    this.team2PlayerTradeData = value;
                    NotifyPropertyChanged("Team2PlayerTradeData");
                }
            }
        }

        private DataView team1PickTradeData;
        public DataView Team1PickTradeData
        {
            get
            {
                return this.team1PickTradeData;
            }
            set
            {
                if (value != this.team1PickTradeData)
                {
                    this.team1PickTradeData = value;
                    NotifyPropertyChanged("Team1PickTradeData");
                }
            }
        }

        private DataView team2PickTradeData;
        public DataView Team2PickTradeData
        {
            get
            {
                return this.team2PickTradeData;
            }
            set
            {
                if (value != this.team2PickTradeData)
                {
                    this.team2PickTradeData = value;
                    NotifyPropertyChanged("Team2PickTradeData");
                }
            }
        }

        private DataView team1DraftData;
        public DataView Team1DraftData
        {
            get
            {
                return this.team1DraftData;
            }
            set
            {
                if (value != this.team1DraftData)
                {
                    this.team1DraftData = value;
                    NotifyPropertyChanged("Team1DraftData");
                }
            }
        }

        private DataView team2DraftData;
        public DataView Team2DraftData
        {
            get
            {
                return this.team2DraftData;
            }
            set
            {
                if (value != this.team2DraftData)
                {
                    this.team2DraftData = value;
                    NotifyPropertyChanged("Team2DraftData");
                }
            }
        } 


        private int team1CapSpace;
        public int Team1CapSpace
        {
            get
            {
                return team1CapSpace;
            }
            set
            {
                if (value != team1CapSpace)
                {
                    team1CapSpace = value;
                    NotifyPropertyChanged("Team1CapSpace");
                }
            }
        }

        private int team1Counts;
        public int Team1Counts
        {
            get
            {
                return team1Counts;
            }
            set
            {
                if (value != team1Counts)
                {
                    team1Counts = value;
                    NotifyPropertyChanged("Team1Counts");
                }
            }
        }

        private int team2CapSpace;
        public int Team2CapSpace
        {
            get
            {
                return team2CapSpace;
            }
            set
            {
                if (value != team2CapSpace)
                {
                    team2CapSpace = value;
                    NotifyPropertyChanged("Team2CapSpace");
                }
            }
        }

        private int team2Counts;
        public int Team2Counts
        {
            get
            {
                return team2Counts;
            }
            set
            {
                if (value != team2Counts)
                {
                    team2Counts = value;
                    NotifyPropertyChanged("Team2Counts");
                }
            }
        }       
    }
}

﻿
namespace Football.UiClasses.ViewModel
{
    public class PlaceOnIRView
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public bool IsOnIR { get; set; }
    }
}

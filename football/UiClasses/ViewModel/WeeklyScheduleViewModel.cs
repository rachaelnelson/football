﻿using System.ComponentModel;
using System.Data;

using Football.League.Teams;

namespace Football.UiClasses.ViewModel
{
    public class WeeklyScheduleViewModel
    {
        // http://msdn.microsoft.com/en-us/library/ms133020%28v=vs.100%29.aspx
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private DataView scheduleData;
        public DataView ScheduleData
        {
            get
            {
                return this.scheduleData;
            }
            set
            {
                if (value != this.scheduleData)
                {
                    this.scheduleData = value;
                    NotifyPropertyChanged("ScheduleData");
                }
            }
        }

        private int selectedWeek;
        public int SelectedWeek
        {
            get
            {
                return this.selectedWeek;
            }
            set
            {
                if (value != this.selectedWeek)
                {
                    this.selectedWeek = value;
                    NotifyPropertyChanged("SelectedWeek");
                }
            }
        }

        private int currentWeek;
        public int CurrentWeek
        {
            get
            {
                return this.currentWeek;
            }
            set
            {
                if (value != this.currentWeek)
                {
                    this.currentWeek = value;
                    NotifyPropertyChanged("CurrentWeek");
                }
            }
        }

        private RosterCountView counts;
        public RosterCountView Counts
        {
            get
            {
                return counts;
            }
            set
            {
                if (value != counts)
                {
                    counts = value;
                    NotifyPropertyChanged("Counts");
                }
            }
        }

        public bool IsSimButtonEnabled()
        {
            return (SelectedWeek == CurrentWeek && Counts.ActiveCount >= Counts.MinRosterCount && Counts.ActiveCount <= Counts.ActiveLimit);
        }
    }
}

﻿using System;
using System.ComponentModel;
using System.Data;
using Football.League.Players;
using Football.Settings;

namespace Football.UiClasses.ViewModel
{
    public class LineupViewModel : INotifyPropertyChanged
    {
        // http://msdn.microsoft.com/en-us/library/ms133020%28v=vs.100%29.aspx
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private DataView lineupData;
        public DataView LineupData
        {
            get
            {
                return lineupData;
            }
            set
            {
                if (value != lineupData)
                {
                    lineupData = value;
                    NotifyPropertyChanged("LineupData");
                }
            }
        }

        private Array formations;
        public Array Formations
        {
            get
            {
                return formations;
            }
            set
            {
                if (value != formations)
                {
                    formations = value;
                    NotifyPropertyChanged("Formations");
                }
            }
        }

        private BaseAll selectedFormation;
        public BaseAll SelectedFormation
        {
            get
            {
                return selectedFormation;
            }
            set
            {
                if (value != selectedFormation)
                {
                    selectedFormation = value;
                    NotifyPropertyChanged("SelectedFormation");
                }
            }
        }

        private PlayerType selectedPosition;
        public PlayerType SelectedPosition
        {
            get
            {
                return selectedPosition;
            }
            set
            {
                if (value != selectedPosition)
                {
                    selectedPosition = value;
                    NotifyPropertyChanged("SelectedPosition");
                }
            }
        }

        private int rowSelection;
        public int RowSelection
        {
            get
            {
                return rowSelection;
            }
            set
            {
                if (value != rowSelection)
                {
                    rowSelection = value;
                    NotifyPropertyChanged("RowSelection");
                }
            }
        }

    }
}

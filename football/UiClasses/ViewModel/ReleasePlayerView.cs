﻿namespace Football.UiClasses.ViewModel
{
    public class ReleasePlayerView
    {
        public int Id {get;set;}
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string TeamAbbr { get; set; }
        public bool IsOnIR { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

using Football.League.Teams;

namespace Football.UiClasses.ViewModel
{
    public class TeamScheduleViewModel
    {
        // http://msdn.microsoft.com/en-us/library/ms133020%28v=vs.100%29.aspx
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private DataView scheduleData;
        public DataView ScheduleData
        {
            get
            {
                return this.scheduleData;
            }
            set
            {
                if (value != this.scheduleData)
                {
                    this.scheduleData = value;
                    NotifyPropertyChanged("ScheduleData");
                }
            }
        }

        private List<string> teams;
        public List<string> Teams
        {
            get
            {
                return this.teams;
            }
            set
            {
                if (value != this.teams)
                {
                    this.teams = value;
                    NotifyPropertyChanged("Teams");
                }
            }
        }

        private string selectedTeamAbbr;
        public string SelectedTeamAbbr
        {
            get
            {
                return this.selectedTeamAbbr;
            }
            set
            {
                if (value != this.selectedTeamAbbr)
                {
                    this.selectedTeamAbbr = value;
                    NotifyPropertyChanged("SelectedTeamAbbr");
                }
            }
        }

        private int lastWeek;
        public int LastWeek
        {
            get
            {
                return this.lastWeek;
            }
            set
            {
                if (value != this.lastWeek)
                {
                    this.lastWeek = value;
                    NotifyPropertyChanged("LastWeek");
                }
            }
        }

        private int currentWeek;
        public int CurrentWeek
        {
            get
            {
                return this.currentWeek;
            }
            set
            {
                if (value != this.currentWeek)
                {
                    this.currentWeek = value;
                    NotifyPropertyChanged("CurrentWeek");
                }
            }
        }

        private RosterCountView counts;
        public RosterCountView Counts
        {
            get
            {
                return counts;
            }
            set
            {
                if (value != counts)
                {
                    counts = value;
                    NotifyPropertyChanged("Counts");
                }
            }
        }

        public bool IsSimButtonEnabled()
        {
            return (CurrentWeek <= LastWeek && Counts.ActiveCount >= Counts.MinRosterCount && Counts.ActiveCount <= Counts.ActiveLimit);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

namespace Football.UiClasses.ViewModel
{
    public class InjuryViewModel : INotifyPropertyChanged
    {      
        // http://msdn.microsoft.com/en-us/library/ms133020%28v=vs.100%29.aspx
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private List<string> teams;
        public List<string> Teams 
        {
            get
            {
                return this.teams;
            }
            set
            {
                if (value != this.teams)
                {
                    this.teams = value;
                    NotifyPropertyChanged("Teams");
                }
            }
        }

        private DataView injuredList;
        public DataView InjuredList
        {
            get
            {
                return this.injuredList;
            }
            set
            {
                if (value != this.injuredList)
                {
                    this.injuredList = value;
                    NotifyPropertyChanged("InjuredList");
                }
            }
        }

        private string selectedTeamAbbr;
        public string SelectedTeamAbbr
        {
            get
            {
                return this.selectedTeamAbbr;
            }
            set
            {
                if (value != this.selectedTeamAbbr)
                {
                    this.selectedTeamAbbr = value;
                    NotifyPropertyChanged("SelectedTeamAbbr");
                }
            }
        }

        private string myTeam;
        public string MyTeam
        {
            get
            {
                return this.myTeam;
            }
            set
            {
                this.myTeam = value;
            }
        }

        private PlaceOnIRView irPlayer;
        public PlaceOnIRView IRPlayer
        {
            get
            {
                return irPlayer;
            }
            set
            {
                if (value != irPlayer)
                {
                    irPlayer = value;
                    NotifyPropertyChanged("IRPlayer");
                }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using Football.League.Players;
using Football.League.Teams;

namespace Football.UiClasses.ViewModel
{
    public class RosterViewModel : INotifyPropertyChanged
    {      
        // http://msdn.microsoft.com/en-us/library/ms133020%28v=vs.100%29.aspx
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private PlayerType selectedPosition;
        public PlayerType SelectedPosition 
        { 
            get 
            {
                return this.selectedPosition;
            }

            set
            {
                if (value != this.selectedPosition)
                {
                    this.selectedPosition = value;
                    NotifyPropertyChanged("SelectedPosition");
                }
            }
        }
        
        public List<PlayerType> Positions { get; set; }

        private DataView rosterData;
        public DataView RosterData 
        {
            get
            {
                return this.rosterData;
            }
            set
            {
                if (value != this.rosterData)
                {
                    this.rosterData = value;
                    NotifyPropertyChanged("RosterData");
                }
            }
        }

        private ReleasePlayerView releasePlayer;
        public ReleasePlayerView ReleasePlayer
        {
            get
            {
                return this.releasePlayer;
            }
            set
            {
                if (value != this.releasePlayer)
                {
                    this.releasePlayer = value;
                    NotifyPropertyChanged("ReleasePlayer");
                }
            }
        }

        private int capSpace;
        public int CapSpace
        {
            get
            {
                return capSpace;
            }
            set
            {
                if (value != capSpace)
                {
                    capSpace = value;
                    NotifyPropertyChanged("CapSpace");
                }
            }
        }

        private RosterCountView counts;
        public RosterCountView Counts
        {
            get
            {
                return counts;
            }
            set
            {
                if (value != counts)
                {
                    counts = value;
                    NotifyPropertyChanged("Counts");
                }
            }
        }

        private int currentCapYear;
        public int CurrentCapYear
        {
            get
            {
                return currentCapYear;
            }
            set
            {
                if (value != currentCapYear)
                {
                    currentCapYear = value;
                    NotifyPropertyChanged("CurrentCapYear");
                }
            }
        }

    }
}

﻿using System;
using System.ComponentModel;

namespace Football.UiClasses.ViewModel
{
    public class SignPlayerView : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public int SalaryRequested { get; set; }
        public string SalaryRequestedFormatted { get { return SalaryRequested.ToString("0,0"); } }
        public int SalaryYears { get; set; }
        public bool? SignSuccess { get; set; }       
    }
}

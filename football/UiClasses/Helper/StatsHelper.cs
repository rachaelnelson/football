﻿using System.Collections.Generic;
using Football.Library;
using Football.Stats.Converters;
using Football.Stats.Records;

namespace Football.UiClasses.Helper
{
    public static class StatsHelper
    {       
        public static SortableBindingList<T> getStatRecords<T>(List<StatsRecord> statsRecord)
        {
            List<T> tList = statsRecord.ConvertAll<T>(StatListConverter.ConvertFromStatsRecord<StatsRecord, T>);
            SortableBindingList<T> records = new SortableBindingList<T>();

            foreach (var item in tList)
            {                               
                records.Add(item);
            }            
            return records;
        }      
       
    }
}

﻿using System.Collections.Generic;
using System.Data;
using Football.League.Conferences;
using Football.League.Teams.TeamRecord;
using Football.Settings;


namespace UiClasses.Helper
{
    public class StandingsHelper
    {
        private FootballSettings setting;

        public StandingsHelper(FootballSettings setting)
        {
            this.setting = setting;
        }

        // returns choices for division combobox
        public List<string> getDivisionChoices()
        {
            // TODO:  Might want to sort these....
            List<string> comboList = new List<string>();
            comboList.Insert(0, "ALL");              
            comboList.AddRange(setting.leagueStructure.getAllConferenceNames());
            comboList.AddRange(setting.leagueStructure.getAllDivisionNames());           
                                           
            return comboList;
        }        

        public DataTable loadStandings()
        {            
            DataTable table = new DataTable();

            this.loadHeaders(table); // headers
            this.loadRows(table);// rows
            return table;
        }

        // might could load all of these as strings and do toString on the rows....
        private void loadHeaders( DataTable table )
        {
            table.Columns.Add("Team");
            table.Columns.Add("Wins");
            table.Columns.Add("Losses");
            table.Columns.Add("Ties");
            table.Columns.Add("Win %");
            table.Columns.Add("Conference");  // hidden
            table.Columns.Add("Division"); // hidden           
        }
        
        private void loadRows( DataTable table )
        {                     
            List<Conference> conferences = setting.leagueStructure.getConferences();            
            
            // TODO:  See if there is a way to make this with less loops....                   
            foreach(var conf in conferences)
            {
                foreach (var div in conf.Divisions)
                {
                    foreach (var team in div.Teams)
                    {                       
                        TeamRecord record = setting.teamRecordList.GetTeamRecord(team);

                        string teamName = team.Name;
                        string wins = record.OverallRecord.getWin().ToString();
                        string losses = record.OverallRecord.getLoss().ToString();
                        string ties = record.OverallRecord.getTie().ToString();
                        string winPerc = record.OverallRecord.getWinPercentage().ToString("0.000");                                             
                        // load data into table
                        table.Rows.Add(teamName, wins, losses, ties, winPerc, conf.Name, div.Name);
                    }
                }
            }           
        }  
    
    }
}

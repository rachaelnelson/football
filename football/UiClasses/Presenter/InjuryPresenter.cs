﻿using System.Collections.Generic;
using Football.League.Injuries;
using Football.League.Players;
using Football.Services;
using Football.UiClasses.DataViews;
using Football.UiClasses.ViewModel;

namespace Football.UiClasses.Presenter
{
    public class InjuryPresenter
    {
        private InjuryViewModel viewModel;
        public InjuryViewModel ViewModel { get { return this.viewModel; } }

        private FootballSettingsService settingsService;
        private RosterService rosterService;
        private InjuryDataView injuryDataView;

        public InjuryPresenter(FootballSettingsService settingsService, RosterService rosterService)
        {
            this.settingsService = settingsService;
            this.rosterService = rosterService;

            injuryDataView = new InjuryDataView();

            viewModel = new InjuryViewModel();
            viewModel.Teams = settingsService.getTeamAbbreviations();
            viewModel.InjuredList = injuryDataView.getInjuryDataView();
            viewModel.MyTeam = settingsService.getMyTeamByAbbreviation();
        }

        public void TeamChanged(object selected = null)
        {
            string teamAbbr = null;

            if (selected != null)
            {
                teamAbbr = (string)selected;
            }

            loadInjuryData(teamAbbr);
            viewModel.SelectedTeamAbbr = teamAbbr;
        }

        private void loadInjuryData(string teamAbbr)
        {
            IEnumerable<InjuryView> injuryList = rosterService.getInjuredList(teamAbbr);
            injuryDataView.updateInjuryDataTable(injuryList);
        }

        public void InitiatePlaceOnIR(object item = null)
        {
            viewModel.IRPlayer = null;

            int id = 0;

            if (item != null)
            {
                id = (int)item;
            }

            Player player = rosterService.GetPlayerById(id);
            ViewModel.IRPlayer = new PlaceOnIRView
            {
                Id = player.id,
                FirstName = player.firstName,
                LastName = player.lastName,
                Position = player.position.ToString(),
                IsOnIR = rosterService.IsOnIR(player)
            };
        }

        public void PlaceOnIR()
        {
            int playerId = ViewModel.IRPlayer.Id;
            Player player = rosterService.GetPlayerById(playerId);
            rosterService.AddPlayerToIR(player);
            viewModel.IRPlayer = null;
            TeamChanged(viewModel.SelectedTeamAbbr);
        }
    }
}

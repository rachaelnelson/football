﻿using System;
using System.Collections.Generic;
using Football.League.Players;
using Football.League.Teams;
using Football.Services;
using Football.Services.Starter;
using Football.UiClasses.DataViews;
using Football.UiClasses.ViewModel;

namespace Football.UiClasses.Presenter
{   
    public class RosterPresenter
    {
        private RosterDataView rosterDataView;
        private CapManagementService capService;
        private RosterViewModel viewModel;
        private RosterService rosterService;
        private ReleasePlayerService releasePlayerService;

        public RosterViewModel ViewModel { get { return viewModel; } }

        public RosterPresenter(CapManagementService capService, RosterService rosterService, ReleasePlayerService releasePlayerService)
        {
            this.capService = capService;
            this.rosterService = rosterService;
            this.releasePlayerService = releasePlayerService;

            rosterDataView = new RosterDataView(capService.getCurrentCapYear());

            viewModel = new RosterViewModel();
            viewModel.Positions =  PlayerType.GetPlayerTypesList();
            viewModel.RosterData = rosterDataView.getDataView();
            viewModel.CapSpace = capService.getCurrentCapSpace();
            viewModel.Counts = rosterService.GetRosterCounts();
            viewModel.CurrentCapYear = capService.getCurrentCapYear();
        }        
        
        public void PositionChanged(PlayerType selected = null)
        {
            PlayerType type = PlayerType.QB;

            if (selected != null)
            {                
                type = selected;
            }
            
            this.loadRosterData(type);
            this.viewModel.SelectedPosition = type;            
        }
        
        private void loadRosterData(PlayerType position) 
        {       
            IEnumerable<PlayerRosterView> rosterList = rosterService.getRosterListByPosition(position);
            rosterDataView.updateDataTable(rosterList);                            
        }

        public void StatusChanged(object item)
        {
            if (item != null)
            {
                int id = Convert.ToInt32(item);

                Player player = rosterService.GetPlayerById(id);

                rosterService.ChangeRosterStatus(player);

                viewModel.Counts = rosterService.GetRosterCounts();
                PositionChanged(ViewModel.SelectedPosition);
            }
        }

        public void InitiateReleasePlayer(object item)
        {
            this.viewModel.ReleasePlayer = null;

            if (item != null)
            {
                int id = Convert.ToInt32(item);                

                Player player = rosterService.GetPlayerById(id);

                this.ViewModel.ReleasePlayer = new ReleasePlayerView
                {
                    Id = player.id,
                    FirstName = player.firstName,
                    LastName = player.lastName,
                    Position = player.position.ToString(),
                    IsOnIR = rosterService.IsOnIR(player),
                    TeamAbbr = player.teamAbbr
                };
            }           
        }

        public void ReleasePlayer()
        {
            releasePlayerService.ReleasePlayer( viewModel.ReleasePlayer );

            viewModel.ReleasePlayer = null;            
            
            viewModel.CapSpace = capService.getCurrentCapSpace();
            viewModel.Counts = rosterService.GetRosterCounts();

            PositionChanged(ViewModel.SelectedPosition);
        }
    }    
}

﻿using System.Collections.Generic;

using Football.League.Schedules;
using Football.Services;
using Football.UiClasses.DataViews;
using Football.UiClasses.ViewModel;

namespace Football.UiClasses.Presenter
{
    public class WeeklySchedulePresenter
    {
        private RosterService rosterService;
        private ScheduleService scheduleService;
        private SimService simService;
        private WeeklyScheduleDataView dataView;
        private WeeklyScheduleViewModel viewModel;
        public WeeklyScheduleViewModel ViewModel { get { return this.viewModel; } }

        public WeeklySchedulePresenter(ScheduleService scheduleService, SimService simService, RosterService rosterService)
        {
            this.scheduleService = scheduleService;
            this.simService = simService;
            this.rosterService = rosterService;

            dataView = new WeeklyScheduleDataView();
            viewModel = new WeeklyScheduleViewModel();
            viewModel.ScheduleData = dataView.getDataView();
            viewModel.SelectedWeek = scheduleService.GetCurrentWeek();
            viewModel.CurrentWeek = scheduleService.GetCurrentWeek();
            viewModel.Counts = rosterService.GetRosterCounts();
        }

        public void WeekChanged()
        {            
            this.loadScheduleData(viewModel.SelectedWeek);
        }

        public void WeekChangedIncrease()
        {
            if (viewModel.SelectedWeek >= 1 && viewModel.SelectedWeek < scheduleService.GetNumberOfWeeks())
            {
                viewModel.SelectedWeek++;                
            }
            this.loadScheduleData(viewModel.SelectedWeek);
        }

        public void WeekChangedDecrease()
        {
            if (viewModel.SelectedWeek > 1 && viewModel.SelectedWeek <= scheduleService.GetNumberOfWeeks())
            {
                viewModel.SelectedWeek--;                
            }
            this.loadScheduleData(viewModel.SelectedWeek);
        }

        private void loadScheduleData(int week)
        {            
            IEnumerable<WeeklyScheduleView> scheduleList = scheduleService.GetScheduleByWeek(week);
            dataView.updateDataTable(scheduleList);
        }
        
        public void InitiateSim()
        {            
            SimWeek();
        }
        
        private void SimWeek()
        {
            int week = viewModel.CurrentWeek;
            List<ScheduleItem> scheduleList = scheduleService.GetRawScheduleByWeek(week);
            simService.SimWeek( scheduleList );
            loadScheduleData(week);
            viewModel.CurrentWeek = scheduleService.GetCurrentWeek();
        }

        public string LoadGameLog(string log)
        {
            return scheduleService.LoadGameLog(log);            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Football.League.Players;
using Football.League.Teams;
using Football.Services;
using Football.Settings;
using Football.UiClasses.DataViews;
using Football.UiClasses.ViewModel;

namespace Football.UiClasses.Presenter
{
    public class LineupPresenter
    {
        private RosterService rosterService;
        private LineupViewModel viewModel;
        public LineupViewModel ViewModel { get { return viewModel; } }

        private LineupDataView lineupDataView;

        public LineupPresenter(RosterService rosterService)
        {
            this.rosterService = rosterService;

            lineupDataView = new LineupDataView();

            viewModel = new LineupViewModel();
            viewModel.LineupData = lineupDataView.getDataView();
            viewModel.Formations = Enum.GetValues(typeof(BaseAll));            
        }

        public void PositionChanged(PlayerType position)
        {
            loadLineupData(viewModel.SelectedFormation, position);
            viewModel.SelectedPosition = position;
        }

        public void FormationChanged(object selected)
        {
            BaseAll formationSelected = BaseAll.TwoRB;

            if (selected != null)
            {
                formationSelected = (BaseAll)selected;
            }
            
            loadLineupData(formationSelected,viewModel.SelectedPosition);
            viewModel.SelectedFormation = formationSelected;
        }

        private void loadLineupData(BaseAll formation, PlayerType position)
        {
            IEnumerable<LineupView> lineupList = rosterService.GetLineupPlayers(formation,position);
            lineupDataView.updateDataTable(lineupList);
        }

        public void Promote(object item)
        {
            int playerId = 0;

            if (item != null)
            {                
                playerId = (int)item;                
            }

            Player player = rosterService.GetPlayerById(playerId);

            viewModel.RowSelection = rosterService.PromotePlayer(player, viewModel.SelectedFormation);
            PositionChanged(viewModel.SelectedPosition);
        }

        public void Demote(object item)
        {
            int playerId = 0;

            if (item != null)
            {
                playerId = (int)item;
            }

            Player player = rosterService.GetPlayerById(playerId);

            viewModel.RowSelection = rosterService.DemotePlayer(player, viewModel.SelectedFormation);
            PositionChanged(viewModel.SelectedPosition);
        }
    }
}

﻿using System;
using System.Collections.Generic;

using Football.League.Players;
using Football.League.Teams;
using Football.Services;
using Football.UiClasses.DataViews;
using Football.UiClasses.ViewModel;
using System.Collections;

namespace Football.UiClasses.Presenter
{    
    public class TradePresenter
    {           
        private CapManagementService capService;

        private RosterDataView team1RosterDataView;
        private RosterDataView team2RosterDataView;
               
        private TradePlayerDataView team1TradePlayerDataView;
        private TradePlayerDataView team2TradePlayerDataView;

        private TradePickDataView team1TradePickDataView;
        private TradePickDataView team2TradePickDataView;

        private DraftPickDataView team1DraftPickDataView;
        private DraftPickDataView team2DraftPickDataView;

        private RosterService rosterService;

        private TradeViewModel viewModel;                                           
        public  TradeViewModel ViewModel { get { return viewModel; } }    

        private TransactionService transactionService;
        private FootballSettingsService settingsService;
        
        public TradePresenter(FootballSettingsService settingsService, TransactionService transactionService, CapManagementService capService, RosterService rosterService)
        {
            this.transactionService = transactionService;
            this.capService = capService;
            this.rosterService = rosterService;
            this.settingsService = settingsService;

            transactionService.NewTransaction();

            // Available Players
            team1RosterDataView = new RosterDataView(capService.getCurrentCapYear());
            team2RosterDataView = new RosterDataView(capService.getCurrentCapYear());

            // Available Picks
            team1DraftPickDataView = new DraftPickDataView();
            team2DraftPickDataView = new DraftPickDataView();

            // Players in Trade
            team1TradePlayerDataView = new TradePlayerDataView();
            team2TradePlayerDataView = new TradePlayerDataView();

            // Picks in Trade
            team1TradePickDataView = new TradePickDataView();
            team2TradePickDataView = new TradePickDataView();                        
            
            viewModel = new TradeViewModel();            
            viewModel.Teams = settingsService.getTeamAbbreviations();
            viewModel.Team1 = settingsService.getMyTeamByAbbreviation();            

            // ViewModel Roster
            viewModel.Team1RosterData = team1RosterDataView.getDataView();
            viewModel.Team2RosterData = team2RosterDataView.getDataView();

            // ViewModel Picks
            viewModel.Team1DraftData = team1DraftPickDataView.getDataView();          
            viewModel.Team2DraftData = team2DraftPickDataView.getDataView();

            // View Model Players in Trade
            viewModel.Team1PlayerTradeData = team1TradePlayerDataView.getDataView();
            viewModel.Team2PlayerTradeData = team2TradePlayerDataView.getDataView();

            // View Model Picks in Trade
            viewModel.Team1PickTradeData = team1TradePickDataView.getDataView();
            viewModel.Team2PickTradeData = team2TradePickDataView.getDataView();
                        
            viewModel.Teams.Remove(viewModel.Team1);

            // View Model Cap and Counts
            viewModel.Team1CapSpace = capService.getCurrentCapSpace();
            viewModel.Team1Counts = transactionService.GetPostTradeActiveCount(viewModel.Team1);
            
            loadRosterData(viewModel.Team1, team1RosterDataView); // load my team's data                
        }

        public void AddPickToTrade( object item, object abbr = null )
        {
            if (item != null)
            {
                string id = item as string;
                DraftPicks.DraftPick pick = settingsService.GetPickById( id );
                transactionService.AddDraftPickToTransaction( pick );
                UpdateViewModel();
            }
        }

        public void AddToTrade(object item, object abbr=null)
        {
            if (item != null)
            {
                int id = Convert.ToInt32(item);
                string teamAbbr = Convert.ToString(abbr);
                Player player = rosterService.GetPlayerById(id, teamAbbr);
                transactionService.AddPlayerToTransaction(player);
                UpdateViewModel();
            }                   
        }        
        
        public void TeamChanged(object selected = null)
        {
            string team2Abbr = null;

            if (selected != null)
            {
                team2Abbr = (string)selected;
            }
                        
            viewModel.SelectedTeamAbbr = team2Abbr;
            viewModel.Team2 = team2Abbr;            
            UpdateViewModel();   
        }
        
        public void UpdateViewModel()
        {
            viewModel.Team1CapSpace = transactionService.GetPostTradeSalaryCap(viewModel.Team1);
            viewModel.Team1Counts = transactionService.GetPostTradeActiveCount(viewModel.Team1);

            viewModel.Team2CapSpace = transactionService.GetPostTradeSalaryCap(viewModel.Team2);
            viewModel.Team2Counts = transactionService.GetPostTradeActiveCount(viewModel.Team2);
            
            // Update players in trade box
            team1TradePlayerDataView.updateDataTable( transactionService.GetPlayerTradeList( viewModel.Team1 ) );
            team2TradePlayerDataView.updateDataTable( transactionService.GetPlayerTradeList( viewModel.Team2 ) );

            // Update picks in trade box
            team1TradePickDataView.updateDataTable(transactionService.GetDraftTradeList(viewModel.Team1));
            team2TradePickDataView.updateDataTable(transactionService.GetDraftTradeList(viewModel.Team2));                                    
            
            // Update Available Picks
            team1DraftPickDataView.updateDataTable( settingsService.GetPicksByTeam( viewModel.Team1 ) );            
            team2DraftPickDataView.updateDataTable( settingsService.GetPicksByTeam( viewModel.Team2 ) );

            // Update Available Players
            loadRosterData(viewModel.Team1, team1RosterDataView);
            loadRosterData(viewModel.Team2, team2RosterDataView);            
        }

        private void loadRosterData(string teamAbbr, RosterDataView dataView)
        {
            IEnumerable<PlayerRosterView> rosterList = rosterService.GetRosterList(teamAbbr);
            dataView.updateDataTable(rosterList);              
        }
        
        public void RemoveFromTrade(object item, object abbr = null)
        {
            if (item != null)
            {
                int id = Convert.ToInt32(item);
                string teamAbbr = Convert.ToString(abbr);
                Player player = rosterService.GetPlayerById(id,teamAbbr);
                transactionService.RemovePlayerFromTrade(player);
                UpdateViewModel();
            }
        }

        public void RemovePickFromTrade(object item, object abbr = null)
        {
            if (item != null)
            {
                string id = item as string;
                string teamAbbr = Convert.ToString(abbr);
                DraftPicks.DraftPick pick = settingsService.GetPickById(id);
                transactionService.RemovePickFromTrade(pick);
                UpdateViewModel();
            }
        }        
        
        public void ClearTrade()
        {
            transactionService.ClearTransaction();
            team1TradePlayerDataView.Clear();
            team2TradePlayerDataView.Clear();
            UpdateViewModel();
        }

        public void Trade()
        {
            transactionService.ConductTransaction();
            ClearTrade();
            UpdateViewModel();
        }        

        public bool IsTradeValid()
        {
            return transactionService.IsValid();
        }
    }
}

﻿using System.Collections.Generic;

using Football.League.Schedules;
using Football.Services;
using Football.UiClasses.DataViews;
using Football.UiClasses.ViewModel;

namespace Football.UiClasses.Presenter
{
    public class TeamSchedulePresenter
    {
        private RosterService rosterService;
        private FootballSettingsService settingsService;
        private ScheduleService scheduleService;
        private SimService simService;

        private TeamScheduleDataView dataView;

        private TeamScheduleViewModel viewModel;
        public TeamScheduleViewModel ViewModel { get { return this.viewModel; } }

        public TeamSchedulePresenter(FootballSettingsService settingsService, ScheduleService scheduleService, SimService simService, RosterService rosterService)
        {
            this.settingsService = settingsService;
            this.scheduleService = scheduleService;
            this.simService = simService;
            this.rosterService = rosterService;

            dataView = new TeamScheduleDataView();

            viewModel = new TeamScheduleViewModel();
            viewModel.CurrentWeek = this.scheduleService.GetCurrentWeek();
            viewModel.LastWeek = this.scheduleService.GetNumberOfWeeks();
            viewModel.Teams = this.settingsService.getTeamAbbreviations();
            viewModel.ScheduleData = dataView.getDataView();
            viewModel.Counts = rosterService.GetRosterCounts();
        }

        public void TeamChanged(object selected = null)
        {
            string teamAbbr = null;

            if (selected != null)
            {
                teamAbbr = (string)selected;
            }

            loadScheduleData(teamAbbr);
            viewModel.SelectedTeamAbbr = teamAbbr;
        }

        private void loadScheduleData(string teamAbbr)
        {
            IEnumerable<TeamScheduleView> scheduleList = scheduleService.GetScheduleByTeam(teamAbbr);
            dataView.updateDataTable(scheduleList);
        }

        public void SimWeek()
        {
            int week = viewModel.CurrentWeek;
            List<ScheduleItem> scheduleList = scheduleService.GetRawScheduleByWeek(week);
            simService.SimWeek(scheduleList);
            loadScheduleData(viewModel.SelectedTeamAbbr);
            viewModel.CurrentWeek = scheduleService.GetCurrentWeek();
        }

        public string LoadGameLog(string log)
        {
            return scheduleService.LoadGameLog(log);
        }
    }
}

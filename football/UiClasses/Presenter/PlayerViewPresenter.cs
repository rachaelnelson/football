﻿using System;
using System.Collections.Generic;
using System.Data;

using Football.League.Players;
using Football.Library;
using Football.Services;
using Football.Stats;
using Football.Stats.Records;
using Football.UiClasses.Helper;
using Football.UiClasses.PlayerViewer;


namespace Football.UiClasses.Presenter
{
    public class PlayerViewPresenter
    {
        private RosterService rosterService;
        private IPlayerViewer playerViewer;
        private Dictionary<string, Delegate> delegateDict;
        private FootballStats stats;
        private int currentCapYear;  // hax to get salary to display...

        public PlayerViewPresenter(RosterService rosterService, IPlayerViewer playerViewer, FootballStats stats)
        {
            this.rosterService = rosterService;
            this.playerViewer = playerViewer;
            this.stats = stats;

            this.delegateDict = new Dictionary<string, Delegate>()
            {
                {"Passing", new Func<List<StatsRecord>,SortableBindingList<PassingStatRecord>>(StatsHelper.getStatRecords<PassingStatRecord>)},
                {"Rushing", new Func<List<StatsRecord>,SortableBindingList<RushingStatRecord>>(StatsHelper.getStatRecords<RushingStatRecord>)},
                {"Receiving", new Func<List<StatsRecord>,SortableBindingList<ReceivingStatRecord>>(StatsHelper.getStatRecords<ReceivingStatRecord>)},
                {"Defense", new Func<List<StatsRecord>,SortableBindingList<DefenseStatRecord>>(StatsHelper.getStatRecords<DefenseStatRecord>)},
                {"Kicking", new Func<List<StatsRecord>,SortableBindingList<KickingStatRecord>>(StatsHelper.getStatRecords<KickingStatRecord>)},
                {"Punting", new Func<List<StatsRecord>,SortableBindingList<PuntingStatRecord>>(StatsHelper.getStatRecords<PuntingStatRecord>)}
            };
        }        

        // TODO:  make this not suck...        
        public object GetStats(Player player)
        {
            string desc = player.position.StatDesc;

            return (delegateDict.ContainsKey(desc)) ? delegateDict[desc].DynamicInvoke(stats.getPlayerStats(desc, player)) : null;                   
        }

        public void Display(object item, int year)
        {
            this.currentCapYear = year;

            Display(item);
        }

        public void Display(object item)
        {
            Player player = null;

            if (item != null)
            {
                int id = Convert.ToInt32(item);

                // TODO:  Bury this lower into the service layer.  Should be able to call GetPlayerById and have him returned, 
                //        regardless if FA, potential/current draftee, retired or whomever.
                player = rosterService.GetPlayerById(id, "FA");

                if (player == null)
                {
                    player = rosterService.GetPlayerById(id);
                }

                PlayerViewModel playerViewModel = new PlayerViewModel
                {
                    playerInfo = new PVMPersonal
                    {
                        Name = String.Format("{0} {1}", player.firstName, player.lastName),
                        Position = player.position.ToString(),
                        DOB = player.birthDate,
                        Team = player.teamAbbr
                    },
                    playerAttributes = PVMAttributesFactory.GetAttributes(player),
                    playerStats = GetStats(player),
                    playerSalary = GetSalary(player)
                };

                // eventually pass a playerViewModel with all the necessary info such as stats.
                playerViewer.Display(playerViewModel);
            }            
        }

        // TODO:  abstract this out.  Is also used in the RosterDataView and in player contract. 
        // Probably needs to be a method on player contract.
        private DataTable GetSalary(Player player)
        {
            DataTable table = new DataTable();
            List<object> rowList = new List<object>();                        
            
            for (int i = player.contract.Year - 1; i < player.contract.Salary.Length; i++)
            {                
                int year = currentCapYear + i;
                table.Columns.Add("Salary for " + year.ToString(), typeof(string));
                rowList.Add(player.contract.Salary[i].ToString("0,0"));
            }
            
            table.Rows.Add(rowList.ToArray());
            return table;
        }       
    }    
}

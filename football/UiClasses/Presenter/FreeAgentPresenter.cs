﻿using System;
using System.Collections.Generic;
using Football.League.Players;
using Football.League.Teams;
using Football.Services;
using Football.UiClasses.DataViews;
using Football.UiClasses.ViewModel;

namespace Football.UiClasses.Presenter
{
    public class FreeAgentPresenter
    {
        private CapManagementService capService;
        private RosterService rosterService;
        private SignPlayerService faService;
        private FreeAgentDataView freeAgentDataView;
        private FreeAgentViewModel viewModel;

        public FreeAgentViewModel ViewModel { get { return this.viewModel; } }      

        public FreeAgentPresenter(CapManagementService capService, RosterService rosterService, SignPlayerService faService)
        {
            this.capService = capService;
            this.rosterService = rosterService;
            this.faService = faService;

            this.freeAgentDataView = new FreeAgentDataView();

            this.viewModel = new FreeAgentViewModel();
            this.viewModel.Positions = PlayerType.GetPlayerTypes();
            this.viewModel.FreeAgentData = this.freeAgentDataView.getDataView();
            this.viewModel.CapSpace = this.capService.getCurrentCapSpace().ToString("0,0");
        }

        public void PositionChanged(object selected = null)
        {
            PlayerType type = PlayerType.QB;

            if (selected != null)
            {
                type = PlayerType.GetPlayerTypeByString((string)selected);
            }

            this.LoadFreeAgentData(type);
            this.viewModel.SelectedPosition = type;
        }

        private void LoadFreeAgentData(PlayerType position)
        {
            IEnumerable<FreeAgentView> freeAgentList = faService.GetFreeAgentListByPosition(position);
            freeAgentDataView.updateDataTable(freeAgentList);
        }

        public void InitiateSignPlayer(object item, object item2)
        {
            this.viewModel.SignPlayer = null;

            if (item != null && item2!=null)
            {
                int id = Convert.ToInt32(item);
                int salary = Convert.ToInt32(item2);

                Player player = rosterService.GetPlayerById(id,"FA");

                this.ViewModel.SignPlayer = new SignPlayerView
                {
                    SignSuccess = null,
                    Id = player.id,
                    FirstName = player.firstName,
                    LastName = player.lastName,
                    Position = player.position.ToString(),
                    SalaryRequested = salary,
                    SalaryYears = 1 // default to 1 .. TODO: This isn't synching nicely...if bored, see if you can fix.
                };
            }
        }

        public void SignPlayer(object item)
        {
            int playerId = this.viewModel.SignPlayer.Id;
            int salary = Convert.ToInt32(this.viewModel.SignPlayer.SalaryRequested);
            int salaryYears = Convert.ToInt32(item);

            Player player = rosterService.GetPlayerById(playerId,"FA");            
            bool isSigned = faService.SignPlayer(player, salary, salaryYears);

            if (isSigned)
            {
                this.PositionChanged(this.ViewModel.SelectedPosition.ToString());
                this.viewModel.SignPlayer.SignSuccess = true;
            }
            else
            {
                this.viewModel.SignPlayer.SignSuccess = false;
            }
        }

        public void CompleteSigning()
        {
            this.viewModel.CapSpace = this.capService.getCurrentCapSpace().ToString("0,0");
            this.viewModel.SignPlayer = null;
        }

    }
}
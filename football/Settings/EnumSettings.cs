﻿
namespace Football.Settings
{
    public enum BaseSpecial { KP }
    public enum BaseOffense { TwoTE, TwoRB, ThreeWR }
    public enum BaseDefense { FourThree, ThreeFour }
    public enum BaseAll { TwoTE, TwoRB, ThreeWR, FourThree, ThreeFour, KP }   
}

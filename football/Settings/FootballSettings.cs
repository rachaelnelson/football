﻿using System;
using System.Collections.Generic;
using Football.League.Conferences;
using Football.League.Draft;
using Football.League.Players;
using Football.League.Playoffs;
using Football.League.Schedules;
using Football.League.Teams;
using Football.League.Teams.TeamRecord;
using Football.League.Transaction;
using Football.Stats.Records;

namespace Football.Settings
{
    [Serializable()]
    public sealed class FootballSettings
    {       
        public string myTeam { get; set; }  //TODO:  figure out why this is a string...update - it's the key to the roster hash       
        public TeamList teams { get; set; }  // set in Util.loadTeams       
        public Team freeAgentsTeam { get; set; }  // set in Util.loadTeams             
        public byte thisWeek { get; set; }
        public int year { get; set; }
        public Schedule schedule { get; set; }
        public bool injuriesEnabled { get; set; }
        public string tempLogDirectory { get; set; }

        public Dictionary<string, List<StatsRecord>> statsList { get; set; }
        public Dictionary<string, Roster> rosterList { get; set; }
        public DraftPickList DraftList { get; set; }
        public TeamRecordList teamRecordList { get; set; }
        public TradeValue TradeValue { get; set; }
        
        public Structure leagueStructure { get; set; }

        public PlayerTypeValues playerTypeValues { get;set; }

        private static FootballSettings instance = FootballSettings.setSettings();

        public List<FootballPlayoffBracket> PlayoffBracket {get;set;}                
                    
        public String appPath
        {
            get
            {
                return String.Format(@"{0}\FootballSimulator\", System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData));
            }
        }
       
        // Only called in the Open game close dialog event handler to return the new setting
        public static FootballSettings getInstance()
        {
            return instance;
        }    

        // Should only ever be called from Util.openGame and Main;                
        public static FootballSettings setSettings(FootballSettings newSetting=null)
        {
            if (newSetting == null)
            {
                instance = new FootballSettings();
            }
            else
            {
                instance = newSetting;
            }
            return instance;
        }        
        
        private FootballSettings()
        {
            myTeam = "";            
            tempLogDirectory = "";
            thisWeek = 1; // default current week to 1 ... need to increment in sim logic
            injuriesEnabled = false;
            statsList = new Dictionary<String, List<StatsRecord>>(); // TODO:  make this configurable
            rosterList = new Dictionary<String, Roster>(); // TODO:  make this configurable
            teamRecordList = new TeamRecordList(); // TODO:  make this configurable
            PlayoffBracket = new List<FootballPlayoffBracket>();
            //DraftList = new DraftPickList();
        }

        // TODO:  This team stuff should be in TeamService....

        // returns all teams in a list
        public Dictionary<string, Team> getTeams()
        {
            return teams.getTeamList();
        }

        public Team getMyTeam()
        {
            return teams.getTeam(myTeam);
        }

        public Team getFATeam()
        {
            return freeAgentsTeam;
        }
    }
}
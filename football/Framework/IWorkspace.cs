namespace HelloScreensWPF.Framework {
    public interface IWorkspace {
        string Icon { get; }
        string IconName { get; set; }
        string Status { get; set; }

        void Show();
    }
}
using Caliburn.Micro;
namespace HelloScreensWPF.Framework {
    public interface IShell : IConductor, IGuardClose {
        IDialogManager Dialogs { get; }
    }
}
using Caliburn.Micro;
namespace HelloScreensWPF.Framework {
    public interface IHaveShutdownTask {
        IResult GetShutdownTask();
    }
}
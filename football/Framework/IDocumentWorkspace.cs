namespace HelloScreensWPF.Framework {
    public interface IDocumentWorkspace : IWorkspace {
        void Edit(object document);
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using Football.League.Players;
using Football.League.PlayerValue;
using Football.League.Salary;
using Football.League.Teams;
using Football.UiClasses.ViewModel;

namespace Football.Services
{    
    public class SignPlayerService
    {        
        private RosterService rosterService;
        private StarterService starterService;
        private TransactionService transService;
        private FootballSettingsService settingsService;

        public SignPlayerService(RosterService rosterService, StarterService starterService, TransactionService transService, FootballSettingsService settingsService)
        {
            this.rosterService = rosterService;
            this.starterService = starterService;
            this.transService = transService;
            this.settingsService = settingsService;
        }

        public bool SignFreeAgentByPosition( string team, PlayerType position, int maxSalary )
        {
            /*
            (1) Lowest rated starter rating * 1.5.

            (2) Look for free agents that are 1.2+ times better than current rated starter. Start with highest rated and proceed down to lowest (if necessary)

            (3) If player fits in budget*, sign the player. Else go next player.

            (4) If no free agent meets the criteria, sign best available cheapest player.

            Need to determine what "fits in budget" really means
             */
            bool isSigned = false;                        
            FreeAgentView freeAgent = GetTopRatedFreeAgentByPosition( team, position, maxSalary );
           
            if( freeAgent == null ) // go for bottom of barrel
            {
                freeAgent = GetBestCheapPlayer(team, position, maxSalary);
            }

            if (freeAgent != null)
            {
                isSigned = SignPlayer(team, freeAgent, 1); // Default to 1 year deal
            }

            return isSigned;
        }

        private FreeAgentView GetBestCheapPlayer(string team, PlayerType position, int maxSalary)
        {
            return GetFreeAgentListByPosition(position)
                   .Where(p => p.SalaryRequest <= maxSalary)
                   .OrderBy(p => p.SalaryRequest)
                   .ThenByDescending(p => p.PlayerValue)
                   .FirstOrDefault();
        }

        private FreeAgentView GetTopRatedFreeAgentByPosition(string team, PlayerType position, int maxSalary)
        {            
            // TODO: Should there be a starter service?
            int starterCount = starterService.GetRequiredStarterCountByPosition(team, position);

            PlayerRosterView playerView = starterService.GetBottomStarterByPosition(team, position, starterCount);

            // get lowest starter rating or 0, if short players
            decimal lowestStarterRating = (playerView != null) ? playerView.PlayerValue : 0.0M;

            decimal idealRating = lowestStarterRating * 1.2M;

            // search FA for all players 120% or better than this rating            
            return FindQualifiedFreeAgent(position, idealRating, maxSalary);
        }

        private FreeAgentView FindQualifiedFreeAgent( PlayerType position, decimal idealRating, int maxSalary )
        {
            return GetFreeAgentListByPosition(position)
                   .Where(p => p.PlayerValue >= idealRating && p.SalaryRequest <= maxSalary )
                   .OrderByDescending(p => p.PlayerValue)
                   .FirstOrDefault();                   
        }        

        public bool SignPlayer(string teamAbbr, FreeAgentView freeAgent, int years)
        {
            Player player = rosterService.GetPlayerById(freeAgent.Id, "FA");
            Team team = settingsService.GetTeam(teamAbbr);

            return SignPlayer( team, player, freeAgent.SalaryRequest, years );
        }

        public bool SignPlayer(Player player, int salary, int salaryYears)
        {            
            return SignPlayer(settingsService.GetMyTeam(), player, salary, salaryYears);
        }

        public bool SignPlayer(Team team, Player player, int salary, int salaryYears)
        {
            int[] salaryArray = new int[salaryYears];

            for(int i=0; i < salaryArray.Length; i++)
            {
                salaryArray[i] = salary; 
            }

            player.contract = new PlayerContract(team, player, salaryArray);

            transService.NewTransaction();
            transService.AddPlayerToTransaction(player);
            transService.AddTeamToTransaction(team);

            bool isSigned = transService.ConductTransaction();
                        
            if(isSigned)
            {
                player.expectedSalary = 0; // needed to reset for salary calculator
            }          
  
            return isSigned;
        }
        
        public IEnumerable<FreeAgentView> GetFreeAgentListByPosition(PlayerType position)
        {
            List<Player> playersList = settingsService.getFreeAgentPlayersByPosition(position);

            List<FreeAgentView> freeAgentList = new List<FreeAgentView>();

            foreach (var item in playersList)
            {
                freeAgentList.Add(
                    new FreeAgentView
                    {
                        Id = item.id,
                        FirstName = item.firstName,
                        LastName = item.lastName,
                        Position = item.position.ToString(),
                        DOB = item.birthDate.ToString(),
                        Experience = item.yearsExperience,
                        IsInjured = item.isInjured,
                        Accuracy = item.accuracy,
                        Distance = item.distance,
                        Awareness = item.awareness,
                        Speed = item.speed,
                        Receiving = item.receiving,
                        PassBlocking = item.passBlocking,
                        RunBlocking = item.runBlocking,
                        PassDefense = item.passDefense,
                        RunDefense = item.runDefense,
                        Sack = item.sack,
                        SalaryRequest = SalaryCalculator.CalculateExpectedSalary(item),
                        PlayerValue = PlayerValues.GetPlayerValue(item),
                        InjuryDuration = item.injury.duration
                    }
                );
            }

            return freeAgentList;
        }
       
    }
}
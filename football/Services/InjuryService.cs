﻿using System.Collections.Generic;

using Football.League.Injuries;
using Football.League.Players;

namespace Football.Services
{
    public class InjuryService
    {
        private FootballSettingsService settingsService;
        private RosterService rosterService;

        public InjuryService(FootballSettingsService settingsService, RosterService rosterService)
        {
            this.settingsService = settingsService;
            this.rosterService = rosterService;
        }

        public List<Player> GetInjuredPlayers(string teamAbbr)
        {
            return settingsService.getInjuredPlayers(teamAbbr);
        }

        public List<Player> GetIRPlayers(string teamAbbr)
        {
            return settingsService.getIRPlayers(teamAbbr);
        }

        /**
         *  Updates injuries for each team after simming the week. 
         */
        public void UpdateInjuries()
        {            
            if (settingsService.AreInjuriesEnabled())
            {
                foreach (var teamAbbr in settingsService.getTeamAbbreviations())
                {                    
                    List<Player> injuredList = settingsService.getInjuredPlayers(teamAbbr);

                    injuredList.ForEach(delegate(Player injuredPlayer)
                    {
                        injuredPlayer.injury.duration--;
                        if (injuredPlayer.injury.duration == 0)
                        {
                            injuredPlayer.injury.injury = Injury.None;
                        }
                    });
                }
            }
        }

        public void AddPlayersToIR()
        {
            // loop through teams(except MyTeam) and add players to IR
            if (settingsService.AreInjuriesEnabled())
            {
                List<string> teamAbbrs = settingsService.getTeamAbbreviations();
                string myTeam = settingsService.getMyTeamByAbbreviation();                
                teamAbbrs.Remove(myTeam);

                foreach (var teamAbbr in teamAbbrs)
                {                   
                    List<Player> injuredList = settingsService.getInjuredPlayers(teamAbbr);
                    injuredList.ForEach(delegate(Player injuredPlayer)
                    {
                        if ( !rosterService.IsOnIR(injuredPlayer) && injuredPlayer.injury.duration >= settingsService.GetRemainingWeeks())
                        {
                            // place on IR
                            rosterService.AddPlayerToIR(injuredPlayer);
                        }                        
                    });
                }                
            }
        }
    }
}

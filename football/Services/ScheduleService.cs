﻿using System;
using System.Collections.Generic;
using Football.FileIO;
using Football.League.Schedules;

namespace Football.Services
{
    public class ScheduleService
    {
        private FootballSettingsService settingsService;

        public ScheduleService(FootballSettingsService settingsService)
        {
            this.settingsService = settingsService;
        }

        public int GetCurrentWeek()
        {
            return settingsService.GetCurrentWeek();
        }

        public IEnumerable<WeeklyScheduleView> GetScheduleByWeek(int week)
        {
            List<ScheduleItem> rawSchedule = settingsService.GetScheduleByWeek(week.ToString());
            List<WeeklyScheduleView> schedule = new List<WeeklyScheduleView>();

            foreach (var item in rawSchedule)
            {
                WeeklyScheduleView weekItem = new WeeklyScheduleView
                {
                    AwayTeam = item.awayTeam.Name,
                    HomeTeam = item.homeTeam.Name,
                    Score = "",
                    GameLog = ""
                };

                if (item.result.gamePlayed)
                {
                    weekItem.Score = String.Format("{0}-{1}", item.result.homeTeamScore.ToString(), item.result.awayTeamScore.ToString());
                    weekItem.GameLog = item.result.gamePath;
                }

                schedule.Add(weekItem);
            }

            return schedule;
        }

        public IEnumerable<TeamScheduleView> GetScheduleByTeam(string teamAbbr)
        {
            Dictionary<string,ScheduleItem> rawSchedule = settingsService.GetScheduleByTeam(teamAbbr);
            List<TeamScheduleView> schedule = new List<TeamScheduleView>();

            foreach (KeyValuePair<string,ScheduleItem> item in rawSchedule)
            {
                TeamScheduleView weekItem = new TeamScheduleView
                {
                    Week = item.Key,
                    Score = "",
                    GameLog = "",
                    Opponent = ""
                };

                if (item.Value != null)
                {
                    weekItem.Opponent = (item.Value.awayTeam.Abbr == teamAbbr) ? item.Value.homeTeam.Abbr : item.Value.awayTeam.Abbr;

                    if (item.Value.result.gamePlayed)
                    {
                        weekItem.Score = String.Format("{0}-{1}", item.Value.result.homeTeamScore.ToString(), item.Value.result.awayTeamScore.ToString());
                        weekItem.GameLog = item.Value.result.gamePath;
                    }
                }                
                                
                schedule.Add(weekItem);                
            }

            return schedule;
        }

        public List<ScheduleItem> GetRawScheduleByWeek(int week)
        {
            return settingsService.GetScheduleByWeek(week.ToString());
        }

        public bool IsPlayoffs()
        {
            return settingsService.IsPlayoffs();
        }

        public int GetNumberOfWeeks()
        {
            return settingsService.GetNumberOfWeeks();
        }

        public string LoadGameLog(string logPath)
        {
            GameLogIO gameLog = new GameLogIO(settingsService.GetSettingsObject());
            return gameLog.loadLog(logPath);    
        }
    }
}

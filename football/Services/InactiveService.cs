﻿using System;
using System.Collections.Generic;
using Football.League.Players;
using Football.League.PlayerValue;
using Football.League.Teams.FormationFilters;
using Football.Services;
using Football.Settings;

namespace Football.League.Teams
{
    public class InactiveService
    {
        private FootballSettingsService settingsService;
        private PlayerTypeValues playerTypeValues;

        public InactiveService(FootballSettingsService settingsService, PlayerTypeValues playerTypeValues)
        {
            this.settingsService = settingsService;
            this.playerTypeValues = playerTypeValues;
        }

        public void ClearInactives()
        {
            Dictionary<string, Roster>.ValueCollection allRosters = settingsService.GetTeamRosters();

            foreach (var teamRoster in allRosters)
            {
                teamRoster.clearInactiveList();
            }
        }

        // TODO:  This probably needs to be in its own class.
        public void SelectInactives()
        {            
            Dictionary<string, Roster>.ValueCollection allRosters = settingsService.GetTeamRosters();

            foreach (var teamRoster in allRosters)
            {
                if (!(teamRoster.GetTeam() == settingsService.GetMyTeam()) && !(teamRoster.GetTeam() == settingsService.GetFATeam()))
                {
                    if (teamRoster.activeRosterSize() > teamRoster.activeLimit())
                    {
                        // 1.  Sort active, Non-IR players by PlayerValue, with worst at top of list.                         
                        List<Player> filteredList = PlayerValues.SortByLowestPlayerValue(teamRoster.GetNonIRPlayers());
                                                
                        int listIndex = 0;

                        List<PlayerType> atMinPos = new List<PlayerType>();

                        //            all  - IR  - inactive >  53
                        while (teamRoster.activeRosterSize() > teamRoster.activeLimit())
                        {
                            // Inactivate Players until at 46 players
                            // potential array index out of bounds error....
                            // Bingo!  Got this error:  Index was out of range. Must be non-negative and less than the size of the collection.                          
                         
                            int x = teamRoster.activeRosterSize();
                            int y = teamRoster.activeLimit();
                                
                            Player player = filteredList[listIndex];
                            listIndex++;

                            if ( !atMinPos.Contains( player.position ) )
                            {

                                // 2.  Continue inactivating until the activeLimit is reached
                                // Need to account for depth requirements       
                                //TODO:  Encapsulate this better                                       
                                BaseAll selection = (playerTypeValues.IsOffense(player.position)) ? teamRoster.GetTeam().Coach.BaseOffense : teamRoster.GetTeam().Coach.BaseDefense;
                                FormationFilter filter = FormationFilterList.GetFormationFilter(selection);

                                int requiredCount = filter.GetRequiredCountByPosition(player.position);                                
                                int currentCount = teamRoster.GetAvailableNonInactiveCountByPosition(player.position);

                                // if there are excess players at this position, then inactivate the player
                                if (requiredCount < currentCount)
                                {
                                    teamRoster.AddToInactive(player);
                                }
                                else
                                {
                                    atMinPos.Add( player.position );
                                }
                            }                            
                            
                        }
                    }
                }
            }
        }  
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using Football.League.Injuries;
using Football.League.Players;
using Football.League.PlayerValue;
using Football.League.Teams;
using Football.Settings;

namespace Football.Services
{
    public class RosterService
    {        
        private FootballSettingsService settingsService;        
               
        public RosterService(FootballSettingsService settingsService)
        {            
            this.settingsService = settingsService;            
        }       
        
        public Player GetPlayerById(int id, string abbr=null)
        {
            return settingsService.GetPlayerById(id, abbr);            
        }        
        
        public IEnumerable<PlayerRosterView> getRosterListByPosition(PlayerType position, string team = null)
        {
            if (team == null)
            {
                team = settingsService.getMyTeamByAbbreviation();
            }

            List<Player> playersList = settingsService.getTeamPlayersByPosition(team, position);

            return GetPlayerRosterView(playersList);
        }

        public IEnumerable<PlayerRosterView> GetRosterList(string team)
        {
            List<Player> playersList = settingsService.GetTeamRoster(team).GetAll();

            return GetPlayerRosterView( playersList );            
        }

        private IEnumerable<PlayerRosterView> GetPlayerRosterView(List<Player> playerList)
        {
            List<PlayerRosterView> rosterList = new List<PlayerRosterView>();

            foreach (var item in playerList)
            {
                rosterList.Add(
                    new PlayerRosterView
                    {
                        Id = item.id,
                        FirstName = item.firstName,
                        LastName = item.lastName,
                        Position = item.position.ToString(),
                        DOB = item.birthDate.ToString(),
                        Experience = item.yearsExperience,
                        IsInactive = IsInactive(item),
                        IsInjured = item.isInjured,
                        IsOnIR = IsOnIR(item),
                        Accuracy = item.accuracy,
                        Distance = item.distance,
                        Awareness = item.awareness,
                        Speed = item.speed,
                        Receiving = item.receiving,
                        PassBlocking = item.passBlocking,
                        RunBlocking = item.runBlocking,
                        PassDefense = item.passDefense,
                        RunDefense = item.runDefense,
                        Sack = item.sack,
                        Salaries = item.contract.getSalaryView(),
                        PlayerValue = PlayerValues.GetPlayerValue(item),
                        InjuryDuration = item.injury.duration
                    }
                );
            }

            return rosterList;
        }
                
        public int getTeamPayroll(string abbr = null)
        {
            return settingsService.GetTeamPayroll(abbr);            
        }

        public IEnumerable<InjuryView> getInjuredList(string teamKey)
        {
            List<Player> playerList = settingsService.getInjuredPlayers(teamKey);
            List<Player> IRList = settingsService.getIRPlayers(teamKey);

            List<InjuryView> injuryList = new List<InjuryView>();
            
            foreach (var item in IRList)
            {
                injuryList.Add(
                    new InjuryView
                    {
                        Id=item.id, 
                        FirstName = item.firstName,
                        LastName = item.lastName,
                        Position = item.position.ToString(),
                        Injury = item.injury.injury.ToString(),
                        InjuryDuration = item.injury.duration,
                        IsOnIR = IRList.Contains(item)
                    }
                );
            }

            foreach (var item2 in playerList)
            {
                if (!IRList.Contains(item2))
                {
                    injuryList.Add(
                        new InjuryView
                        {
                            Id = item2.id,
                            FirstName = item2.firstName,
                            LastName = item2.lastName,
                            Position = item2.position.ToString(),
                            Injury = item2.injury.injury.ToString(),
                            InjuryDuration = item2.injury.duration,
                            IsOnIR = IRList.Contains(item2)
                        }
                    );
                }
            }

            return injuryList;
        }

        public IEnumerable<PlayerRosterView> GetTransactionView(List<Player> playerList)
        {
            return GetPlayerRosterView(playerList);
        }

        public void AddPlayerToIR(Player player)
        {
            settingsService.AddPlayerToIR(player);
        }

        public bool IsOnIR(Player player)
        {
            // awful haxs
            List<Player> IRList = settingsService.getIRPlayers(player.teamAbbr);
            return IRList.Contains(player);
        }

        public bool IsInactive(Player player)
        {
            // awful haxs
            List<Player> inactiveList = settingsService.getInactivePlayers(player.teamAbbr);
            return inactiveList.Contains(player);
        }

        public IEnumerable<LineupView> GetLineupPlayers(BaseAll formation,PlayerType position)
        {
            List<Player> playerList = settingsService.GetPlayersByLineup(formation,position);
            List<LineupView> lineup = new List<LineupView>();

            foreach (var item in playerList)
            {                
                lineup.Add(
                    new LineupView
                    {
                        Id = item.id,
                        FirstName = item.firstName,
                        LastName = item.lastName,
                        Position = item.position.ToString(),
                        DOB = item.birthDate.ToString(),
                        Experience = item.yearsExperience,
                        IsInjured = item.isInjured,
                        Accuracy = item.accuracy,
                        Distance = item.distance,
                        Awareness = item.awareness,
                        Speed = item.speed,
                        Receiving = item.receiving,
                        PassBlocking = item.passBlocking,
                        RunBlocking = item.runBlocking,
                        PassDefense = item.passDefense,
                        RunDefense = item.runDefense,
                        Sack = item.sack,
                        IsOnIR = IsOnIR(item)
                    }
                );
            }

            return lineup;
        }

        public int PromotePlayer(Player player, BaseAll formation)
        {
            return settingsService.PromotePlayer(player,formation);
        }

        public int DemotePlayer(Player player, BaseAll formation)
        {
            return settingsService.DemotePlayer(player, formation);
        }

        public RosterCountView GetRosterCounts(string teamAbbr=null)
        {
            if (teamAbbr == null)
            {
                teamAbbr = settingsService.getMyTeamByAbbreviation();
            }

            return settingsService.GetRosterCounts(teamAbbr);
        }

        public void ChangeRosterStatus(Player player)
        {
            if (!IsOnIR(player))
            {
                if (IsInactive(player))
                {
                    // activate player
                    settingsService.RemoveFromInactive(player);
                }
                else
                {
                    // make inactive
                    settingsService.AddToInactive(player);
                }
            }
        }        
       
        private void CutPlayers(Roster roster, int requiredCount)
        {
            // This section cuts a player....
            // TODO:  If signing player would add too many to roster, must cut another player OR go without depth
            //        This would require taking into consideration the salary cap hit as well for cutting and signing.
            //        Also need to determine the quality of player to sign.  Cheapest player?  Best Value?  Should
            //        length of injuries to players ahead determine quality vs cheap? 
            List<Player> filteredList = PlayerValues.SortByLowestPlayerValue(roster.GetNonIRPlayers());

            int listIndex = 0;

            while (roster.activeRosterSize() > roster.activeLimit())
            {
                Player playerToRelease = filteredList[listIndex]; // potential array index out of bounds error.....                        
                listIndex++;

                if (requiredCount <= roster.GetAvailableCountByPosition(playerToRelease.position))
                {
                    roster.releasePlayer(playerToRelease);
                }
            }
        }

        public int GetAvailablePlayerCountByPosition(string team, PlayerType position)
        {
            return settingsService.GetAvailablePlayerCount( team, position );
        }
       
    }
}
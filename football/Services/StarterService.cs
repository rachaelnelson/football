﻿using System.Collections.Generic;
using System.Linq;
using Football.League.Players;
using Football.League.Teams;
using Football.Services.Starter;
using Football.Settings;

namespace Football.Services
{
    public class StarterService
    {
        private FootballSettingsService settingsService;
        private PlayerTypeValues playerTypeValues;
        private RosterService rosterService;

        public StarterService(FootballSettingsService settingsService, PlayerTypeValues playerTypeValues, RosterService rosterService)
        {
            this.settingsService = settingsService;
            this.playerTypeValues = playerTypeValues;
            this.rosterService = rosterService;
        }

        public int GetRequiredStarterCountByPosition(string team, PlayerType position)
        {
            BaseAll formation = settingsService.GetTeam(team).Coach.BaseOffense;

            if (playerTypeValues.IsDefense(position))
            {
                formation = settingsService.GetTeam(team).Coach.BaseDefense;
            }

            return FormationFilterList.GetFormationFilter(formation).GetStarterCountByPosition(position);            
        }

        public int GetRequiredMinimumCountByPosition(string team, PlayerType position)
        {
            BaseAll formation = settingsService.GetTeam(team).Coach.BaseOffense;

            if (playerTypeValues.IsDefense(position))
            {
                formation = settingsService.GetTeam(team).Coach.BaseDefense;
            }

            return FormationFilterList.GetFormationFilter(formation).GetRequiredCountByPosition(position);
        }

        // this actually gets the bottom rated player even though he may not start....
        public PlayerRosterView GetBottomStarterByPosition(string team, PlayerType position, int starterCount)
        {
            int injuryLimit = 8; // sign vet 8 weeks - TODO:  set as config somewhere

            return rosterService.getRosterListByPosition(position, team)
                                .Where(p => p.InjuryDuration < injuryLimit)
                                .OrderByDescending(p => p.PlayerValue)
                                .ElementAtOrDefault(starterCount - 1);  // grabs bottom starter, if applicable
        }

        // virtual for unit tests
        public virtual decimal GetAverageStarterRating(string team, PlayerType position)
        {
            int requiredStarterCount = GetRequiredStarterCountByPosition(team, position);

            var result = rosterService.getRosterListByPosition(position, team)
                                      .OrderByDescending(p => p.PlayerValue)
                                      .Take(requiredStarterCount)
                                      .Select(p => p.PlayerValue)
                                      .ToList();                      
                
            while( result.Count() < requiredStarterCount  )
            {
                result.Add(0.0M);
            }
            
            return result.Average();
        }
        
        public List<OpenStarter> GetOpenStarterSpots(string team)
        {                  
            List<OpenStarter> openSpots = new List<OpenStarter>();

            foreach( var position in PlayerType.GetPlayerTypesList() )
            {
                int required = GetRequiredStarterCountByPosition(team, position);

                int available = rosterService.GetAvailablePlayerCountByPosition(team, position);
               
                decimal avgStarterRating = GetAverageStarterRating(team, position);

                if (required > available)
                {
                    int count = 0;

                    while ((required - count) > available)
                    {
                        openSpots.Add(new OpenStarter() { Position = position, AvgStarterRating = avgStarterRating, IsStarterPosition =  true });
                        count = count + 1;
                    }
                }                       
                
            }
            return openSpots;
        }

        public List<OpenStarter> GetMinimumDepthSpots(string team)
        {
            List<OpenStarter> openSpots = new List<OpenStarter>();

            foreach (var position in PlayerType.GetPlayerTypesList())
            {
                int required = GetRequiredStarterCountByPosition(team, position);

                int available = rosterService.GetAvailablePlayerCountByPosition(team, position);

                int minimum = GetRequiredMinimumCountByPosition(team, position);

                decimal avgStarterRating = GetAverageStarterRating(team, position);

                // starter = 2 (required)
                // min = 4
                // available = 1


                // sign minimums
                if (minimum > available && // must be a shortage of players at the position to meet the minimum needs
                    minimum > required &&  // must need required depth beyond the required number of starters
                    available >= required) // must have enough players on roster to fill the starter roles since this function is for depth
                {
                    int count = 0;

                    while ((minimum - count) > available)
                    {
                        openSpots.Add(new OpenStarter() { Position = position, AvgStarterRating = avgStarterRating, IsStarterPosition = false });
                        count = count + 1;
                    }
                }

            }
            return openSpots;
        }       
    }
}

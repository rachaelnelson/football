﻿namespace Football.Services
{
    public class CapManagementService
    {
        private FootballSettingsService settingsService;
        private RosterService rosterService;

        private int maxCap = 100000000; 

        public CapManagementService(FootballSettingsService settingsService, RosterService rosterService)
        {
            this.settingsService = settingsService;
            this.rosterService = rosterService;
        }        

        public int getCurrentCapYear()
        {
            return this.settingsService.getCurrentYear();
        }

        // for now assume no cap growth...
        // and assume cap space is only pulled for current team, which will need to change for off season free agency
        public int getCurrentCapSpace(string abbr = null)
        {
            return this.maxCap - this.rosterService.getTeamPayroll(abbr);
        }

        public int GetCapLimit()
        {
            return maxCap;
        }
    }
}

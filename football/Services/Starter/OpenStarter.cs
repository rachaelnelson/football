﻿using Football.League.Players;

namespace Football.Services.Starter
{
    public class OpenStarter
    {
        public PlayerType Position { get; set; }
        public bool IsStarterPosition { get; set; }
        public decimal AvgStarterRating { get; set; }
    }
}

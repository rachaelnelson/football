﻿using System.Collections.Generic;

using Football.League.Players;
using Football.League.Transaction;
using Football.League.Teams;

using Microsoft.FSharp.Core;
using Microsoft.FSharp.Collections;
using System.Collections;

namespace Football.Services
{
    public class TransactionService
    {
        private bool isTrade;

        private CapManagementService capService;
        private RosterService rosterService;
        private FootballSettingsService settingsService;
        private TeamTransaction transaction;
        private TransactionView transactionView;

        public TransactionService(RosterService rosterService, CapManagementService capService, FootballSettingsService settingsService)
        {
            this.rosterService = rosterService;
            this.capService = capService;
            this.settingsService = settingsService;
        }

        public void AddDraftPickToTransaction( DraftPicks.DraftPick draftpick )
        {
            transaction.AddPickToTransaction( draftpick );
        }

        public void AddPlayerToTransaction(Player player)
        {
            transaction.AddPlayerToTransaction(player);
        }
        
        public void AddTeamToTransaction(Team team)
        {
            transaction.AddTeamToTransaction(team.Abbr);
        }

        public void ClearTransaction()
        {
            transaction.ClearTransaction();
        }

        public int GetPostTradeActiveCount(string team)
        {
            int activeCount = 0;

            RosterCountView rosterCounts = rosterService.GetRosterCounts(team);

            if (team == transactionView.Team1)
            {
                activeCount = rosterCounts.ActiveCount - transactionView.Team1PlayerCount + transactionView.Team2PlayerCount;
            }
            else
            {
                activeCount = rosterCounts.ActiveCount - transactionView.Team2PlayerCount + transactionView.Team1PlayerCount;
            }

            return activeCount;
        }

        public int GetPostTradeSalaryCap(string team)
        {
            int postTransCapSpace = 0;

            int preTransCapSpace = capService.getCurrentCapSpace(team);

            if (team == transactionView.Team1)
            {
                postTransCapSpace = preTransCapSpace + transactionView.Team1SalarySum - transactionView.Team2SalarySum;
            }
            else
            {
                postTransCapSpace = preTransCapSpace + transactionView.Team2SalarySum - transactionView.Team1SalarySum;
            }

            return postTransCapSpace;
        }

        public List<Player> GetPlayerTradeList(string teamAbbr)
        {
            return transaction.GetPlayerTransaction( teamAbbr );                       
        }

        public List<DraftPicks.DraftPick> GetDraftTradeList(string teamAbbr)
        {
            return transaction.GetPickTransaction( teamAbbr );
        }
        
        private bool HasTransactionItems()
        {
            // isTrade flag is needed for one sided deals such as sign, release, waiver, etc
            return ( (transactionView.Team2PickCount > 0 || transactionView.Team1PlayerCount > 0) && ( isTrade == false || (transactionView.Team2PlayerCount > 0 || transactionView.Team2PickCount > 0 )));
        }

        private bool IsTeamOverCap(string team)
        {
            int postTransCapSpace = GetPostTradeSalaryCap(team);

            return (postTransCapSpace > capService.GetCapLimit());
        }

        private bool IsTeamOverMaxRosterSize(string team)
        {
            RosterCountView rosterCounts = rosterService.GetRosterCounts(team);

            int activeCount = GetPostTradeActiveCount(team);

            return (activeCount > rosterCounts.MaxRosterCount);
        }

        public bool IsValid()
        {
            this.transactionView = transaction.GetTransactionView();

            bool isValid = true;

            // non trade (free agent, waiver, etc)
            if (!isTrade && transaction.GetPlayerTransaction().Keys.Count == 0)
            {
                foreach (var item in transaction.GetPlayerTransaction().Keys)
                {
                    // don't need checks for FA team
                    if (item == "FA")
                    {
                        continue;
                    }

                    if (!HasTransactionItems() || IsTeamOverMaxRosterSize(item) || IsTeamOverCap(item))
                    {
                        isValid = false;
                        break;
                    }
                }
            }
            else // trade
            {                 
                // if trade has players or draft picks
                if (transaction.GetPlayerTransaction().Keys.Count == 0 && transaction.GetPickTransaction().Keys.Count == 0)
                {
                    isValid = false;
                }                
                
                if (transaction.GetPlayerTransaction().Keys.Count > 0)
                {
                    foreach (var item in transaction.GetPlayerTransaction().Keys)
                    {
                        if (!HasTransactionItems() || IsTeamOverMaxRosterSize(item) || IsTeamOverCap(item))
                        {
                            isValid = false;
                            break;
                        }
                    }
                }
            }
            
            return isValid;
        }            

        public void NewTransaction(bool isTrade=false)
        {
            this.isTrade = isTrade;

            transaction = new TeamTransaction();
            transactionView = transaction.GetTransactionView();
        }

        public void RemovePickFromTrade(DraftPicks.DraftPick pick)
        {
            transaction.RemovePickFromTransaction(pick);
        }       

        public void RemovePlayerFromTrade(Player player)
        {
            transaction.RemovePlayerFromTransaction(player);
        }       
        
        public bool ConductTransaction()
        {
            bool valid = IsValid();

            if(valid)
            {
                Dictionary<string, List<Player>> playerDict = transaction.GetPlayerTransaction();
                Dictionary<string, List<DraftPicks.DraftPick>> pickDict = transaction.GetPickTransaction();

                // if no second team in transaction, then assume the transaction
                // is for working with free agents/signings
                // Used when cutting to FA
                if ( playerDict.Keys.Count == 1 && pickDict.Keys.Count == 0)
                {
                    transaction.AddTeamToTransaction("FA");
                }

                TransactionView transView = transaction.GetTransactionView();

                string team1NewTeam = transView.Team2;
                string team2NewTeam = transView.Team1;

                if (playerDict.Keys.Count > 0)
                {
                    if (playerDict.ContainsKey(transView.Team1))
                    {
                        foreach (var item in playerDict[transView.Team1])
                        {
                            settingsService.TradePlayer(item, team1NewTeam);
                        }
                    }

                    if (playerDict.ContainsKey(transView.Team2))
                    {
                        foreach (var item in playerDict[transView.Team2])
                        {
                            settingsService.TradePlayer(item, team2NewTeam);
                        }
                    }
                }
                
                if (pickDict.Keys.Count > 0)
                {
                    if (pickDict.ContainsKey(transView.Team1))
                    {
                        settingsService.TradePicks(transView.Team1, team1NewTeam, pickDict[transView.Team1]);
                    }
                    if (pickDict.ContainsKey(transView.Team2))
                    {
                        settingsService.TradePicks(transView.Team2, team2NewTeam, pickDict[transView.Team2]);
                    }
                }

            }

            return valid;

        }                  

    }
}

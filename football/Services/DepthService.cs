﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.League.Players;
using Football.League.Teams.FormationFilters;
using Football.Services;
using Football.Services.Depth;
using Football.Services.Starter;

namespace Football.League.Teams
{
    public class DepthService
    {        
        private FootballSettingsService settingsService;
        private DepthSign depthSign;
        private PositionPriorityService ppService;  // ha ha!                          

        public DepthService(FootballSettingsService settingsService, PositionPriorityService ppService, DepthSign depthSign)
        {           
            this.settingsService = settingsService;
            this.depthSign = depthSign;
            this.ppService = ppService;            
        }
                
        public void FillRoster()
        {
            // loop through all teams (except user team) and sign for depth if necessary.
            // one potential minor issue is a team on a bye week may not need the player for the upcoming game
            
            Dictionary<string, Roster>.ValueCollection allRosters = settingsService.GetTeamRosters();

            Team myTeam = settingsService.GetMyTeam();
            Team faTeam = settingsService.GetFATeam();
            
            // 1. foreach team, build position priority list
            // 2. foreach team with needs, determine the number of positions needed and make room for them on roster (ie cut players)
            // 3. sort by worst team (for now this will be lowest average player value), and fill one position of need for all teams
            // 4. update average per team, update position of needs, then loop again.  
            // 5. Continue this until all positions of need are filled
           
            // Set queues
            foreach (var teamRoster in allRosters)
            {
                Team team = teamRoster.GetTeam();

                if (team != myTeam && team != faTeam)
                {                    
                    ppService.SetOpenPositions(team.Abbr);
                }
            }            
            
            while (ppService.hasOpenSpots())
            {
                // cycle through queues, signing players
                // should only return team rosters of those that have a queue....
                // starts with the teams with lowest average player value
                foreach (var teamRoster in allRosters.Where(x => ppService.GetTeamsWithLists().Contains(x.GetTeam().Abbr)).OrderBy(x => x.GetAveragePlayerValue()))
                {
                    Team team = teamRoster.GetTeam();
                    OpenStarter openPosition = ppService.GetNext(team.Abbr);
                   
                    int numOpenings = ppService.Count(team.Abbr);
                    depthSign.SignPlayer( team.Abbr, openPosition.Position, numOpenings );                    
                }
            }        

        }
        
    }
}

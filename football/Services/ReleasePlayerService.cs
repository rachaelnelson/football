﻿using System.Collections.Generic;
using System.Linq;
using Football.League.Players;
using Football.League.Teams;
using Football.UiClasses.ViewModel;

namespace Football.Services.Starter
{
    public class ReleasePlayerService
    {
        private FootballSettingsService settingsService; // May not be needed
        private RosterService rosterService;
        private StarterService starterService;
        private TransactionService transService;        
        
        public ReleasePlayerService(RosterService rosterService, StarterService starterService, TransactionService transService, FootballSettingsService settingsService)
        {
            this.settingsService = settingsService;
            this.rosterService = rosterService;
            this.starterService = starterService;
            this.transService = transService;
        }

        public bool ReleasePlayer(ReleasePlayerView playerView)
        {
            Player player = rosterService.GetPlayerById(playerView.Id, playerView.TeamAbbr);
            Team team = player.team;

            return ReleasePlayer(team, player);
        }

        public bool ReleasePlayer(Player player)
        {            
            Team team = player.team;

            return ReleasePlayer(team, player);
        }

        public bool ReleasePlayer(Team team, Player player)
        {
            transService.NewTransaction();
            transService.AddPlayerToTransaction(player);

            return transService.ConductTransaction();
        }
        
        public void CutPlayers(string team)
        {
            int numToCut = GetNumberToCut(team);
            Queue<Player> playerQueue = GetPlayersToCut( team, numToCut );
            ReleasePlayers(playerQueue);            
        }

        private Queue<Player> GetPlayersToCut(string team, int getNumToCut)
        {
            Queue<Player> cutQueue = new Queue<Player>();

            int injuryLimit = 1; // don't get players injured for more than 1 week

            IEnumerable<PlayerRosterView> players = rosterService.GetRosterList(team)
                                                    .Where(p => p.InjuryDuration <= injuryLimit)
                                                    .OrderByDescending(p => p.PlayerValue)
                                                    .Take(getNumToCut);

            foreach (var item in players)
            {
                Player player = rosterService.GetPlayerById(item.Id);
                cutQueue.Enqueue(player);
            }

            return cutQueue;
        }

        private int GetNumberToCut(string team)
        {          
            int numNeeded = GetNumberNeeded(team); // pull from function

            RosterCountView counts = rosterService.GetRosterCounts(team);                        
            
            int rosterRoom = counts.MaxRosterCount - counts.ActiveCount - numNeeded;

            int count = 0;

            while( (rosterRoom + count) < 0 )
            {
                count++;
            }

            return count;            
        }

        private int GetNumberNeeded(string team)
        {            
             return starterService.GetOpenStarterSpots(team).Count + starterService.GetMinimumDepthSpots(team).Count;            
        }

        private void ReleasePlayers(Queue<Player> playerQueue)
        {
            if (playerQueue.Count > 0)
            {
                Player player = playerQueue.Dequeue();
                ReleasePlayer(player);

                ReleasePlayers(playerQueue);
            }
        }
    }
}

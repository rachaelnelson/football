﻿using Football.League.Teams;
using Football.Services.Sim;

namespace Football.Services
{
    public class PreSim : ISim
    {
        private InjuryService injuryService;
        private DepthService depthService;
        private InactiveService inactiveService;

        public PreSim ( InjuryService injuryService, DepthService depthService, InactiveService inactiveService )
        {
            this.injuryService = injuryService;
            this.depthService = depthService;
            this.inactiveService = inactiveService;
        }

        public void run()
        {
            injuryService.AddPlayersToIR();
            inactiveService.ClearInactives();
            depthService.FillRoster();
            inactiveService.SelectInactives();
        }        
    }
}

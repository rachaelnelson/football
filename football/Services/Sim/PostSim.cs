﻿using Football.League.Playoffs;

namespace Football.Services.Sim
{
    public class PostSim : ISim
    {
        private FootballSettingsService settingService;
        private InjuryService injuryService;
        private PlayoffHandler playoffHandler;

        public PostSim(FootballSettingsService settingService, PlayoffHandler playoffHandler, InjuryService injuryService)
        {
            this.settingService = settingService;
            this.playoffHandler = playoffHandler;
            this.injuryService = injuryService;
        }

        public void run()
        {
            settingService.IncrementWeek();

            injuryService.UpdateInjuries();

            if (playoffHandler.isPlayoffs(settingService.GetCurrentWeek()))
            {
                playoffHandler.nextRound(settingService.GetSchedule());
            }
        }        
    }
}

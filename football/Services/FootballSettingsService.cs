﻿using System.Collections.Generic;
using System.Linq;

using Football.League.Players;
using Football.League.Salary;
using Football.League.Schedules;
using Football.League.Teams;
using Football.Settings;

namespace Football.Services
{   
    public class FootballSettingsService
    {
        private FootballSettings setting;
        
        public FootballSettingsService(FootballSettings setting)
        {
            this.setting = setting;          
        }

        // Currently needed for sim game and load game log
        public FootballSettings GetSettingsObject()
        {
            return setting;
        }        
       
        public List<Player> getTeamPlayersByPosition(string teamAbbr, PlayerType position = null)
        {
            if (position == null)
            {
                position = PlayerType.QB;
            }

            return setting.rosterList[teamAbbr].GetAllPlayersByPosition(position);
        }
        
        public List<Player> getFreeAgentPlayersByPosition(PlayerType position = null)
        {
            if (position == null)
            {
                position = PlayerType.QB;
            }

            return setting.rosterList["FA"].GetAllPlayersByPosition(position);
        }

        public int getCurrentYear()
        {
            return this.setting.year;
        }
        
        // Probably can move these draft ones out of here
        public List<DraftPicks.DraftPick> GetPicksByTeam(string teamAbbr)
        {
            return setting.DraftList.GetPicksByTeam(teamAbbr);
        }

        public DraftPicks.DraftPick GetPickById(string id)
        {
            return setting.DraftList.GetPickById(id);
        }

        // this is used for trades, signings, free agents, and waivers
        public void TradePlayer(Player player, string newTeamAbbr)
        {
            if (player != null)
            {
                setting.rosterList[player.teamAbbr].removePlayer(player);
                setting.rosterList[newTeamAbbr].addPlayer(player);
            }
        }

        // this is used for trades, signings, free agents, and waivers
        public void TradePicks( string currentTeam, string newTeamAbbr, List<DraftPicks.DraftPick> picksToUpdate )
        {
            if (picksToUpdate.Count > 0)
            {
                setting.DraftList.UpdateHoldingTeam( currentTeam, newTeamAbbr, picksToUpdate );
            }
        }
        
        public Player GetPlayerById(int id,string teamAbbr=null)
        {
            Player player = null;
                    
            // Ewwww
            if(teamAbbr == null || teamAbbr == "") // if not on my team or not found on indicated team, look in free agency
            {
                player = GetFreeAgentById(id);

                if (player == null)
                {
                    teamAbbr = setting.myTeam;
                }   
            }

            if (!(teamAbbr == null || teamAbbr == "")) 
            {                
                player = setting.rosterList[teamAbbr].getPlayerById(id);
            }

            return player;
        }

        
        private Player GetFreeAgentById(int id)
        {
            Player player = setting.rosterList["FA"].getPlayerById(id);            
            return player;
        }          

        public int GetTeamPayroll(string abbr = null)
        {
            string team = abbr;

            if (abbr == null)
            {
                team = setting.myTeam;
            }

            return setting.rosterList[team].getSumSalaries();
        }

        public List<string> getTeamAbbreviations()
        {
            List<string> teams = new List<string>();

            foreach (var item in setting.teams.getTeamList())
            {
                teams.Add(item.Key);
            }

            return teams;
        }

        public List<Player> getInjuredPlayers(string teamKey)
        {
            return setting.rosterList[teamKey].getInjuredPlayers();
        }

        public List<Player> getInactivePlayers(string teamKey)
        {
            return setting.rosterList[teamKey].getInactivePlayers();
        }

        public List<Player> getIRPlayers(string teamKey)
        {
            return setting.rosterList[teamKey].getIRPlayers();
        }

        public void AddPlayerToIR(Player player)
        {
            setting.rosterList[player.team.Abbr].addPlayerToIR(player);
        }

        public string getMyTeamByAbbreviation()
        {
            return setting.myTeam;
        }

        public Team GetFATeam()
        {
            return setting.freeAgentsTeam;
        }

        public Team GetMyTeam()
        {
            return setting.teams.getTeam(setting.myTeam);
        }

        public Team GetTeam(string abbr)
        {
            return setting.teams.getTeam(abbr);
        }

        public int GetCurrentWeek()
        {
            return setting.thisWeek;
        }

        public List<ScheduleItem> GetScheduleByWeek(string week)
        {
            return setting.schedule.getScheduleByWeek(week);
        }

        public Dictionary<string,ScheduleItem> GetScheduleByTeam(string teamAbbr)
        {
            return setting.schedule.getScheduleByTeam(teamAbbr);
        }

        public bool IsPlayoffs()
        {
            return (setting.thisWeek == 18);
        }

        public void UpdateTeamRecords(Team homeTeam, Team awayTeam, int scoreDiff)
        {
            bool isSameConf = setting.leagueStructure.areTeamsinSameConference(homeTeam, awayTeam);
            bool isSameDiv = setting.leagueStructure.areTeamsinSameDivison(homeTeam, awayTeam);

            setting.teamRecordList.UpdateRecord(homeTeam, awayTeam, scoreDiff, isSameConf, isSameDiv);
        }

        public void IncrementWeek()
        {
            setting.thisWeek++;
        }

        public Schedule GetSchedule()
        {
            return setting.schedule;
        }

        public int GetNumberOfWeeks()
        {
            return setting.schedule.GetNumberOfWeeks();
        }

        public List<Player> GetPlayersByLineup(BaseAll formation, PlayerType position)
        {
            return setting.rosterList[setting.myTeam].fetchList(formation, position);
        }

        public int PromotePlayer(Player player, BaseAll formation)
        {
            return setting.rosterList[setting.myTeam].PromotePlayer(player,formation);
        }

        public int DemotePlayer(Player player, BaseAll formation)
        {
            return setting.rosterList[setting.myTeam].DemotePlayer(player, formation);
        }

        public bool AreInjuriesEnabled()
        {
            return setting.injuriesEnabled;
        }

        public int GetRemainingWeeks()
        {
            return 22 - setting.thisWeek;
        }

        public RosterCountView GetRosterCounts(string teamAbbr)
        {
            RosterCountView counts = new RosterCountView();
            counts.ActiveCount = setting.rosterList[teamAbbr].activeRosterSize();
            counts.ActiveLimit = setting.rosterList[teamAbbr].activeLimit();
            counts.InactiveCount = setting.rosterList[teamAbbr].inactiveSize();
            counts.IRCount = setting.rosterList[teamAbbr].IRSize();
            counts.MinRosterCount = setting.rosterList[teamAbbr].minSize();
            counts.MaxRosterCount = setting.rosterList[teamAbbr].maxSize();
            counts.NonIRCount = setting.rosterList[teamAbbr].NonIRSize();
            counts.TotalCount = setting.rosterList[teamAbbr].totalSize();            

            return counts;
        }

        public void RemoveFromInactive(Player player)
        {
            setting.rosterList[player.team.Abbr].RemoveFromInactive(player);
        }

        public void AddToInactive(Player player)
        {
            setting.rosterList[player.team.Abbr].AddToInactive(player);
        }

        public Dictionary<string,Roster>.ValueCollection GetTeamRosters()
        {
            return setting.rosterList.Values;
        }        
        
        public Roster GetTeamRoster(string abbr)
        {
            return setting.rosterList[abbr];
        }

        public int GetAvailablePlayerCount(string team, PlayerType position)
        {
            return setting.rosterList[team].GetAvailableCountByPosition(position);
        }
    }
}

﻿using Football.League.Players;

namespace Football.Services.Depth
{
    public class DepthSign
    {
        private SignPlayerService faService;
        private CapManagementService capService;

        public DepthSign(SignPlayerService faService, CapManagementService capService)
        {
            this.faService = faService;
            this.capService = capService;
        }

        // virtual for tests
        public virtual void SignPlayer(string team, PlayerType position, int numOpenings)
        {
            int maxForPosition = capService.getCurrentCapSpace() - (numOpenings * 400000); // TODO:  pull minimum from somewhere instead of hard code

            faService.SignFreeAgentByPosition(team, position, maxForPosition);
        }
    }
}

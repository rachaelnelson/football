﻿using System.Collections.Generic;

using Football.League.Playoffs;
using Football.League.Schedules;
using Football.Settings;
using Football.Sim.Games;
using Football.Sim.Plays;
using Football.Services.Sim;

namespace Football.Services
{
    // This contains most of the original code.  Prolly needs a nicer refactoring
    public class SimService
    {
        private FootballSettingsService settingService;       
        private GamePlay gamePlay;
        private ISim postSimWeek;
        private ISim preSimWeek;

        public SimService(FootballSettingsService settingService, GamePlay gamePlay, ISim preSimWeek, ISim postSimWeek)
        {
            this.settingService = settingService;                  
            this.gamePlay = gamePlay;
            this.postSimWeek = postSimWeek;
            this.preSimWeek = preSimWeek;
        }

        public void SimWeek(List<ScheduleItem> schedule)
        {
            preSimWeek.run();
            SimGames(schedule);
            postSimWeek.run();            
        }

        private void SimGames(List<ScheduleItem> schedule)
        {            
            SimGame sim;
            FootballSettings setting = settingService.GetSettingsObject();
            
            foreach( var item in schedule )
            {                                       
                sim = new SimGame(item, setting, gamePlay);
                item.result = sim.simGame();
                this.updateStandings(item.result);      
            }            
        }                      
        
        private void updateStandings(GameResult game)
        {
            int scoreDiff = game.homeTeamScore - game.awayTeamScore;
            settingService.UpdateTeamRecords(game.HomeTeam, game.AwayTeam, scoreDiff);
        }          
    }
}

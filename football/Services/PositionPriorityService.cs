﻿using System.Collections.Generic;
using System.Linq;
using Football.League.Players;
using Football.League.Teams;
using Football.Services.Starter;

namespace Football.Services
{
    public class PositionPriorityService
    {
        private PlayerTypeValues playerTypeValues;
        private StarterService starterService;

        private Dictionary<string, List<OpenStarter>> teamLists;

        public PositionPriorityService(PlayerTypeValues playerTypeValues, 
                                       StarterService starterService, 
                                       Dictionary<string, List<OpenStarter>> teamLists = null)  // last one for testing
        {
            if( teamLists == null )
            {
                teamLists = new Dictionary<string, List<OpenStarter>>(); 
            }
                        
            this.playerTypeValues = playerTypeValues;
            this.starterService = starterService;
            this.teamLists = teamLists;
        }

        public int Count(string teamAbbr)
        {            
            return teamLists[teamAbbr].Count;
        }

        public OpenStarter GetNext(string teamAbbr)
        {
            foreach (var item in teamLists[teamAbbr])
            {
                item.AvgStarterRating = starterService.GetAverageStarterRating(teamAbbr, item.Position);
            }

            
            OpenStarter pos = teamLists[teamAbbr].OrderByDescending(x=>x.IsStarterPosition)
                                                 .ThenBy(x => x.AvgStarterRating)
                                                 .FirstOrDefault();                     

            teamLists[teamAbbr].Remove(pos);            
            
            return pos;
        }
        
        // this is initially intended for filling empty starter roles, but should be expanded to roster upgrade (aka free agency, waivers)
        // and needed spots (meet starter minimum, but not overall depth       
        public void SetOpenPositions( string team )
        {         
            List<OpenStarter> openSpots = starterService.GetOpenStarterSpots(team);
            List<OpenStarter> minDepthSpots = starterService.GetMinimumDepthSpots(team);                                    
            openSpots.AddRange(minDepthSpots);

            if (!teamLists.ContainsKey(team))
            {
                teamLists.Add(team, openSpots);
            }
            else
            {
                teamLists[team] = openSpots;
            }
        }

        // omg...i didn't just write this....
        public bool hasOpenSpots()
        {
            return teamLists.Where(x => x.Value.Count() > 0).Count() > 0;
        }

        public List<string> GetTeamsWithLists()
        {
            return teamLists.Where(x=>x.Value.Count() > 0).Select(x=>x.Key).ToList<string>();
        }
    }
}

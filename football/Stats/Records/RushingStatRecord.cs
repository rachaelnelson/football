﻿using System;

//using NHibernate.Mapping.Attributes;
//using NHibernate;
//using NHibernate.Criterion;

namespace Football.Stats.Records
{
    // http://nhibernate.sourceforge.net/NHibernateEg/NHibernateEg.Tutorial1A.html
  //  [Class(Table="rushingStats")]      
    [Serializable()]
    public class RushingStatRecord : StatsRecord
    {
        //    [Id(Name = "Id")]
        //    [Generator(1, Class = "native")]
        public virtual int Id { get; set; }

        //    [Property(NotNull = true)]
        public virtual int PlayerHash { get; set; }

        //    [Property(NotNull = true)]
        public virtual String Team { get; set; }

        //    [Property(NotNull = true)]
        public virtual int Year { get; set; }

        //    [Property(NotNull = true)]
        public virtual String PlayerFirstName { get; set; }

        //    [Property(NotNull = true)]
        public virtual String PlayerLastName { get; set; }

        //    [Property(NotNull = true)]
        public virtual short Attempts { get; set; }

        //    [Property(NotNull = true)]
        public virtual short Yards { get; set; }

        //    [Property(NotNull = true)]
        public virtual byte TDs { get; set; }       
    }
}

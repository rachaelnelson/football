﻿using System;

namespace Football.Stats.Records
{
    [Serializable()]
    public class DefenseStatRecord : StatsRecord
    {
        public virtual int Id { get; set; }

        public virtual int PlayerHash { get; set; }

        public virtual String Team { get; set; }

        public virtual int Year { get; set; }

        public virtual String PlayerFirstName { get; set; }

        public virtual String PlayerLastName { get; set; }

        public virtual short Tackles { get; set; }

        public virtual byte Sacks { get; set; }

        public virtual byte INTs { get; set; }   
    }
}

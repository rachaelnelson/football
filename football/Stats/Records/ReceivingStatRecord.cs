﻿using System;

namespace Football.Stats.Records
{
    [Serializable()]
    public class ReceivingStatRecord : StatsRecord
    {
        public virtual int Id { get; set; }

        public virtual int PlayerHash { get; set; }

        public virtual String Team { get; set; }

        public virtual int Year { get; set; }

        public virtual String PlayerFirstName { get; set; }

        public virtual String PlayerLastName { get; set; }
        
        public virtual byte Receptions { get; set; }

        public virtual short Yards { get; set; }

        public virtual byte TDs { get; set; }        
    }
}

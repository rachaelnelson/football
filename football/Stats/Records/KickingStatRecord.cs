﻿using System;

namespace Football.Stats.Records
{
    [Serializable()]
    public class KickingStatRecord : StatsRecord
    {
        public virtual int Id { get; set; }

        public virtual int PlayerHash { get; set; }

        public virtual String Team { get; set; }

        public virtual int Year { get; set; }

        public virtual String PlayerFirstName { get; set; }

        public virtual String PlayerLastName { get; set; }

        public virtual byte FGAttempts { get; set; }

        public virtual byte FGMade { get; set; }

        public virtual byte XPAttempts { get; set; }

        public virtual byte XPMade { get; set; }
    }
}

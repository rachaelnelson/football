﻿using System;
using System.Collections.Generic;
using Football.League.Players;
using Football.Stats.Records;

namespace Football.Stats.Helpers
{
    interface IStatsHelper
    {  
        List<StatsRecord> getStatList();
        List<StatsRecord> getStatsByTeam(String team);     
        void updateStats(int result, Player player);
        List<StatsRecord> getStatsByPlayer( Player player );     
    }
}
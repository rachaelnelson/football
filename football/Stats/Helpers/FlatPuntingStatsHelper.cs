﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.League.Players;
using Football.Settings;
using Football.Stats.Converters;
using Football.Stats.Records;

namespace Football.Stats.Helpers
{
    class FlatPuntingStatsHelper : IStatsHelper
    {
        private List<PuntingStatRecord> statsList;
        private FootballSettings setting;

        public FlatPuntingStatsHelper(List<StatsRecord> statsList, FootballSettings setting)
        {
            this.setting = setting;
            this.statsList = statsList.ConvertAll<PuntingStatRecord>(StatListConverter.ConvertFromStatsRecord<StatsRecord, PuntingStatRecord>);
        }       

        public List<StatsRecord> getStatList()
        {
            return this.statsList.ConvertAll<StatsRecord>(StatListConverter.ConvertFromStatsRecord<PuntingStatRecord,StatsRecord>);
        }

        public List<StatsRecord> getStatsByTeam(String team)
        {
            List<StatsRecord> existingRecord;

            if (team != "")
            {
                existingRecord = (from item in statsList
                                  where item.Year == this.setting.year
                                  && item.Team == team
                                  select item).ToList<StatsRecord>();

            }
            else
            {
                existingRecord = (from item in statsList
                                  where item.Year == this.setting.year
                                  select item).ToList<StatsRecord>();
            }
            return existingRecord;
        }
        
        public void updateStats(int result, Player player)
        {
            var existingRecord = (from item in statsList
                                  where item.PlayerHash == player.id
                                  && item.PlayerLastName == player.lastName
                                  && item.PlayerFirstName == player.firstName
                                  && item.Year == this.setting.year
                                  && item.Team == player.team.Abbr
                                  select item).ToList();

            PuntingStatRecord puntingStats;

            if (existingRecord.Count == 0)
            {
                puntingStats = new PuntingStatRecord();
                puntingStats.PlayerHash = player.id;
                puntingStats.Team = player.team.Abbr;
                puntingStats.Year = this.setting.year;
                puntingStats.PlayerFirstName = player.firstName;
                puntingStats.PlayerLastName = player.lastName;
                puntingStats.Punts = 0;
                puntingStats.Yards = 0;

                this.statsList.Add(puntingStats);
            }
            else
            {
                puntingStats = existingRecord[0];
            }
            puntingStats.Punts += 1;
            puntingStats.Yards += (short)result;      
        }
       
        public List<StatsRecord> getStatsByPlayer(Player player)
        {
            return (from item in statsList
                    where item.PlayerHash == player.id
                    select item).ToList<StatsRecord>();
        }          
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.League.Players;
using Football.Settings;
using Football.Stats.Converters;
using Football.Stats.Records;

namespace Football.Stats.Helpers
{
    class FlatKickingStatsHelper : IStatsHelper
    {
        private List<KickingStatRecord> statsList;
        private Dictionary<int, Delegate> kickDelegate;
        private FootballSettings setting;

        public FlatKickingStatsHelper(List<StatsRecord> statsList, FootballSettings setting)
        {
            this.setting = setting;

            this.statsList = statsList.ConvertAll<KickingStatRecord>(StatListConverter.ConvertFromStatsRecord<StatsRecord, KickingStatRecord>);

            this.kickDelegate = new Dictionary<int, Delegate>()
            {
                {0,new Action<KickingStatRecord>(this.missedExtraPoint)},
                {1,new Action<KickingStatRecord>(this.madeExtraPoint)},
                {2,new Action<KickingStatRecord>(this.missedFieldGoal)},
                {3,new Action<KickingStatRecord>(this.madeFieldGoal)}
            };
        }
        
        public List<StatsRecord> getStatList()
        {
            return this.statsList.ConvertAll<StatsRecord>(StatListConverter.ConvertFromStatsRecord<KickingStatRecord,StatsRecord>);
        }

        public List<StatsRecord> getStatsByTeam(String team)
        {
            List<StatsRecord> existingRecord;

            if (team != "")
            {
                existingRecord = (from item in statsList
                                  where item.Year == this.setting.year
                                  && item.Team == team
                                  select item).ToList<StatsRecord>();

            }
            else
            {
                existingRecord = (from item in statsList
                                  where item.Year == this.setting.year
                                  select item).ToList<StatsRecord>();
            }
            return existingRecord;
        }

        private KickingStatRecord getExistingPlayerRecord(Player player)
        {
            var existingRecord = (from item in statsList
                                  where item.PlayerHash == player.id
                                  && item.PlayerLastName == player.lastName
                                  && item.PlayerFirstName == player.firstName
                                  && item.Year == this.setting.year
                                  && item.Team == player.team.Abbr
                                  select item).ToList();

            if (existingRecord.Count == 0)
            {
                return null;
            }
            return existingRecord.ToList().First();
        }

        private KickingStatRecord createRecord(Player player)
        {
            KickingStatRecord kickingStats = new KickingStatRecord();
            kickingStats.PlayerHash = player.id;
            kickingStats.Team = player.team.Abbr;
            kickingStats.Year = this.setting.year;
            kickingStats.PlayerFirstName = player.firstName;
            kickingStats.PlayerLastName = player.lastName;
            kickingStats.FGAttempts = 0;
            kickingStats.FGMade = 0;
            kickingStats.XPAttempts = 0;
            kickingStats.XPMade = 0;

            return kickingStats;            
        }       
       
        private void missedExtraPoint(KickingStatRecord kickingStats)
        {
            kickingStats.XPAttempts += 1;            
        }
        private void madeExtraPoint(KickingStatRecord kickingStats)
        {
            kickingStats.XPAttempts += 1;
            kickingStats.XPMade += 1;            
        }
        private void missedFieldGoal(KickingStatRecord kickingStats)
        {
            kickingStats.FGAttempts += 1;            
        }
        private void madeFieldGoal(KickingStatRecord kickingStats)
        {
            kickingStats.FGAttempts += 1;
            kickingStats.FGMade += 1;            
        }   
    
        public void updateStats(int result, Player player)
        {
            KickingStatRecord kickingStats  = this.getExistingPlayerRecord(player);

            if (kickingStats == null)
            {
                kickingStats = this.createRecord(player);
                this.statsList.Add(kickingStats);
            }

            this.kickDelegate[result].DynamicInvoke(kickingStats);            
        }        

        public List<StatsRecord> getStatsByPlayer(Player player)
        {
            return (from item in statsList
                    where item.PlayerHash == player.id
                    select item).ToList<StatsRecord>();
        }          
    }
}

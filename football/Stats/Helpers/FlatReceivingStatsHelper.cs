﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.League.Players;
using Football.Settings;
using Football.Sim.Games;
using Football.Stats.Converters;
using Football.Stats.Records;

namespace Football.Stats.Helpers
{
    class FlatReceivingStatsHelper : IStatsHelper
    {
        private List<ReceivingStatRecord> statsList;
        private FootballSettings setting;
        private GameStateHandler gameHandler;

        public FlatReceivingStatsHelper(List<StatsRecord> statsList, FootballSettings setting, GameStateHandler gameHandler)
        {
            this.setting = setting;
            this.gameHandler = gameHandler;
            this.statsList = statsList.ConvertAll<ReceivingStatRecord>(StatListConverter.ConvertFromStatsRecord<StatsRecord, ReceivingStatRecord>);
        }        

        public List<StatsRecord> getStatList()
        {
            return this.statsList.ConvertAll<StatsRecord>(StatListConverter.ConvertFromStatsRecord<ReceivingStatRecord, StatsRecord>);
        }

        public List<StatsRecord> getStatsByTeam(String team)
        {
            List<StatsRecord> existingRecord;

            if (team != "")
            {
                existingRecord = (from item in statsList
                                  where item.Year == this.setting.year
                                  && item.Team == team
                                  select item).ToList<StatsRecord>();

            }
            else
            {
                existingRecord = (from item in statsList
                                  where item.Year == this.setting.year
                                  select item).ToList<StatsRecord>();
            }
            return existingRecord;
        }        

        public void updateStats(int result, Player player)
        {
            var existingRecord = (from item in statsList
                                  where item.PlayerHash == player.id
                                  && item.PlayerLastName == player.lastName
                                  && item.PlayerFirstName == player.firstName
                                  && item.Year == this.setting.year
                                  && item.Team == player.team.Abbr
                                  select item).ToList();

            ReceivingStatRecord receivingStats;

            if (existingRecord.Count == 0)
            {
                receivingStats = new ReceivingStatRecord();
                receivingStats.PlayerHash = player.id;
                receivingStats.Team = player.team.Abbr;
                receivingStats.Year = this.setting.year;
                receivingStats.PlayerFirstName = player.firstName;
                receivingStats.PlayerLastName = player.lastName;
                receivingStats.Receptions = 0;
                receivingStats.Yards = 0;
                receivingStats.TDs = 0;

                this.statsList.Add(receivingStats);
            }
            else
            {
                receivingStats = existingRecord[0];
            }

            int touchdown = (this.gameHandler.IsTouchdown) ? 1 : 0;
            int completion = (this.gameHandler.IsIncomplete) ? 0 : 1;

            receivingStats.Receptions += (byte)completion;
            receivingStats.Yards += (short)result; //TODO:  result should be short            
            receivingStats.TDs += (byte)touchdown;     
        }       

        public List<StatsRecord> getStatsByPlayer(Player player)
        {
            return (from item in statsList
                    where item.PlayerHash == player.id
                    select item).ToList<StatsRecord>();
        }         
    }
}

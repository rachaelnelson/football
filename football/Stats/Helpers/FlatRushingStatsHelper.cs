﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.League.Players;
using Football.Settings;
using Football.Sim.Games;
using Football.Stats.Converters;
using Football.Stats.Records;

namespace Football.Stats.Helpers
{
    class FlatRushingStatsHelper : IStatsHelper
    {
        private List<RushingStatRecord> statsList;
        private FootballSettings setting;
        private GameStateHandler gameHandler;

        public FlatRushingStatsHelper(List<StatsRecord> statsList, FootballSettings setting, GameStateHandler gameHandler)
        {
            this.setting = setting;
            this.gameHandler = gameHandler;
            this.statsList = statsList.ConvertAll<RushingStatRecord>(StatListConverter.ConvertFromStatsRecord<StatsRecord, RushingStatRecord>);
        }        

        public List<StatsRecord> getStatList()
        {
            return this.statsList.ConvertAll<StatsRecord>(StatListConverter.ConvertFromStatsRecord<RushingStatRecord,StatsRecord>);
        }

        public List<StatsRecord> getStatsByTeam(String team)
        {
            List<StatsRecord> existingRecord;

            if (team != "")
            {
                existingRecord = (from item in statsList
                                  where item.Year == this.setting.year
                                  && item.Team == team
                                  select item).ToList<StatsRecord>();

            }
            else
            {
                existingRecord = (from item in statsList
                                  where item.Year == this.setting.year
                                  select item).ToList<StatsRecord>();
            }
            return existingRecord;
        }        

        public void updateStats(int result, Player player)
        {
            var existingRecord = (from item in statsList
                                  where item.PlayerHash == player.id
                                  && item.PlayerLastName == player.lastName
                                  && item.PlayerFirstName == player.firstName
                                  && item.Year == this.setting.year
                                  && item.Team == player.team.Abbr
                                  select item).ToList();

            RushingStatRecord rushingStats;

            if (existingRecord.Count == 0)
            {
                rushingStats = new RushingStatRecord();
                rushingStats.PlayerHash = player.id;
                rushingStats.Team = player.team.Abbr;
                rushingStats.Year = this.setting.year;
                rushingStats.PlayerFirstName = player.firstName;
                rushingStats.PlayerLastName = player.lastName;
                rushingStats.Attempts = 0;
                rushingStats.Yards = 0;
                rushingStats.TDs = 0;

                this.statsList.Add(rushingStats);
            }
            else
            {
                rushingStats = existingRecord[0];
            }
            int touchdown = (this.gameHandler.IsTouchdown) ? 1 : 0;

            rushingStats.Attempts += (short)1;
            rushingStats.Yards += (short)result; //TODO:  result should be short            
            rushingStats.TDs += (byte)touchdown;    
        }       

        public List<StatsRecord> getStatsByPlayer(Player player)
        {
            return (from item in statsList
                    where item.PlayerHash == player.id
                    select item).ToList<StatsRecord>();
        }              
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.League.Players;
using Football.Settings;
using Football.Sim.Games;
using Football.Stats.Converters;
using Football.Stats.Records;

namespace Football.Stats.Helpers
{
    class FlatPassingStatsHelper :  IStatsHelper
    {        
        private List<PassingStatRecord> statsList;
        private FootballSettings setting;
        private GameStateHandler gameHandler;

        public FlatPassingStatsHelper(List<StatsRecord> statsList, FootballSettings setting, GameStateHandler gameHandler)
        {
            this.setting = setting;
            this.gameHandler = gameHandler;
            this.statsList = statsList.ConvertAll<PassingStatRecord>(StatListConverter.ConvertFromStatsRecord<StatsRecord, PassingStatRecord>);                       
        }        

        public List<StatsRecord> getStatList()
        {
            return this.statsList.ConvertAll<StatsRecord>(StatListConverter.ConvertFromStatsRecord<PassingStatRecord,StatsRecord>);
        }

        public List<StatsRecord> getStatsByTeam(String team)
        {
            List<StatsRecord> existingRecord;

            if (team != "")
            {
                existingRecord = (from item in statsList
                                      where item.Year == this.setting.year
                                      && item.Team == team
                                  select item).ToList<StatsRecord>();
               
            }
            else
            {
                existingRecord = (from item in statsList
                                  where item.Year == this.setting.year
                                  select item).ToList<StatsRecord>();
            }
            return existingRecord;
        }        

        public void updateStats(int result, Player player)
        {
            var existingRecord = (from item in statsList
                                 where item.PlayerHash == player.id
                                 && item.PlayerLastName == player.lastName
                                 && item.PlayerFirstName == player.firstName
                                 && item.Year == this.setting.year
                                 && item.Team == player.team.Abbr
                                 select item).ToList();

            PassingStatRecord passingStats;

            if (existingRecord.Count == 0)
            {
                passingStats = new PassingStatRecord();
                passingStats.PlayerHash = player.id;
                passingStats.Team = player.team.Abbr;
                passingStats.Year = this.setting.year;
                passingStats.PlayerFirstName = player.firstName;
                passingStats.PlayerLastName = player.lastName;
                passingStats.Completions = 0;
                passingStats.Attempts = 0;
                passingStats.Yards = 0;
                passingStats.TDs = 0;
                passingStats.INTs = 0;

                this.statsList.Add(passingStats);
            }
            else
            {
                passingStats = existingRecord[0];
            }
            byte touchdown = (byte)((this.gameHandler.IsTouchdown) ? 1 : 0);            
            byte completion = (byte)((this.gameHandler.IsIncomplete) ? 0 : 1);
            byte interception = (byte)((this.gameHandler.IsInterception) ? 1 : 0);

            passingStats.Attempts += (short)1;
            passingStats.Completions += (short)completion;
            passingStats.Yards += (short)result; //TODO:  result should be short            
            passingStats.TDs += touchdown;
            passingStats.INTs += interception;                                                        
        }

        public List<StatsRecord> getStatsByPlayer(Player player)
        {
            return (from item in statsList
                    where item.PlayerHash == player.id
                    select item).ToList<StatsRecord>();
        }         
    }
}

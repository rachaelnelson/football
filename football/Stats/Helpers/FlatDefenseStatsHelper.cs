﻿using System;
using System.Collections.Generic;
using System.Linq;
using Football.League.Players;
using Football.Settings;
using Football.Sim.Games;
using Football.Stats.Converters;
using Football.Stats.Records;

namespace Football.Stats.Helpers
{    
    class FlatDefenseStatsHelper : IStatsHelper
    {
        private List<DefenseStatRecord> statsList;
        private FootballSettings setting;
        private GameStateHandler gameHandler;

        public FlatDefenseStatsHelper(List<StatsRecord> statsList, FootballSettings setting, GameStateHandler gameHandler)
        {
            this.setting = setting;
            this.gameHandler = gameHandler;
            this.statsList = statsList.ConvertAll<DefenseStatRecord>(StatListConverter.ConvertFromStatsRecord<StatsRecord, DefenseStatRecord>);                  
        }                                  

        public List<StatsRecord> getStatList()
        {            
            return this.statsList.ConvertAll<StatsRecord>(StatListConverter.ConvertFromStatsRecord<DefenseStatRecord, StatsRecord>);
        }

        public List<StatsRecord> getStatsByTeam(String team)
        {
            List<StatsRecord> existingRecord;

            if (team != "")
            {
                existingRecord = (from item in statsList
                                  where item.Year == this.setting.year
                                  && item.Team == team
                                  select item).ToList<StatsRecord>();

            }
            else
            {
                existingRecord = (from item in statsList
                                  where item.Year == this.setting.year
                                  select item).ToList<StatsRecord>();
            }
            return existingRecord;
        }
        
        public void updateStats(int result, Player player)
        {
            var existingRecord = (from item in statsList
                                  where item.PlayerHash == player.id
                                  && item.PlayerLastName == player.lastName
                                  && item.PlayerFirstName == player.firstName
                                  && item.Year == this.setting.year
                                  && item.Team == player.team.Abbr
                                  select item).ToList();

            DefenseStatRecord tacklingStats;

            if (existingRecord.Count == 0)
            {
                tacklingStats = new DefenseStatRecord();
                tacklingStats.PlayerHash = player.id;
                tacklingStats.Team = player.team.Abbr;
                tacklingStats.Year = this.setting.year;
                tacklingStats.PlayerFirstName = player.firstName;
                tacklingStats.PlayerLastName = player.lastName;
                tacklingStats.Tackles = 0;
                tacklingStats.Sacks = 0;
                tacklingStats.INTs = 0;

                this.statsList.Add(tacklingStats);
            }
            else
            {
                tacklingStats = existingRecord[0];
            }

            tacklingStats.Tackles += 1;

            if (this.gameHandler.IsInterception)
            {
                tacklingStats.INTs += 1;
            }
            if (this.gameHandler.IsSack)
            {
                tacklingStats.Sacks += 1;
            }
        }        

        public List<StatsRecord> getStatsByPlayer(Player player)
        {
            return (from item in statsList
                     where item.PlayerHash == player.id                                  
                     select item).ToList<StatsRecord>();
        }       
    }
}

﻿using System;
using System.Collections.Generic;
using Football.League.Players;
using Football.Settings;
using Football.Sim.Games;
using Football.Stats.Helpers;
using Football.Stats.Records;

namespace Football.Stats
{   
    public class FootballStats
    {                  
        private Dictionary<String, IStatsHelper> statsDict;
        private FootballSettings setting;
        private GameStateHandler gameHandler;

        public FootballStats(FootballSettings setting, GameStateHandler gameHandler)
        {
            this.setting = setting;
            this.gameHandler = gameHandler;
            this.statsDict = this.initDict(gameHandler);   
        }

        private Dictionary<String, IStatsHelper> initDict(GameStateHandler gameHandler)
        {            
            return new Dictionary<String, IStatsHelper>()
            {
                {"Passing",new FlatPassingStatsHelper(setting.statsList["Passing"],setting, gameHandler)},            
                {"Rushing",new FlatRushingStatsHelper(setting.statsList["Rushing"],setting, gameHandler)},            
                {"Receiving",new FlatReceivingStatsHelper(setting.statsList["Receiving"],setting, gameHandler)},           
                {"Defense",new FlatDefenseStatsHelper(setting.statsList["Defense"],setting, gameHandler)},           
                {"Kicking",new FlatKickingStatsHelper(setting.statsList["Kicking"],setting)},            
                {"Punting",new FlatPuntingStatsHelper(setting.statsList["Punting"],setting)}            
            };
        }

        public List<StatsRecord> getStats(String key, String abbr)
        {
            abbr = (abbr == "--")?"":abbr;            
                            
            return this.statsDict[key].getStatsByTeam( abbr );            
        }

        public List<StatsRecord> getPlayerStats(String key, Player player)
        {            
            return this.statsDict[key].getStatsByPlayer(player);
        }

        public void updateStats()
        {          
            foreach(var item in this.statsDict)
            {
                setting.statsList[item.Key] = item.Value.getStatList();
            }
        }        
          
        public void updateRushingStats(int result, List<Player> players)
        {
            if (result != -500)
            {
                this.statsDict["Rushing"].updateStats(result, players[0]);

                if (players.Count > 1)
                {
                    this.statsDict["Defense"].updateStats(result, players[1]);
                }
            }
        }

        public void updatePassingStats(int result, List<Player> players)
        {
            if (result != -500)
            {
                this.statsDict["Passing"].updateStats(result, players[0]);

                if (players.Count > 1)
                {
                    this.statsDict["Receiving"].updateStats(result, players[1]);
                }
                if (players.Count > 2)
                {
                    this.statsDict["Defense"].updateStats(result, players[2]);
                }
            }
        }       

        public void updateKickingStats(bool isMade, List<Player> players, bool isFG)
        {
            // Made FG 3
            // Missed FG 2
            // Made not a FG 1
            // Missed xp 0

            int result = 0;

            if (isMade)
            {
                result += 1; 
            }

            if (isFG)
            {
                result += 2;
            }
                                   
            this.statsDict["Kicking"].updateStats(result, players[0]);
        }

        public void updatePuntingStats(int result, List<Player> players)
        {
            this.statsDict["Punting"].updateStats(result, players[0]);
        }


        /*
        private static int t1runyds, t2runyds, t1passyds, t2passyds;
        private static int t1patts, t2patts, t1pcomp, t2pcomp;
        private static int t1ratts, t2ratts;

        public Stats()
        {
            t1runyds = 0;
            t2runyds = 0;
            t1passyds = 0;
            t2passyds = 0;
            t1patts = 0;
            t2patts = 0;
            t1pcomp = 0;
            t2pcomp = 0;
            t1ratts = 0;
            t2ratts = 0;
        }

        public void addRunYds(int result, bool equal)
        {
            // if team 1 has ball, add to t1 stats 
            if (equal == true)
                t1runyds = t1runyds + result;
            else
                t2runyds = t2runyds + result;
        }
        public int getT1RunYds()
        { return t1runyds; }

        public int getT2RunYds()
        { return t2runyds; }

        public void addPComp(bool equal)
        {
            // if team 1 has ball, add to t1 stats 
            if (equal == true)
                t1pcomp++;
            else
                t2pcomp++;
        }

        public int getT1PComp()
        { return t1pcomp; }

        public int getT2PComp()
        { return t2pcomp; }

        public void addPAtt(bool equal)
        {
            // if team 1 has ball, add to t1 stats 
            if (equal == true)
                t1patts++;
            else
                t2patts++;
        }

        public int getT1PAtt()
        { return t1patts; }

        public int getT2PAtt()
        { return t2patts; }

        public void addRAtt(bool equal)
        {
            // if team 1 has ball, add to t1 stats 
            if (equal == true)
                t1ratts++;
            else
                t2ratts++;
        }

        public int getT1RAtt()
        { return t1ratts; }

        public int getT2RAtt()
        { return t2ratts; }

        public void addPassYds(int result, bool equal)
        {
            // if team 1 has ball, add to t1 stats 
            if (equal == true)
                t1passyds = t1passyds + result;
            else
                t2passyds = t2passyds + result;
        }

        public int getT1PassYds()
        { return t1passyds; }

        public int getT2PassYds()
        { return t2passyds; }
        */

    }
}
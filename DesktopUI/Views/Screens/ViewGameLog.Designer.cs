﻿namespace DesktopUI.Views.Screens
{
    partial class ViewGameLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlGameLog = new System.Windows.Forms.Panel();
            this.txtGameLog = new System.Windows.Forms.TextBox();
            this.pnlGameLog.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlGameLog
            // 
            this.pnlGameLog.AutoScroll = true;
            this.pnlGameLog.Controls.Add(this.txtGameLog);
            this.pnlGameLog.Location = new System.Drawing.Point(2, 2);
            this.pnlGameLog.Name = "pnlGameLog";
            this.pnlGameLog.Size = new System.Drawing.Size(571, 275);
            this.pnlGameLog.TabIndex = 0;
            // 
            // txtGameLog
            // 
            this.txtGameLog.BackColor = System.Drawing.Color.AliceBlue;
            this.txtGameLog.Location = new System.Drawing.Point(3, 3);
            this.txtGameLog.Multiline = true;
            this.txtGameLog.Name = "txtGameLog";
            this.txtGameLog.ReadOnly = true;
            this.txtGameLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtGameLog.Size = new System.Drawing.Size(565, 269);
            this.txtGameLog.TabIndex = 0;
            // 
            // ViewGameLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 279);
            this.Controls.Add(this.pnlGameLog);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ViewGameLog";
            this.Text = "View Game Log";
            this.pnlGameLog.ResumeLayout(false);
            this.pnlGameLog.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlGameLog;
        private System.Windows.Forms.TextBox txtGameLog;
    }
}
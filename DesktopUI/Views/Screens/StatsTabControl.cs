﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Football.League.Teams;
using Football.Library;
using Football.Settings;
using Football.Stats;
using Football.Stats.Records;
using Football.UiClasses.Helper;

namespace DesktopUI.Views.Screens
{
    public partial class StatsTabControl : UserControl
    {
        private BindingSource lbs;       
        private FootballSettings setting;
        private FootballStats stats;
        
        private List<String> statTypes = new List<String>()
        {
            "Passing", 
            "Rushing",
            "Receiving",
            "Defense",
            "Kicking",
            "Punting"
        };

        private Dictionary<string,Delegate> delegateDict;
        //private StatsHelper statsHelper;

        public StatsTabControl(FootballSettings setting, FootballStats stats)
        {
            InitializeComponent();
            this.setting = setting;
            this.stats = stats;
            this.lbs = new BindingSource();
          //  this.statsHelper = new StatsHelper(); 
            this.initDict();
            this.initCombos();
            this.lbs.Position = 0;                                    
        }

        private void initCombos()
        {           
            this.statTypeCombo.DataSource = new BindingSource(statTypes,null);            
            this.populateTeamSelectCombo();
            this.chooseTeamCombo.SelectedIndex = chooseTeamCombo.FindStringExact(setting.getMyTeam().Abbr);
            this.statTypeCombo.SelectedIndex = 0;
            this.statsGrid.Columns["Id"].Visible = false;
            this.statsGrid.Columns["Year"].Visible = false;
            this.statsGrid.Columns["PlayerHash"].Visible = false;            
                    
            this.statsGrid.Show();           
        }

        private void initDict()
        {
            /* This still fucking sucks but couldn't get the god damn reflection to do this:
             * 
             * getStats<type passed in at runtime>()
             *
             */
            this.delegateDict = new Dictionary<string, Delegate>()
            {
                {"Passing", new Func<List<StatsRecord>,SortableBindingList<PassingStatRecord>>(StatsHelper.getStatRecords<PassingStatRecord>)},
                {"Rushing", new Func<List<StatsRecord>,SortableBindingList<RushingStatRecord>>(StatsHelper.getStatRecords<RushingStatRecord>)},
                {"Receiving", new Func<List<StatsRecord>,SortableBindingList<ReceivingStatRecord>>(StatsHelper.getStatRecords<ReceivingStatRecord>)},
                {"Defense", new Func<List<StatsRecord>,SortableBindingList<DefenseStatRecord>>(StatsHelper.getStatRecords<DefenseStatRecord>)},
                {"Kicking", new Func<List<StatsRecord>,SortableBindingList<KickingStatRecord>>(StatsHelper.getStatRecords<KickingStatRecord>)},
                {"Punting", new Func<List<StatsRecord>,SortableBindingList<PuntingStatRecord>>(StatsHelper.getStatRecords<PuntingStatRecord>)}
            };
        }

        private void getStatsGrid()
        {
            if ( this.chooseTeamCombo.Items.Count==0 )
            {
                return;
            }
            
            String statType = this.statTypeCombo.SelectedValue.ToString();           
            String teamAbbr = this.chooseTeamCombo.SelectedValue.ToString();
           
            if (statType.Length == 0)
            {                
                statType = "Passing";
            }
                  
            List<StatsRecord> statsRecord = this.stats.getStats(statType, teamAbbr);
            this.lbs.DataSource = this.delegateDict[statType].DynamicInvoke(statsRecord);                        
            this.statsGrid.DataSource = this.lbs;            
                        
            this.statsGrid.Columns["Id"].Visible = false;
            this.statsGrid.Columns["Year"].Visible = false;
            this.statsGrid.Columns["PlayerHash"].Visible = false;
            this.lbs.ResetBindings(false);                        
        }       
                

        private void chooseTeamCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.getStatsGrid();
            Console.WriteLine("team");
        }

        private void positionCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.getStatsGrid();
            Console.WriteLine("pos");
        }        

        /*
        *   Populate select box
        */
        private void populateTeamSelectCombo()
        {
            Dictionary<String, Team> teams = setting.getTeams();

            try
            {
                List<String> tList = new List<String>();
                tList.Add("--");
                foreach (string key in teams.Keys)
                {
                    tList.Add(teams[key].Abbr);
                }
                // binds dictionary of strings since you can't bind dict with objects nicely...
                this.chooseTeamCombo.DataSource = new BindingSource(tList, null);                              
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.StackTrace);
            }
        }       
    }
}

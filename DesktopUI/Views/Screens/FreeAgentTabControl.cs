﻿using System;
using System.Windows.Forms;
using Football.UiClasses.Presenter;
using Football.UiClasses.ViewModel;

namespace DesktopUI.Views.Screens
{
    public partial class FreeAgentTabControl : UserControl
    {       
        private FreeAgentPresenter presenter;
        private PlayerViewPresenter playerViewPresenter;

        public FreeAgentTabControl(FreeAgentPresenter presenter, PlayerViewPresenter playerViewPresenter)
        {
            InitializeComponent();
            
            this.positionCombo.SelectedIndexChanged += new System.EventHandler(this.positionCombo_SelectedIndexChanged);
            this.signPlayer.Click += new System.EventHandler(signPlayer_Click);

            this.presenter = presenter;
            this.playerViewPresenter = playerViewPresenter;

            positionCombo.DataSource = this.presenter.ViewModel.Positions;
            rosterGrid.DataSource = this.presenter.ViewModel.FreeAgentData;

            HideAttr(); // should be hidden with SelectedIndexChanged, but not working

            // for some stupid reason, the gui designer won't let me set the size.
            this.lblCap.Height = 50;
            this.lblCap.Width = 200;

            // YAY found how to bind to view model!!
            // http://www.dreamincode.net/forums/topic/208833-using-the-inotifypropertychanged-functionality/
            this.lblCap.DataBindings.Add("Text", this.presenter.ViewModel, "CapSpace");            
        }             
        
        private void positionCombo_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            HideAttr();            
        }

        private void HideAttr()
        {
            presenter.PositionChanged(positionCombo.SelectedValue);
            HideColumns.HideEmptyAttributes(rosterGrid);
            rosterGrid.Columns["Id"].Visible = false; // hide id column
            rosterGrid.Columns["Salary Request Hidden"].Visible = false; // hide salary int column
        }

        //private void HideSignButton()
        //{
        //    signPlayer.Enabled = ( rosterGrid.Rows.Count > 0 );   
        //}
        
        // TODO:  prolly need to move this out of here....
        private void signPlayer_Click(object sender, EventArgs e)
        {
            if (rosterGrid.Rows.Count > 0)
            {
                presenter.InitiateSignPlayer(rosterGrid.CurrentRow.Cells[0].Value, rosterGrid.CurrentRow.Cells[7].Value);

                SignPlayerView player = this.presenter.ViewModel.SignPlayer;
                SignPlayerForm signPlayerForm = new SignPlayerForm(presenter, player); // TODO:  move this dependency out....
                signPlayerForm.Show();
            } 
        }

        private void rosterGrid_DoubleClick(object sender, EventArgs e)
        {
            playerViewPresenter.Display(rosterGrid.CurrentRow.Cells[0].Value);           
        }                    
    }
}

﻿using System;
using System.Windows.Forms;

using Football.Settings;

namespace DesktopUI.Views.Screens
{
    public partial class GameSettingsTabControl : UserControl
    {
        private TabPage gameSettingsTab;
        private Button btnSave;
        public TabControl tabControl;
        private CheckBox injuriesCheckbox;
        private FootballSettings setting;

        public GameSettingsTabControl(FootballSettings setting)
        {
           InitializeComponent();
           this.setting = setting;  
           this.injuriesCheckbox.Checked = this.setting.injuriesEnabled;     
        }       

        private void InitializeComponent()
        {
            this.gameSettingsTab = new System.Windows.Forms.TabPage();
            this.injuriesCheckbox = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.gameSettingsTab.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // gameSettingsTab
            // 
            this.gameSettingsTab.BackColor = System.Drawing.Color.AliceBlue;
            this.gameSettingsTab.Controls.Add(this.injuriesCheckbox);
            this.gameSettingsTab.Controls.Add(this.btnSave);
            this.gameSettingsTab.Location = new System.Drawing.Point(4, 22);
            this.gameSettingsTab.Name = "gameSettingsTab";
            this.gameSettingsTab.Padding = new System.Windows.Forms.Padding(3);
            this.gameSettingsTab.Size = new System.Drawing.Size(742, 301);
            this.gameSettingsTab.TabIndex = 2;
            this.gameSettingsTab.Text = "Game Settings";
            // 
            // injuriesCheckbox
            // 
            this.injuriesCheckbox.AutoSize = true;
            this.injuriesCheckbox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.injuriesCheckbox.Location = new System.Drawing.Point(6, 15);
            this.injuriesCheckbox.Name = "injuriesCheckbox";
            this.injuriesCheckbox.Size = new System.Drawing.Size(59, 17);
            this.injuriesCheckbox.TabIndex = 5;
            this.injuriesCheckbox.Text = "Injuries";
            this.injuriesCheckbox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.injuriesCheckbox.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(274, 233);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.gameSettingsTab);
            this.tabControl.Location = new System.Drawing.Point(3, 3);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(750, 327);
            this.tabControl.TabIndex = 3;
            // 
            // GameSettingsTabControl
            // 
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.tabControl);
            this.Name = "GameSettingsTabControl";
            this.Size = new System.Drawing.Size(756, 333);
            this.gameSettingsTab.ResumeLayout(false);
            this.gameSettingsTab.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }        

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.setting.injuriesEnabled = this.injuriesCheckbox.Checked;
            MessageBox.Show("Settings Saved.", "Success", MessageBoxButtons.OK);
        }            
    }
}

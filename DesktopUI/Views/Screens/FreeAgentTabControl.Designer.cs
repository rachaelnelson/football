﻿namespace DesktopUI.Views.Screens
{
    partial class FreeAgentTabControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.lineupTab = new System.Windows.Forms.TabPage();
            this.lblCapLabel = new System.Windows.Forms.Label();
            this.lblCap = new System.Windows.Forms.Label();
            this.rosterGrid = new System.Windows.Forms.DataGridView();
            this.signPlayer = new System.Windows.Forms.Button();
            this.positionCombo = new System.Windows.Forms.ComboBox();
            this.tabControl.SuspendLayout();
            this.lineupTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rosterGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.lineupTab);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(781, 463);
            this.tabControl.TabIndex = 3;
            // 
            // lineupTab
            // 
            this.lineupTab.BackColor = System.Drawing.Color.AliceBlue;
            this.lineupTab.Controls.Add(this.lblCapLabel);
            this.lineupTab.Controls.Add(this.lblCap);
            this.lineupTab.Controls.Add(this.rosterGrid);
            this.lineupTab.Controls.Add(this.signPlayer);
            this.lineupTab.Controls.Add(this.positionCombo);
            this.lineupTab.Location = new System.Drawing.Point(4, 22);
            this.lineupTab.Name = "lineupTab";
            this.lineupTab.Padding = new System.Windows.Forms.Padding(3);
            this.lineupTab.Size = new System.Drawing.Size(773, 437);
            this.lineupTab.TabIndex = 2;
            this.lineupTab.Text = "Free Agents";
            // 
            // lblCapLabel
            // 
            this.lblCapLabel.AutoSize = true;
            this.lblCapLabel.Location = new System.Drawing.Point(224, 8);
            this.lblCapLabel.Name = "lblCapLabel";
            this.lblCapLabel.Size = new System.Drawing.Size(63, 13);
            this.lblCapLabel.TabIndex = 8;
            this.lblCapLabel.Text = "Cap Space:";
            // 
            // lblCap
            // 
            this.lblCap.AutoSize = true;
            this.lblCap.Location = new System.Drawing.Point(296, 8);
            this.lblCap.Name = "lblCap";
            this.lblCap.Size = new System.Drawing.Size(0, 13);
            this.lblCap.TabIndex = 7;
            // 
            // rosterGrid
            // 
            this.rosterGrid.AllowUserToAddRows = false;
            this.rosterGrid.AllowUserToDeleteRows = false;
            this.rosterGrid.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.rosterGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.rosterGrid.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.rosterGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rosterGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rosterGrid.Location = new System.Drawing.Point(4, 30);
            this.rosterGrid.Name = "rosterGrid";
            this.rosterGrid.ReadOnly = true;
            this.rosterGrid.RowHeadersVisible = false;
            this.rosterGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.rosterGrid.Size = new System.Drawing.Size(763, 401);
            this.rosterGrid.TabIndex = 5;            
            this.rosterGrid.DoubleClick += new System.EventHandler(this.rosterGrid_DoubleClick);
            // 
            // signPlayer
            // 
            this.signPlayer.Location = new System.Drawing.Point(130, 3);
            this.signPlayer.Name = "signPlayer";
            this.signPlayer.Size = new System.Drawing.Size(75, 23);
            this.signPlayer.TabIndex = 3;
            this.signPlayer.Text = "Sign";
            this.signPlayer.UseVisualStyleBackColor = true;
            // 
            // positionCombo
            // 
            this.positionCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.positionCombo.FormattingEnabled = true;
            this.positionCombo.Location = new System.Drawing.Point(3, 3);
            this.positionCombo.Name = "positionCombo";
            this.positionCombo.Size = new System.Drawing.Size(121, 21);
            this.positionCombo.TabIndex = 1;
            // 
            // FreeAgentTabControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.tabControl);
            this.Name = "FreeAgentTabControl";
            this.Size = new System.Drawing.Size(781, 463);
            this.tabControl.ResumeLayout(false);
            this.lineupTab.ResumeLayout(false);
            this.lineupTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rosterGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage lineupTab;
        private System.Windows.Forms.DataGridView rosterGrid;
        private System.Windows.Forms.Button signPlayer;
        private System.Windows.Forms.ComboBox positionCombo;
        private System.Windows.Forms.Label lblCap;
        private System.Windows.Forms.Label lblCapLabel;


    }
}

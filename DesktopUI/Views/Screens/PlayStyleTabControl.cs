﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Football.League.Coaches;
using Football.Settings;

namespace DesktopUI.Views.Screens
{
    public partial class PlayStyleTabControl : UserControl
    {
        private TabPage playStyleTab;
        private Button btnSave;
        private ComboBox comboBaseDefense;
        private Label baseDefense;
        private Label lblOffense;
        private ComboBox comboBaseOffense;
        private Label lblSlider;
        private TrackBar sliderRunPass;
        private TextBox txtSlider;
        public TabControl tabControl;
        private TextBox txtTwoPoint;
        private TrackBar sliderTwoPoint;
        private Label lblTwoPoint;
        private TextBox txtBehind;
        private TrackBar sliderBehind;
        private Label lblBehind;
        private FootballSettings setting;

        public PlayStyleTabControl(FootballSettings setting)
        {
           InitializeComponent();
           this.setting = setting;
           List<BaseAll> offList = new List<BaseAll>(){ BaseAll.TwoRB, BaseAll.TwoTE, BaseAll.ThreeWR };
           List<BaseAll> defList = new List<BaseAll>(){ BaseAll.FourThree, BaseAll.ThreeFour };
  
           this.comboBaseOffense.DataSource = offList;               //Enum.GetValues(typeof(BaseAll));  //populate combo    
           this.comboBaseDefense.DataSource = defList; //Enum.GetValues(typeof(BaseAll));  //populate combo    

           Coach coach = this.setting.getMyTeam().Coach;            
           this.comboBaseOffense.SelectedItem = coach.BaseOffense;
           this.comboBaseDefense.SelectedItem = coach.BaseDefense;
           this.sliderRunPass.Value = coach.Balance;
           this.sliderBehind.Value = coach.Behind;
           this.sliderTwoPoint.Value = coach.TwoPoint;

           txtSlider.Text = "" + sliderRunPass.Value;
           txtBehind.Text = "" + sliderBehind.Value;
           txtTwoPoint.Text = "" + sliderTwoPoint.Value;
        }       

        private void InitializeComponent()
        {
            this.playStyleTab = new System.Windows.Forms.TabPage();
            this.txtTwoPoint = new System.Windows.Forms.TextBox();
            this.sliderTwoPoint = new System.Windows.Forms.TrackBar();
            this.lblTwoPoint = new System.Windows.Forms.Label();
            this.txtBehind = new System.Windows.Forms.TextBox();
            this.sliderBehind = new System.Windows.Forms.TrackBar();
            this.lblBehind = new System.Windows.Forms.Label();
            this.txtSlider = new System.Windows.Forms.TextBox();
            this.lblSlider = new System.Windows.Forms.Label();
            this.sliderRunPass = new System.Windows.Forms.TrackBar();
            this.btnSave = new System.Windows.Forms.Button();
            this.comboBaseDefense = new System.Windows.Forms.ComboBox();
            this.baseDefense = new System.Windows.Forms.Label();
            this.lblOffense = new System.Windows.Forms.Label();
            this.comboBaseOffense = new System.Windows.Forms.ComboBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.playStyleTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sliderTwoPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sliderBehind)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sliderRunPass)).BeginInit();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // playStyleTab
            // 
            this.playStyleTab.BackColor = System.Drawing.Color.AliceBlue;
            this.playStyleTab.Controls.Add(this.txtTwoPoint);
            this.playStyleTab.Controls.Add(this.sliderTwoPoint);
            this.playStyleTab.Controls.Add(this.lblTwoPoint);
            this.playStyleTab.Controls.Add(this.txtBehind);
            this.playStyleTab.Controls.Add(this.sliderBehind);
            this.playStyleTab.Controls.Add(this.lblBehind);
            this.playStyleTab.Controls.Add(this.txtSlider);
            this.playStyleTab.Controls.Add(this.lblSlider);
            this.playStyleTab.Controls.Add(this.sliderRunPass);
            this.playStyleTab.Controls.Add(this.btnSave);
            this.playStyleTab.Controls.Add(this.comboBaseDefense);
            this.playStyleTab.Controls.Add(this.baseDefense);
            this.playStyleTab.Controls.Add(this.lblOffense);
            this.playStyleTab.Controls.Add(this.comboBaseOffense);
            this.playStyleTab.Location = new System.Drawing.Point(4, 22);
            this.playStyleTab.Name = "playStyleTab";
            this.playStyleTab.Padding = new System.Windows.Forms.Padding(3);
            this.playStyleTab.Size = new System.Drawing.Size(742, 301);
            this.playStyleTab.TabIndex = 2;
            this.playStyleTab.Text = "Play Style";
            // 
            // txtTwoPoint
            // 
            this.txtTwoPoint.Location = new System.Drawing.Point(475, 184);
            this.txtTwoPoint.Name = "txtTwoPoint";
            this.txtTwoPoint.ReadOnly = true;
            this.txtTwoPoint.Size = new System.Drawing.Size(75, 20);
            this.txtTwoPoint.TabIndex = 13;
            // 
            // sliderTwoPoint
            // 
            this.sliderTwoPoint.Location = new System.Drawing.Point(475, 133);
            this.sliderTwoPoint.Maximum = 100;
            this.sliderTwoPoint.Name = "sliderTwoPoint";
            this.sliderTwoPoint.Size = new System.Drawing.Size(104, 45);
            this.sliderTwoPoint.TabIndex = 12;
            this.sliderTwoPoint.TickStyle = System.Windows.Forms.TickStyle.None;
            this.sliderTwoPoint.Scroll += new System.EventHandler(this.sliderTwoPoint_Scroll);
            // 
            // lblTwoPoint
            // 
            this.lblTwoPoint.AutoSize = true;
            this.lblTwoPoint.Location = new System.Drawing.Point(472, 104);
            this.lblTwoPoint.Name = "lblTwoPoint";
            this.lblTwoPoint.Size = new System.Drawing.Size(196, 13);
            this.lblTwoPoint.TabIndex = 11;
            this.lblTwoPoint.Text = "Two Point Conversion (% Passing Plays)";
            // 
            // txtBehind
            // 
            this.txtBehind.Location = new System.Drawing.Point(274, 184);
            this.txtBehind.Name = "txtBehind";
            this.txtBehind.ReadOnly = true;
            this.txtBehind.Size = new System.Drawing.Size(75, 20);
            this.txtBehind.TabIndex = 10;
            // 
            // sliderBehind
            // 
            this.sliderBehind.Location = new System.Drawing.Point(274, 133);
            this.sliderBehind.Maximum = 100;
            this.sliderBehind.Name = "sliderBehind";
            this.sliderBehind.Size = new System.Drawing.Size(104, 45);
            this.sliderBehind.TabIndex = 9;
            this.sliderBehind.TickStyle = System.Windows.Forms.TickStyle.None;
            this.sliderBehind.Scroll += new System.EventHandler(this.sliderBehind_Scroll);
            // 
            // lblBehind
            // 
            this.lblBehind.AutoSize = true;
            this.lblBehind.Location = new System.Drawing.Point(271, 104);
            this.lblBehind.Name = "lblBehind";
            this.lblBehind.Size = new System.Drawing.Size(125, 13);
            this.lblBehind.TabIndex = 8;
            this.lblBehind.Text = "Behind (% Passing Plays)";
            // 
            // txtSlider
            // 
            this.txtSlider.Location = new System.Drawing.Point(83, 184);
            this.txtSlider.Name = "txtSlider";
            this.txtSlider.ReadOnly = true;
            this.txtSlider.Size = new System.Drawing.Size(75, 20);
            this.txtSlider.TabIndex = 7;
            // 
            // lblSlider
            // 
            this.lblSlider.AutoSize = true;
            this.lblSlider.Location = new System.Drawing.Point(9, 104);
            this.lblSlider.Name = "lblSlider";
            this.lblSlider.Size = new System.Drawing.Size(131, 13);
            this.lblSlider.TabIndex = 6;
            this.lblSlider.Text = "Balance (% Passing Plays)";
            // 
            // sliderRunPass
            // 
            this.sliderRunPass.Location = new System.Drawing.Point(70, 133);
            this.sliderRunPass.Maximum = 100;
            this.sliderRunPass.Name = "sliderRunPass";
            this.sliderRunPass.Size = new System.Drawing.Size(104, 45);
            this.sliderRunPass.TabIndex = 5;
            this.sliderRunPass.TickStyle = System.Windows.Forms.TickStyle.None;
            this.sliderRunPass.Scroll += new System.EventHandler(this.sliderRunPass_Scroll);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(274, 233);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // comboBaseDefense
            // 
            this.comboBaseDefense.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBaseDefense.FormattingEnabled = true;
            this.comboBaseDefense.Items.AddRange(new object[] {
            "4-3",
            "3-4"});
            this.comboBaseDefense.Location = new System.Drawing.Point(365, 12);
            this.comboBaseDefense.Name = "comboBaseDefense";
            this.comboBaseDefense.Size = new System.Drawing.Size(121, 21);
            this.comboBaseDefense.TabIndex = 3;
            // 
            // baseDefense
            // 
            this.baseDefense.AutoSize = true;
            this.baseDefense.Location = new System.Drawing.Point(275, 20);
            this.baseDefense.Name = "baseDefense";
            this.baseDefense.Size = new System.Drawing.Size(74, 13);
            this.baseDefense.TabIndex = 2;
            this.baseDefense.Text = "Base Defense";
            // 
            // lblOffense
            // 
            this.lblOffense.AutoSize = true;
            this.lblOffense.Location = new System.Drawing.Point(6, 15);
            this.lblOffense.Name = "lblOffense";
            this.lblOffense.Size = new System.Drawing.Size(71, 13);
            this.lblOffense.TabIndex = 1;
            this.lblOffense.Text = "Base Offense";
            // 
            // comboBaseOffense
            // 
            this.comboBaseOffense.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBaseOffense.FormattingEnabled = true;
            this.comboBaseOffense.Location = new System.Drawing.Point(83, 12);
            this.comboBaseOffense.Name = "comboBaseOffense";
            this.comboBaseOffense.Size = new System.Drawing.Size(121, 21);
            this.comboBaseOffense.TabIndex = 0;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.playStyleTab);
            this.tabControl.Location = new System.Drawing.Point(3, 3);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(750, 327);
            this.tabControl.TabIndex = 3;
            // 
            // PlayStyleTabControl
            // 
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.tabControl);
            this.Name = "PlayStyleTabControl";
            this.Size = new System.Drawing.Size(756, 333);
            this.playStyleTab.ResumeLayout(false);
            this.playStyleTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sliderTwoPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sliderBehind)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sliderRunPass)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        /*
        public void SettingsTab_SizeChanged(object sender, EventArgs e)
        {
            System.Console.WriteLine("inside settings tab");
            this.tabControl.Width = this.settingsTab.Width;
            this.tabControl.Height = this.settingsTab.Height;
        }
         */ 

        private void sliderRunPass_Scroll(object sender, System.EventArgs e)
        {
            // Display the trackbar value in the text box.
            txtSlider.Text = "" + sliderRunPass.Value;
        }

        private void sliderBehind_Scroll(object sender, System.EventArgs e)
        {
            // Display the trackbar value in the text box.
            txtBehind.Text = "" + sliderBehind.Value;
        }

        private void sliderTwoPoint_Scroll(object sender, System.EventArgs e)
        {
            // Display the trackbar value in the text box.
            txtTwoPoint.Text = "" + sliderTwoPoint.Value;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Coach coach = this.setting.getMyTeam().Coach;
            coach.BaseOffense = (BaseAll)this.comboBaseOffense.SelectedValue;
            coach.BaseDefense = (BaseAll)this.comboBaseDefense.SelectedValue;
            coach.Balance = this.sliderRunPass.Value;
            coach.Behind = this.sliderBehind.Value;
            coach.TwoPoint = this.sliderTwoPoint.Value; 

            MessageBox.Show("Settings Saved.", "Success", MessageBoxButtons.OK);
        }
    }
}

﻿namespace DesktopUI.Views.Screens
{
    public partial class LineupTabControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.lineupTab = new System.Windows.Forms.TabPage();
            this.lineupGrid = new System.Windows.Forms.DataGridView();
            this.demotePlayer = new System.Windows.Forms.Button();
            this.promotePlayer = new System.Windows.Forms.Button();
            this.formationCombo = new System.Windows.Forms.ComboBox();
            this.tabControl.SuspendLayout();
            this.lineupTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lineupGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.lineupTab);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(781, 463);
            this.tabControl.TabIndex = 2;
            // 
            // lineupTab
            // 
            this.lineupTab.BackColor = System.Drawing.Color.AliceBlue;
            this.lineupTab.Controls.Add(this.lineupGrid);
            this.lineupTab.Controls.Add(this.demotePlayer);
            this.lineupTab.Controls.Add(this.promotePlayer);
            this.lineupTab.Controls.Add(this.formationCombo);
            this.lineupTab.Location = new System.Drawing.Point(4, 22);
            this.lineupTab.Name = "lineupTab";
            this.lineupTab.Padding = new System.Windows.Forms.Padding(3);
            this.lineupTab.Size = new System.Drawing.Size(773, 437);
            this.lineupTab.TabIndex = 2;
            this.lineupTab.Text = "Lineup";
            // 
            // lineupGrid
            // 
            this.lineupGrid.AllowUserToAddRows = false;
            this.lineupGrid.AllowUserToDeleteRows = false;
            this.lineupGrid.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lineupGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.lineupGrid.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.lineupGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lineupGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lineupGrid.Location = new System.Drawing.Point(3, 250);
            this.lineupGrid.MultiSelect = false;
            this.lineupGrid.Name = "lineupGrid";
            this.lineupGrid.ReadOnly = true;
            this.lineupGrid.RowHeadersVisible = false;
            this.lineupGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.lineupGrid.Size = new System.Drawing.Size(763, 224);
            this.lineupGrid.TabIndex = 5;
            // 
            // demotePlayer
            // 
            this.demotePlayer.Location = new System.Drawing.Point(87, 250);
            this.demotePlayer.Name = "demotePlayer";
            this.demotePlayer.Size = new System.Drawing.Size(75, 23);
            this.demotePlayer.TabIndex = 4;
            this.demotePlayer.Text = "Demote";
            this.demotePlayer.UseVisualStyleBackColor = true;
            this.demotePlayer.Click += new System.EventHandler(this.demotePlayer_Click);
            // 
            // promotePlayer
            // 
            this.promotePlayer.Location = new System.Drawing.Point(6, 250);
            this.promotePlayer.Name = "promotePlayer";
            this.promotePlayer.Size = new System.Drawing.Size(75, 23);
            this.promotePlayer.TabIndex = 3;
            this.promotePlayer.Text = "Promote";
            this.promotePlayer.UseVisualStyleBackColor = true;
            this.promotePlayer.Click += new System.EventHandler(this.promotePlayer_Click);
            // 
            // formationCombo
            // 
            this.formationCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.formationCombo.FormattingEnabled = true;
            this.formationCombo.Location = new System.Drawing.Point(3, 3);
            this.formationCombo.Name = "formationCombo";
            this.formationCombo.Size = new System.Drawing.Size(121, 21);
            this.formationCombo.TabIndex = 1;
            this.formationCombo.SelectedIndexChanged += new System.EventHandler(this.formationCombo_SelectedIndexChanged);
            // 
            // LineupTabControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.tabControl);
            this.Name = "LineupTabControl";
            this.Size = new System.Drawing.Size(781, 463);
            this.tabControl.ResumeLayout(false);
            this.lineupTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lineupGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage lineupTab;
        private System.Windows.Forms.Button demotePlayer;
        private System.Windows.Forms.Button promotePlayer;
        private System.Windows.Forms.ComboBox formationCombo;
        private System.Windows.Forms.DataGridView lineupGrid;

    }
}

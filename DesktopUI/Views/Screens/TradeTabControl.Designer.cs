﻿using System.Windows.Forms;

namespace DesktopUI.Views.Screens
{
    partial class TradeTabControl : UserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.capTabControl = new System.Windows.Forms.TabControl();
            this.tradeTab = new System.Windows.Forms.TabPage();
            this.team2PickTradeDGV = new System.Windows.Forms.DataGridView();
            this.team1PickTradeDGV = new System.Windows.Forms.DataGridView();
            this.team2DraftDGV = new System.Windows.Forms.DataGridView();
            this.team1DraftDGV = new System.Windows.Forms.DataGridView();
            this.lblOtherTeamActivePlayers = new System.Windows.Forms.Label();
            this.lblOtherTeamCap = new System.Windows.Forms.Label();
            this.team2PlayerTradeDGV = new System.Windows.Forms.DataGridView();
            this.team1PlayerTradeDGV = new System.Windows.Forms.DataGridView();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnTrade = new System.Windows.Forms.Button();
            this.team2RosterDGV = new System.Windows.Forms.DataGridView();
            this.lblCap = new System.Windows.Forms.Label();
            this.lblActivePlayers = new System.Windows.Forms.Label();
            this.team1RosterDGV = new System.Windows.Forms.DataGridView();
            this.teamCombo = new System.Windows.Forms.ComboBox();
            this.capTabControl.SuspendLayout();
            this.tradeTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.team2PickTradeDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.team1PickTradeDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.team2DraftDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.team1DraftDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.team2PlayerTradeDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.team1PlayerTradeDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.team2RosterDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.team1RosterDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // capTabControl
            // 
            this.capTabControl.Controls.Add(this.tradeTab);
            this.capTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.capTabControl.Location = new System.Drawing.Point(0, 0);
            this.capTabControl.Name = "capTabControl";
            this.capTabControl.SelectedIndex = 0;
            this.capTabControl.Size = new System.Drawing.Size(781, 580);
            this.capTabControl.TabIndex = 4;
            // 
            // tradeTab
            // 
            this.tradeTab.BackColor = System.Drawing.Color.AliceBlue;
            this.tradeTab.Controls.Add(this.team2PickTradeDGV);
            this.tradeTab.Controls.Add(this.team1PickTradeDGV);
            this.tradeTab.Controls.Add(this.team2DraftDGV);
            this.tradeTab.Controls.Add(this.team1DraftDGV);
            this.tradeTab.Controls.Add(this.lblOtherTeamActivePlayers);
            this.tradeTab.Controls.Add(this.lblOtherTeamCap);
            this.tradeTab.Controls.Add(this.team2PlayerTradeDGV);
            this.tradeTab.Controls.Add(this.team1PlayerTradeDGV);
            this.tradeTab.Controls.Add(this.btnClear);
            this.tradeTab.Controls.Add(this.btnTrade);
            this.tradeTab.Controls.Add(this.team2RosterDGV);
            this.tradeTab.Controls.Add(this.lblCap);
            this.tradeTab.Controls.Add(this.lblActivePlayers);
            this.tradeTab.Controls.Add(this.team1RosterDGV);
            this.tradeTab.Controls.Add(this.teamCombo);
            this.tradeTab.Location = new System.Drawing.Point(4, 22);
            this.tradeTab.Name = "tradeTab";
            this.tradeTab.Padding = new System.Windows.Forms.Padding(3);
            this.tradeTab.Size = new System.Drawing.Size(773, 554);
            this.tradeTab.TabIndex = 2;
            this.tradeTab.Text = "Trade";
            // 
            // team2PickTradeDGV
            // 
            this.team2PickTradeDGV.AllowUserToAddRows = false;
            this.team2PickTradeDGV.AllowUserToDeleteRows = false;
            this.team2PickTradeDGV.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.team2PickTradeDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.team2PickTradeDGV.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.team2PickTradeDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.team2PickTradeDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.team2PickTradeDGV.Location = new System.Drawing.Point(390, 356);
            this.team2PickTradeDGV.MultiSelect = false;
            this.team2PickTradeDGV.Name = "team2PickTradeDGV";
            this.team2PickTradeDGV.ReadOnly = true;
            this.team2PickTradeDGV.RowHeadersVisible = false;
            this.team2PickTradeDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.team2PickTradeDGV.ShowEditingIcon = false;
            this.team2PickTradeDGV.Size = new System.Drawing.Size(175, 140);
            this.team2PickTradeDGV.TabIndex = 20;
            this.team2PickTradeDGV.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.team2PickTradeDGV_CellDoubleClick);
            // 
            // team1PickTradeDGV
            // 
            this.team1PickTradeDGV.AllowUserToAddRows = false;
            this.team1PickTradeDGV.AllowUserToDeleteRows = false;
            this.team1PickTradeDGV.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.team1PickTradeDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.team1PickTradeDGV.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.team1PickTradeDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.team1PickTradeDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.team1PickTradeDGV.Location = new System.Drawing.Point(208, 356);
            this.team1PickTradeDGV.MultiSelect = false;
            this.team1PickTradeDGV.Name = "team1PickTradeDGV";
            this.team1PickTradeDGV.ReadOnly = true;
            this.team1PickTradeDGV.RowHeadersVisible = false;
            this.team1PickTradeDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.team1PickTradeDGV.ShowEditingIcon = false;
            this.team1PickTradeDGV.Size = new System.Drawing.Size(175, 140);
            this.team1PickTradeDGV.TabIndex = 19;
            this.team1PickTradeDGV.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.team1PickTradeDGV_CellDoubleClick);
            // 
            // team2DraftDGV
            // 
            this.team2DraftDGV.AllowUserToAddRows = false;
            this.team2DraftDGV.AllowUserToDeleteRows = false;
            this.team2DraftDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.team2DraftDGV.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.team2DraftDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.team2DraftDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.team2DraftDGV.Location = new System.Drawing.Point(570, 356);
            this.team2DraftDGV.MultiSelect = false;
            this.team2DraftDGV.Name = "team2DraftDGV";
            this.team2DraftDGV.ReadOnly = true;
            this.team2DraftDGV.RowHeadersVisible = false;
            this.team2DraftDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.team2DraftDGV.ShowEditingIcon = false;
            this.team2DraftDGV.Size = new System.Drawing.Size(190, 140);
            this.team2DraftDGV.TabIndex = 18;
            this.team2DraftDGV.DoubleClick += new System.EventHandler(this.team2DraftDGV_DoubleClick);
            // 
            // team1DraftDGV
            // 
            this.team1DraftDGV.AllowUserToAddRows = false;
            this.team1DraftDGV.AllowUserToDeleteRows = false;
            this.team1DraftDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.team1DraftDGV.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.team1DraftDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.team1DraftDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.team1DraftDGV.Location = new System.Drawing.Point(0, 356);
            this.team1DraftDGV.MultiSelect = false;
            this.team1DraftDGV.Name = "team1DraftDGV";
            this.team1DraftDGV.ReadOnly = true;
            this.team1DraftDGV.RowHeadersVisible = false;
            this.team1DraftDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.team1DraftDGV.ShowEditingIcon = false;
            this.team1DraftDGV.Size = new System.Drawing.Size(190, 140);
            this.team1DraftDGV.TabIndex = 17;
            this.team1DraftDGV.DoubleClick += new System.EventHandler(this.team1DraftDGV_DoubleClick);
            // 
            // lblOtherTeamActivePlayers
            // 
            this.lblOtherTeamActivePlayers.AutoSize = true;
            this.lblOtherTeamActivePlayers.Location = new System.Drawing.Point(570, 51);
            this.lblOtherTeamActivePlayers.Name = "lblOtherTeamActivePlayers";
            this.lblOtherTeamActivePlayers.Size = new System.Drawing.Size(0, 13);
            this.lblOtherTeamActivePlayers.TabIndex = 16;
            // 
            // lblOtherTeamCap
            // 
            this.lblOtherTeamCap.AutoSize = true;
            this.lblOtherTeamCap.Location = new System.Drawing.Point(570, 34);
            this.lblOtherTeamCap.Name = "lblOtherTeamCap";
            this.lblOtherTeamCap.Size = new System.Drawing.Size(0, 13);
            this.lblOtherTeamCap.TabIndex = 15;
            // 
            // team2PlayerTradeDGV
            // 
            this.team2PlayerTradeDGV.AllowUserToAddRows = false;
            this.team2PlayerTradeDGV.AllowUserToDeleteRows = false;
            this.team2PlayerTradeDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.team2PlayerTradeDGV.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.team2PlayerTradeDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.team2PlayerTradeDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.team2PlayerTradeDGV.Location = new System.Drawing.Point(390, 85);
            this.team2PlayerTradeDGV.Name = "team2PlayerTradeDGV";
            this.team2PlayerTradeDGV.ReadOnly = true;
            this.team2PlayerTradeDGV.RowHeadersVisible = false;
            this.team2PlayerTradeDGV.Size = new System.Drawing.Size(160, 275);
            this.team2PlayerTradeDGV.TabIndex = 14;
            this.team2PlayerTradeDGV.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.team2TradeDGV_CellDoubleClick);
            // 
            // team1PlayerTradeDGV
            // 
            this.team1PlayerTradeDGV.AllowUserToAddRows = false;
            this.team1PlayerTradeDGV.AllowUserToDeleteRows = false;
            this.team1PlayerTradeDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.team1PlayerTradeDGV.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.team1PlayerTradeDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.team1PlayerTradeDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.team1PlayerTradeDGV.Location = new System.Drawing.Point(208, 85);
            this.team1PlayerTradeDGV.Name = "team1PlayerTradeDGV";
            this.team1PlayerTradeDGV.ReadOnly = true;
            this.team1PlayerTradeDGV.RowHeadersVisible = false;
            this.team1PlayerTradeDGV.Size = new System.Drawing.Size(160, 275);
            this.team1PlayerTradeDGV.TabIndex = 13;
            this.team1PlayerTradeDGV.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.team1TradeDGV_CellDoubleClick);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(416, 8);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(88, 32);
            this.btnClear.TabIndex = 12;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnTrade
            // 
            this.btnTrade.Location = new System.Drawing.Point(264, 8);
            this.btnTrade.Name = "btnTrade";
            this.btnTrade.Size = new System.Drawing.Size(88, 32);
            this.btnTrade.TabIndex = 11;
            this.btnTrade.Text = "Trade";
            this.btnTrade.UseVisualStyleBackColor = true;
            this.btnTrade.Click += new System.EventHandler(this.btnTrade_Click);
            // 
            // team2RosterDGV
            // 
            this.team2RosterDGV.AllowUserToAddRows = false;
            this.team2RosterDGV.AllowUserToDeleteRows = false;
            this.team2RosterDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.team2RosterDGV.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.team2RosterDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.team2RosterDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.team2RosterDGV.Location = new System.Drawing.Point(570, 85);
            this.team2RosterDGV.Name = "team2RosterDGV";
            this.team2RosterDGV.ReadOnly = true;
            this.team2RosterDGV.RowHeadersVisible = false;
            this.team2RosterDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.team2RosterDGV.Size = new System.Drawing.Size(200, 260);
            this.team2RosterDGV.TabIndex = 8;
            this.team2RosterDGV.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.team2TradeDGV_RowsAdded);
            this.team2RosterDGV.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.team2TradeDGV_RowsRemoved);
            this.team2RosterDGV.DoubleClick += new System.EventHandler(this.team2RosterDGV_DoubleClick);
            // 
            // lblCap
            // 
            this.lblCap.AutoSize = true;
            this.lblCap.Location = new System.Drawing.Point(0, 34);
            this.lblCap.Name = "lblCap";
            this.lblCap.Size = new System.Drawing.Size(0, 13);
            this.lblCap.TabIndex = 6;
            // 
            // lblActivePlayers
            // 
            this.lblActivePlayers.AutoSize = true;
            this.lblActivePlayers.Location = new System.Drawing.Point(0, 51);
            this.lblActivePlayers.Name = "lblActivePlayers";
            this.lblActivePlayers.Size = new System.Drawing.Size(0, 13);
            this.lblActivePlayers.TabIndex = 7;
            // 
            // team1RosterDGV
            // 
            this.team1RosterDGV.AllowUserToAddRows = false;
            this.team1RosterDGV.AllowUserToDeleteRows = false;
            this.team1RosterDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.team1RosterDGV.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.team1RosterDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.team1RosterDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.team1RosterDGV.Location = new System.Drawing.Point(0, 85);
            this.team1RosterDGV.Name = "team1RosterDGV";
            this.team1RosterDGV.ReadOnly = true;
            this.team1RosterDGV.RowHeadersVisible = false;
            this.team1RosterDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.team1RosterDGV.Size = new System.Drawing.Size(200, 260);
            this.team1RosterDGV.TabIndex = 5;
            this.team1RosterDGV.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.team1TradeDGV_RowsAdded);
            this.team1RosterDGV.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.team1TradeDGV_RowsRemoved);
            this.team1RosterDGV.DoubleClick += new System.EventHandler(this.team1RosterDGV_DoubleClick);
            // 
            // teamCombo
            // 
            this.teamCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.teamCombo.FormattingEnabled = true;
            this.teamCombo.Location = new System.Drawing.Point(570, 7);
            this.teamCombo.Name = "teamCombo";
            this.teamCombo.Size = new System.Drawing.Size(121, 21);
            this.teamCombo.TabIndex = 1;
            // 
            // TradeTabControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.capTabControl);
            this.Name = "TradeTabControl";
            this.Size = new System.Drawing.Size(781, 580);
            this.capTabControl.ResumeLayout(false);
            this.tradeTab.ResumeLayout(false);
            this.tradeTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.team2PickTradeDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.team1PickTradeDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.team2DraftDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.team1DraftDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.team2PlayerTradeDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.team1PlayerTradeDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.team2RosterDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.team1RosterDGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl capTabControl;
        private System.Windows.Forms.TabPage tradeTab;
        private System.Windows.Forms.DataGridView team1RosterDGV;
        private System.Windows.Forms.ComboBox teamCombo;
        private Label lblCap;
        private Label lblActivePlayers;
        private DataGridView team2RosterDGV;
        private Button btnClear;
        private Button btnTrade;
        private DataGridView team1PlayerTradeDGV;
        private Label lblOtherTeamActivePlayers;
        private Label lblOtherTeamCap;
        private DataGridView team2DraftDGV;
        private DataGridView team1DraftDGV;
        private DataGridView team1PickTradeDGV;
        private DataGridView team2PickTradeDGV;
        private DataGridView team2PlayerTradeDGV;

    }
}

﻿namespace DesktopUI.Views.Screens
{
    partial class StatsTabControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.lineupTab = new System.Windows.Forms.TabPage();
            this.chooseTeamCombo = new System.Windows.Forms.ComboBox();
            this.statsGrid = new System.Windows.Forms.DataGridView();
            this.statTypeCombo = new System.Windows.Forms.ComboBox();
            this.tabControl.SuspendLayout();
            this.lineupTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.lineupTab);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(781, 463);
            this.tabControl.TabIndex = 3;
            // 
            // lineupTab
            // 
            this.lineupTab.BackColor = System.Drawing.Color.AliceBlue;
            this.lineupTab.Controls.Add(this.chooseTeamCombo);
            this.lineupTab.Controls.Add(this.statsGrid);
            this.lineupTab.Controls.Add(this.statTypeCombo);
            this.lineupTab.Location = new System.Drawing.Point(4, 22);
            this.lineupTab.Name = "lineupTab";
            this.lineupTab.Padding = new System.Windows.Forms.Padding(3);
            this.lineupTab.Size = new System.Drawing.Size(773, 437);
            this.lineupTab.TabIndex = 2;
            this.lineupTab.Text = "Stats";
            // 
            // chooseTeamCombo
            // 
            this.chooseTeamCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chooseTeamCombo.FormattingEnabled = true;
            this.chooseTeamCombo.Location = new System.Drawing.Point(6, 6);
            this.chooseTeamCombo.Name = "chooseTeamCombo";
            this.chooseTeamCombo.Size = new System.Drawing.Size(121, 21);
            this.chooseTeamCombo.TabIndex = 6;
            this.chooseTeamCombo.SelectedIndexChanged += new System.EventHandler(this.chooseTeamCombo_SelectedIndexChanged);
            // 
            // statsGrid
            // 
            this.statsGrid.AllowUserToAddRows = false;
            this.statsGrid.AllowUserToDeleteRows = false;
            this.statsGrid.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.statsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.statsGrid.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.statsGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.statsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.statsGrid.Location = new System.Drawing.Point(4, 30);
            this.statsGrid.Name = "statsGrid";
            this.statsGrid.ReadOnly = true;
            this.statsGrid.RowHeadersVisible = false;
            this.statsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.statsGrid.Size = new System.Drawing.Size(763, 401);
            this.statsGrid.TabIndex = 5;
            // 
            // statTypeCombo
            // 
            this.statTypeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.statTypeCombo.FormattingEnabled = true;
            this.statTypeCombo.Location = new System.Drawing.Point(145, 6);
            this.statTypeCombo.Name = "statTypeCombo";
            this.statTypeCombo.Size = new System.Drawing.Size(121, 21);
            this.statTypeCombo.TabIndex = 1;
            this.statTypeCombo.SelectedIndexChanged += new System.EventHandler(this.positionCombo_SelectedIndexChanged);
            // 
            // StatsTabControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.tabControl);
            this.Name = "StatsTabControl";
            this.Size = new System.Drawing.Size(781, 463);
            this.tabControl.ResumeLayout(false);
            this.lineupTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.statsGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage lineupTab;
        private System.Windows.Forms.DataGridView statsGrid;
        private System.Windows.Forms.ComboBox statTypeCombo;
        private System.Windows.Forms.ComboBox chooseTeamCombo;


    }
}

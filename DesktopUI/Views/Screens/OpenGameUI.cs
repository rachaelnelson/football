﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Football.FileIO.OpenGame;
using Football.Settings;
using Football.Sim.Games;
using Football.Sim.Penalties;
using Football.Sim.Situations;

namespace DesktopUI.Views.Screens
{
    public partial class OpenGameUI : Form
    {
        private FootballSettings setting;
        private PenaltyHandler penaltyHandler;
        private SituationHandler situationHandler;
        private GameStateHandler gameStateHandler;

        private OpenGame openGameObj;

        public OpenGameUI(FootballSettings setting, PenaltyHandler penaltyHandler,SituationHandler situationHandler, GameStateHandler gameStateHandler)
        {
            InitializeComponent();
            this.setting = setting;
            this.penaltyHandler = penaltyHandler;
            this.situationHandler = situationHandler;
            this.gameStateHandler = gameStateHandler;
            this.initList();
            this.openGameObj = new OpenGame(setting, penaltyHandler, situationHandler, gameStateHandler);
        }

        private void initList()
        {
            this.fileList.DataSource = this.getDirList();
        }

        private List<String> getDirList()
        {
            String[] dirs = Directory.GetDirectories(this.setting.appPath);

            List<String> nameList = new List<String>();

            foreach (var dir in dirs)
            {
                DirectoryInfo info = new DirectoryInfo(dir);
                nameList.Add(info.Name);
            }

            return nameList;
        }

        private void open_Click(object sender, EventArgs e)
        {
            String fileName = this.fileList.Text.Trim();

            if (fileName.Length > 0)
            {
                if (this.openGame(fileName))
                {
                    this.Close();
                    MessageBox.Show("Game Loaded!", "Success", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("Error loading game.", "Error", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("Please select a game to load.", "Error", MessageBoxButtons.OK);
            }                       
        }       

        private bool openGame(String fileName)
        {            
            try
            {
                return openGameObj.openGame(fileName);               
            }
            catch (Exception e)
            {
                throw new Exception(String.Format("Error loading game: {0}", e.Message)); 
            }      
        }       
    }
}

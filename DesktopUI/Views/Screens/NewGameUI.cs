﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Football.FileIO.NewGame;
using Football.League.Teams;
using Football.Settings;
using Football.Sim.Games;
using Football.Sim.Penalties;
using Football.Sim.Situations;


namespace DesktopUI.Views.Screens
{
    public partial class NewGameUI : Form
    {
        private FootballSettings setting;
        private PenaltyHandler penaltyHandler;
        private SituationHandler situationHandler;
        private GameStateHandler gameStateHandler;

        public NewGameUI(FootballSettings setting, PenaltyHandler penaltyHandler, SituationHandler situationHandler, GameStateHandler gameStateHandler)
        {            
            InitializeComponent();
            this.setting = setting;
            this.penaltyHandler = penaltyHandler;
            this.situationHandler = situationHandler;
            this.gameStateHandler = gameStateHandler;
        }

        private void chooseTeamBtn_Click(object sender, EventArgs e)
        {
            String selectedTeam = chooseTeamCombo.SelectedValue.ToString();
            this.createNewGame(selectedTeam);
            this.Close();
        }

        private void createNewGame(String selectedTeam)
        {
            this.setting.myTeam = selectedTeam;                   
        }

        private bool init()
        {
            NewGame newGame = new NewGame(setting,penaltyHandler,situationHandler,gameStateHandler);
            bool result = false;
            if( newGame.initNewGame() )
            {
                result = true;
            }
            else
            {
               MessageBox.Show("Error loading game", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);                                 
            }
            return result;
        }

        public void newGame()
        {
            if (this.init())
            {
                this.populateTeamSelectCombo();   
                //this.Show();
            }            
        }

        /*
         *   Populate select box
         */ 
        private void populateTeamSelectCombo()
        {            
            Dictionary<string, Team> teams = setting.getTeams();
            
            try
            {                
                Dictionary<string,string> tList = new Dictionary<string,string>();
                foreach (string key in teams.Keys)
                {                                  
                    tList.Add(key,teams[key].Name);
                }      
                // binds dictionary of strings since you can't bind dict with objects nicely...
                chooseTeamCombo.DataSource = new BindingSource(tList, null);               
                chooseTeamCombo.DisplayMember = "Value";
                chooseTeamCombo.ValueMember = "Key";
                chooseTeamCombo.SelectedIndex = 0;
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.StackTrace);
            }
        }                
    }
}

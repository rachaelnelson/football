﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Football.League.Players;
using Football.Settings;
using Football.UiClasses.Presenter;
using UiClasses.Lineup;

namespace DesktopUI.Views.Screens
{
    public partial class LineupTabControl : UserControl
    {
        private LineupPresenter presenter;
        private BindingSource lbs;
        private Dictionary<BaseAll,UserControl> formationPanelDict;

        public LineupTabControl(LineupPresenter presenter)
        {
            InitializeComponent();

            this.presenter = presenter;

            // binding source needed for selection to work properly after promote/demote
            lbs = new BindingSource();
            lbs.DataSource = presenter.ViewModel.LineupData;
            formationCombo.DataSource = presenter.ViewModel.Formations;
            lineupGrid.DataSource = lbs;                                    
            
            initFormationPanelDict();
            setPictureBoxHandler();
            setControls();
            disableSorting();           
           
        }        

        private void formationCombo_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            presenter.FormationChanged(formationCombo.SelectedValue);
            this.setControls();
            lineupGrid.Columns["Id"].Visible = false; // hide id column                    
        }

        private void setControls()
        {            
            setHighlight(null, SystemColors.Control);
            displayFormationPanel();
            promotePlayer.Hide();
            demotePlayer.Hide();
            lineupGrid.Hide();
        }

        private void promotePlayer_Click(object sender, EventArgs e)
        {           
            if(lineupGrid.SelectedRows!=null)
            {             
                presenter.Promote(lineupGrid.SelectedRows[0].Cells[0].Value);
                lbs.ResetBindings(false);
                lbs.Position = presenter.ViewModel.RowSelection;                
            }            
        }

        private void demotePlayer_Click(object sender, EventArgs e)
        {            
            if (lineupGrid.SelectedRows != null)
            {             
                presenter.Demote(lineupGrid.SelectedRows[0].Cells[0].Value);
                lbs.ResetBindings(false);
                lbs.Position = presenter.ViewModel.RowSelection;                
            }            
        }

        private void updateRowPosition()
        {
            int newPosition = presenter.ViewModel.RowSelection;
            lineupGrid.ClearSelection();
            lineupGrid.Rows[newPosition].Selected = true;
        }

        private void disableSorting()
        {
            for (int i = 0; i < lineupGrid.Columns.Count; i++)
            {
                lineupGrid.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

        private void initFormationPanelDict()
        {
            formationPanelDict = new Dictionary<BaseAll, UserControl>()
            {
                {BaseAll.TwoRB, new TwoRBPanel()},
                {BaseAll.TwoTE,new TwoTEPanel()},
                {BaseAll.ThreeWR,new ThreeWRPanel()},
                {BaseAll.FourThree, new FourThreePanel()}, 
                {BaseAll.ThreeFour, new ThreeFourPanel()}, 
                {BaseAll.KP, new KPPanel()}
            };   
        }

        
        //   Adds handler to picture boxes          
        private void setPictureBoxHandler()
        {            
            foreach (var item in this.formationPanelDict)
            {
                foreach (var ctl in item.Value.Controls[0].Controls.OfType<PictureBox>())
                {
                    (ctl as PictureBox).BorderStyle = BorderStyle.None;
                    (ctl as PictureBox).SizeMode = PictureBoxSizeMode.AutoSize;
                    (ctl as PictureBox).Padding = new Padding(1);
                    (ctl as PictureBox).Click += new System.EventHandler(this.pictureBoxHandler);                    
                }
            }
        }

        
       //    Handles picture box clicks         
        protected void pictureBoxHandler(object sender, EventArgs e)
        {
            PictureBox icon = (PictureBox)sender;
            PlayerType iconType = PlayerType.GetPlayerTypeByString(icon.Tag.ToString());
            setHighlight(icon.Tag.ToString(),Color.White);

            presenter.PositionChanged(iconType);

            promotePlayer.Show();
            demotePlayer.Show();
            lineupGrid.Show();

            lineupGrid.Columns["Id"].Visible = false; // hide id column
        }

        
        //   Sets background highlight          
        private void setHighlight(String tag, Color color)
        {
            BaseAll formation = (BaseAll)formationCombo.SelectedValue;            
            foreach(var ctl in formationPanelDict[formation].Controls[0].Controls.OfType<PictureBox>()) 
            {
                PictureBox picBox = (ctl as PictureBox);                
                picBox.BackColor = (picBox.Tag.Equals(tag)) ? color : SystemColors.Control;
                picBox.BorderStyle = (picBox.Tag.Equals(tag)) ? BorderStyle.FixedSingle : BorderStyle.None;                
            }            
        }        
        
        //    Show panel          
        private void displayFormationPanel()
        {
            BaseAll selected = presenter.ViewModel.SelectedFormation;

            int x = 1000;
            Point location = new Point(3,31);
            TabPage parent = TabPage.GetTabPageOfComponent(formationCombo);

            foreach (var item in formationPanelDict)
            {
                item.Value.Hide();
                item.Value.Visible = false;
                item.Value.Location = new Point(x,x);
                x = x + 100;
            }
            formationPanelDict[selected].Location = location;                                    
            formationPanelDict[selected].Visible = true;
            formationPanelDict[selected].Parent = parent;            
        }       
    }
}

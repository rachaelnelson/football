﻿using System.Windows.Forms;

using Football.UiClasses.PlayerViewer;

namespace DesktopUI.Views.Screens
{
    public partial class PlayerView : Form
    {       
        public PlayerView(PlayerViewModel playerViewModel)
        {
            InitializeComponent();
            dgvPersonal.DataSource = playerViewModel.GetPlayerInfo();
            dgvAttributes.DataSource = playerViewModel.GetPlayerAttributes();
            dgvStats.DataSource = playerViewModel.GetPlayerStats();
            dgvSalary.DataSource = playerViewModel.GetPlayerSalary();

            HideColumns.HideEmptyAttributes(dgvAttributes);           
            HideStatsColumns(dgvStats);
        }

        private void HideStatsColumns(DataGridView grid)
        {
            if (grid.ColumnCount > 0)
            {
                grid.Columns["Id"].Visible = false;
                grid.Columns["PlayerHash"].Visible = false;
                grid.Columns["Team"].Visible = false;
                grid.Columns["PlayerFirstName"].Visible = false;
                grid.Columns["PlayerLastName"].Visible = false;
                grid.Show();
            }
        }

        public void Display()
        {            
            Show();
        }
    }
}

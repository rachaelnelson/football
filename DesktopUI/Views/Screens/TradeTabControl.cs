﻿using System;
using System.Windows.Forms;

using Football.UiClasses.Presenter;

namespace DesktopUI.Views.Screens
{
    public partial class TradeTabControl : UserControl
    {
        private TradePresenter presenter;         

        public TradeTabControl(TradePresenter presenter)
        {
            InitializeComponent();

            teamCombo.SelectedIndexChanged += new System.EventHandler(teamCombo_SelectedIndexChanged);       

            this.presenter = presenter;            

            teamCombo.DataSource = presenter.ViewModel.Teams;

            team1RosterDGV.DataSource = presenter.ViewModel.Team1RosterData;
            team2RosterDGV.DataSource = presenter.ViewModel.Team2RosterData;

            team1PlayerTradeDGV.DataSource = presenter.ViewModel.Team1PlayerTradeData;           
            team2PlayerTradeDGV.DataSource = presenter.ViewModel.Team2PlayerTradeData;

            team1PickTradeDGV.DataSource = presenter.ViewModel.Team1PickTradeData;
            team2PickTradeDGV.DataSource = presenter.ViewModel.Team2PickTradeData;
        
            team1DraftDGV.DataSource = presenter.ViewModel.Team1DraftData;
            team2DraftDGV.DataSource = presenter.ViewModel.Team2DraftData;

            HidePlayerColumns();
            
            ToggleTradeButton();

            setCapSpaceLabel();
            setCountsLabel();
        }

        // load selected team (don't show current team)

        // Position, name, age, base value 
        private void teamCombo_SelectedIndexChanged(object sender, System.EventArgs e)
        {            
            presenter.TeamChanged(teamCombo.SelectedValue);
            presenter.ClearTrade();

            HidePlayerColumns();                   
        }

        private void UpdateSpaceAndCounts()
        {
            setCapSpaceLabel();
            setCountsLabel();       
        }

        private void HidePlayerColumns()
        {
            team1RosterDGV.Columns["Id"].Visible = false;            
            team2RosterDGV.Columns["Id"].Visible = false;

            team1DraftDGV.Columns["Id"].Visible = false;
            team2DraftDGV.Columns["Id"].Visible = false;

            team1PlayerTradeDGV.Columns["Id"].Visible = false;
            team2PlayerTradeDGV.Columns["Id"].Visible = false;

            team1PickTradeDGV.Columns["Id"].Visible = false;
            team2PickTradeDGV.Columns["Id"].Visible = false;            
        }

        private void team1DraftDGV_DoubleClick(object sender, EventArgs e)
        {
            presenter.AddPickToTrade(team1DraftDGV.CurrentRow.Cells[0].Value);
            UpdateSpaceAndCounts();
            ToggleTradeButton();
        }

        private void team2DraftDGV_DoubleClick(object sender, EventArgs e)
        {
            presenter.AddPickToTrade(team2DraftDGV.CurrentRow.Cells[0].Value, teamCombo.SelectedValue);
            UpdateSpaceAndCounts();
            ToggleTradeButton();    
        }

        private void team1RosterDGV_DoubleClick(object sender, EventArgs e)
        {
            presenter.AddToTrade(team1RosterDGV.CurrentRow.Cells[0].Value);            
            UpdateSpaceAndCounts();
            ToggleTradeButton();
        }

        private void team2RosterDGV_DoubleClick(object sender, EventArgs e)
        {
            presenter.AddToTrade(team2RosterDGV.CurrentRow.Cells[0].Value, teamCombo.SelectedValue);
            UpdateSpaceAndCounts();
            ToggleTradeButton();
        }


        private void btnClear_Click(object sender, EventArgs e)
        {            
            presenter.ClearTrade();
            UpdateSpaceAndCounts();            
        }

        private void btnTrade_Click(object sender, EventArgs e)
        {            
            presenter.Trade();            
            UpdateSpaceAndCounts();
        }                                          

        private void team1TradeDGV_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {            
            UpdateSpaceAndCounts();            
            ToggleTradeButton();              
        }

        private void team1TradeDGV_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            UpdateSpaceAndCounts();
            ToggleTradeButton();   
        }

        private void team2TradeDGV_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            UpdateSpaceAndCounts();
            ToggleTradeButton();   
        }

        private void team2TradeDGV_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            UpdateSpaceAndCounts();
            ToggleTradeButton();   
        }

        private void ToggleTradeButton()
        {    
            if (presenter.IsTradeValid())
            {
                btnTrade.Enabled = true;
            }
            else
            {
                btnTrade.Enabled = false;
            }  
        }       

        private void team1TradeDGV_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            presenter.RemoveFromTrade(team1PlayerTradeDGV.CurrentRow.Cells[0].Value);
            UpdateSpaceAndCounts();
            ToggleTradeButton();
        }

        private void team2TradeDGV_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            presenter.RemoveFromTrade(team2PlayerTradeDGV.CurrentRow.Cells[0].Value, teamCombo.SelectedValue);
            UpdateSpaceAndCounts();
            ToggleTradeButton();   
        }

        private void team1PickTradeDGV_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            presenter.RemovePickFromTrade(team1PickTradeDGV.CurrentRow.Cells[0].Value);
            UpdateSpaceAndCounts();
            ToggleTradeButton();
        }

        private void team2PickTradeDGV_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            presenter.RemovePickFromTrade(team2PickTradeDGV.CurrentRow.Cells[0].Value, teamCombo.SelectedValue);
            UpdateSpaceAndCounts();
            ToggleTradeButton();
        }    

        private void setCapSpaceLabel()
        {
            this.lblCap.Text = String.Format("Cap Space: ${0}", this.presenter.ViewModel.Team1CapSpace.ToString("0,0"));
            this.lblOtherTeamCap.Text = String.Format("Cap Space: ${0}", this.presenter.ViewModel.Team2CapSpace.ToString("0,0"));
        }

        private void setCountsLabel()
        {
            this.lblActivePlayers.Text = String.Format("Active Players: {0}", presenter.ViewModel.Team1Counts);
            this.lblOtherTeamActivePlayers.Text = String.Format("Active Players: {0}", presenter.ViewModel.Team2Counts);
        }                        
    }
}

﻿namespace DesktopUI.Views.Screens
{
    partial class SignPlayerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.lblSalary = new System.Windows.Forms.Label();
            this.nudYears = new System.Windows.Forms.NumericUpDown();
            this.btnSign = new System.Windows.Forms.Button();
            this.lblYears = new System.Windows.Forms.Label();
            this.lblSalaryLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudYears)).BeginInit();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(13, 13);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(0, 13);
            this.lblName.TabIndex = 0;
            // 
            // lblSalary
            // 
            this.lblSalary.AutoSize = true;
            this.lblSalary.Location = new System.Drawing.Point(111, 44);
            this.lblSalary.Name = "lblSalary";
            this.lblSalary.Size = new System.Drawing.Size(0, 13);
            this.lblSalary.TabIndex = 1;
            // 
            // nudYears
            // 
            this.nudYears.Location = new System.Drawing.Point(114, 73);
            this.nudYears.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudYears.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudYears.Name = "nudYears";
            this.nudYears.ReadOnly = true;
            this.nudYears.Size = new System.Drawing.Size(40, 20);
            this.nudYears.TabIndex = 2;
            this.nudYears.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnSign
            // 
            this.btnSign.Location = new System.Drawing.Point(54, 118);
            this.btnSign.Name = "btnSign";
            this.btnSign.Size = new System.Drawing.Size(75, 23);
            this.btnSign.TabIndex = 3;
            this.btnSign.Text = "Submit Offer";
            this.btnSign.UseVisualStyleBackColor = true;
            // 
            // lblYears
            // 
            this.lblYears.AutoSize = true;
            this.lblYears.Location = new System.Drawing.Point(16, 73);
            this.lblYears.Name = "lblYears";
            this.lblYears.Size = new System.Drawing.Size(83, 13);
            this.lblYears.TabIndex = 4;
            this.lblYears.Text = "Contract Length";
            // 
            // lblSalaryLabel
            // 
            this.lblSalaryLabel.AutoSize = true;
            this.lblSalaryLabel.Location = new System.Drawing.Point(13, 44);
            this.lblSalaryLabel.Name = "lblSalaryLabel";
            this.lblSalaryLabel.Size = new System.Drawing.Size(68, 13);
            this.lblSalaryLabel.TabIndex = 5;
            this.lblSalaryLabel.Text = "Yearly Salary";
            // 
            // SignPlayerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(247, 169);
            this.Controls.Add(this.lblSalaryLabel);
            this.Controls.Add(this.lblYears);
            this.Controls.Add(this.btnSign);
            this.Controls.Add(this.nudYears);
            this.Controls.Add(this.lblSalary);
            this.Controls.Add(this.lblName);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SignPlayerForm";
            this.Text = "SignPlayer";
            ((System.ComponentModel.ISupportInitialize)(this.nudYears)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblSalary;
        private System.Windows.Forms.NumericUpDown nudYears;
        private System.Windows.Forms.Button btnSign;
        private System.Windows.Forms.Label lblYears;
        private System.Windows.Forms.Label lblSalaryLabel;
    }
}
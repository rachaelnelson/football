﻿namespace DesktopUI.Views.Screens
{
    partial class SaveGameUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileList = new System.Windows.Forms.ListBox();
            this.save = new System.Windows.Forms.Button();
            this.AddFolderBtn = new System.Windows.Forms.Button();
            this.gameNameTxt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // fileList
            // 
            this.fileList.FormattingEnabled = true;
            this.fileList.Location = new System.Drawing.Point(13, 13);
            this.fileList.Name = "fileList";
            this.fileList.Size = new System.Drawing.Size(201, 95);
            this.fileList.TabIndex = 0;
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(71, 189);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(75, 23);
            this.save.TabIndex = 1;
            this.save.Text = "Save Game";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // AddFolderBtn
            // 
            this.AddFolderBtn.Location = new System.Drawing.Point(119, 122);
            this.AddFolderBtn.Name = "AddFolderBtn";
            this.AddFolderBtn.Size = new System.Drawing.Size(75, 23);
            this.AddFolderBtn.TabIndex = 2;
            this.AddFolderBtn.Text = "Add";
            this.AddFolderBtn.UseVisualStyleBackColor = true;
            this.AddFolderBtn.Click += new System.EventHandler(this.AddFolderBtn_Click);
            // 
            // gameNameTxt
            // 
            this.gameNameTxt.Location = new System.Drawing.Point(13, 124);
            this.gameNameTxt.Name = "gameNameTxt";
            this.gameNameTxt.Size = new System.Drawing.Size(100, 20);
            this.gameNameTxt.TabIndex = 3;
            // 
            // SaveGameUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(228, 226);
            this.Controls.Add(this.gameNameTxt);
            this.Controls.Add(this.AddFolderBtn);
            this.Controls.Add(this.save);
            this.Controls.Add(this.fileList);
            this.Name = "SaveGameUI";
            this.Text = "SaveGame";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox fileList;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Button AddFolderBtn;
        private System.Windows.Forms.TextBox gameNameTxt;
    }
}
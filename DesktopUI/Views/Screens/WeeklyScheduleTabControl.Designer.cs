﻿using System.Windows.Forms;

namespace DesktopUI.Views.Screens
{
    public partial class WeeklyScheduleTabControl : UserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WeeklyScheduleTabControl));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.weekScheduleTab = new System.Windows.Forms.TabPage();
            this.weekTextLabel = new System.Windows.Forms.Label();
            this.weekLabel = new System.Windows.Forms.Label();
            this.weekArrowLeft = new System.Windows.Forms.PictureBox();
            this.weekArrowRight = new System.Windows.Forms.PictureBox();
            this.weekScheduleGrid = new System.Windows.Forms.DataGridView();
            this.simWeekBtn = new System.Windows.Forms.Button();
            this.tabControl.SuspendLayout();
            this.weekScheduleTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weekArrowLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weekArrowRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weekScheduleGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.weekScheduleTab);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(781, 463);
            this.tabControl.TabIndex = 4;
            // 
            // weekScheduleTab
            // 
            this.weekScheduleTab.BackColor = System.Drawing.Color.AliceBlue;
            this.weekScheduleTab.Controls.Add(this.weekTextLabel);
            this.weekScheduleTab.Controls.Add(this.weekLabel);
            this.weekScheduleTab.Controls.Add(this.weekArrowLeft);
            this.weekScheduleTab.Controls.Add(this.weekArrowRight);
            this.weekScheduleTab.Controls.Add(this.weekScheduleGrid);
            this.weekScheduleTab.Controls.Add(this.simWeekBtn);
            this.weekScheduleTab.Location = new System.Drawing.Point(4, 22);
            this.weekScheduleTab.Name = "weekScheduleTab";
            this.weekScheduleTab.Padding = new System.Windows.Forms.Padding(3);
            this.weekScheduleTab.Size = new System.Drawing.Size(773, 437);
            this.weekScheduleTab.TabIndex = 2;
            this.weekScheduleTab.Text = "Weekly Schedule";
            // 
            // weekTextLabel
            // 
            this.weekTextLabel.AutoSize = true;
            this.weekTextLabel.Location = new System.Drawing.Point(3, 10);
            this.weekTextLabel.Name = "weekTextLabel";
            this.weekTextLabel.Size = new System.Drawing.Size(42, 13);
            this.weekTextLabel.TabIndex = 10;
            this.weekTextLabel.Text = "Week: ";
            // 
            // weekLabel
            // 
            this.weekLabel.AutoSize = true;
            this.weekLabel.Location = new System.Drawing.Point(51, 10);
            this.weekLabel.Name = "weekLabel";
            this.weekLabel.Size = new System.Drawing.Size(13, 13);
            this.weekLabel.TabIndex = 9;
            this.weekLabel.Text = "1";
            // 
            // weekArrowLeft
            // 
            this.weekArrowLeft.Image = ((System.Drawing.Image)(resources.GetObject("weekArrowLeft.Image")));
            this.weekArrowLeft.Location = new System.Drawing.Point(71, 0);
            this.weekArrowLeft.Name = "weekArrowLeft";
            this.weekArrowLeft.Size = new System.Drawing.Size(32, 32);
            this.weekArrowLeft.TabIndex = 6;
            this.weekArrowLeft.TabStop = false;
            this.weekArrowLeft.Click += new System.EventHandler(this.weekArrowLeft_Click);
            // 
            // weekArrowRight
            // 
            this.weekArrowRight.Image = ((System.Drawing.Image)(resources.GetObject("weekArrowRight.Image")));
            this.weekArrowRight.Location = new System.Drawing.Point(109, 0);
            this.weekArrowRight.Name = "weekArrowRight";
            this.weekArrowRight.Size = new System.Drawing.Size(32, 32);
            this.weekArrowRight.TabIndex = 5;
            this.weekArrowRight.TabStop = false;
            this.weekArrowRight.Click += new System.EventHandler(this.weekArrowRight_Click);
            // 
            // weekScheduleGrid
            // 
            this.weekScheduleGrid.AllowUserToAddRows = false;
            this.weekScheduleGrid.AllowUserToDeleteRows = false;
            this.weekScheduleGrid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.weekScheduleGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.weekScheduleGrid.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.weekScheduleGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.weekScheduleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.weekScheduleGrid.Location = new System.Drawing.Point(6, 38);
            this.weekScheduleGrid.MultiSelect = false;
            this.weekScheduleGrid.Name = "weekScheduleGrid";
            this.weekScheduleGrid.ReadOnly = true;
            this.weekScheduleGrid.RowHeadersVisible = false;
            this.weekScheduleGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.weekScheduleGrid.Size = new System.Drawing.Size(781, 455);
            this.weekScheduleGrid.TabIndex = 4;
            this.weekScheduleGrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.weekScheduleGrid_CellDoubleClick);
            // 
            // simWeekBtn
            // 
            this.simWeekBtn.Enabled = false;
            this.simWeekBtn.Location = new System.Drawing.Point(157, 0);
            this.simWeekBtn.Name = "simWeekBtn";
            this.simWeekBtn.Size = new System.Drawing.Size(75, 23);
            this.simWeekBtn.TabIndex = 3;
            this.simWeekBtn.Text = "Sim Week";
            this.simWeekBtn.UseVisualStyleBackColor = true;
            this.simWeekBtn.Click += new System.EventHandler(this.simWeekBtn_Click);
            // 
            // WeeklyScheduleTabControl
            // 
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.tabControl);
            this.Name = "WeeklyScheduleTabControl";
            this.Size = new System.Drawing.Size(781, 463);
            this.tabControl.ResumeLayout(false);
            this.weekScheduleTab.ResumeLayout(false);
            this.weekScheduleTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weekArrowLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weekArrowRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weekScheduleGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl;
        private TabPage weekScheduleTab;        
        private Button simWeekBtn;
        private DataGridView weekScheduleGrid;
        private PictureBox weekArrowRight;
        private PictureBox weekArrowLeft;
        private Label weekLabel;
        private Label weekTextLabel;
    }
}
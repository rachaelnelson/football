﻿using System.Windows.Forms;

namespace DesktopUI.Views.Screens
{
    public partial class TeamScheduleTabControl : UserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage = new System.Windows.Forms.TabPage();
            this.simWeekBtn = new System.Windows.Forms.Button();
            this.teamScheduleGrid = new System.Windows.Forms.DataGridView();
            this.teamCombo = new System.Windows.Forms.ComboBox();
            this.settingsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabControl.SuspendLayout();
            this.tabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teamScheduleGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(781, 463);
            this.tabControl.TabIndex = 3;
            // 
            // tabPage
            // 
            this.tabPage.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage.Controls.Add(this.simWeekBtn);
            this.tabPage.Controls.Add(this.teamScheduleGrid);
            this.tabPage.Controls.Add(this.teamCombo);
            this.tabPage.Location = new System.Drawing.Point(4, 22);
            this.tabPage.Name = "tabPage";
            this.tabPage.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage.Size = new System.Drawing.Size(773, 437);
            this.tabPage.TabIndex = 2;
            this.tabPage.Text = "Team Schedule";
            // 
            // simWeekBtn
            // 
            this.simWeekBtn.Enabled = false;
            this.simWeekBtn.Location = new System.Drawing.Point(144, 3);
            this.simWeekBtn.Name = "simWeekBtn";
            this.simWeekBtn.Size = new System.Drawing.Size(75, 23);
            this.simWeekBtn.TabIndex = 4;
            this.simWeekBtn.Text = "Sim Week";
            this.simWeekBtn.UseVisualStyleBackColor = true;
            this.simWeekBtn.Click += new System.EventHandler(this.simWeekBtn_Click);
            // 
            // teamScheduleGrid
            // 
            this.teamScheduleGrid.AllowUserToAddRows = false;
            this.teamScheduleGrid.AllowUserToDeleteRows = false;
            this.teamScheduleGrid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.teamScheduleGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.teamScheduleGrid.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.teamScheduleGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.teamScheduleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.teamScheduleGrid.Location = new System.Drawing.Point(3, 3);
            this.teamScheduleGrid.MultiSelect = false;
            this.teamScheduleGrid.Name = "teamScheduleGrid";
            this.teamScheduleGrid.ReadOnly = true;
            this.teamScheduleGrid.RowHeadersVisible = false;
            this.teamScheduleGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.teamScheduleGrid.Size = new System.Drawing.Size(767, 431);
            this.teamScheduleGrid.TabIndex = 2;
            this.teamScheduleGrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.teamScheduleGrid_CellDoubleClick);
            // 
            // teamCombo
            // 
            this.teamCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.teamCombo.FormattingEnabled = true;
            this.teamCombo.Location = new System.Drawing.Point(3, 3);
            this.teamCombo.Name = "teamCombo";
            this.teamCombo.Size = new System.Drawing.Size(121, 21);
            this.teamCombo.TabIndex = 1;
            this.teamCombo.SelectedIndexChanged += new System.EventHandler(this.teamCombo_SelectedIndexChanged);
            // 
            // settingsBindingSource
            // 
            this.settingsBindingSource.DataSource = typeof(Football.Settings.FootballSettings);
            // 
            // TeamScheduleTabControl
            // 
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.tabControl);
            this.Name = "TeamScheduleTabControl";
            this.Size = new System.Drawing.Size(781, 463);
            this.tabControl.ResumeLayout(false);
            this.tabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teamScheduleGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        public TabControl tabControl;
        public TabPage tabPage;
        private ComboBox teamCombo;        
        private DataGridView teamScheduleGrid;
        private BindingSource settingsBindingSource;
        private Button simWeekBtn;        
    }
}

﻿using System.Windows.Forms;

namespace DesktopUI.Views.Screens
{
    public partial class PlayoffViewControl : UserControl
    {
        private TabControl tabControl;
        private TabPage playoffsTab;            

        public PlayoffViewControl()
        {
           InitializeComponent();                           
        }
        
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlayoffViewControl));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.playoffsTab = new System.Windows.Forms.TabPage();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.playoffsTab);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(781, 463);
            this.tabControl.TabIndex = 3;
            // 
            // playoffsTab
            // 
            this.playoffsTab.BackColor = System.Drawing.Color.AliceBlue;
            this.playoffsTab.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("playoffsTab.BackgroundImage")));
            this.playoffsTab.Location = new System.Drawing.Point(4, 22);
            this.playoffsTab.Name = "playoffsTab";
            this.playoffsTab.Padding = new System.Windows.Forms.Padding(3);
            this.playoffsTab.Size = new System.Drawing.Size(773, 437);
            this.playoffsTab.TabIndex = 2;
            this.playoffsTab.Text = "Playoffs";
            // 
            // PlayoffViewControl
            // 
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.tabControl);
            this.Name = "PlayoffViewControl";
            this.Size = new System.Drawing.Size(781, 463);
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }    
    }
}

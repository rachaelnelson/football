﻿namespace DesktopUI.Views.Screens
{
    partial class NewGameUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.chooseTeamCombo = new System.Windows.Forms.ComboBox();
            this.chooseTeamBtn = new System.Windows.Forms.Button();
            this.teamListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.teamListBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // chooseTeamCombo
            // 
            this.chooseTeamCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chooseTeamCombo.FormattingEnabled = true;
            this.chooseTeamCombo.Location = new System.Drawing.Point(13, 13);
            this.chooseTeamCombo.Name = "chooseTeamCombo";
            this.chooseTeamCombo.Size = new System.Drawing.Size(121, 21);
            this.chooseTeamCombo.TabIndex = 0;
            // 
            // chooseTeamBtn
            // 
            this.chooseTeamBtn.Location = new System.Drawing.Point(29, 58);
            this.chooseTeamBtn.Name = "chooseTeamBtn";
            this.chooseTeamBtn.Size = new System.Drawing.Size(75, 23);
            this.chooseTeamBtn.TabIndex = 1;
            this.chooseTeamBtn.Text = "Play";
            this.chooseTeamBtn.UseVisualStyleBackColor = true;
            this.chooseTeamBtn.Click += new System.EventHandler(this.chooseTeamBtn_Click);
            // 
            // teamListBindingSource
            // 
            this.teamListBindingSource.DataSource = typeof(Football.League.Teams.TeamList);
            // 
            // NewGameUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(151, 121);
            this.Controls.Add(this.chooseTeamBtn);
            this.Controls.Add(this.chooseTeamCombo);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewGameUI";
            this.Text = "Choose Team";
            ((System.ComponentModel.ISupportInitialize)(this.teamListBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox chooseTeamCombo;
        private System.Windows.Forms.Button chooseTeamBtn;
        private System.Windows.Forms.BindingSource teamListBindingSource;
    }
}
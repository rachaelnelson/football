﻿using System.Windows.Forms;

namespace DesktopUI.Views.Screens
{
    partial class RosterManagementTabControl : UserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.capTabControl = new System.Windows.Forms.TabControl();
            this.lineupTab = new System.Windows.Forms.TabPage();
            this.btnInActive = new System.Windows.Forms.Button();
            this.lblCap = new System.Windows.Forms.Label();
            this.lblActivePlayers = new System.Windows.Forms.Label();
            this.rosterGrid = new System.Windows.Forms.DataGridView();
            this.releasePlayer = new System.Windows.Forms.Button();
            this.positionCombo = new System.Windows.Forms.ComboBox();
            this.capTabControl.SuspendLayout();
            this.lineupTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rosterGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // capTabControl
            // 
            this.capTabControl.Controls.Add(this.lineupTab);
            this.capTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.capTabControl.Location = new System.Drawing.Point(0, 0);
            this.capTabControl.Name = "capTabControl";
            this.capTabControl.SelectedIndex = 0;
            this.capTabControl.Size = new System.Drawing.Size(781, 463);
            this.capTabControl.TabIndex = 4;
            // 
            // lineupTab
            // 
            this.lineupTab.BackColor = System.Drawing.Color.AliceBlue;
            this.lineupTab.Controls.Add(this.btnInActive);
            this.lineupTab.Controls.Add(this.lblCap);
            this.lineupTab.Controls.Add(this.lblActivePlayers);
            this.lineupTab.Controls.Add(this.rosterGrid);
            this.lineupTab.Controls.Add(this.releasePlayer);
            this.lineupTab.Controls.Add(this.positionCombo);
            this.lineupTab.Location = new System.Drawing.Point(4, 22);
            this.lineupTab.Name = "lineupTab";
            this.lineupTab.Padding = new System.Windows.Forms.Padding(3);
            this.lineupTab.Size = new System.Drawing.Size(773, 437);
            this.lineupTab.TabIndex = 2;
            this.lineupTab.Text = "Roster Management";
            // 
            // btnInActive
            // 
            this.btnInActive.Location = new System.Drawing.Point(130, 32);
            this.btnInActive.Name = "btnInActive";
            this.btnInActive.Size = new System.Drawing.Size(92, 23);
            this.btnInActive.TabIndex = 8;
            this.btnInActive.Text = "Active/Inactive";
            this.btnInActive.UseVisualStyleBackColor = true;
            this.btnInActive.Click += new System.EventHandler(this.btnInActive_Click);
            // 
            // lblCap
            // 
            this.lblCap.AutoSize = true;
            this.lblCap.Location = new System.Drawing.Point(248, 7);
            this.lblCap.Name = "lblCap";
            this.lblCap.Size = new System.Drawing.Size(0, 13);
            this.lblCap.TabIndex = 6;
            // 
            // lblActivePlayers
            // 
            this.lblActivePlayers.AutoSize = true;
            this.lblActivePlayers.Location = new System.Drawing.Point(248, 37);
            this.lblActivePlayers.Name = "lblActivePlayers";
            this.lblActivePlayers.Size = new System.Drawing.Size(0, 13);
            this.lblActivePlayers.TabIndex = 7;
            // 
            // rosterGrid
            // 
            this.rosterGrid.AllowUserToAddRows = false;
            this.rosterGrid.AllowUserToDeleteRows = false;
            this.rosterGrid.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.rosterGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.rosterGrid.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.rosterGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rosterGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rosterGrid.Location = new System.Drawing.Point(4, 61);
            this.rosterGrid.Name = "rosterGrid";
            this.rosterGrid.ReadOnly = true;
            this.rosterGrid.RowHeadersVisible = false;
            this.rosterGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.rosterGrid.Size = new System.Drawing.Size(763, 370);
            this.rosterGrid.TabIndex = 5;
            this.rosterGrid.DoubleClick += new System.EventHandler(this.rosterGrid_DoubleClick);
            // 
            // releasePlayer
            // 
            this.releasePlayer.Location = new System.Drawing.Point(130, 3);
            this.releasePlayer.Name = "releasePlayer";
            this.releasePlayer.Size = new System.Drawing.Size(92, 23);
            this.releasePlayer.TabIndex = 3;
            this.releasePlayer.Text = "Release";
            this.releasePlayer.UseVisualStyleBackColor = true;
            // 
            // positionCombo
            // 
            this.positionCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.positionCombo.FormattingEnabled = true;
            this.positionCombo.Location = new System.Drawing.Point(3, 3);
            this.positionCombo.Name = "positionCombo";
            this.positionCombo.Size = new System.Drawing.Size(121, 21);
            this.positionCombo.TabIndex = 1;
            // 
            // RosterManagementTabControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.capTabControl);
            this.Name = "RosterManagementTabControl";
            this.Size = new System.Drawing.Size(781, 463);
            this.capTabControl.ResumeLayout(false);
            this.lineupTab.ResumeLayout(false);
            this.lineupTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rosterGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl capTabControl;
        private System.Windows.Forms.TabPage lineupTab;
        private System.Windows.Forms.DataGridView rosterGrid;
        private System.Windows.Forms.Button releasePlayer;
        private System.Windows.Forms.ComboBox positionCombo;
        private Label lblCap;
        private Label lblActivePlayers;
        private Button btnInActive;

    }
}

﻿using System.Windows.Forms;

namespace DesktopUI.Views.Screens
{
    public partial class ViewGameLog : Form
    {           
        public ViewGameLog(string logText)
        {            
            InitializeComponent();
           /* ScrollBar vScrollBar1 = new VScrollBar();
            vScrollBar1.Dock = DockStyle.Right;
            vScrollBar1.Scroll += (sender, e) => { this.pnlGameLog.VerticalScroll.Value = vScrollBar1.Value; };
            this.pnlGameLog.Controls.Add(vScrollBar1);
            */ 
            //this.lblGameLog.Text = logText;
            this.txtGameLog.Text = logText;
        }       
    }
}

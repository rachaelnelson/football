﻿using System;
using System.Windows.Forms;
using Football.UiClasses.Presenter;
using Football.UiClasses.ViewModel;

namespace DesktopUI.Views.Screens
{
    public partial class SignPlayerForm : Form
    {
        private SignPlayerView player;
        public SignPlayerView PlayerView { get { return player; } }
        private FreeAgentPresenter presenter;

        public SignPlayerForm(FreeAgentPresenter presenter,SignPlayerView player)
        {
            InitializeComponent();

            this.presenter = presenter;
            this.player = player;
            this.lblName.Text = String.Format("{0} {1} - {2}", player.FirstName, player.LastName, player.Position);
            this.lblSalary.Text = player.SalaryRequestedFormatted;                       
            this.btnSign.Click += new System.EventHandler(signPlayer_Click);
        }

        private void signPlayer_Click(object sender, EventArgs e)
        {            
            presenter.SignPlayer(nudYears.Value);            
            this.Close();

            // TODO:  Make this customizable for the error.  Could pass it on the view model
            if (player.SignSuccess == false)
            {
                MessageBox.Show("Unable to sign player due to roster limits");
            }
            else if (player.SignSuccess == true) // third option is a null boolean, so need explicit check for true and false
            {
                presenter.CompleteSigning();
            }
        }
    }
}

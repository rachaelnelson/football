﻿namespace DesktopUI.Views.Screens
{
    partial class PlayerViewOld
    {
        /*
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PlayerInfoBackPanel = new System.Windows.Forms.Panel();
            this.attributeGrid = new System.Windows.Forms.PropertyGrid();
            this.PnlDemographic = new System.Windows.Forms.Panel();
            this.StatsPanel = new System.Windows.Forms.Panel();
            this.statsGrid2 = new System.Windows.Forms.DataGridView();
            this.statsGrid1 = new System.Windows.Forms.DataGridView();
            this.playerHeader = new System.Windows.Forms.Panel();
            this.lblInjury = new System.Windows.Forms.Label();
            this.lblPlayerTeam = new System.Windows.Forms.Label();
            this.lblPlayerPosition = new System.Windows.Forms.Label();
            this.lblPlayerName = new System.Windows.Forms.Label();
            this.ContractPanel = new System.Windows.Forms.Panel();
            this.PlayerInfoBackPanel.SuspendLayout();
            this.StatsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.statsGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statsGrid1)).BeginInit();
            this.playerHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // PlayerInfoBackPanel
            // 
            this.PlayerInfoBackPanel.BackColor = System.Drawing.SystemColors.Control;
            this.PlayerInfoBackPanel.Controls.Add(this.attributeGrid);
            this.PlayerInfoBackPanel.Controls.Add(this.PnlDemographic);
            this.PlayerInfoBackPanel.Controls.Add(this.StatsPanel);
            this.PlayerInfoBackPanel.Controls.Add(this.playerHeader);
            this.PlayerInfoBackPanel.Controls.Add(this.ContractPanel);
            this.PlayerInfoBackPanel.Location = new System.Drawing.Point(0, 0);
            this.PlayerInfoBackPanel.Name = "PlayerInfoBackPanel";
            this.PlayerInfoBackPanel.Size = new System.Drawing.Size(745, 412);
            this.PlayerInfoBackPanel.TabIndex = 0;
            // 
            // attributeGrid
            // 
            this.attributeGrid.CommandsVisibleIfAvailable = false;
            this.attributeGrid.HelpVisible = false;
            this.attributeGrid.LineColor = System.Drawing.SystemColors.Control;
            this.attributeGrid.Location = new System.Drawing.Point(203, 54);
            this.attributeGrid.Name = "attributeGrid";
            this.attributeGrid.Size = new System.Drawing.Size(212, 150);
            this.attributeGrid.TabIndex = 5;
            this.attributeGrid.ToolbarVisible = false;
            this.attributeGrid.ViewBackColor = System.Drawing.SystemColors.Control;
            // 
            // PnlDemographic
            // 
            this.PnlDemographic.BackColor = System.Drawing.SystemColors.Control;
            this.PnlDemographic.Location = new System.Drawing.Point(0, 54);
            this.PnlDemographic.Name = "PnlDemographic";
            this.PnlDemographic.Size = new System.Drawing.Size(205, 150);
            this.PnlDemographic.TabIndex = 4;
            // 
            // StatsPanel
            // 
            this.StatsPanel.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.StatsPanel.Controls.Add(this.statsGrid2);
            this.StatsPanel.Controls.Add(this.statsGrid1);
            this.StatsPanel.Location = new System.Drawing.Point(0, 202);
            this.StatsPanel.Name = "StatsPanel";
            this.StatsPanel.Size = new System.Drawing.Size(744, 210);
            this.StatsPanel.TabIndex = 3;
            // 
            // statsGrid2
            // 
            this.statsGrid2.AllowUserToAddRows = false;
            this.statsGrid2.AllowUserToDeleteRows = false;
            this.statsGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.statsGrid2.Location = new System.Drawing.Point(374, 0);
            this.statsGrid2.Name = "statsGrid2";
            this.statsGrid2.ReadOnly = true;
            this.statsGrid2.Size = new System.Drawing.Size(371, 210);
            this.statsGrid2.TabIndex = 1;
            // 
            // statsGrid1
            // 
            this.statsGrid1.AllowUserToAddRows = false;
            this.statsGrid1.AllowUserToDeleteRows = false;
            this.statsGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.statsGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.statsGrid1.Location = new System.Drawing.Point(0, 0);
            this.statsGrid1.Name = "statsGrid1";
            this.statsGrid1.ReadOnly = true;
            this.statsGrid1.RowHeadersVisible = false;
            this.statsGrid1.Size = new System.Drawing.Size(376, 210);
            this.statsGrid1.TabIndex = 0;
            // 
            // playerHeader
            // 
            this.playerHeader.BackColor = System.Drawing.SystemColors.Info;
            this.playerHeader.Controls.Add(this.lblInjury);
            this.playerHeader.Controls.Add(this.lblPlayerTeam);
            this.playerHeader.Controls.Add(this.lblPlayerPosition);
            this.playerHeader.Controls.Add(this.lblPlayerName);
            this.playerHeader.Location = new System.Drawing.Point(0, 0);
            this.playerHeader.Name = "playerHeader";
            this.playerHeader.Size = new System.Drawing.Size(745, 57);
            this.playerHeader.TabIndex = 2;
            // 
            // lblInjury
            // 
            this.lblInjury.BackColor = System.Drawing.SystemColors.Control;
            this.lblInjury.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInjury.ForeColor = System.Drawing.Color.DarkRed;
            this.lblInjury.Location = new System.Drawing.Point(610, 0);
            this.lblInjury.Name = "lblInjury";
            this.lblInjury.Padding = new System.Windows.Forms.Padding(5);
            this.lblInjury.Size = new System.Drawing.Size(135, 57);
            this.lblInjury.TabIndex = 4;
            // 
            // lblPlayerTeam
            // 
            this.lblPlayerTeam.BackColor = System.Drawing.SystemColors.Control;
            this.lblPlayerTeam.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerTeam.Location = new System.Drawing.Point(441, 0);
            this.lblPlayerTeam.Name = "lblPlayerTeam";
            this.lblPlayerTeam.Padding = new System.Windows.Forms.Padding(5);
            this.lblPlayerTeam.Size = new System.Drawing.Size(172, 57);
            this.lblPlayerTeam.TabIndex = 3;
            // 
            // lblPlayerPosition
            // 
            this.lblPlayerPosition.BackColor = System.Drawing.SystemColors.Control;
            this.lblPlayerPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerPosition.Location = new System.Drawing.Point(312, 0);
            this.lblPlayerPosition.Name = "lblPlayerPosition";
            this.lblPlayerPosition.Padding = new System.Windows.Forms.Padding(5);
            this.lblPlayerPosition.Size = new System.Drawing.Size(132, 57);
            this.lblPlayerPosition.TabIndex = 2;
            // 
            // lblPlayerName
            // 
            this.lblPlayerName.BackColor = System.Drawing.SystemColors.Control;
            this.lblPlayerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerName.Location = new System.Drawing.Point(0, 0);
            this.lblPlayerName.Name = "lblPlayerName";
            this.lblPlayerName.Padding = new System.Windows.Forms.Padding(5);
            this.lblPlayerName.Size = new System.Drawing.Size(322, 57);
            this.lblPlayerName.TabIndex = 1;
            // 
            // ContractPanel
            // 
            this.ContractPanel.BackColor = System.Drawing.SystemColors.GrayText;
            this.ContractPanel.Location = new System.Drawing.Point(528, 54);
            this.ContractPanel.Name = "ContractPanel";
            this.ContractPanel.Size = new System.Drawing.Size(217, 150);
            this.ContractPanel.TabIndex = 1;
            // 
            // PlayerView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 412);
            this.Controls.Add(this.PlayerInfoBackPanel);
            this.Name = "PlayerView";
            this.Text = "PlayerView";
            this.PlayerInfoBackPanel.ResumeLayout(false);
            this.StatsPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.statsGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statsGrid1)).EndInit();
            this.playerHeader.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PlayerInfoBackPanel;
        private System.Windows.Forms.Panel ContractPanel;
        private System.Windows.Forms.Panel playerHeader;
        private System.Windows.Forms.Panel StatsPanel;
        private System.Windows.Forms.Label lblPlayerName;
        private System.Windows.Forms.Label lblPlayerTeam;
        private System.Windows.Forms.Label lblPlayerPosition;
        private System.Windows.Forms.Panel PnlDemographic;
        private System.Windows.Forms.Label lblInjury;
        private System.Windows.Forms.PropertyGrid attributeGrid;
        private System.Windows.Forms.DataGridView statsGrid2;
        private System.Windows.Forms.DataGridView statsGrid1;
         * 
         */ 
    }
        
}
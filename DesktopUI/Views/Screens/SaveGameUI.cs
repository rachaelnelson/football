﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Football.FileIO.SaveGame;
//using Football.FileIO.SaveGame;
using Football.Settings;


namespace DesktopUI.Views.Screens
{
    public partial class SaveGameUI : Form
    {
        private FootballSettings setting;

        public SaveGameUI(FootballSettings setting)
        {
            InitializeComponent();
            this.setting = setting;
            this.initList();
        }

        private void initList()
        {
            this.fileList.DataSource = this.getDirList();            
        }

        private List<String> getDirList()
        {
            String[] dirs = Directory.GetDirectories(this.setting.appPath);

            List<String> nameList = new List<String>();

            foreach (var dir in dirs)
            {
                DirectoryInfo info = new DirectoryInfo(dir);
                nameList.Add(info.Name);
            }

            return nameList;
        }       

        private void AddFolderBtn_Click(object sender, EventArgs e)
        {
            String folderName = this.gameNameTxt.Text.Trim();
            if (folderName.Length > 0)
            {
                String newDir = String.Format(@"{0}{1}", this.setting.appPath, folderName);
                this.createDir( newDir );                
            }
            else
            {
                MessageBox.Show("Please enter a save game name to add.", "Error", MessageBoxButtons.OK);
            }
        }

        private void createDir( String newDir )
        {
            if (Directory.Exists(newDir))
            {
                MessageBox.Show("Save game already exists.  Please enter a different name.", "Error", MessageBoxButtons.OK);
            }
            else
            {
                Directory.CreateDirectory(newDir);
                this.initList();
            }
        }

        private void save_Click(object sender, EventArgs e)
        {            
            String fileName = this.fileList.Text.Trim();

            if (fileName.Length > 0)
            {
                SaveGame saveGame = new SaveGame(setting);
                if (saveGame.saveGame(fileName))
                {
                    this.Close();
                    MessageBox.Show("Game Saved!", "Success", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("Error saving game.", "Error", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("Please select a save game.", "Error", MessageBoxButtons.OK);
            }            
        }        
    }
}

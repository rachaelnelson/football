﻿
using Football.UiClasses.PlayerViewer;

namespace DesktopUI.Views.Screens
{
    public class PlayerViewer : IPlayerViewer
    {       
        public void Display(PlayerViewModel playerViewModel)
        {
            new PlayerView(playerViewModel).Show();
        }
    }
}

﻿using System;
using System.Windows.Forms;

using Football.UiClasses.Presenter;

namespace DesktopUI.Views.Screens
{
    public partial class WeeklyScheduleTabControl : UserControl
    {       
        private WeeklySchedulePresenter presenter;

        public WeeklyScheduleTabControl(WeeklySchedulePresenter presenter)
        {
            InitializeComponent();

            this.presenter = presenter;
            weekScheduleGrid.DataSource = presenter.ViewModel.ScheduleData;
            presenter.WeekChanged(); // trigger init of grid
            weekLabel.Text = presenter.ViewModel.SelectedWeek.ToString();
            disableSorting();
            weekScheduleGrid.Columns["Game Log"].Visible = false;
            checkSimButton();
        }

        private void weekArrowRight_Click(object sender, EventArgs e)
        {            
            presenter.WeekChangedIncrease();
            weekLabel.Text = presenter.ViewModel.SelectedWeek.ToString();
            checkSimButton();  
        }

        private void weekArrowLeft_Click(object sender, EventArgs e)
        {            
            presenter.WeekChangedDecrease();
            weekLabel.Text = presenter.ViewModel.SelectedWeek.ToString();
            checkSimButton();   
        }

        public void disableSorting()
        {
            for (int i = 0; i < weekScheduleGrid.Columns.Count; i++)
            {
                weekScheduleGrid.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

        private void checkSimButton()
        {
            this.simWeekBtn.Enabled = presenter.ViewModel.IsSimButtonEnabled();     
        }

        //   Sims the week          
        private void simWeekBtn_Click(object sender, EventArgs e)
        {
            presenter.InitiateSim();
            checkSimButton();   
        }
                
        private void weekScheduleGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            String logPath = this.weekScheduleGrid.Rows[e.RowIndex].Cells[3].Value.ToString();
            string gameLog = presenter.LoadGameLog(logPath);
            Form displayGameLog = new ViewGameLog(gameLog);
            displayGameLog.ShowDialog();
        }       
    }
}
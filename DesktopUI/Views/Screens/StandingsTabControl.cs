﻿using System;
using System.Data;
using System.Windows.Forms;

using Football.Settings;
using UiClasses.Helper;

namespace DesktopUI.Views.Screens
{
    public partial class StandingsTabControl : UserControl
    {
        private TabControl tabControl;
        private TabPage standingsTab;
        private ComboBox divisionCombo;
        private BindingSource lbs;
        private DataGridView standingsGrid;
        private StandingsHelper sh;

        public StandingsTabControl(FootballSettings setting)
        {
           InitializeComponent();                
           sh = new StandingsHelper(setting);                                           
           // assign to binding source        
           lbs = new BindingSource();                 
           this.divisionCombo.SelectedIndexChanged += new System.EventHandler(this.divisionCombo_SelectedIndexChanged);
           // populate combobox           
           this.divisionCombo.DataSource = sh.getDivisionChoices();
        }

        // on division change, fire event
        private void divisionCombo_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            string selected = this.divisionCombo.SelectedValue.ToString();
            DataView dv = new DataView(sh.loadStandings());            
            lbs.DataSource = dv;              

            // set binding source to grid
            standingsGrid.DataSource = lbs;            

            // hide conf and div - only used for filtering 
            standingsGrid.Columns["Conference"].Visible = false;
            standingsGrid.Columns["Division"].Visible = false;

            lbs.Sort = "Win % DESC";
            string[] filter = selected.Split(' ');
            string stringFilter=null;

            if (filter.Length == 1 && !filter[0].Equals("ALL")) // choose ALL, AFC, NFC
            { 
                stringFilter = String.Format("Conference = '{0}'", filter[0]);
            }
            else if( filter.Length>1 )
            {
                stringFilter = String.Format("Conference = '{0}' AND Division = '{1}'",filter[0], selected);
            }
            lbs.Filter = stringFilter;
            lbs.ResetBindings(false);
        }

        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.standingsTab = new System.Windows.Forms.TabPage();
            this.standingsGrid = new System.Windows.Forms.DataGridView();
            this.divisionCombo = new System.Windows.Forms.ComboBox();
            this.tabControl.SuspendLayout();
            this.standingsTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.standingsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.standingsTab);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(781, 463);
            this.tabControl.TabIndex = 3;
            // 
            // standingsTab
            // 
            this.standingsTab.BackColor = System.Drawing.Color.AliceBlue;
            this.standingsTab.Controls.Add(this.standingsGrid);
            this.standingsTab.Controls.Add(this.divisionCombo);
            this.standingsTab.Location = new System.Drawing.Point(4, 22);
            this.standingsTab.Name = "standingsTab";
            this.standingsTab.Padding = new System.Windows.Forms.Padding(3);
            this.standingsTab.Size = new System.Drawing.Size(773, 437);
            this.standingsTab.TabIndex = 2;
            this.standingsTab.Text = "Standings";
            // 
            // standingsGrid
            // 
            this.standingsGrid.AllowUserToAddRows = false;
            this.standingsGrid.AllowUserToDeleteRows = false;
            this.standingsGrid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.standingsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.standingsGrid.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.standingsGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.standingsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.standingsGrid.Location = new System.Drawing.Point(3, 30);
            this.standingsGrid.Name = "standingsGrid";
            this.standingsGrid.ReadOnly = true;
            this.standingsGrid.RowHeadersVisible = false;
            this.standingsGrid.Size = new System.Drawing.Size(767, 431);
            this.standingsGrid.TabIndex = 2;
            // 
            // divisionCombo
            // 
            this.divisionCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.divisionCombo.FormattingEnabled = true;
            this.divisionCombo.Location = new System.Drawing.Point(3, 3);
            this.divisionCombo.Name = "divisionCombo";
            this.divisionCombo.Size = new System.Drawing.Size(121, 21);
            this.divisionCombo.TabIndex = 1;
            // 
            // StandingsTabControl
            // 
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.tabControl);
            this.Name = "StandingsTabControl";
            this.Size = new System.Drawing.Size(781, 463);
            this.tabControl.ResumeLayout(false);
            this.standingsTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.standingsGrid)).EndInit();
            this.ResumeLayout(false);

        }    
    }
}

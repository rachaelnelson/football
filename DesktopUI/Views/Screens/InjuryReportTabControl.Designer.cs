﻿namespace DesktopUI.Views.Screens
{
    partial class InjuryReportTabControl
    {  
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage = new System.Windows.Forms.TabPage();
            this.btnIR = new System.Windows.Forms.Button();
            this.injuryGrid = new System.Windows.Forms.DataGridView();
            this.teamCombo = new System.Windows.Forms.ComboBox();
            this.tabControl.SuspendLayout();
            this.tabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.injuryGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(781, 463);
            this.tabControl.TabIndex = 3;
            // 
            // tabPage
            // 
            this.tabPage.BackColor = System.Drawing.Color.AliceBlue;
            this.tabPage.Controls.Add(this.btnIR);
            this.tabPage.Controls.Add(this.injuryGrid);
            this.tabPage.Controls.Add(this.teamCombo);
            this.tabPage.Location = new System.Drawing.Point(4, 22);
            this.tabPage.Name = "tabPage";
            this.tabPage.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage.Size = new System.Drawing.Size(773, 437);
            this.tabPage.TabIndex = 2;
            this.tabPage.Text = "Injury Report";
            // 
            // btnIR
            // 
            this.btnIR.Location = new System.Drawing.Point(144, 3);
            this.btnIR.Name = "btnIR";
            this.btnIR.Size = new System.Drawing.Size(72, 24);
            this.btnIR.TabIndex = 3;
            this.btnIR.Text = "Place on IR";
            this.btnIR.UseVisualStyleBackColor = true;
            // 
            // injuryGrid
            // 
            this.injuryGrid.AllowUserToAddRows = false;
            this.injuryGrid.AllowUserToDeleteRows = false;
            this.injuryGrid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.injuryGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.injuryGrid.BackgroundColor = System.Drawing.Color.AliceBlue;
            this.injuryGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.injuryGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.injuryGrid.Location = new System.Drawing.Point(3, 3);
            this.injuryGrid.MultiSelect = false;
            this.injuryGrid.Name = "injuryGrid";
            this.injuryGrid.ReadOnly = true;
            this.injuryGrid.RowHeadersVisible = false;
            this.injuryGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.injuryGrid.Size = new System.Drawing.Size(767, 431);
            this.injuryGrid.TabIndex = 2;
            // 
            // teamCombo
            // 
            this.teamCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.teamCombo.FormattingEnabled = true;
            this.teamCombo.Location = new System.Drawing.Point(3, 3);
            this.teamCombo.Name = "teamCombo";
            this.teamCombo.Size = new System.Drawing.Size(121, 21);
            this.teamCombo.TabIndex = 1;
            // 
            // InjuryReportTabControl
            // 
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.Controls.Add(this.tabControl);
            this.Name = "InjuryReportTabControl";
            this.Size = new System.Drawing.Size(781, 463);
            this.tabControl.ResumeLayout(false);
            this.tabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.injuryGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage;
        private System.Windows.Forms.ComboBox teamCombo;
        private System.Windows.Forms.DataGridView injuryGrid;
        private System.Windows.Forms.Button btnIR;
    }
}

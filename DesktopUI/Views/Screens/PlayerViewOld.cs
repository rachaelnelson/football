﻿


namespace DesktopUI.Views.Screens
{

    /* TODO:  redo all of this
     * 
     * bootstrap:  new PlayerView(FootballStats statsHandler)  <-- pass to views that need this ability
     * in view:  PlayerView.Display(player);  <-- shows window
     * * 
     */
    
    /*
    public partial class PlayerViewOld : Form
    {
        private Player player;
        private FootballStats stats;
        private BindingSource lbs;
        private Dictionary<string, Delegate> delegateDict;
        private StatsHelper statsHelper;

        public PlayerViewOld(Player player, FootballStats stats)
        {
            InitializeComponent();

            this.stats = stats;
            this.lbs = new BindingSource();
            this.statsHelper = new StatsHelper();

            this.lbs.Position = 0;                       
            this.player = player;

            this.initDict();
            this.loadPlayerHeader();
            this.loadAttributesPanel();
            this.loadStatsPanel();
        }

        private void initDict()
        {
            // TODO:  put this in one spot...is also used in Stats.
              
            //  This still fucking sucks but couldn't get the god damn reflection to do this:
              
            //  getStats<type passed in at runtime>()                            
            
            this.delegateDict = new Dictionary<string, Delegate>()
            {
                {"Passing", new Func<List<StatsRecord>,SortableBindingList<PassingStatRecord>>(this.statsHelper.getStatRecords<PassingStatRecord>)},
                {"Rushing", new Func<List<StatsRecord>,SortableBindingList<RushingStatRecord>>(this.statsHelper.getStatRecords<RushingStatRecord>)},
                {"Receiving", new Func<List<StatsRecord>,SortableBindingList<ReceivingStatRecord>>(this.statsHelper.getStatRecords<ReceivingStatRecord>)},
                {"Defense", new Func<List<StatsRecord>,SortableBindingList<DefenseStatRecord>>(this.statsHelper.getStatRecords<DefenseStatRecord>)},
                {"Kicking", new Func<List<StatsRecord>,SortableBindingList<KickingStatRecord>>(this.statsHelper.getStatRecords<KickingStatRecord>)},
                {"Punting", new Func<List<StatsRecord>,SortableBindingList<PuntingStatRecord>>(this.statsHelper.getStatRecords<PuntingStatRecord>)}
            };
        }

        private void loadPlayerHeader()
        {
            this.lblPlayerName.Text = String.Format("{0} {1}",this.player.firstName, this.player.lastName);
            this.lblPlayerPosition.Text = this.player.position.ToString();
            this.lblPlayerTeam.Text = String.Format("{0} {1}",this.player.team.Name, this.player.team.Nickname);

            if (this.player.injury.duration > 0)
            {
                this.lblInjury.Text = String.Format("Injury:  {0} \nWeeks: {1}", this.player.injury.injury, this.player.injury.duration);
            }
        }

        private void loadAttributesPanel()
        {
            PlayerAttributeDelegate pad = new PlayerAttributeDelegate(player);
            this.attributeGrid.SelectedObject = pad.getAttributes();                   
        }
        
        private void loadStatsPanel()
        {
            if ( this.player.position != PlayerType.OL  )
            {
                // This shit is the only thing that worked....            
                this.setStatsPanel1(this.player);

                this.statsGrid1.Columns["Id"].Visible = false;
                this.statsGrid1.Columns["PlayerHash"].Visible = false;
                this.statsGrid1.Columns["Team"].Visible = false;
                this.statsGrid1.Columns["PlayerFirstName"].Visible = false;
                this.statsGrid1.Columns["PlayerLastName"].Visible = false;

                this.statsGrid1.Show();
            }
        }

        // TODO:  make this not suck...
        public void setStatsPanel1(Player player)
        {           
            switch (player.position)
            {
                case PlayerType.QB:
                    this.lbs.DataSource = this.delegateDict["Passing"].DynamicInvoke(stats.getPlayerStats("Passing", player));
                    break;

                case PlayerType.RB:
                    this.lbs.DataSource = this.delegateDict["Rushing"].DynamicInvoke(stats.getPlayerStats("Rushing", player));
                    break;

                case PlayerType.TE:
                    this.lbs.DataSource = this.delegateDict["Receiving"].DynamicInvoke(stats.getPlayerStats("Receiving", player));
                    break;

                case PlayerType.WR:
                    this.lbs.DataSource = this.delegateDict["Receiving"].DynamicInvoke(stats.getPlayerStats("Receiving", player));
                    break;

                case PlayerType.OL:
                        this.lbs.DataSource = null;
                    break;

                case PlayerType.DL:
                case PlayerType.LB:
                case PlayerType.DB:
                    this.lbs.DataSource = this.delegateDict["Defense"].DynamicInvoke(stats.getPlayerStats("Defense", player));
                    break;

                case PlayerType.K:
                    this.lbs.DataSource = this.delegateDict["Kicking"].DynamicInvoke(stats.getPlayerStats("Kicking", player));
                    break;

                case PlayerType.P:
                    this.lbs.DataSource = this.delegateDict["Punting"].DynamicInvoke(stats.getPlayerStats("Punting", player));
                    break;
            }
                       
            this.statsGrid1.DataSource = this.lbs;
        }

        // TODO:  make this not suck...
        public void setStatsPanel2(Player player)
        {            

            switch (player.position)
            {
                case PlayerType.QB:
                    this.lbs.DataSource = this.delegateDict["Rushing"].DynamicInvoke(stats.getPlayerStats("Rushing", player));
                    break;

                case PlayerType.RB:
                    this.lbs.DataSource = this.delegateDict["Receiving"].DynamicInvoke(stats.getPlayerStats("Receiving", player));
                    break;

                default:

                    break;
            }

            this.statsGrid2.DataSource = this.lbs;
        }     
    }
     */ 
}

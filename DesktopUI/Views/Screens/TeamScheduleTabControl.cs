﻿using System;
using System.Windows.Forms;

using Football.UiClasses.Presenter;

namespace DesktopUI.Views.Screens
{
    public partial class TeamScheduleTabControl : UserControl
    {
        private TeamSchedulePresenter presenter;

        public TeamScheduleTabControl(TeamSchedulePresenter presenter)
        {
            InitializeComponent();

            this.presenter = presenter;
            teamCombo.DataSource = this.presenter.ViewModel.Teams;
            teamScheduleGrid.DataSource = this.presenter.ViewModel.ScheduleData;
            teamScheduleGrid.Columns["Game Log"].Visible = false;
            checkSimButton();
        }

        private void teamCombo_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            presenter.TeamChanged(teamCombo.SelectedValue);
        }

        private void teamScheduleGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            string logPath = this.teamScheduleGrid.Rows[e.RowIndex].Cells[3].Value.ToString();
            string gameLog = presenter.LoadGameLog(logPath);
            Form displayGameLog = new ViewGameLog(gameLog);
            displayGameLog.ShowDialog();
        }

        private void checkSimButton()
        {
            this.simWeekBtn.Enabled = presenter.ViewModel.IsSimButtonEnabled();
        }

        //   Sims the week          
        private void simWeekBtn_Click(object sender, EventArgs e)
        {
            presenter.SimWeek();
            checkSimButton();
        }        
    }
}

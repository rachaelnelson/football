﻿using System;
using System.Windows.Forms;
using Football.League.Players;
using Football.UiClasses.Presenter;
using Football.UiClasses.ViewModel;

namespace DesktopUI.Views.Screens
{
    public partial class RosterManagementTabControl : UserControl
    {        
        private RosterPresenter presenter;
        private PlayerViewPresenter playerViewPresenter;

        public RosterManagementTabControl(RosterPresenter presenter, PlayerViewPresenter playerViewPresenter)
        {
            InitializeComponent();

            this.positionCombo.SelectedIndexChanged += new System.EventHandler(this.positionCombo_SelectedIndexChanged);
            this.releasePlayer.Click += new System.EventHandler(releasePlayer_Click);

            this.presenter = presenter;
            this.playerViewPresenter = playerViewPresenter;

            positionCombo.DataSource = this.presenter.ViewModel.Positions;            
            rosterGrid.DataSource = this.presenter.ViewModel.RosterData;

            HideAttr();  // position change should trigger this in SelectedIndexChanged, but isn't working for some reason.

            // for some stupid reason, the gui designer won't let me set the size.
            this.lblCap.Height = 50;
            this.lblCap.Width = 200;
            this.lblActivePlayers.Height = 50;
            this.lblActivePlayers.Width = 200;
            this.setCapSpaceLabel();
            this.setCountsLabel();
        }

        private void positionCombo_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            this.HideAttr();  
        }

        private void HideAttr()
        {
            presenter.PositionChanged(positionCombo.SelectedValue as PlayerType);
            HideColumns.HideEmptyAttributes(rosterGrid);
            rosterGrid.Columns["Id"].Visible = false; // hide id column  
        }

        private void btnInActive_Click(object sender, EventArgs e)
        {
            presenter.StatusChanged(rosterGrid.CurrentRow.Cells[0].Value);
            this.setCountsLabel();
        }

        // TODO:  prolly need to move this out of here....
        private void releasePlayer_Click(object sender, EventArgs e)
        {            
            presenter.InitiateReleasePlayer(rosterGrid.CurrentRow.Cells[0].Value);

            ReleasePlayerView player = this.presenter.ViewModel.ReleasePlayer;

            if (player != null)
            {
                string message = null;

                if (!player.IsOnIR)
                {
                    message = String.Format("Release {0} {1} {2}?", player.Position, player.FirstName, player.LastName);

                    var result = MessageBox.Show(message, "Release Player",
                                             MessageBoxButtons.YesNo,
                                             MessageBoxIcon.Warning);

                    if (result == DialogResult.Yes)
                    {
                        presenter.ReleasePlayer();
                        this.setCapSpaceLabel();
                        this.setCountsLabel();
                    }
                }
                else
                {
                    message = String.Format("Player {0} {1} {2} cannot be released because he's on IR.", player.Position, player.FirstName, player.LastName);

                    var result = MessageBox.Show(message, "Release Player",
                                             MessageBoxButtons.OK,
                                             MessageBoxIcon.Information);
                }
            }            
        }

        private void setCapSpaceLabel()
        {
            this.lblCap.Text = String.Format("Cap Space: ${0}", this.presenter.ViewModel.CapSpace.ToString("0,0"));
        }

        private void setCountsLabel()
        {
            this.lblActivePlayers.Text = String.Format("Min/Active/Max: {0}/{1}/{2}      NonIR Count/Max: {3}/{4}      Inactive/IR: {5}/{6}      Total Count: {7}",                                
                presenter.ViewModel.Counts.MinRosterCount,
                presenter.ViewModel.Counts.ActiveCount,
                presenter.ViewModel.Counts.ActiveLimit,                
                presenter.ViewModel.Counts.NonIRCount,
                presenter.ViewModel.Counts.MaxRosterCount,
                presenter.ViewModel.Counts.InactiveCount,
                presenter.ViewModel.Counts.IRCount,
                presenter.ViewModel.Counts.TotalCount
                );
        }

        private void rosterGrid_DoubleClick(object sender, EventArgs e)
        {
            playerViewPresenter.Display(rosterGrid.CurrentRow.Cells[0].Value, presenter.ViewModel.CurrentCapYear);
        }     

        /* // Original code from first roster management control
        private void rosterGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Player selectedPlayer = (Player)this.lbs.Current;

            PlayerView playerView = new PlayerView(selectedPlayer, stats);
            playerView.Show();
        }
         */ 
    }
}

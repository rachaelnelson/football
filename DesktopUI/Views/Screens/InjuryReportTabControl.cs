﻿using System;
using System.Windows.Forms;
using Football.UiClasses.Presenter;
using Football.UiClasses.ViewModel;

namespace DesktopUI.Views.Screens
{
    public partial class InjuryReportTabControl : UserControl
    {       
        private InjuryPresenter presenter;
        
        public InjuryReportTabControl(InjuryPresenter presenter)
        {
            InitializeComponent();

            this.teamCombo.SelectedIndexChanged += new System.EventHandler(this.teamCombo_SelectedIndexChanged);            
            this.btnIR.Click += new System.EventHandler(this.btnIR_Click);
            
            this.presenter = presenter;
            this.teamCombo.DataSource = this.presenter.ViewModel.Teams;
            this.injuryGrid.DataSource = this.presenter.ViewModel.InjuredList;

            this.injuryGrid.Columns["Id"].Visible = false; // hide id column                       
        }

        private void teamCombo_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            presenter.TeamChanged(teamCombo.SelectedValue);
            btnIR.Visible = ((string)teamCombo.SelectedValue == presenter.ViewModel.MyTeam)?true:false; //TODO:  should be in viewmodel               
        }        
        
        private void btnIR_Click(object sender, EventArgs e)
        {
            presenter.InitiatePlaceOnIR(injuryGrid.CurrentRow.Cells[0].Value);

            PlaceOnIRView player = this.presenter.ViewModel.IRPlayer;

            if (player != null)
            {
                String message = null;

                if (!player.IsOnIR)
                {
                    message = String.Format("Place {0} {1} {2} on IR?  The player will be inactive for the rest of the season.", player.Position, player.FirstName, 
                        player.LastName);

                    var result = MessageBox.Show(message, "Place on IR?",
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Warning);

                    if (result == DialogResult.Yes)
                    {
                        presenter.PlaceOnIR();
                    }
                }
                else
                {
                    message = String.Format("Player {0} {1} {2} is already on IR.", player.Position, player.FirstName, player.LastName);
                    var result = MessageBox.Show(message, "Place on IR?",
                                         MessageBoxButtons.OK,
                                         MessageBoxIcon.Information);
                }                
            }                       
        }
    }
}

﻿
using System.Drawing;
using System.Windows.Forms;

namespace DesktopUI.Views
{
    public class MenuRender: ProfessionalColorTable
    {
        public override Color MenuItemSelected
        {
            get { return Color.NavajoWhite; }
        }

        public override Color MenuItemBorder
        {
            get { return Color.AliceBlue; }
        }

        public override Color MenuBorder  //added for changing the menu border
        {
            get { return Color.AliceBlue; }
        }    
    }
}

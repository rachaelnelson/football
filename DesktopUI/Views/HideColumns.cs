﻿using System.Windows.Forms;

namespace DesktopUI.Views
{
    public class HideColumns
    {
        // Hides Attributes if they are empty
        public static void HideEmptyAttributes(DataGridView grid)
        {
            foreach (DataGridViewColumn clm in grid.Columns)
            {
                bool FoundData = false;
                foreach (DataGridViewRow row in grid.Rows)
                {
                    object cell = row.Cells[clm.Index].Value;
                    if ( cell!= null && cell.ToString()!=string.Empty && cell.ToString() !="0" )
                    {                        
                        FoundData = true;
                        break;
                    }
                }
                grid.Columns[clm.Index].Visible = FoundData;                
            }
        }
    }
}

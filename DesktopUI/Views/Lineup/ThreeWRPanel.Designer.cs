﻿namespace UiClasses.Lineup
{
    partial class ThreeWRPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThreeWRPanel));
            this.panel1 = new System.Windows.Forms.Panel();
            this.WR3 = new System.Windows.Forms.PictureBox();
            this.TE = new System.Windows.Forms.PictureBox();
            this.OL5 = new System.Windows.Forms.PictureBox();
            this.OL1 = new System.Windows.Forms.PictureBox();
            this.OL2 = new System.Windows.Forms.PictureBox();
            this.OL4 = new System.Windows.Forms.PictureBox();
            this.OL3 = new System.Windows.Forms.PictureBox();
            this.WR2 = new System.Windows.Forms.PictureBox();
            this.WR1 = new System.Windows.Forms.PictureBox();
            this.RB = new System.Windows.Forms.PictureBox();
            this.QB = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WR3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OL5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OL1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OL2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OL4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OL3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WR2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WR1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QB)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.WR3);
            this.panel1.Controls.Add(this.TE);
            this.panel1.Controls.Add(this.OL5);
            this.panel1.Controls.Add(this.OL1);
            this.panel1.Controls.Add(this.OL2);
            this.panel1.Controls.Add(this.OL4);
            this.panel1.Controls.Add(this.OL3);
            this.panel1.Controls.Add(this.WR2);
            this.panel1.Controls.Add(this.WR1);
            this.panel1.Controls.Add(this.RB);
            this.panel1.Controls.Add(this.QB);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(763, 213);
            this.panel1.TabIndex = 7;
            // 
            // WR3
            // 
            this.WR3.Image = ((System.Drawing.Image)(resources.GetObject("WR3.Image")));
            this.WR3.InitialImage = ((System.Drawing.Image)(resources.GetObject("WR3.InitialImage")));
            this.WR3.Location = new System.Drawing.Point(84, 70);
            this.WR3.Name = "WR3";
            this.WR3.Size = new System.Drawing.Size(48, 48);
            this.WR3.TabIndex = 10;
            this.WR3.TabStop = false;
            this.WR3.Tag = "WR";
            // 
            // TE
            // 
            this.TE.Image = ((System.Drawing.Image)(resources.GetObject("TE.Image")));
            this.TE.InitialImage = ((System.Drawing.Image)(resources.GetObject("TE.InitialImage")));
            this.TE.Location = new System.Drawing.Point(574, 54);
            this.TE.Name = "TE";
            this.TE.Size = new System.Drawing.Size(48, 48);
            this.TE.TabIndex = 9;
            this.TE.TabStop = false;
            this.TE.Tag = "TE";
            // 
            // OL5
            // 
            this.OL5.Image = ((System.Drawing.Image)(resources.GetObject("OL5.Image")));
            this.OL5.InitialImage = ((System.Drawing.Image)(resources.GetObject("OL5.InitialImage")));
            this.OL5.Location = new System.Drawing.Point(503, 19);
            this.OL5.Name = "OL5";
            this.OL5.Size = new System.Drawing.Size(48, 48);
            this.OL5.TabIndex = 8;
            this.OL5.TabStop = false;
            this.OL5.Tag = "OL";
            // 
            // OL1
            // 
            this.OL1.Image = ((System.Drawing.Image)(resources.GetObject("OL1.Image")));
            this.OL1.InitialImage = ((System.Drawing.Image)(resources.GetObject("OL1.InitialImage")));
            this.OL1.Location = new System.Drawing.Point(184, 19);
            this.OL1.Name = "OL1";
            this.OL1.Size = new System.Drawing.Size(48, 48);
            this.OL1.TabIndex = 7;
            this.OL1.TabStop = false;
            this.OL1.Tag = "OL";
            // 
            // OL2
            // 
            this.OL2.Image = ((System.Drawing.Image)(resources.GetObject("OL2.Image")));
            this.OL2.InitialImage = ((System.Drawing.Image)(resources.GetObject("OL2.InitialImage")));
            this.OL2.Location = new System.Drawing.Point(263, 19);
            this.OL2.Name = "OL2";
            this.OL2.Size = new System.Drawing.Size(48, 48);
            this.OL2.TabIndex = 6;
            this.OL2.TabStop = false;
            this.OL2.Tag = "OL";
            // 
            // OL4
            // 
            this.OL4.Image = ((System.Drawing.Image)(resources.GetObject("OL4.Image")));
            this.OL4.InitialImage = ((System.Drawing.Image)(resources.GetObject("OL4.InitialImage")));
            this.OL4.Location = new System.Drawing.Point(427, 19);
            this.OL4.Name = "OL4";
            this.OL4.Size = new System.Drawing.Size(48, 48);
            this.OL4.TabIndex = 5;
            this.OL4.TabStop = false;
            this.OL4.Tag = "OL";
            // 
            // OL3
            // 
            this.OL3.Image = ((System.Drawing.Image)(resources.GetObject("OL3.Image")));
            this.OL3.InitialImage = ((System.Drawing.Image)(resources.GetObject("OL3.InitialImage")));
            this.OL3.Location = new System.Drawing.Point(344, 19);
            this.OL3.Name = "OL3";
            this.OL3.Size = new System.Drawing.Size(48, 48);
            this.OL3.TabIndex = 4;
            this.OL3.TabStop = false;
            this.OL3.Tag = "OL";
            // 
            // WR2
            // 
            this.WR2.Image = ((System.Drawing.Image)(resources.GetObject("WR2.Image")));
            this.WR2.InitialImage = ((System.Drawing.Image)(resources.GetObject("WR2.InitialImage")));
            this.WR2.Location = new System.Drawing.Point(700, 19);
            this.WR2.Name = "WR2";
            this.WR2.Size = new System.Drawing.Size(48, 48);
            this.WR2.TabIndex = 3;
            this.WR2.TabStop = false;
            this.WR2.Tag = "WR";
            // 
            // WR1
            // 
            this.WR1.Image = ((System.Drawing.Image)(resources.GetObject("WR1.Image")));
            this.WR1.InitialImage = ((System.Drawing.Image)(resources.GetObject("WR1.InitialImage")));
            this.WR1.Location = new System.Drawing.Point(3, 19);
            this.WR1.Name = "WR1";
            this.WR1.Size = new System.Drawing.Size(48, 48);
            this.WR1.TabIndex = 2;
            this.WR1.TabStop = false;
            this.WR1.Tag = "WR";
            // 
            // RB
            // 
            this.RB.Image = ((System.Drawing.Image)(resources.GetObject("RB.Image")));
            this.RB.InitialImage = ((System.Drawing.Image)(resources.GetObject("RB.InitialImage")));
            this.RB.Location = new System.Drawing.Point(344, 152);
            this.RB.Name = "RB";
            this.RB.Size = new System.Drawing.Size(48, 48);
            this.RB.TabIndex = 1;
            this.RB.TabStop = false;
            this.RB.Tag = "RB";
            // 
            // QB
            // 
            this.QB.Image = ((System.Drawing.Image)(resources.GetObject("QB.Image")));
            this.QB.InitialImage = ((System.Drawing.Image)(resources.GetObject("QB.InitialImage")));
            this.QB.Location = new System.Drawing.Point(344, 86);
            this.QB.Name = "QB";
            this.QB.Size = new System.Drawing.Size(48, 48);
            this.QB.TabIndex = 0;
            this.QB.TabStop = false;
            this.QB.Tag = "QB";
            // 
            // ThreeWRPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "ThreeWRPanel";
            this.Size = new System.Drawing.Size(772, 224);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WR3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OL5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OL1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OL2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OL4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OL3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WR2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WR1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QB)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox WR3;
        private System.Windows.Forms.PictureBox TE;
        private System.Windows.Forms.PictureBox OL5;
        private System.Windows.Forms.PictureBox OL1;
        private System.Windows.Forms.PictureBox OL2;
        private System.Windows.Forms.PictureBox OL4;
        private System.Windows.Forms.PictureBox OL3;
        private System.Windows.Forms.PictureBox WR2;
        private System.Windows.Forms.PictureBox WR1;
        private System.Windows.Forms.PictureBox RB;
        private System.Windows.Forms.PictureBox QB;
    }
}

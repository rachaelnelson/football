﻿using System;
using System.Windows.Forms;

using DesktopUI.Views.Screens;

using Football.Init;
using Football.Settings;
using Football.UiClasses.Presenter;


//using Football.Container;

namespace DesktopUI
{
    // http://visualstudiomagazine.com/Articles/2011/10/01/WPF-and-Inversion-of-Control.aspx?Page=1

    // http://codebetter.com/jeremymiller/2005/07/20/a-simple-example-of-the-humble-dialog-box/

    public partial class MainForm : Form
    {       
        private FootballInit init;
        private PlayerViewPresenter playerViewPresenter;        

        public MainForm()
        {
            InitializeComponent();
            
            init = new FootballInit();

            menuStrip1.Renderer = new ToolStripProfessionalRenderer(new DesktopUI.Views.MenuRender());
        }             
        
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            init.Exit();            
            Application.Exit();
        }

        /**
         *  New Game
         */
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.newGame();

        }      

        /* Code to handle new game */
        private void newGame()
        {
            NewGameUI newGame = new NewGameUI(init.Setting,init.PenaltyHandler,init.SituationHandler,init.GameHandler);            
            newGame.StartPosition = FormStartPosition.CenterParent;
            newGame.FormClosed += new FormClosedEventHandler(StartGameEnableMenus);            
            newGame.newGame();
            newGame.Show(this);
            this.initRest();
        }

        private void initRest()
        {
            init.InitRest();
            playerViewPresenter = new PlayerViewPresenter(init.RosterService, new PlayerViewer(), init.StatsHandler);
        }

        // Open game
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenGameUI openGame = new OpenGameUI(init.Setting,init.PenaltyHandler,init.SituationHandler,init.GameHandler);
            openGame.FormClosed += new FormClosedEventHandler(StartGameEnableMenus);
            openGame.Show();
            this.initRest();     
        }

        /**
        *   Enable menus after starting a new game or opening a game
        */
        public void StartGameEnableMenus(object sender, EventArgs ee)
        {            
            init.Setting = FootballSettings.getInstance();

            if (!string.IsNullOrEmpty(init.Setting.myTeam) ) // if my team is populated (string), then enable menus
            {
                editToolStripMenuItem.Visible = true;
                saveAsToolStripMenuItem.Visible = true;
                saveToolStripMenuItem.Visible = true;
                viewToolStripMenuItem.Visible = true;
                this.initRest();
            }
        }

        /**
         *  Call to SaveAs 
         */
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.saveGameDialog();                        
        }               

        /**
         *  Used in both Save and Save As to handle the dialog boxes 
         */
        private void saveGameDialog()
        {
            SaveGameUI saveDialog = new SaveGameUI(init.Setting);
            saveDialog.Show();           
        }                       
        
        /**
         *  Remove panel for new selected panel 
         */
        private void removePanel()
        {
            for (int index = 0; index < this.mainPanel.Controls.Count; index++)
            {
                this.mainPanel.Controls.RemoveAt(index);
            }
        }

        /**
        *  Populates lineup tab 
        */
        private void lineupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LineupPresenter presenter = new LineupPresenter(init.RosterService);
            this.setupPanel(new LineupTabControl(presenter));            
        }

        private void setupPanel(UserControl panel)
        {
            this.removePanel();
            this.mainPanel.Controls.Add(panel);
            this.resizeTab(panel);
        }

        private void standingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.setupPanel(new StandingsTabControl(init.Setting));            
        }

        private void weeklyScheduleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WeeklySchedulePresenter presenter = new WeeklySchedulePresenter(init.ScheduleService,init.SimService,init.RosterService);
            this.setupPanel( new WeeklyScheduleTabControl(presenter) );                                               
        }

        private void teamScheduleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TeamSchedulePresenter presenter = new TeamSchedulePresenter(init.SettingsService,init.ScheduleService,init.SimService,init.RosterService);
            this.setupPanel(new TeamScheduleTabControl(presenter));                            
        }             

        private void resizeTab(UserControl control)
        {
            control.Height = this.Height - 65;  //535;
            control.Width = this.Width - 20; //880;        
        }

        private void injuryReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // needs list of teams and their players
            InjuryPresenter presenter = new InjuryPresenter(init.SettingsService, init.RosterService);
            this.setupPanel(new InjuryReportTabControl(presenter));                 
        }

        private void gameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.setupPanel(new GameSettingsTabControl(init.Setting));            
        }

        private void playStyleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.setupPanel(new PlayStyleTabControl(init.Setting));            
        }        

        private void rosterToolStripMenuItem_Click(object sender, EventArgs e)
        {                                    
            RosterPresenter presenter = new RosterPresenter(init.CapService, init.RosterService, init.ReleasePlayerService);
            this.setupPanel(new RosterManagementTabControl(presenter, playerViewPresenter));
        }

        private void freeAgentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FreeAgentPresenter presenter = new FreeAgentPresenter(init.CapService, init.RosterService, init.SignPlayerService);
            this.setupPanel(new FreeAgentTabControl(presenter, playerViewPresenter));                       
        }

        private void statsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.setupPanel(new StatsTabControl(init.Setting, init.StatsHandler));            
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {           
            init.Exit();
        }

        private void tradeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TradePresenter presenter = new TradePresenter(init.SettingsService, init.TransactionService, init.CapService, init.RosterService);
            this.setupPanel(new TradeTabControl(presenter));
        }

        private void calendarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new CalendarUI().Show();
        }   
        
    } 
}
